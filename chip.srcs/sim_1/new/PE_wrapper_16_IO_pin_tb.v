`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06/10/2019 06:30:23 PM
// Design Name: 
// Module Name: PE_wrapper_16_IO_pin_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module PE_wrapper_16_IO_pin_tb#(
                        parameter dataWidth = 32,
                        parameter weight_addrW = 9,
                        parameter data_addrW = 9,
                        parameter out_addrW = 9,
                        parameter PE = 16
                   );
                   
       reg clk;
       reg reset;
       reg [dataWidth-1:0] N = 0;
       reg [dataWidth-1:0] M = 0;
       reg [dataWidth-1:0] f = 0;
       reg [dataWidth-1:0] r =  0;//r-1 no of zeros between non-zero numbers
       reg rd_wr;
       reg [weight_addrW-1:0] weightAddr = 0;
       reg [data_addrW-1:0] dataAddr = 0;
       reg [out_addrW-1:0] outAddr = 0;
       reg cen_outMem = 0;
       reg [dataWidth-1:0] weightMem_in = 0, dataMem_in = 0;
       reg start = 0;
       reg cen_DM = 0;
       reg cen_WM = 0;
       wire [dataWidth-1:0] outMem_data;
       wire done;
       integer period  = 10, i,j,k, l,data, weight, out_data, w_addr, o_addr;
       
       PE_wrapper_16_PE_IO_pin_edit #(
                    .dataWidth(dataWidth),
                    .weight_addrW(weight_addrW),
                    .data_addrW(data_addrW),
                    .out_addrW(out_addrW),
                    .PE(PE)
       )uut(
                    .clk(clk),
                    .reset(reset),
                    .N(N),
                    .M(M),
                    .f(f),
                    .r(r),
                    .cen_DM(cen_DM),
                    .cen_WM(cen_WM),
                    .rd_wr(rd_wr),
                    .weightAddr(weightAddr),
                    .dataAddr(dataAddr),
                    .outAddr(outAddr),
                    .cen_outMem(cen_outMem),
                    .weightMem_in(weightMem_in),
                    .dataMem_in(dataMem_in),
                    .start(start),
                    .outMem_data(outMem_data),
                    .done(done)
       );
       
       initial begin
            forever #(period/2) clk = ~clk;
       end
       
       
       initial begin
            clk = 0;
            reset = 1'b1;
            cen_outMem = 1'b0;
            
            #(period);
            reset = 1'b0;
            
            #(2*period);
            start = 1'b1;
            rd_wr = 1'b1;
            cen_DM = 1'b1;
            data = $fopen("data_1.mem", "r");
            $fscanf(data, "%h/n", N);
            $fscanf(data, "%h/n", M);
            $fscanf(data, "%h/n", f);
            $fscanf(data, "%h/n", r);
            
            #(period);
            start = 1'b0;
            for(l=0;l<N;l=l+1) begin
                dataAddr = l;
                $fscanf(data, "%h/n", dataMem_in);
                #(period);
            end
            $fclose(data);
            cen_DM = 1'b0;
            
            #(period);
            cen_WM = 1'b1;
            weight = $fopen("weight_1.mem", "r");
            w_addr = $fopen("w_addr_1.mem", "r");
            for(i=0; i<16; i=i+1) begin
                #(period);
                weightAddr = j+i*32;
                $fscanf(w_addr, "%d/n", weightAddr);
                $fscanf(weight, "%h/n", weightMem_in);
            end
            $fclose(weight);
            $fclose(w_addr);
            cen_WM = 1'b0;
            
            #(period);
            rd_wr = 1'b0;
            
            #(period);
            start = 1'b1;
            
            #(period)
            start = 1'b0;
            
            #(3*period);
            for(k=0;k<=(f*M)/PE;k=k+1) begin
                #(period);
            end
            
            #(10*period);
            cen_outMem = 1'b1;
            o_addr = $fopen("o_addr_1.mem", "r");
            out_data= $fopen("output.mem", "w");
            for(i=0; i<5; i=i+1) begin
                $fscanf(o_addr, "%d/n", outAddr);
                $fwrite(out_data, "%h/n", outMem_data);
                #(period);
            end
            $fclose(out_data);

            #period;
            cen_outMem = 1'b0;
       end
endmodule


`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 02/08/2020 01:13:15 PM
// Design Name: 
// Module Name: FPGA_TOP_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module FPGA_TOP_tb#(
                baud_rate = 9600,
                sys_clk_freq = 100000000,
                dataWidth = 8,
                addrW = 15
                )(
                );
    
integer period = 10, data = 0 , addr = 0, data_f, addr_f, EOF = "eof", i;
reg clk, reset, address_in_en, data_in_en, start, load_WM, cen_OM_rd, transmit,test_trigger;
wire rx, tx, done, fifo_empty, error, rx_tb, tx_tb, is_transmitting, is_receiving;
reg [7:0] tx_byte;
reg [7:0] tx_buffer_address;
reg flag = 0;
wire received, received_tb;
wire [7:0] rx_byte;
reg [1:0] memory_test;

FPGA_TOP_3 #(
        .baud_rate(baud_rate),
        .sys_clk_freq(sys_clk_freq),
        .dataWidth(dataWidth),
        .addrW(addrW)
        )dut(
        .clk_in(clk),
        .reset_in(reset),
        .rx(rx),
//        .test_trigger(test_trigger),
        .load_WM(load_WM),
//        .test_mode(1'b1),
        .tx(tx),
        .recv_error(error)
    );
    
initial begin
    clk = 0;
    forever #(period/2) clk = ~clk;
end
    
assign rx = tx_tb;
assign rx_tb = tx;

initial begin
    data = $fopen("data_in_fpga.mem", "r");
    addr = $fopen("addr_fpga.mem", "r");
end

uart#(
    .baud_rate(baud_rate),            // default is 9600
    .sys_clk_freq(sys_clk_freq)       // default is 100000000
    )
uart_instance(
    .clk(clk),                        // The master clock for this module
    .rst(reset),                      // Synchronous reset
    .rx(rx_tb),                          // Incoming serial line
    .tx(tx_tb),                          // Outgoing serial line
    .transmit(transmit),              // Signal to transmit
    .tx_byte(tx_byte),                // Byte to transmit       
    .received(received_tb),              // Indicated that a byte has been received
    .rx_byte(rx_byte),                // Byte received
    .is_receiving(is_receiving),        // Low when receive line is idle
    .is_transmitting(is_transmitting),     // Low when transmit line is idle
    .recv_error()           // Indicates error in receiving packet.
);

initial begin
    data = $fopen("data_in_fpga.mem", "r");
end

initial begin
    #(500*period);
    reset = 1;
    load_WM = 0;
    transmit = 0;
    start = 0;
    address_in_en = 0;
    cen_OM_rd = 0;
    memory_test = 2'b00;
    #(period);
    
//    test_trigger = 1;
    
    reset = 0;
//    #(5*period);
//    test_trigger = 0;
//    while(!$feof(data)) begin
//        if(!is_transmitting) begin
//                transmit = 1;
//                data_f = $fscanf(data, "%h/n", tx_byte);
//            end
//            else begin 
//                transmit = 0;
//            end
//        #(period);
//    end
    #(period);
    #(500*period);
    
    load_WM = 1;
    #(period);
    
    load_WM = 0;
    #(10000*period);
    
//    $finish();
    
    
    
end


endmodule


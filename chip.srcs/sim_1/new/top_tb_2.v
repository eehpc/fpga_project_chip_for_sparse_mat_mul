`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: EEHPC
// Engineer: Nitheesh Manjunath
// 
// Create Date: 06/02/2019 07:36:39 PM
// Design Name: 
// Module Name: PE_wrapper_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module top_tb_2    #(
                        parameter dataWidth = 16,
                        parameter weight_addrW = 9,
                        parameter data_addrW = 9,
                        parameter out_addrW = 9,
                        parameter PE = 4
                   );
                   
       reg clk;
       reg reset;
       reg [dataWidth-1:0] N = 20;
       reg [dataWidth-1:0] M = 16;
       reg [dataWidth-1:0] f = 4;
       reg [dataWidth-1:0] r = 5;//r-1 no of zeros between non-zero numbers
       reg rd_wr;
       reg [weight_addrW-1:0] weightAddr = 0;
       reg [data_addrW-1:0] dataAddr = 0;
       reg [out_addrW-1:0] outAddr = 0;
       reg cen_outMem = 0;
       reg [dataWidth-1:0] weightMem_in = 0, dataMem_in = 0;
       reg start = 0;
       wire [dataWidth-1:0] outMem_data;
       wire done;
       integer period  = 10, i,j,k;
       
       top #(
                    .dataWidth(dataWidth),
                    .weight_addrW(weight_addrW),
                    .data_addrW(data_addrW),
                    .out_addrW(out_addrW),
                    .PE(PE)
       )uut(
                    .clk(clk),
                    .reset(reset),
                    .N(N),
                    .M(M),
                    .f(f),
                    .r(r),
                    .rd_wr(rd_wr),
                    .weightAddr(weightAddr),
                    .dataAddr(dataAddr),
                    .outAddr(outAddr),
                    .cen_outMem(cen_outMem),
                    .weightMem_in(weightMem_in),
                    .dataMem_in(dataMem_in),
                    .start(start),
                    .outMem_data(outMem_data),
                    .done(done)
       );
       
       initial begin
            forever #(period/2) clk = ~clk;
       end
       
       initial begin
            clk = 0;
            reset = 1'b1;
            cen_outMem = 1'b0;
            
            #(period);
            reset = 1'b0;
            start = 1'b1;
            rd_wr = 1'b1;
            
            for(i=0; i<PE; i=i+1) begin
                for(j=0; j<128; j=j+1) begin
                    #(period);
                    start = 1'b0;
                    weightAddr = j+i*128;
                    dataMem_in = j+i*128;
                    dataAddr = j+i*128;;
                    if(weightAddr>=0 && weightAddr<16 ) begin
                        weightMem_in = weightAddr;
//                        dataMem_in = weightAddr;
                    end
                    else if(weightAddr>=128  && weightAddr < 144) begin
                        weightMem_in = weightAddr-112;
//                        dataMem_in = weightAddr-122;                        
                    end
                    else if(weightAddr>=256 && weightAddr < 272) begin
                        weightMem_in = weightAddr-224;
//                        dataMem_in = weightAddr-244;
                    end
                    else if(weightAddr>=384 && weightAddr <400) begin
                        weightMem_in = weightAddr-336;
//                        dataMem_in = weightAddr-366;
                    end
                    else begin
                        
                        weightMem_in = 0;
                    end
                end
            end
            
            #(2*period);
            rd_wr = 1'b0;
            
            #(2*period);
            start = 1'b1;
            
            #(period)
            start = 1'b0;
            
            #(300*period);
            cen_outMem = 1'b1;
            for(k=0; k<512; k=k+1) begin
            #(period);
            outAddr= k;
            end
            
            #period;
            cen_outMem = 1'b0;
            $finish;
       end
endmodule

/*for(i=0; i<PE; i=i+1) begin
                for(j=0; j<128; j=j+1) begin
                    #(period);
                    start = 1'b0;
                    weightAddr = j+i*128;
                    dataMem_in = j+i*128;
                    dataAddr = j+i*128;;
                    if(weightAddr>=0 && weightAddr<8 ) begin
                        weightMem_in = weightAddr;
//                        dataMem_in = weightAddr;
                    end
                    else if(weightAddr>=128  && weightAddr < 136) begin
                        weightMem_in = weightAddr-120;
//                        dataMem_in = weightAddr-122;                        
                    end
                    else if(weightAddr>=256 && weightAddr < 264) begin
                        weightMem_in = weightAddr-240;
//                        dataMem_in = weightAddr-244;
                    end
                    else if(weightAddr>=384 && weightAddr <392) begin
                        weightMem_in = weightAddr-360;
//                        dataMem_in = weightAddr-366;
                    end
                    else begin
                        
                        weightMem_in = 0;
                    end
                end
            end*/
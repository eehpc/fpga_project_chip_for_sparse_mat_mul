`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06/06/2019 08:20:30 PM
// Design Name: 
// Module Name: lvl_1_dec_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module lvl_1_dec_tb(

    );
    
    reg [1:0]i;
    reg en;
    wire[3:0] out;
    
    lvl_1_dec uut(.i(i), .en(en), .out(out));
    
    initial begin
    
    en = 0;
    i = 2'b00;
    #10;
    
    i = 2'b01;
    #10;
    
    en = 1;
    i  = 2'b00;
    #10;
    
    i = 2'b01;
    #10;
    
    i = 2'b10;
    #10;
    
    i = 2'b11;
    #10
    
    $finish;
    
    
    
    
    
    
    end
endmodule

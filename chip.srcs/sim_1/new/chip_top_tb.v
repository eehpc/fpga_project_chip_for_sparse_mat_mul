`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: EEHPC
// Engineer: Nitheesh Manjunath
// 
// Create Date: 06/10/2019 08:00:42 PM
// Design Name: 
// Module Name: chip_top_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments: Added num_repeat where one can run the loop as many times as num_repeat
// 
//////////////////////////////////////////////////////////////////////////////////


module chip_top_tb #(
                    parameter dataWidth = 8,
                    parameter weight_addrW = 13,
                    parameter data_addrW = 10,
                    parameter out_addrW = 10,
                    parameter PE = 16,
                    parameter num_repeat = 1
                   );
reg clk = 0;
reg reset = 0;
reg start = 0;
reg write = 0;
reg [dataWidth-1:0] data_in =0;
reg [weight_addrW+1:0] address_in = 0;
wire [dataWidth-1:0] data_out;
wire done;
reg [2*dataWidth-1:0] N = 0, M = 0, r = 0, f = 0, shift = 0;
reg cen_OM_rd = 0;
reg [weight_addrW-1:0] addr = 0;
reg [dataWidth-1:0] data_out_expected;
reg error = 0;

integer period  = 10, i, j, k, l, data, weight, out_data, w_addr, d_addr, o_addr, rep;

chip_core#(
         .dataWidth(dataWidth),
         .weight_addrW(weight_addrW),
         .data_addrW(data_addrW),
         .out_addrW(out_addrW),
         .PE(PE)
         )uut(
         .clk(clk),
         .reset(reset),
         .start(start),
         .write(write),
         .data_in(data_in),
         .address_in(address_in),
         .cen_OM_rd(cen_OM_rd),
         .data_out(data_out),
         .done(done)
         );

initial begin
    clk = 1;
    forever #(period/2) clk = ~clk;
end
//C://Users//nitheesh//Desktop//chip_test//reverilogandtestbench//
initial begin
    data = $fopen("data.mem", "r");
    d_addr = $fopen("d_addr.mem", "r");
    weight = $fopen("weight.mem", "r");
    w_addr = $fopen("w_addr.mem", "r");
    out_data= $fopen("output.mem", "r"); //this is your MATLAB generated file. Please move into chip_for_sparse_mat_mul/chip.srcs/sim_1/new/output.mem
    o_addr = $fopen("o_addr.mem", "r");
end

initial begin
//    reset = 1'b0;
//    #(5*period);
    for(rep=0; rep<num_repeat; rep=rep+1) begin
        reset = 1'b0;
        cen_OM_rd = 0;
        #(period);
        reset = 1'b1;
        
        #(period);
        start = 1'b1;
        write = 1'b1;
        address_in = 15'b110000000000000;
        $fscanf(data, "%h/n", data_in);
        N[15:8] = data_in;
        #(period);
        
        start = 1'b0;
        $fscanf(data, "%h/n", data_in);
        N[7:0] = data_in;
        #(period);
        
        $fscanf(data, "%h/n", data_in);
        M[15:8] = data_in;
        #(period);
        
        $fscanf(data, "%h/n", data_in);
        M[7:0] = data_in;
        #(period);
        
        $fscanf(data, "%h/n", data_in);
        f[15:8] = data_in;
        #(period);
        
        $fscanf(data, "%h/n", data_in);
        f[7:0] = data_in;
        #(period);
        
        $fscanf(data, "%h/n", data_in);
        r[15:8] = data_in;
        #(period);
        
        $fscanf(data, "%h/n", data_in);
        r[7:0] = data_in;
        #(period);
        
        $fscanf(data, "%h/n", data_in); // m_div_n
        #(period);
        
        $fscanf(data, "%h/n", data_in);
        shift = data_in;
        #(period);
        
        for(l=0;l<N;l=l+1) begin
            $fscanf(d_addr, "%h/n", addr);
            address_in = {2'b10,addr};
            $fscanf(data, "%h/n", data_in);
            #(period);
        end
    //            $fclose(data);
    //            $fclose(d_addr);
        #(period);
        
        for(i=0; i<M*f; i=i+1) begin
            $fscanf(w_addr, "%d/n", addr);
            address_in = {2'b01,addr};
            $fscanf(weight, "%h/n", data_in);
            #(period);
        end
    //            $fclose(weight);
    //            $fclose(w_addr);
        
        #(period);
        write = 1'b0;
        
        #(period);
        start = 1'b1;
        
        #(period)
        start = 1'b0;
        
        #(3*period);
        for(k=0;k<=(f*M)/PE;k=k+1) begin
            #(period);
        end   
    
        for(i=0; i<M; i=i+1) begin
            $fscanf(o_addr, "%d/n", addr);
            address_in = {2'b00,addr};
            cen_OM_rd = 1;
            #(4*period);
            $fscanf(out_data, "%h/n", data_out_expected);
                if(data_out == data_out_expected ) begin
                    error = 1'b0;
                end
                else begin
                    error = 1'b1;
                end
        end
        #(period);
    //            $fclose(out_data);
    //            $fclose(o_addr);
        cen_OM_rd = 0;
        #(10*period);
        
        address_in = 15'b000000000000000;
        #(10*period);
        
        address_in = 15'b000000000000001;
        #(10*period);
        
        address_in = 15'b000000000000010;
        #(10*period);
        
        address_in = 15'b000000000000011;
        #(10*period);
    
        address_in = 15'b000000000000100;
        #(10*period);
        
        address_in = 15'b000000000000101;
        #(10*period);
        
        address_in = 15'b000000000000110;
        #(10*period);
        
        address_in = 15'b000000000000111;
        #(10*period);
    end
//////////////////////////////end of one tv////////////////////////////// 
//    $fclose(out_data);
    $finish;
end

endmodule

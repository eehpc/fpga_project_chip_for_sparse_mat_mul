`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06/06/2019 07:38:25 PM
// Design Name: 
// Module Name: dec2_1_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module dec2_1_tb(

    );
    reg i, en;
    wire[1:0] out;
    dec2_1 uut(i, en, out);
    
    initial begin
        en = 0;
        i = 0;
        #10;
        
        en = 1;
        i = 0;
        #20;
        
        i = 1;
        #20;
        $finish;
    end
endmodule

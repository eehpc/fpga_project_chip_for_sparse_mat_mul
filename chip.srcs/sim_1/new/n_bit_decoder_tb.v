`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/22/2019 10:43:14 PM
// Design Name: 
// Module Name: n_bit_decoder_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module n_bit_decoder_tb(

    );
    
    reg en;
    reg [8-1:0] i;
    wire [256-1:0] out;
    integer period = 10;
    
    n_bit_decoder uut(.en(en), .i(i), .out(out));
    
    initial begin
    en = 0;
    i = 8'd5;
    #(period);
    
    en = 1;
    i = 8'd5;
    #(period);
    
    i = 8'd255;
    #(period);
    
    i = 8'd0;
    #(period)
    
    i = 8'd150;
    #(period)
    en = 0;
    end
endmodule

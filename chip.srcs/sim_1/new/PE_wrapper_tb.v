
//////////////////////////////////////////////////////////////////////////////////
// Company
// Engineer: 
// 
// Create Date: 06/02/2019 07:36:39 PM
// Design Name: 
// Module Name: PE_wrapper_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module PE_wrapper_tb    #(
                        parameter dataWidth = 16,
                        parameter weight_addrW = 8,
                        parameter data_addrW = 7,
                        parameter out_addrW = 7
                   );
                   
       reg clk;
       reg reset;
       reg [dataWidth-1:0] N = 9;
//       reg [dataWidth-1:0] M = 9;
       reg [dataWidth-1:0] f = 3;
       reg [dataWidth-1:0] r = 3;
       reg rd_wr;
       reg [weight_addrW-1:0] weightAddr = 0;
       reg [data_addrW-1:0] dataAddr = 0;
       reg [out_addrW-1:0] outAddr = 0;
       reg cen_outMem = 0;
       reg [dataWidth-1:0] weightMem_in = 0, dataMem_in = 0;
       reg start = 0;
       wire [dataWidth-1:0] outMem_data;
       wire done;
       integer period  = 5, i;
       
       PE_wrapper 
	  #(
                    .dataWidth(dataWidth),
                    .weight_addrW(weight_addrW),
                    .data_addrW(data_addrW),
                    .out_addrW(out_addrW)
       ) 
	   uut(
                    .clk(clk),
                    .reset(reset),
                    .N(N),
//                    .M(M),
                    .f(f),
                    .r(r),
                    .rd_wr(rd_wr),
                    .weightAddr(weightAddr),
                    .dataAddr(dataAddr),
                    .outAddr(outAddr),
                    .cen_outMem(cen_outMem),
                    .weightMem_in(weightMem_in),
                    .dataMem_in(dataMem_in),
                    .start(start),
                    .outMem_data(outMem_data),
                    .done(done)
       );
       
       initial begin
			clk = 0;
            forever #(period/2) clk = ~clk;
       end
       
       initial begin
            reset = 1'b1;
            
            #(period)
            reset = 1'b0;
            start = 1'b1;
            rd_wr = 1'b1;
            
            #(period)
            start = 1'b0;
            for(i=0; i<81; i=i+1) begin
                #(period);
                weightAddr = i;
                dataAddr = i;
                weightMem_in = i;
                dataMem_in = i;
            end
            
            #(period)
            rd_wr = 1'b0;
            
            #(period)
            start = 1'b1;
            
            #(period)
            start = 1'b0;
			
            
			#(100*period)
			for (i=0; i < 12; i = i + 1) begin
				#(period)
				outAddr = i;
			end
			
			
            #(200*period);
            $finish;
       end
endmodule

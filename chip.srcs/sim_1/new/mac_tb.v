`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06/02/2019 12:31:51 AM
// Design Name: 
// Module Name: mac_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module mac_tb#(parameter dataWidth = 16)(

    );
    
     reg clk;
    reg reset;
    reg [dataWidth-1:0] dataA;
    reg [dataWidth-1:0] dataB;
    reg begin_ops;
    reg end_ops;
    reg [4-1:0]shift;
    wire signed [dataWidth-1:0] dataOut;
    
    MAC #(.dataWidth(dataWidth))uut(
           .clk(clk),
           .reset(reset),
           .shift(shift),
           .dataA(dataA),
           .dataB(dataB),
           .begin_ops(begin_ops),
           .end_ops(end_ops),
           .dataOut(dataOut));
           
    integer period = 10;
    
    initial begin
        clk = 0;
        forever #(period/2) clk = ~clk;
    end
           
   initial begin
    reset = 1;
    shift = 4'd6;
    end_ops = 0;
    #period
    reset = 0;
    begin_ops = 1;
    dataA = 16'd999;
    dataB = 16'd110;
    #period 
    begin_ops = 0;
    dataA = 16'd200;
    dataB = 16'd999;
    #period
    dataA = 16'd955;
    dataB = 16'd909;
    #period
    dataA = 16'd988;
    dataB = 16'd988;
    end_ops = 1;
    #period
    end_ops = 0;
   end
       
    
endmodule

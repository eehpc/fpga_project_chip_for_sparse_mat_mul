`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: UMBC-EEHPC
// Engineer: Nitheesh Manjunath
// 
// Create Date: 10/22/2019 04:06:06 PM
// Design Name: 
// Module Name: PE_wrapper_256_PE
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module PE_wrapper_256_PE#(
            parameter dataWidth = 8,
            parameter PE = 256,
            parameter weight_addrW = 9+$clog2(PE),
            parameter data_addrW = 10,
            parameter out_addrW = 6+$clog2(PE),
            parameter decoderAddressBits = $clog2(PE)
               )(
            input wire clk,
            input wire reset,
            input wire [2*dataWidth-1:0] N,
            input wire [2*dataWidth-1:0] M,
            input wire [2*dataWidth-1:0] f,
            input wire [2*dataWidth-1:0] r,
            input wire [dataWidth-1:0] M_div_N,
            input wire [5-1:0] shift,
            input wire rd_wr,
            input wire cen_DM,
            input wire cen_WM,
            input wire read_test,
            input wire [weight_addrW-1:0] weightAddr,
            input wire [data_addrW-1:0] dataAddr,
            input wire [out_addrW-1:0] outAddr,
            input wire cen_outMem,
            input wire [dataWidth-1:0] weightMem_in, dataMem_in,
            input wire start,
            (*keep = "true" *)output reg [dataWidth-1:0] outMem_data,
            output wire done
            );
    
    localparam weight_addrW_local = weight_addrW - decoderAddressBits, //address width for memories of PE log2(PE) = 8
               out_addrW_local  = out_addrW - decoderAddressBits, //log2(PE)
               counterWidth = 32 ;
                
    
    wire [dataWidth-1:0] outMem_data_wire;
    (*keep = "true" *)reg [counterWidth-1:0] hAddr_cnt, vAddr_cnt;
    
    (*keep = "true" *)reg [weight_addrW_local-1:0] weightAddr_internal, weightAddr_internal1, weightAddr_internal2;
    wire [weight_addrW_local-1:0] weightAddress;
    
    wire [PE*data_addrW-1:0]  dataAddr_internal_temp1, dataAddr_internal_temp3, dataAddress;
    (*keep = "true"*)reg [PE*data_addrW-1:0] dataAddr_internal_temp2;
    wire [data_addrW-1:0] dataAddr_internal_temp; 
    (*keep = "true"*)reg [data_addrW-1:0] dataAddr_internal_temp_register;
    wire [PE*data_addrW-1:0] diff;
    (*keep = "true"*)reg [PE*data_addrW-1:0] diff_register;
    wire [data_addrW-1:0] diff2;
    (*keep = "true"*)reg [data_addrW-1:0] diff2_register;
    wire [PE*data_addrW-1:0] temp_1;
         
    (*keep = "true" *)reg [out_addrW_local-1:0] outAddr_internal;
    
    wire [PE*dataWidth-1:0] weightMem_out;
    wire [PE*dataWidth-1:0] dataMem_out;
    wire [PE*dataWidth-1:0] MAC_out;
    
//	(*keep = "true" *)reg end_ops_internal2_temp;
//    (*keep = "true" *)reg [PE*dataWidth-1:0] weightMem_out_internal, dataMem_out_internal;
    (*keep = "true" *)reg cen_weightMemWR, cen_dataMemWR, cen_MemR, begin_ops_internal, end_ops_internal,  cen_MemR1, cen_MemR2;
    (*keep = "true" *)reg begin_ops_internal1, begin_ops_internal2, begin_ops_internal3, end_ops_internal1, end_ops_internal2, end_ops_internal3, end_ops_internal4, end_ops_internal5;
    (*keep = "true" *)reg done_internal, done_internal1, done_internal2, done_internal3;
    
    (*keep = "true" *)reg [1:0] currentState;
    localparam idle = 2'b00, s1 = 2'b01, s2 = 2'b10, s3 = 2'b11;  //changed from 3'b to 2'b
    
   
    wire [PE-1:0] cen_WM_wr, cen_OM_rd;
     (*keep = "true" *)reg [PE-1:0] cen_OM_rd_reg;
    
     (*keep = "true" *)reg [counterWidth-1:0] h_max;
    wire [PE*dataWidth-1:0] temp_out, test_data; //goes to output & test_data
    
//    (*keep = "true" *)reg error_internal;
   

//    lvl_3_dec i_decoder(
//                        .i(weightAddr[weight_addrW-1:weight_addrW_local]),
//                        .en(cen_weightMemWR), //cen_WM //cen_weightMemWR
//                        .out(cen_WM_wr)
//                        );
    
//    lvl_3_dec o_decoder( 
//                        .i(outAddr[out_addrW-1:out_addrW_local]),
//                        .en(cen_outMem),
//                        .out(cen_OM_rd)
//                        );
    
    n_bit_decoder #(
                 .PE(PE),
                 .log2_PE(decoderAddressBits)
                 )i_decoder(
                .i(weightAddr[weight_addrW-1:weight_addrW_local]),
                .en(cen_weightMemWR), //cen_WM //cen_weightMemWR
                .out(cen_WM_wr)
                );
    
    n_bit_decoder#(
                .PE(PE),
                .log2_PE(decoderAddressBits) 
                )o_decoder( 
                .i(outAddr[out_addrW-1:out_addrW_local]),
                .en(cen_outMem),
                .out(cen_OM_rd)
                );


    genvar k;
    generate
        for(k=0; k<PE; k=k+1) begin : WM_GEN
            rdp #(
                            .dataWidth(dataWidth),
                            .addrW(weight_addrW_local)
            )WM(
                            .clk(clk),
                            .data_inA(weightMem_in),
                            .addrA(weightAddr[weight_addrW_local-1:0]),
                            .addrB(weightAddress), // weightAddr_internal2
                            .wrA(cen_WM_wr[k]), //cen_weightMemWR
                            .rdB(cen_MemR2 | read_test),
                            .data_outB(weightMem_out[dataWidth*(k+1)-1:dataWidth*k])
            );
        end
    endgenerate
    
    genvar l;
    generate
        for(l=0; l<PE; l=l+1) begin : DM_GEN
            rdp#(
                            .dataWidth(dataWidth),
                            .addrW(data_addrW)
            )DM(
                            .clk(clk),
                            .data_inA(dataMem_in),
                            .addrA(dataAddr),
                            .addrB(dataAddress[data_addrW*(1+l)-1:data_addrW*l]),  //dataAddr_internal_temp3
                            .wrA(cen_dataMemWR), //cen_DM cen_dataMemWR
                            .rdB(cen_MemR2 | read_test),
                            .data_outB(dataMem_out[dataWidth*(1+l)-1:dataWidth*l])
            );
        end
    endgenerate
    
    genvar m;
    generate
        for (m=0; m<PE; m=m+1) begin : OM_GEN
            rdp#(
                            .dataWidth(dataWidth),
                            .addrW(out_addrW_local)
            )OM(
                            .clk(clk),
                            .data_inA(MAC_out[dataWidth*(m+1)-1:dataWidth*m]),
                            .addrA(outAddr_internal),
                            .addrB(outAddr[out_addrW_local-1:0]),
                            .wrA(end_ops_internal5),
                            .rdB(cen_OM_rd[m]),
                            .data_outB(temp_out[dataWidth*(m+1)-1:dataWidth*m])
            ); 
        end
    endgenerate
	

    genvar j;
    generate
        for (j=0; j<PE; j=j+1) begin : MU_GEN
            MAC 
            #(
                    .dataWidth(dataWidth)
            )
            MU(
                    .clk(clk),
                    .reset(reset),
                    .shift(shift),
                    .dataA(weightMem_out[dataWidth*(j+1)-1:dataWidth*j]),
                    .dataB(dataMem_out[dataWidth*(j+1)-1:dataWidth*j]),
                    .begin_ops(begin_ops_internal3),
                    .end_ops(end_ops_internal3),
                    .dataOut(MAC_out[dataWidth*(j+1)-1:dataWidth*j])
            );
            end
    endgenerate
    
    assign done = done_internal3; //finished matrix multiplication
    
    assign weightAddress =  read_test ? weightAddr : weightAddr_internal2; //for testing
    assign dataAddress = read_test ? {150'b0, dataAddr} : dataAddr_internal_temp3; //for testing
    
    assign test_data = read_test ? (dataMem_out[7:0] ^ weightMem_out[7:0]) : 0;

    
    always @(posedge clk) begin
        if(reset) begin
            begin_ops_internal1 <= 1'b0;
            begin_ops_internal2 <= 1'b0;
            begin_ops_internal3 <= 0;
            end_ops_internal1 <= 1'b0;
            end_ops_internal2 <= 1'b0;
            end_ops_internal3 <= 1'b0;
            end_ops_internal4 <= 1'b0;
            end_ops_internal5 <= 0;
            done_internal1 <= 1'b0;
            done_internal2 <= 1'b0;
            done_internal3 <= 1'b0;
            cen_OM_rd_reg <= 0;
            weightAddr_internal1 <= 0;
            dataAddr_internal_temp2 <= 0;
            weightAddr_internal2 <= 0;
            dataAddr_internal_temp_register <= 0;
            diff_register <= 0;
            diff2_register <= 0;
            cen_MemR1 <= 0;
            cen_MemR2 <= 0;
        end
        else begin
            begin_ops_internal1 <= begin_ops_internal;
            begin_ops_internal2 <= begin_ops_internal1;
            begin_ops_internal3 <= begin_ops_internal2;
            end_ops_internal1 <= end_ops_internal;
            end_ops_internal2 <= end_ops_internal1;
            end_ops_internal3 <= end_ops_internal2;
            end_ops_internal4 <= end_ops_internal3;
            end_ops_internal5 <= end_ops_internal4;
            done_internal1 <= done_internal;
            done_internal2 <= done_internal1;
            done_internal3 <= done_internal2;   
            cen_OM_rd_reg <= cen_OM_rd;
            weightAddr_internal1 <= weightAddr_internal;
            weightAddr_internal2 <= weightAddr_internal1;
            dataAddr_internal_temp2 <= dataAddr_internal_temp1;
            dataAddr_internal_temp_register <= dataAddr_internal_temp;
            diff_register <= diff;
            diff2_register <= diff2;
            cen_MemR1 <= cen_MemR;
            cen_MemR2 <= cen_MemR1;         
        end
    end
    
    
    //Generating data_address i.e vector address
    assign diff2 = (f*r == N) ? f*r : N;
    assign dataAddr_internal_temp = (hAddr_cnt * r + vAddr_cnt) < N  ? 
                                        (hAddr_cnt * r + vAddr_cnt) : 
                                        (hAddr_cnt * r + vAddr_cnt)- N;                                  
    genvar h;
    generate 
        for(h=0; h<PE; h=h+1) begin : DIFF_GEN
            assign temp_1[data_addrW*(1+h)-1:data_addrW*h] = ((h*M_div_N)/(PE)); //assign temp_1[dataWidth*(1+h)-1:dataWidth*h] = ((h*M)/(PE*N));
            assign diff[data_addrW*(1+h)-1:data_addrW*h] = N*temp_1[data_addrW*(1+h)-1:data_addrW*h]; 
        end
    endgenerate
    
    genvar i;
    generate
        for(i=0; i<PE; i=i+1) begin : DA_GEN
                  assign dataAddr_internal_temp1[data_addrW*(1+i)-1:data_addrW*i] = dataAddr_internal_temp_register + ((i*M)/PE) - diff_register[data_addrW*(1+i)-1:data_addrW*i]  < N ? 
                                                                                    dataAddr_internal_temp_register + ((i*M)/PE) - diff_register[data_addrW*(1+i)-1:data_addrW*i] : 
                                                                                    dataAddr_internal_temp_register + ((i*M)/PE) - diff_register[data_addrW*(1+i)-1:data_addrW*i] - diff2_register;
                                                                                    
                  assign dataAddr_internal_temp3[data_addrW*(1+i)-1:data_addrW*i] = dataAddr_internal_temp2[data_addrW*(1+i)-1:data_addrW*i] < N ? 
                                                                                    dataAddr_internal_temp2[data_addrW*(1+i)-1:data_addrW*i] :
                                                                                    dataAddr_internal_temp2[data_addrW*(1+i)-1:data_addrW*i] - N;
        end
    endgenerate
    
    
    
    //output assignment
    assign outMem_data_wire = read_test ? test_data : temp_out[ cen_OM_rd_reg*dataWidth +: dataWidth ] ;
    
    always@(*) begin
        if(reset) begin
            outMem_data  = {dataWidth{1'bz}};
        end
        else begin
            outMem_data = outMem_data_wire;
        end
    end
    //DUMB way to do stuff
//    always@(cen_OM_rd_reg, temp_out, read_test, test_data) begin
//        case({read_test,cen_OM_rd_reg})
//            17'b00000000000000001 : begin
//                outMem_data = temp_out[dataWidth-1:0];
//            end 
            
//            17'b00000000000000010 : begin
//                outMem_data = temp_out[2*dataWidth-1:dataWidth];
//            end
            
//            17'b00000000000000100 : begin
//                outMem_data = temp_out[3*dataWidth-1:2*dataWidth];
//            end
            
//            17'b00000000000001000 : begin
//                outMem_data = temp_out[4*dataWidth-1:3*dataWidth];
//            end
 
//             17'b00000000000010000 : begin
//                outMem_data = temp_out[5*dataWidth-1:4*dataWidth];
//            end

//             17'b00000000000100000 : begin
//                outMem_data = temp_out[6*dataWidth-1:5*dataWidth];
//            end
            
//             17'b00000000001000000 : begin
//               outMem_data = temp_out[7*dataWidth-1:6*dataWidth];
//            end
           
//             17'b00000000010000000 : begin
//              outMem_data = temp_out[8*dataWidth-1:7*dataWidth];
//            end
            
//             17'b00000000100000000 : begin
//               outMem_data = temp_out[9*dataWidth-1:8*dataWidth];
//            end
           
//             17'b00000001000000000 : begin
//              outMem_data = temp_out[10*dataWidth-1:9*dataWidth];
//            end

//             17'b00000010000000000 : begin
//                outMem_data = temp_out[11*dataWidth-1:10*dataWidth];
//            end
            
//             17'b00000100000000000 : begin
//               outMem_data = temp_out[12*dataWidth-1:11*dataWidth];
//             end
           
//             17'b00001000000000000 : begin
//                outMem_data = temp_out[13*dataWidth-1:12*dataWidth];
//             end
          
//            17'b00010000000000000 : begin
//                outMem_data = temp_out[14*dataWidth-1:13*dataWidth];
//            end
         
//             17'b00100000000000000 : begin
//                 outMem_data = temp_out[15*dataWidth-1:14*dataWidth];
//             end
        
//             17'b01000000000000000 : begin
//                outMem_data = temp_out[16*dataWidth-1:15*dataWidth];
//             end
            
//            17'b10000000000000000 : begin
//                outMem_data =  test_data;
//            end

//            default : begin
//                outMem_data = {dataWidth{1'bz}};
//            end
//        endcase
//    end
    
    always @(currentState, hAddr_cnt, vAddr_cnt, reset, f, r) begin
        if(reset) begin
            weightAddr_internal = 0;

            begin_ops_internal = 1'b0;
            end_ops_internal = 1'b0;
        end
        else begin
            weightAddr_internal = hAddr_cnt + vAddr_cnt * f;
            if ((currentState > s2) && (f-1 == 0)) begin
                begin_ops_internal = 1'b1;
                end_ops_internal = 1'b1;
            end
            else begin
                if((currentState > s2) && (hAddr_cnt == 0)) begin
                    begin_ops_internal = 1'b1;
                    end_ops_internal = 1'b0;
                end
                else if((currentState > s2) && (hAddr_cnt == f-1)) begin
                    end_ops_internal = 1'b1;
                    begin_ops_internal = 1'b0;
                end
                else begin
                    begin_ops_internal = 1'b0;
                    end_ops_internal = 1'b0;
                end
            end
        end
    end
    
    always@(posedge clk) begin
        if(reset) begin
            outAddr_internal <= {out_addrW_local{1'b1}};
        end
        else begin
            if(end_ops_internal3) begin
                outAddr_internal <= outAddr_internal + 6'b000001;
            end
            else begin
                outAddr_internal <= outAddr_internal;
            end
        end
    end
    
    always@(M, reset) begin
        if(reset) begin
            h_max = 0;
        end
        else begin
            h_max = M/PE;
        end
    end
    //State Machine
    always @(posedge clk) begin        
        if(reset) begin
            currentState <= idle;
            cen_weightMemWR <= 1'b0;
            cen_dataMemWR <= 1'b0;
            cen_MemR <= 1'b0;
            hAddr_cnt <= 0;
            vAddr_cnt <= 0;
            done_internal <= 1'b0;
//            error_internal <= 1'b0;
        end
        else begin
            case (currentState)
                idle: begin
//                    if(!start) begin
//                        currentState <= idle;
//                        cen_MemR <= 1'b0;
//                        hAddr_cnt <= 0;
//                        vAddr_cnt <= 0;
//                        cen_weightMemWR <= 1'b0;
//                        cen_dataMemWR <= 1'b0;
//                        done_internal <= 1'b0;
//                    end
//                    else begin
                        if(rd_wr) begin
                            if(cen_DM) begin
                                currentState <= s1;
                                cen_dataMemWR <= 1'b1;
                                cen_weightMemWR <= 1'b0;
                                hAddr_cnt <= 0;
                                vAddr_cnt <= 0;
                            end
                            else if(cen_WM) begin
                                currentState <= s1;
                                cen_weightMemWR <= 1'b1;
                                cen_dataMemWR <= 1'b0;
                                hAddr_cnt <= 0;
                                vAddr_cnt <= 0;
                            end
                            else begin
                                currentState <= s1;
                                hAddr_cnt <= 0;
                                vAddr_cnt <= 0;
                            end
                        end
                        else begin
//                            currentState <= s2;
//                            hAddr_cnt <= 0;
//                            vAddr_cnt <= 0;
                            currentState <= idle;
                            cen_MemR <= 1'b0;
                            hAddr_cnt <= 0;
                            vAddr_cnt <= 0;
                            cen_weightMemWR <= 1'b0;
                            cen_dataMemWR <= 1'b0;
                            done_internal <= 1'b0;
//                            error_internal <= 1'b0;
                        end
//                    end
                end
                
                s1: begin
                    if(!rd_wr) begin
                        currentState <= s2;
                        cen_weightMemWR <= 1'b0;
                        cen_dataMemWR <= 1'b0;
                    end
                    else begin
                        if(cen_DM) begin
                            currentState <= s1;
                            cen_dataMemWR <= 1'b1;
                            cen_weightMemWR <= 1'b0;
                        end
                        else if(cen_WM) begin
                            currentState <= s1;
                            cen_weightMemWR <= 1'b1;
                            cen_dataMemWR <= 1'b0;
                        end
                        else begin
                            currentState <= s1;
                        end
                    end
                end
                
                s2: begin
                    if(start) begin
                        currentState <= s3;
                        cen_MemR <= 1'b1;
                    end
                    else begin
                        if(rd_wr) begin
                            currentState <= s1;
                        end
                        else begin
                            currentState <= s2;
                        end
                    end
                end
                
                s3: begin
                    if(hAddr_cnt < f-1 && vAddr_cnt < h_max-1) begin
                        hAddr_cnt <= hAddr_cnt + 11'b00000000001;
                        currentState <= s3;
                    end
                    else if( hAddr_cnt >= f-1 && vAddr_cnt < h_max-1) begin
                        hAddr_cnt <= 0;
                        vAddr_cnt <= vAddr_cnt + 11'b00000000001;
                        currentState <= s3;
                    end
                    else if(hAddr_cnt < f-1 && vAddr_cnt >= h_max-1) begin
                        hAddr_cnt <= hAddr_cnt + 11'b00000000001;
                        currentState <= s3;
                    end
                    else if(hAddr_cnt >= f-1 && vAddr_cnt >= h_max-1) begin
                        hAddr_cnt <= 0;
                        vAddr_cnt <= 0;
                        currentState <= idle;
                        done_internal <= 1'b1;
                    end
                    else begin
                         hAddr_cnt <= {counterWidth{1'bz}};
                         vAddr_cnt <= {counterWidth{1'bz}};
                         currentState <= idle;
//                         error_internal <= 1'b1;
                    end
                end
                
//                default : begin
//                    currentState <= idle;
//                end
            endcase
        end
    end 
endmodule

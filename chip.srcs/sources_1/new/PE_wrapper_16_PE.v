`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: EEHPC 
// Engineer: Nitheesh Manjunath
// 
// Create Date: 06/04/2019 11:44:11 PM
// Design Name: 
// Module Name: PE_wrapper_4_PE
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module PE_wrapper_16_PE  #(
            parameter dataWidth = 16,
            parameter weight_addrW = 9,
            parameter data_addrW = 9,
            parameter out_addrW = 9,
            parameter PE = 16
               )(
            input wire clk,
            input wire reset,
            input wire [dataWidth-1:0] N,
            input wire [dataWidth-1:0] M,
            input wire [dataWidth-1:0] f,
            input wire [dataWidth-1:0] r,
            input wire rd_wr,
            input wire [weight_addrW-1:0] weightAddr,
            input wire [data_addrW-1:0] dataAddr,
            input wire [out_addrW-1:0] outAddr,
            input wire cen_outMem,
            input wire [dataWidth-1:0] weightMem_in, dataMem_in,
            input wire start,
            output reg [dataWidth-1:0] outMem_data,
            output wire done
            );
    
    localparam PE_weight_addrW = weight_addrW - 4, //address width for memories of PE
               PE_out_addrW  = out_addrW - 4;
    
    (*keep = "true" *)reg [weight_addrW-1:0] hAddr_cnt, vAddr_cnt;
                      reg [PE_weight_addrW-1:0]weightAddr_internal;
//    (*keep = "true" *)reg [PE*data_addrW-1:0] dataAddr_internal;
    wire [PE*data_addrW-1:0]  dataAddr_internal_temp1;
    wire [data_addrW-1:0] dataAddr_internal_temp; 
    
           
    (*keep = "true" *)reg [PE_out_addrW-1:0] outAddr_internal;
    
    wire [PE*dataWidth-1:0] weightMem_out;
    wire [PE*dataWidth-1:0] dataMem_out;
    
    wire [(PE*dataWidth):0] MAC_out;
	(*keep = "true" *)reg end_ops_internal2_temp;
    (*keep = "true" *)reg [PE*dataWidth-1:0] weightMem_out_internal, dataMem_out_internal;
    (*keep = "true" *)reg cen_weightMemWR, cen_dataMemWR, cen_MemR, begin_ops_internal, end_ops_internal;
    (*keep = "true" *)reg begin_ops_internal1, begin_ops_internal2, begin_ops_internal3, end_ops_internal1, end_ops_internal2, end_ops_internal3, end_ops_internal4;
    (*keep = "true" *)reg done_internal, done_internal1, done_internal2, done_internal3;
    
    (*keep = "true" *)reg [2:0] currentState;
    localparam idle = 3'b000, s1 = 3'b001, s2 = 3'b010, s3 = 3'b011, s4 = 3'b100, s5 = 3'b101, s6 = 3'b110;
    
    
    wire [PE-1:0] cen_WM_wr, cen_OM_rd;
     (*keep = "true" *)reg [PE-1:0] cen_OM_rd_reg;
    
     (*keep = "true" *)reg [dataWidth-1:0] h_max;
    wire [PE*dataWidth-1:0] temp_out;
      
    lvl_3_dec i_decoder(
                        .i(weightAddr[weight_addrW-1:weight_addrW-4]),
                        .en(cen_weightMemWR),
                        .out(cen_WM_wr)
                        );
    
    lvl_3_dec o_decoder(
                        .i(outAddr[out_addrW-1:out_addrW-4]),
                        .en(cen_outMem),
                        .out(cen_OM_rd)
                        );
    
    
    genvar k;
    generate
        for(k=0; k<PE; k=k+1) begin
            rdp_256X32 #(
                            .dataWidth(dataWidth),
                            .addrW(weight_addrW-4)
            )WM(
                            .clkA(clk),
                            .clkB(clk),
                            .data_inA(weightMem_in),
                            .addrA(weightAddr[PE_out_addrW-1:0]),
                            .addrB(weightAddr_internal),
                            .wrA(cen_WM_wr[k]), //cen_weightMemWR
                            .rdB(cen_MemR),
                            .data_outB(weightMem_out[dataWidth*(k+1)-1:dataWidth*k])
            );
        end
    endgenerate
    
    genvar l;
    generate
        for(l=0; l<PE; l=l+1) begin
            rdp_256X32 #(
                            .dataWidth(dataWidth),
                            .addrW(data_addrW)
            )DM(
                            .clkA(clk),
                            .clkB(clk),
                            .data_inA(dataMem_in),
                            .addrA(dataAddr),
                            .addrB(dataAddr_internal_temp1[data_addrW*(1+l)-1:data_addrW*l]),
                            .wrA(cen_dataMemWR),
                            .rdB(cen_MemR),
                            .data_outB(dataMem_out[dataWidth*(1+l)-1:dataWidth*l])
            );
        end
    endgenerate
    
    genvar m;
    generate
        for (m=0; m<PE; m=m+1) begin 
            rdp_256X32 #(
                            .dataWidth(dataWidth),
                            .addrW(out_addrW-4)
            )OM(
                            .clkA(clk),
                            .clkB(clk),
                            .data_inA(MAC_out[dataWidth*(m+1)-1:dataWidth*m]),
                            .addrA(outAddr_internal),
                            .addrB(outAddr[4:0]),
                            .wrA(end_ops_internal4),
                            .rdB(cen_OM_rd[m]),
                            .data_outB(temp_out[dataWidth*(m+1)-1:dataWidth*m])
            ); 
        end
    endgenerate
	

    genvar j;
    generate
        for (j=0; j<PE; j=j+1) begin
            MAC 
            #(
                    .dataWidth(dataWidth)
            )
            MU(
                    .clk(clk),
                    .reset(reset),
                    .dataA(weightMem_out_internal[dataWidth*(j+1)-1:dataWidth*j]),
                    .dataB(dataMem_out_internal[dataWidth*(j+1)-1:dataWidth*j]),
                    .begin_ops(begin_ops_internal2),
                    .end_ops(end_ops_internal2),
                    .dataOut(MAC_out[dataWidth*(j+1)-1:dataWidth*j])
            );
            end
    endgenerate
    
    assign done = done_internal3; //finished matrix multiplication
	
	
	always@(end_ops_internal1) begin
		if(end_ops_internal1) begin
			end_ops_internal2_temp = 1'b1; 
		end
		else begin
			end_ops_internal2_temp = 1'b0;
		end
	end
	
    
    always @(posedge clk) begin
        if(reset) begin
            weightMem_out_internal <= {(PE*dataWidth){1'b0}};
            dataMem_out_internal <= {(PE*dataWidth){1'b0}};
            begin_ops_internal1 <= 1'b0;
            begin_ops_internal2 <= 1'b0;
            end_ops_internal1 <= 1'b0;
            end_ops_internal2 <= 1'b0;
            end_ops_internal3 <= 1'b0;
            end_ops_internal4 <= 1'b0;
            done_internal1 <= 1'b0;
            done_internal2 <= 1'b0;
            done_internal3 <= 1'b0;
            cen_OM_rd_reg <= 4'b0;
        end
        else begin
            weightMem_out_internal <= weightMem_out;
            dataMem_out_internal <= dataMem_out;
            begin_ops_internal1 <= begin_ops_internal;
            begin_ops_internal2 <= begin_ops_internal1;
            end_ops_internal1 <= end_ops_internal;
            end_ops_internal2 <= end_ops_internal2_temp;
            end_ops_internal3 <= end_ops_internal2;
            end_ops_internal4 <= end_ops_internal3;
            done_internal1 <= done_internal;
            done_internal2 <= done_internal1;
            done_internal3 <= done_internal2;   
            cen_OM_rd_reg <= cen_OM_rd;  
        end
    end
    
    
    
    //Generating data_address i.e vector address
    assign dataAddr_internal_temp = (hAddr_cnt * r + vAddr_cnt) < N  ? 
                                        (hAddr_cnt * r + vAddr_cnt) : 
                                        (hAddr_cnt * r + vAddr_cnt)- N;
    genvar i;
    generate
        for(i=0; i<PE; i=i+1) begin
              assign dataAddr_internal_temp1[data_addrW*(1+i)-1:data_addrW*i] = dataAddr_internal_temp + ((i*M)/PE) < N ? 
                                                                                    dataAddr_internal_temp + ((i*M)/PE) : 
                                                                                    dataAddr_internal_temp + ((i*M)/PE) - f*r;
        end
    endgenerate
    
    //output assignment
    always@(cen_OM_rd_reg,temp_out) begin
        case(cen_OM_rd_reg)
            16'b0000000000000001 : begin
                outMem_data = temp_out[dataWidth-1:0];
            end 
            
            16'b0000000000000010 : begin
                outMem_data = temp_out[2*dataWidth-1:dataWidth];
            end
            
            16'b0000000000000100 : begin
                outMem_data = temp_out[3*dataWidth-1:2*dataWidth];
            end
            
            16'b0000000000001000 : begin
                outMem_data = temp_out[4*dataWidth-1:3*dataWidth];
            end
 
             16'b0000000000010000 : begin
                outMem_data = temp_out[5*dataWidth-1:4*dataWidth];
            end

             16'b0000000000100000 : begin
                outMem_data = temp_out[6*dataWidth-1:5*dataWidth];
            end
            
             16'b0000000001000000 : begin
               outMem_data = temp_out[7*dataWidth-1:6*dataWidth];
            end
           
             16'b0000000010000000 : begin
              outMem_data = temp_out[8*dataWidth-1:7*dataWidth];
            end
            
             16'b0000000100000000 : begin
               outMem_data = temp_out[9*dataWidth-1:8*dataWidth];
            end
           
             16'b0000001000000000 : begin
              outMem_data = temp_out[10*dataWidth-1:9*dataWidth];
            end

             16'b0000010000000000 : begin
                outMem_data = temp_out[11*dataWidth-1:10*dataWidth];
            end
            
             16'b0000100000000000 : begin
               outMem_data = temp_out[12*dataWidth-1:11*dataWidth];
             end
           
             16'b0001000000000000 : begin
                outMem_data = temp_out[13*dataWidth-1:12*dataWidth];
             end
          
            16'b0010000000000000 : begin
                outMem_data = temp_out[14*dataWidth-1:13*dataWidth];
            end
         
             16'b0100000000000000 : begin
                 outMem_data = temp_out[15*dataWidth-1:14*dataWidth];
             end
        
             16'b1000000000000000 : begin
                outMem_data = temp_out[16*dataWidth-1:15*dataWidth];
             end
       

            default : begin
                outMem_data = {dataWidth{1'bz}};
            end
        endcase
    end
    
    always@(M, reset) begin
        if(reset) begin
            h_max = {dataWidth{1'b0}};
        end
        else begin
            h_max = M/PE;
        end
    end
    
    
    //begin and end MAC operation. Calculating weight memory address
    always @(currentState, hAddr_cnt, vAddr_cnt, reset, f, r) begin
        if(reset) begin
            weightAddr_internal = {(weight_addrW){1'b0}};

            begin_ops_internal = 1'b0;
            end_ops_internal = 1'b0;
        end
        else begin
            weightAddr_internal = hAddr_cnt + vAddr_cnt * f;
            if((currentState > s2) && (hAddr_cnt == 0)) begin
                begin_ops_internal = 1'b1;
                end_ops_internal = 1'b0;
            end
            else if((currentState > s2) && (hAddr_cnt == f-1)) begin
                end_ops_internal = 1'b1;
                begin_ops_internal = 1'b0;
            end
            else begin
                begin_ops_internal = 1'b0;
                end_ops_internal = 1'b0;
            end
            
        end
    end
    
    always@(posedge clk) begin
        if(reset) begin
            outAddr_internal <= {out_addrW{1'b1}};
        end
        else begin
            if(end_ops_internal3) begin
                outAddr_internal <= outAddr_internal + 1'b1;
            end
            else begin
                outAddr_internal <= outAddr_internal;
            end
        end
    end
    
    
    //State Machine
    always @(posedge clk) begin        
        if(reset) begin
            currentState <= idle;
            cen_weightMemWR <= 1'b0;
            cen_dataMemWR <= 1'b0;
            cen_MemR <= 1'b0;
            hAddr_cnt <= {(weight_addrW){1'b0}};
            vAddr_cnt <= {(weight_addrW){1'b0}};
            done_internal <= 1'b0;
        end
        else begin
            case (currentState)
                idle: begin
                    if(!start) begin
                        currentState <= idle;
                        cen_MemR <= 1'b0;
                        hAddr_cnt <= {(weight_addrW){1'b0}};
                        vAddr_cnt <= {(weight_addrW){1'b0}};
                        cen_weightMemWR <= 1'b0;
                        cen_dataMemWR <= 1'b0;
                        done_internal <= 1'b0;
                    end
                    else begin
                        done_internal <= 1'b0;
                        if(rd_wr) begin
                            currentState <= s1;
                            cen_weightMemWR <= 1'b1;
                            cen_dataMemWR <= 1'b1;
                            hAddr_cnt <= {(weight_addrW){1'b0}};
                            vAddr_cnt <= {(weight_addrW){1'b0}};
                        end
                        else begin
                            currentState <= s2;
                            hAddr_cnt <= {(weight_addrW){1'b0}};
                            vAddr_cnt <= {(weight_addrW){1'b0}};
                        end
                    end
                end
                
                s1: begin
                    if(!rd_wr) begin
                        currentState <= s2;
                        cen_weightMemWR <= 1'b0;
                        cen_dataMemWR <= 1'b0;
                    end
                    else begin
                        cen_weightMemWR <= 1'b1;
                        cen_dataMemWR <= 1'b1;
                        currentState <= s1;
                    end
                end
                
                s2: begin
                    if(start) begin
                        currentState <= s3;
                        cen_MemR <= 1'b1;
                    end
                    else begin
                        if(rd_wr) begin
                            currentState <= s1;
                        end
                        else begin
                            currentState <= s2;
                        end
                    end
                end
                
                s3: begin
                    if(hAddr_cnt < f-1 && vAddr_cnt < h_max-1) begin
                        hAddr_cnt <= hAddr_cnt + 1'b1;
                        currentState <= s3;
                    end
                    else if( hAddr_cnt >= f-1 && vAddr_cnt < h_max-1) begin
                        hAddr_cnt <= {(weight_addrW){1'b0}};
                        vAddr_cnt <= vAddr_cnt + 1'b1;
                        currentState <= s3;
                    end
                    else if(hAddr_cnt < f-1 && vAddr_cnt >= h_max-1) begin
                        hAddr_cnt <= hAddr_cnt + 1'b1;
                        currentState <= s3;
                    end
                    else if(hAddr_cnt >= f-1 && vAddr_cnt >= h_max-1) begin
                        hAddr_cnt <= {(weight_addrW){1'b0}};
                        vAddr_cnt <= {(weight_addrW){1'b0}};
                        currentState <= idle;
                        done_internal <= 1'b1;
                    end
                    else begin
                         hAddr_cnt <= {weight_addrW{1'bz}};
                         vAddr_cnt <= {weight_addrW{1'bz}};
                    end
                end
            endcase
        end
    end 
endmodule


/*	rf_2p_32x1024 WM(
      .QA                  (weightMem_out),
      .CLKA                (clk),
      .CENA                (cen_MemR),        //cen_MemR  // from decode (convoluted)
      .AA                  (weightAddr_internal),    // from fetch
      .CLKB                (clk),
      .CENB                (cen_weightMemWR), //cen_weightMemWR
      .AB                  (weightAddr),
      .DB                  (weightMem_in),
      .EMAA                (3'b001),
      .EMAB                (3'b001),
      .RETN                (1'b1)               // Enables inst mem
    );
	
	rf_2p_32x1024 DM(
      .QA                  (dataMem_out),
      .CLKA                (clk),
      .CENA                (cen_MemR),       //cen_MemR   // from decode (convoluted)
      .AA                  (dataAddr_internal),    // from fetch
      .CLKB                (clk),
      .CENB                (cen_dataMemWR), //cen_dataMemWR
      .AB                  (dataAddr),
      .DB                  (dataMem_in),
      .EMAA                (3'b001),
      .EMAB                (3'b001),
      .RETN                (1'b1)               // Enables inst mem
    );
	
	rf_2p_32x64 OM(
	  .QA                  (outMem_data),
	  .CLKA                (clk),
	  .CENA                (cen_outMem),      //cen_outMem    // from decode (convoluted)
	  .AA                  (outAddr),    // from fetch
	  .CLKB                (clk),
	  .CENB                (~end_ops_internal4), //end_ops_internal4
	  .AB                  (outAddr_internal),
	  .DB                  (MAC_out[dataWidth-1:0]),
	  .EMAA                (3'b001),
	  .EMAB                (3'b001),
	  .RETN                (1'b1)               // Enables inst mem
	);*/

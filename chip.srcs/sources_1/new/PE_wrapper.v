
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06/02/2019 12:43:37 PM
// Design Name: 
// Module Name: PE_wrapper
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments: changed rdp_edit by adding registers, begin_ops3, end_ops3, changed endops4 to end_ops_internal5 in OM
// 
//////////////////////////////////////////////////////////////////////////////////


module PE_wrapper   #(
                        parameter dataWidth = 16,
                        parameter weight_addrW = 11,
                        parameter data_addrW = 10,
                        parameter out_addrW = 10
                   )(
                    input wire clk,
                    input wire reset,
                    input wire [dataWidth-1:0] N,
//                    input wire [dataWidth-1:0] M,
                    input wire [dataWidth-1:0] f,
                    input wire [dataWidth-1:0] r,
                    input wire rd_wr,
                    input wire [weight_addrW-1:0] weightAddr,
                    input wire [data_addrW-1:0] dataAddr,
                    input wire [out_addrW-1:0] outAddr,
                    input wire cen_outMem,
                    input wire [dataWidth-1:0] weightMem_in, dataMem_in,
                    input wire start,
                    output wire [dataWidth-1:0] outMem_data,
                    output wire done
    );
    
    reg [weight_addrW-1:0] hAddr_cnt, vAddr_cnt, weightAddr_internal;
    reg [data_addrW-1:0] dataAddr_internal;
    reg [out_addrW-1:0] outAddr_internal;
    
    wire [dataWidth-1:0] weightMem_out, dataMem_out;
    wire [(2*dataWidth):0] MAC_out;
	reg end_ops_internal2_temp, end_ops_internal2_temp1;
    reg [dataWidth-1:0] weightMem_out_internal, dataMem_out_internal;
    reg cen_weightMemWR, cen_dataMemWR, cen_MemR, begin_ops_internal, end_ops_internal;
    reg begin_ops_internal1, begin_ops_internal2, begin_ops_internal3, end_ops_internal1, end_ops_internal2, end_ops_internal3, end_ops_internal4, end_ops_internal5;
    reg done_internal, done_internal1, done_internal2, done_internal3;
    reg [2:0] currentState;
    localparam idle = 3'b000, s1 = 3'b001, s2 = 3'b010, s3 = 3'b011, s4 = 3'b100, s5 = 3'b101, s6 = 3'b110;
    
    rdp_256X32_edit 
	#(
                    .dataWidth(dataWidth),
                    .addrW(weight_addrW)
    )
	WM(
                    .clk(clk),
                    .data_inA(weightMem_in),
                    .addrA(weightAddr),
                    .addrB(weightAddr_internal),
                    .wrA(cen_weightMemWR),
                    .rdB(cen_MemR),
                    .data_outB(weightMem_out)
    );
    
    rdp_256X32_edit 
	#(
                    .dataWidth(dataWidth),
                    .addrW(data_addrW)
    )
	DM(
                    .clk(clk),
                    .data_inA(dataMem_in),
                    .addrA(dataAddr),
                    .addrB(dataAddr_internal),
                    .wrA(cen_dataMemWR),
                    .rdB(cen_MemR),
                    .data_outB(dataMem_out)
    );
    
    rdp_256X32 
	#(
                    .dataWidth(dataWidth),
                    .addrW(out_addrW)
    )
	OM(
                    .clkA(clk),
                    .clkB(clk),
                    .data_inA(MAC_out[dataWidth-1:0]),
                    .addrA(outAddr_internal),
                    .addrB(outAddr),
                    .wrA(~end_ops_internal5),
                    .rdB(~cen_outMem),
                    .data_outB(outMem_data)
    ); 
	
	/*rf_2p_32x1024 WM(
      .QA                  (weightMem_out),
      .CLKA                (clk),
      .CENA                (cen_MemR),        //cen_MemR  // from decode (convoluted)
      .AA                  (weightAddr_internal),    // from fetch
      .CLKB                (clk),
      .CENB                (cen_weightMemWR), //cen_weightMemWR
      .AB                  (weightAddr),
      .DB                  (weightMem_in),
      .EMAA                (3'b001),
      .EMAB                (3'b001),
      .RETN                (1'b1)               // Enables inst mem
    );
	
	rf_2p_32x1024 DM(
      .QA                  (dataMem_out),
      .CLKA                (clk),
      .CENA                (cen_MemR),       //cen_MemR   // from decode (convoluted)
      .AA                  (dataAddr_internal),    // from fetch
      .CLKB                (clk),
      .CENB                (cen_dataMemWR), //cen_dataMemWR
      .AB                  (dataAddr),
      .DB                  (dataMem_in),
      .EMAA                (3'b001),
      .EMAB                (3'b001),
      .RETN                (1'b1)               // Enables inst mem
    );
	
	rf_2p_32x64 OM(
	  .QA                  (outMem_data),
	  .CLKA                (clk),
	  .CENA                (cen_outMem),      //cen_outMem    // from decode (convoluted)
	  .AA                  (outAddr),    // from fetch
	  .CLKB                (clk),
	  .CENB                (~end_ops_internal4), //end_ops_internal4
	  .AB                  (outAddr_internal),
	  .DB                  (MAC_out[dataWidth-1:0]),
	  .EMAA                (3'b001),
	  .EMAB                (3'b001),
	  .RETN                (1'b1)               // Enables inst mem
	); */
    
    
    MAC 
	#(
            .dataWidth(dataWidth)
    )
	MU(
            .clk(clk),
            .reset(reset),
            .dataA(weightMem_out_internal),
            .dataB(dataMem_out_internal),
            .begin_ops(begin_ops_internal3),
            .end_ops(end_ops_internal3),
            .dataOut(MAC_out)
    );
    
    assign done = done_internal3; //finished matrix multiplication
	
	
	always@(end_ops_internal1) begin
		if(end_ops_internal1) begin
			end_ops_internal2_temp = 1'b1; 
		end
		else begin
			end_ops_internal2_temp = 1'b0;
		end
	end
	
    
    always @(posedge clk) begin
        if(reset) begin
            weightMem_out_internal <= {(dataWidth){1'b0}};
            dataMem_out_internal <= {(dataWidth){1'b0}};
            begin_ops_internal1 <= 1'b0;
            begin_ops_internal2 <= 1'b0;
            begin_ops_internal3 <= 1'b0;
            end_ops_internal1 <= 1'b0;
            end_ops_internal2 <= 1'b0;
            end_ops_internal3 <= 1'b0;
            end_ops_internal4 <= 1'b0;
            done_internal1 <= 1'b0;
            done_internal2 <= 1'b0;
            done_internal3 <= 1'b0; 
            
        end
        else begin
            weightMem_out_internal <= weightMem_out;
            dataMem_out_internal <= dataMem_out;
            begin_ops_internal1 <= begin_ops_internal;
            begin_ops_internal2 <= begin_ops_internal1;
            begin_ops_internal3 <= begin_ops_internal2;
            end_ops_internal1 <= end_ops_internal;
            end_ops_internal2 <= end_ops_internal2_temp;
            end_ops_internal3 <= end_ops_internal2;
            end_ops_internal4 <= end_ops_internal3;
            end_ops_internal5 <= end_ops_internal4;
            done_internal1 <= done_internal;
            done_internal2 <= done_internal1;
            done_internal3 <= done_internal2;            
        end
    end
    
    always @(currentState, hAddr_cnt, vAddr_cnt, reset, f, r, N) begin
        if(reset) begin
            weightAddr_internal = {(weight_addrW){1'b0}};
            dataAddr_internal = {(weight_addrW){1'b0}};
            //dataAddr_internal_temp = {(weight_addrW){1'b0}};
            begin_ops_internal = 1'b0;
            end_ops_internal = 1'b0;
        end
        else begin
            weightAddr_internal = hAddr_cnt + vAddr_cnt * f;
//            dataAddr_internal_temp = hAddr_cnt * r + vAddr_cnt;
            dataAddr_internal = (hAddr_cnt * r + vAddr_cnt) < N ? (hAddr_cnt * r + vAddr_cnt) : (hAddr_cnt * r + vAddr_cnt)- N;
            
            if((currentState > s2) && (hAddr_cnt == 0)) begin
                begin_ops_internal = 1'b1;
                end_ops_internal = 1'b0;
            end
            else if((currentState > s2) && (hAddr_cnt == f-1)) begin
                end_ops_internal = 1'b1;
                begin_ops_internal = 1'b0;
            end
            else begin
                begin_ops_internal = 1'b0;
                end_ops_internal = 1'b0;
            end
            
        end
    end
    
    always@(posedge clk) begin
        if(reset) begin
            outAddr_internal <= {out_addrW{1'b1}};
        end
        else begin
            if(end_ops_internal3) begin
                outAddr_internal <= outAddr_internal + 1'b1;
            end
            else begin
                outAddr_internal <= outAddr_internal;
            end
        end
    end
    
    always @(posedge clk) begin        
        if(reset) begin
            currentState <= idle;
            cen_weightMemWR <= 1'b0;
            cen_dataMemWR <= 1'b0;
            cen_MemR <= 1'b0;
            hAddr_cnt <= {(weight_addrW){1'b0}};
            vAddr_cnt <= {(weight_addrW){1'b0}};
        end
        else begin
            case (currentState)
                idle: begin
                    if(!start) begin
                        currentState <= idle;
                        cen_MemR <= 1'b0;
                        hAddr_cnt <= {(weight_addrW){1'b0}};
                        vAddr_cnt <= {(weight_addrW){1'b0}};
                        cen_weightMemWR <= 1'b0;
                        cen_dataMemWR <= 1'b1;
                        done_internal <= 1'b0;
                    end
                    else begin
                        done_internal <= 1'b0;
                        if(rd_wr) begin
                            currentState <= s1;
                            cen_weightMemWR <= 1'b1;
                            cen_dataMemWR <= 1'b1;
                            hAddr_cnt <= {(weight_addrW){1'b0}};
                            vAddr_cnt <= {(weight_addrW){1'b0}};
                        end
                        else begin
                            currentState <= s2;
                            hAddr_cnt <= {(weight_addrW){1'b0}};
                            vAddr_cnt <= {(weight_addrW){1'b0}};
                        end
                    end
                end
                
                s1: begin
                    if(!rd_wr) begin
                        currentState <= s2;
                        cen_weightMemWR <= 1'b0;
                        cen_dataMemWR <= 1'b0;
                    end
                    else begin
                        cen_weightMemWR <= 1'b1;
                        cen_dataMemWR <= 1'b1;
                        currentState <= s1;
                    end
                end
                
                s2: begin
                    if(start) begin
                        currentState <= s3;
                        cen_MemR <= 1'b1;
                    end
                    else begin
                        if(rd_wr) begin
                            currentState <= s1;
                        end
                        else begin
                            currentState <= s2;
                        end
                    end
                end
                
                s3: begin
                    if(hAddr_cnt < f-1 && vAddr_cnt < N-1) begin
                        hAddr_cnt <= hAddr_cnt + 1'b1;
                        currentState <= s3;
                    end
                    else if( hAddr_cnt >= f-1 && vAddr_cnt < N-1) begin
                        hAddr_cnt <= {(weight_addrW){1'b0}};
                        vAddr_cnt <= vAddr_cnt + 1'b1;
                        currentState <= s3;
                    end
                    else if(hAddr_cnt < f-1 && vAddr_cnt >= N-1) begin
                        hAddr_cnt <= hAddr_cnt + 1'b1;
                        currentState <= s3;
                    end
                    else if(hAddr_cnt >= f-1 && vAddr_cnt >= N-1) begin
                        hAddr_cnt <= {(weight_addrW){1'b0}};
                        vAddr_cnt <= {(weight_addrW){1'b0}};
                        currentState <= idle;
                        done_internal <= 1'b1;
                    end
                end
            endcase
        end
    end 
    
endmodule

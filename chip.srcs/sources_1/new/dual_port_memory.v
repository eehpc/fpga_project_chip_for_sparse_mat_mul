`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06/08/2019 09:10:03 PM
// Design Name: 
// Module Name: dual_port_memory
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module dual_port_memory
                    #(
                    parameter dataWidth = 16,
                    parameter addrW = 9
                    )(
                    input wire clkA, clkB,
                    input wire reset,
                    input wire [dataWidth-1:0] data_inA,
                    input wire [addrW-1:0] addrA, addrB,
                    input wire wrA, rdB,
                    output reg [dataWidth-1:0] data_outB
    );
    
    localparam RAM_DEPTH = 1 << addrW;
    
    reg  [dataWidth-1:0] r_2p [RAM_DEPTH-1:0] ;
    integer i;
    
    always @(posedge clkA) begin: portA
        if(reset)begin
            for(i=0; i<=RAM_DEPTH-1; i=i+1) 
                r_2p[i] <= {dataWidth{1'b0}};             
        end
        else begin
            if(wrA) begin
                r_2p[addrA] <= data_inA;
            end
        end
    end
    
    always @(posedge clkB) begin: portB
        if(rdB) begin
            data_outB <= r_2p[addrB];
        end
    end
endmodule


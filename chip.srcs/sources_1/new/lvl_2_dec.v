`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06/08/2019 11:43:51 PM
// Design Name: 
// Module Name: lvl_2_dec
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module lvl_2_dec(
            input wire en,
            input wire [2:0] i,
            output wire [7:0] out
             );
             
wire [1:0] o1;

dec2_1 inst0(
       .en(en),
       .i(i[2]),
       .out(o1));
                
lvl_1_dec inst1(
        .en(o1[0]),
        .i(i[1:0]),
        .out(out[3:0]));

lvl_1_dec inst2(
        .en(o1[1]),
        .i(i[1:0]),
        .out(out[7:4]));
       
endmodule

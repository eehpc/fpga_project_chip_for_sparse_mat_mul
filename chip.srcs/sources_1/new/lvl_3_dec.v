`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06/09/2019 12:00:20 AM
// Design Name: 
// Module Name: lvl_3_dec
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module lvl_3_dec(
                input wire en,
                input wire [3:0] i,
                output wire [15:0] out
    );
    
    wire [1:0] o1;
    
    dec2_1 inst0(
          .en(en),
          .i(i[3]),
          .out(o1[1:0]));
          
    lvl_2_dec inst1(
              .en(o1[0]),
              .i(i[2:0]),
              .out(out[7:0]));
              
    lvl_2_dec inst2(
            .en(o1[1]),
            .i(i[2:0]),
            .out(out[15:8]));

endmodule

`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06/01/2019 11:51:15 PM
// Design Name: 
// Module Name: MAC
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module MAC  #(
                parameter dataWidth = 8
    )(
                input wire clk,
                input wire reset,
                input wire [5-1:0] shift,
                input wire signed [dataWidth-1:0] dataA,
                input wire signed [dataWidth-1:0] dataB,
                input wire begin_ops,
                input wire end_ops,
                (*keep = "true"*) output reg [dataWidth-1:0] dataOut
    );
    
    (*keep = "true"*) reg signed [(2*dataWidth)+10:0] prodVal;
    (*keep = "true"*) reg signed [(2*dataWidth)+10:0] sumVal;
    
    (*keep = "true"*) reg signed [(2*dataWidth)+10:0] dataOut_internal;
//    reg begin_ops_internal, end_ops_internal;
    
    (*keep = "true"*) reg [1:0] currentState;
    localparam idle = 2'b00, s1 = 2'b01, s2 = 2'b10;
    
    
    always @(posedge clk) begin
        if(reset) begin
            currentState <= idle;
            prodVal <= 0;
            sumVal <= 0;
            dataOut_internal <= 0;
        end
        else begin
            case(currentState)
                idle: begin
                    if(begin_ops) begin
                        if(end_ops) begin
                            dataOut_internal <= dataA * dataB;
                            currentState <= idle;
                        end
                        else begin
                            currentState <= s1;
                            prodVal <= dataA * dataB;
                            sumVal <= sumVal + prodVal;
                        end
                    end
                    else begin
                        currentState <= idle;
                        sumVal <= 0;
                        prodVal <= 0;
                    end
                end
                
                s1: begin
                    if(!end_ops)begin
                        prodVal <= dataA * dataB;
                        sumVal <= sumVal + prodVal;
                        currentState <= s1;
                    end
                    else begin
                        prodVal <= dataA * dataB;
                        sumVal <= sumVal + prodVal;
                        currentState <= s2;
                    end
                end
                
                s2: begin
                    if(begin_ops) begin
                        prodVal <= dataA * dataB;
                        sumVal <= 0;
                        dataOut_internal <= sumVal + prodVal;
                        currentState <= s1;
                    end
                    else begin
                        dataOut_internal <= sumVal + prodVal;
                        currentState <= idle;
                    end
                end
                
                default : begin
                    currentState <= idle;
                end
            endcase
        end
    end

//assign dataOut = {dataOut_internal[2*dataWidth-1],dataOut_internal[dataWidth-2:0]};

//output assignment using shift operator.
//assign dataOut = dataOut_internal[dataWidth-1:0];

always@(reset, end_ops, shift, dataOut_internal) begin
    case(shift) 
        5'd0 : begin
            dataOut = dataOut_internal[dataWidth-1+0:0];
        end
        5'd1 : begin
            dataOut = dataOut_internal[dataWidth-1+1:1];
        end
        5'd2 : begin
            dataOut = dataOut_internal[dataWidth-1+2:2];
        end
        5'd3 : begin
            dataOut = dataOut_internal[dataWidth-1+3:3];
        end
        5'd4 : begin
            dataOut = dataOut_internal[dataWidth-1+4:4];
        end
        5'd5 : begin
            dataOut = dataOut_internal[dataWidth-1+5:5];
        end
        5'd6 : begin
            dataOut = dataOut_internal[dataWidth-1+6:6];
        end
        5'd7 : begin
            dataOut = dataOut_internal[dataWidth-1+7:7];
        end
        5'd8 : begin
            dataOut = dataOut_internal[dataWidth-1+8:8];
        end
        5'd9 : begin
            dataOut = dataOut_internal[dataWidth-1+9:9];
        end
        5'd10 : begin
            dataOut = dataOut_internal[dataWidth-1+10:10];
        end
        5'd11 : begin
            dataOut = dataOut_internal[dataWidth-1+11:11];
        end
        5'd12 : begin
            dataOut = dataOut_internal[dataWidth-1+12:12];
        end
        5'd13 : begin
            dataOut = dataOut_internal[dataWidth-1+13:13];
        end
        5'd14 : begin
            dataOut = dataOut_internal[dataWidth-1+14:14];
        end
        5'd15 : begin
            dataOut = dataOut_internal[dataWidth-1+15:15];
        end
        5'd16 : begin
            dataOut = dataOut_internal[dataWidth-1+16:16];
        end
        5'd17 : begin
            dataOut = dataOut_internal[dataWidth-1+17:17];
        end
        5'd18 : begin
            dataOut = dataOut_internal[dataWidth-1+18:18];
        end
        5'd19 : begin
            dataOut = dataOut_internal[dataWidth-1+19:19];
        end
//        5'd20 : begin
//            dataOut = dataOut_internal[dataWidth-1+20:20];
//        end
        default : begin
            dataOut = dataOut_internal[dataWidth-1:0];
        end
    endcase
end

endmodule

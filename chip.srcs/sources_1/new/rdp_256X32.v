`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06/02/2019 08:22:24 PM
// Design Name: 
// Module Name: rdp_256X32
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module rdp#(
            parameter dataWidth = 32,
            parameter addrW = 8,
            parameter memFile = ""
            )(
            input wire clk,
            input wire [dataWidth-1:0] data_inA,
            input wire [addrW-1:0] addrA, addrB,
            input wire wrA, rdB,
            output reg [dataWidth-1:0] data_outB
    );
    
    localparam RAM_DEPTH = 1 << addrW;
    
    reg  [dataWidth-1:0] r_2p [RAM_DEPTH-1:0] ;
    
    initial  $readmemh(memFile, r_2p);
    
    always @(posedge clk) begin: portA
        if(wrA) begin
            r_2p[addrA] <= data_inA;
        end
    end
    
    always @(posedge clk) begin: portB
        if(rdB) begin
            data_outB <= r_2p[addrB];
        end
        else begin
            data_outB <= {dataWidth{1'bz}};
        end
    end
endmodule

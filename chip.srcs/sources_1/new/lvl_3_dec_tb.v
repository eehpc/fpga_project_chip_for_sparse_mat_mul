`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06/09/2019 12:16:21 AM
// Design Name: 
// Module Name: lvl_3_dec_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module lvl_3_dec_tb(

    );
    
    reg en;
    reg [3:0]i;
    wire [15:0] out;
    
    lvl_3_dec uut(.en(en), .i(i), .out(out));
    
    initial begin
     en = 0;
     i = 4'b0001;
     #10;
     
     en = 0;
     i = 4'b0010;
     #10;
     
     en = 0;
     i = 4'b0100;
     #10;

      en = 1;
      i = 4'b0001;
      #10;
      
      en = 1;
      i = 4'b0010;
      #10;
      
      en = 1;
      i = 4'b0100;
      #10;
      
        en = 1;
      i = 4'b1001;
      #10;
      
      en = 1;
      i = 4'b1010;
      #10;
      
      en = 1;
      i = 4'b1100;
      $finish();
    end

endmodule

`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: EEHPC-UMBC
// Engineer: Nitheesh Manjunath
// 
// Create Date: 02/10/2020 08:21:30 PM
// Design Name: 
// Module Name: seven_segment
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module seven_segment(
    input clk,
    input [3:0] SW,
//    output[7:0] AN,
    output CA,
    output CB,
    output CC,
    output CD,
    output CE,
    output CF,
    output CG);
    
    reg [6:0] r_encode;
    
    always @(posedge clk) begin
        case(SW)
            4'b0000: begin
                r_encode <= 7'h7E;
            end
            4'b0001: begin
                r_encode <= 7'h30;
            end
            4'b0010: begin
                r_encode <= 7'h6d;
            end
            4'b0011: begin
                r_encode <= 7'h79;
            end
            4'b0100: begin
                r_encode <= 7'h33;
            end
            4'b0101: begin
                r_encode <= 7'h5b;
            end
            4'b0110: begin
                r_encode <= 7'h5f;
            end
            4'b0111: begin
                r_encode <= 7'h70;
            end
            4'b1000: begin
                r_encode <= 7'h7f;
            end
            4'b1001: begin
                r_encode <= 7'h7b;
            end
            4'b1010: begin
                r_encode <= 7'h77;
            end
            4'b1011: begin
                r_encode <= 7'h1f;
            end
            4'b1100: begin
                r_encode <= 7'h4e;
            end
            4'b1101: begin
                r_encode <= 7'h3d;
            end
            4'b1110: begin
                r_encode <= 7'h4f;
            end
            4'b1111: begin
                r_encode <= 7'h47;
            end
        endcase
    end
    
    assign CA = ~r_encode[6];
    assign CB = ~r_encode[5];
    assign CC = ~r_encode[4];
    assign CD = ~r_encode[3];
    assign CE = ~r_encode[2];
    assign CF = ~r_encode[1];
    assign CG = ~r_encode[0];
//    assign AN = 8'b11111110;

/* //paste it in higher hierarchy
 always @(posedge clk) begin
   flag = flag + 1;
end

always @(posedge clk) begin
   if(flag == 16'b1111111111111111)
       set = ~set;
end

always @(posedge clk) begin
   if(set == 1'b0) begin
       count1 = offset_trunc[3:0];
       AN = 8'b11111110;
   end
   else begin
       count1 = offset_trunc[7:4];
       AN = 8'b11111101;
   end
end
*/
endmodule

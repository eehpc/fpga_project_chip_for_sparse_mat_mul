`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: EEHPC
// Engineer: Nitheesh Manjunath
// 
// Create Date: 06/06/2019 01:40:06 PM
// Design Name: 
// Module Name: dec2_1
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module dec2_1(
            input wire i,
            input wire en,
            output wire [1:0] out
    );
    
    assign out = !en ?  2'b00 : i ? 2'b10 : 2'b01;
    
endmodule

`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: UMBC - EEHPC
// Engineer: Nitheesh Manjunath
// 
// Create Date: 10/22/2019 09:13:45 PM
// Design Name: 
// Module Name: n_bit_decoder
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module n_bit_decoder#(
        parameter PE = 256,
        parameter log2_PE = 8
        )(
        input wire en,
        input wire [log2_PE-1:0] i,
        output wire [PE-1:0] out
    );

assign out = en ? (1 << i) : {PE{1'b0}};

endmodule

`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: EEHPC-UMBC
// Engineer: Nitheesh Manjunath
// 
// Create Date: 02/08/2020 11:52:24 AM
// Design Name: 
// Module Name: FPGA_TOP
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// // Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
/////###############Change the 2'b00 to 2'b10####################### in address allocation 

module FPGA_TOP_3 #(
        baud_rate = 9600,
        sys_clk_freq = 100000000,
        dataWidth = 8,
        addrW = 15,
        layer1_N = 512,
        f_1 = 16,
        layer2_N = 512,
        f_2 = 16,
        layer3_N = 16,
        f_3 = 512,
        No_of_parameter = 10
        )(
        input clk_in,
        input reset_in, 
        input rx,
        input load_WM,
        input [1:0] test_input, 
//        input test_trigger,
        
        output tx,
        output is_receiving,
        output is_transmitting,
        output recv_error,
        (*keep = "true"*) output reg [7:0] AN,
        output CA,
        output CB,
        output CC,
        output CD,
        output CE,
        output CF,
        output CG,
        output locked,
        input fifo_clear,
//        input test_mode,
        
//        output full,
//        output empty,
        
        output reg led0,
        output reg led1,
        output reg led2,
        output reg led3,
        output reg led4,
        output reg led5,
        output reg led6,
        output reg led7,
        output reg led8,
        output reg led9
        );      
    
localparam dataAddrW = 10;
localparam [5-1:0] state_0 = 5'b00000,
                   state_1 = 5'b00001,
                   state_2 = 5'b00010,
                   state_3 = 5'b00011,
                   state_4 = 5'b00100,
                   state_5 = 5'b00101,
                   state_6 = 5'b00110,
                   state_7 = 5'b00111,
                   state_8 = 5'b01000,
                   state_9 = 5'b01001,
                   state_10 = 5'b01010,
                   wait_state = 5'b01011,
                   DM_read = 5'b01100,
                   state_11 = 5'b01101,
                   state_12 = 5'b01110,
                   state_13 = 5'b01111,
                   state_14 = 5'b10000,
                   state_15 = 5'b10001,
                   state_16 = 5'b10010;
//Registers
 (*keep = "true"*)reg [5-1:0] currentState;
 (*keep = "true"*)reg read_WM, start, read_WM_1, received_1, OM_Addr_rd, OM_Addr_rd_1, OM_Addr_rd_2, OM_Addr_rd_3, OM_Addr_rd_4, OM_Addr_rd_5, OM_Addr_rd_6, OM_Addr_rd_7;
 (*keep = "true"*)reg [addrW-1:0] WM_PM_Address;
 (*keep = "true"*)reg [dataAddrW-1:0] dataAddress_fifo, dataAddress, dataAddress_fifo_1;
 (*keep = "true"*)reg [dataWidth-1:0] data_in;
 (*keep = "true"*)reg [14:0] address_in;
 (*keep = "true"*)reg [12-1:0] OM_Address_a, OM_write_Address, OM_read_Address, OM_read_Address_1, OM_write_Address_1;
 (*keep = "true"*)reg read_OM2DM, read_OM2DM_1, read_OM2DM_2, read_WM_2;
 (*keep = "true"*)reg [4-1:0] prediction, prediction_count;
 (*keep = "true"*)reg signed [dataWidth-1:0] last_data;
 (*keep = "true"*)reg [dataWidth-1:0] data_out_1;
 (*keep = "true"*)reg reset_chip, read_WM_3, read_OM2DM_3, fifo_read;
 (*keep = "true"*)reg [4-1:0] count1; //7seg
 (*keep = "true"*)reg  fifo_read_1, reset_stable, reset_auto;
 (*keep = "true"*)reg [10-1:0] layer2_N_conditional;
// (*keep = "true"*)reg [10-1:0] test_counter;
//Wires
wire [14:0] WM_PM_chip_Address;
wire [dataWidth-1:0] WM_PM_chip_data, rx_byte, OM_chip_data, fifo_out;
wire done, received;
wire [1:0] chip_State;
wire [15-1:0] OM_address_out;
wire signed [dataWidth-1:0] data_out;
//wire test_3;
wire clk, clk_uart, reset;

 clk_wiz_0 clk_gen(
    .clk_out1(clk_uart),
    .clk_out2(clk),
    .reset(reset_in),
    .locked(locked),
    .clk_in1(clk_in)
    );
 
chip_top_fpga chip(
    .clk(clk),
    .reset(reset | reset_chip | reset_stable),
    .start(start),
    .write(read_WM_1 | read_WM_3 | read_OM2DM_1 | read_OM2DM_2 | read_OM2DM_3 | fifo_read_1), //read_OM2DM_2
    .data_in(data_in),
    .address_in(address_in),
    .cen_OM_rd(OM_Addr_rd_5 |  OM_Addr_rd_2 | OM_Addr_rd_3 | OM_Addr_rd_4 ),
    .data_out(data_out),
    .currentState(chip_State),
    .done(done)
    );
                                
rdp#(
    .dataWidth(15),
    .addrW(addrW),
    .memFile("wm_addr_test.mem")
    )WM_PM_address(
    .clk(clk),
    .data_inA(15'b0),
    .addrA({addrW{1'b0}}),
    .addrB(WM_PM_Address), 
    .wrA(1'b0),
    .rdB(read_WM), 
    .data_outB(WM_PM_chip_Address)
    );
                 
rdp#(
    .dataWidth(dataWidth),
    .addrW(addrW),
    .memFile("wm_data_test.mem")
    )WM_PM_data(
    .clk(clk),
    .data_inA(8'b0),
    .addrA({addrW{1'b0}}),
    .addrB(WM_PM_Address),
    .wrA(1'b0),
    .rdB(read_WM),
    .data_outB(WM_PM_chip_data)
    );
    
rdp#(
    .dataWidth(dataWidth),
    .addrW(12),
    .memFile("")
    )OM(
    .clk(clk),
    .data_inA(data_out_1),
    .addrA(OM_write_Address),
    .addrB(OM_read_Address),
    .wrA(OM_Addr_rd_6),
    .rdB(read_OM2DM), 
    .data_outB(OM_chip_data)
    );
    
rdp#(
    .dataWidth(15),
    .addrW(12),
    .memFile("om_addr_test.mem")
    )OM_address(
    .clk(clk),
    .data_inA(15'b0),
    .addrA(12'b0),
    .addrB(OM_Address_a), 
    .wrA(1'b0),
    .rdB(OM_Addr_rd), 
    .data_outB(OM_address_out)
    );
              
uart#(
    .baud_rate(baud_rate),            // default is 9600
    .sys_clk_freq(sys_clk_freq)       // default is 100000000
    )
    uart_instance(
    .clk(clk_uart),                        // The master clock for this module
    .rst(reset),                      // Synchronous reset
    .rx(rx),                          // Incoming serial line
    .tx(tx),                          // Outgoing serial line
    .transmit(1'b0),              // Signal to transmit
    .tx_byte(8'b0),                // Byte to transmit       
    .received(received),              // Indicated that a byte has been received
    .rx_byte(rx_byte),                // Byte received
    .is_receiving(is_receiving),      // Low when receive line is idle
    .is_transmitting(is_transmitting),// Low when transmit line is idle
    .recv_error(recv_error)           // Indicates error in receiving packet.
    );


generic_fifo_dc  #(
    .dw(8),
    .aw(10)
    )fifo(
    .rd_clk(clk), 
    .wr_clk(clk_uart), 
    .rst(!reset), 
    .clr(fifo_clear), 
    .din(rx_byte), 
    .we(received), 
    .dout(fifo_out), 
    .re(fifo_read)
//    .full(full), 
//    .empty(empty)
    );

seven_segment seven_segment_inst(
    .clk(clk),
    .SW(count1),
    .CA(CA),
    .CB(CB),
    .CC(CC),
    .CD(CD),
    .CE(CE),
    .CF(CF),
    .CG(CG)
    );

/*
Test Trigger
*/
//always@(posedge clk) begin
//    if(reset) begin
//        test_1 <= 0;
//        test_2 <= 0;
//    end
//    begin
//        if(test_trigger) begin
//            test_1 <= test_trigger;
//            test_2 <= 0;
//        end
//        else begin
//            test_2 <= test_1;
//            test_1 <= 0;
//        end
//    end
//end
//assign test_3 = !test_1 & test_2; 


/*
Auto_trigger reset
*/

always@(posedge clk) begin
    if(currentState == state_16) begin
        reset_auto <= 1;
    end
    else begin
        reset_auto <= 0;
    end
end

assign reset = reset_auto | reset_in;

//always@(posedge clk) begin
//    if(reset) begin
//        fifoTestRead <= 0;
//        test_counter <= 0;
//    end
//    else begin
//        if(test_mode && test_counter < 10'd784) begin
//            fifoTestRead <= 1;
//            test_counter <= test_counter + 1;
//        end
//        else begin
//            fifoTestRead <= 0;
//            test_counter <= test_counter;
//        end
//    end
//end


//Stable reset for chip;
always@(posedge clk) begin
    if(reset) begin
        reset_stable <= 1;
    end 
    else begin
        reset_stable <= 0;
    end
end

//Writing to DM on Chip
always@(posedge clk_uart) begin
    if(reset) begin
        dataAddress_fifo <= 0;
    end
    else begin
        if(received) begin
            dataAddress_fifo <= dataAddress_fifo + {{dataAddrW-1{1'b0}}, 1'b1};
        end
        else begin
            dataAddress_fifo <= dataAddress_fifo;
        end
    end
end

//always@(*) begin
//    if(reset) begin
//        write = 0;
//    end
//    else begin
//        if(read_WM_1 || read_WM_2) begin
//            write = 1;
//        end
//        else if(currentState == state_0) begin
//            write = received || received_1;
//        end
//        else if(read_OM2DM_1 || read_OM2DM_2) begin
//            write = 1;
//        end
//        else begin
//            write = 0;
//        end
//    end
//end

always@(posedge clk) begin
    if(reset) begin
        data_in <= 0;
        address_in <= 0;
    end
    else begin
        if(currentState == DM_read) begin
            data_in <= fifo_out;
            address_in <= {{2'b10},{3'b000},dataAddress}; /////###############Chnage the 2'b00 to 2'b10#######################
        end
        else if(read_WM || read_WM_1) begin
            data_in <= WM_PM_chip_data;
            address_in <= WM_PM_chip_Address;
        end
        else if(OM_Addr_rd_1) begin
            address_in <= OM_address_out;
            data_in <= 0;
        end
        else if(read_OM2DM || read_OM2DM_1) begin
            data_in <= OM_chip_data;
            address_in <= {{2'b10},{3'b000},OM_read_Address_1[10-1:0]-layer2_N_conditional};
        end
        else begin
            data_in <= data_in;
            address_in <= address_in;
        end
    end
end

always@(posedge clk ) begin
    if(reset) begin
        dataAddress <= 0;
    end
    else begin
        if(fifo_read) begin
            dataAddress <= dataAddress + {{dataAddrW-1{1'b0}}, 1'b1};
        end
        else begin
            dataAddress <= dataAddress;
        end
    end
end


//Reading from WM
always@(posedge clk) begin
    if(reset) begin
        WM_PM_Address <= 0;
    end
    else begin
        if(read_WM) begin
            WM_PM_Address <= WM_PM_Address + {{addrW-1{1'b0}},1'b1};
        end
        else begin
            WM_PM_Address <= WM_PM_Address;
        end
    end
end

//Reading OM_Address
always@(posedge clk) begin
    if(reset) begin
        OM_Address_a <= 0;
    end
    else begin
        if(OM_Addr_rd) begin
            OM_Address_a <= OM_Address_a + {{10-1{1'b0}},1'b1};
        end
        else begin
            OM_Address_a <= OM_Address_a;
        end
    end 
end

//Pipeline
always@(posedge clk) begin
    if(reset) begin
        read_WM_1 <= 0;
        received_1 <= 0;
        OM_Addr_rd_1 <= 0;
        OM_Addr_rd_2 <= 0;
        OM_Addr_rd_3 <= 0; 
        OM_Addr_rd_4 <= 0;
        OM_Addr_rd_5 <= 0;
        OM_Addr_rd_6 <= 0;
        OM_Addr_rd_7 <= 0;
        read_OM2DM_1 <= 0;
        read_OM2DM_2 <= 0;
        OM_read_Address_1 <= 0;
        OM_write_Address_1 <= 0;
        read_WM_2 <= 0;
        read_WM_3 <= 0;
        read_OM2DM_3 <= 0;
        fifo_read_1 <= 0;
        dataAddress_fifo_1 <= 0;
     end
    else begin
        read_WM_1 <= read_WM;
        read_WM_2 <= read_WM_1;
        read_WM_3 <= read_WM_2;
        received_1 <= received;
        OM_Addr_rd_1 <= OM_Addr_rd;
        OM_Addr_rd_2 <=  OM_Addr_rd_1;
        OM_Addr_rd_3 <= OM_Addr_rd_2;
        OM_Addr_rd_4 <= OM_Addr_rd_3;
        OM_Addr_rd_5 <= OM_Addr_rd_4;
        OM_Addr_rd_6 <= OM_Addr_rd_5;
        OM_Addr_rd_7 <= OM_Addr_rd_6;
        read_OM2DM_1 <= read_OM2DM;
        read_OM2DM_2 <= read_OM2DM_1;
        read_OM2DM_3 <= read_OM2DM_2;
        OM_read_Address_1 <= OM_read_Address;
        OM_write_Address_1 <= OM_write_Address;
        fifo_read_1 <= fifo_read;
        dataAddress_fifo_1 <= dataAddress_fifo;
        
    end
end

//ReLU//
always@(*) begin
    if(reset) begin
        data_out_1 = {dataWidth{1'b0}};
    end
    else begin
        if(OM_Addr_rd_6) begin
            if(data_out[7] == 1'b1) begin
                data_out_1 = {dataWidth{1'b0}};
            end
            else begin
                data_out_1 = data_out;
            end
        end
        else begin
            data_out_1 = {dataWidth{1'b0}};
        end
    end
end

/*
Writing into OM
*/
always@(posedge clk) begin
    if(reset) begin
        OM_write_Address <= 0;
    end
    else begin
        if(OM_Addr_rd_6) begin
            OM_write_Address <= OM_write_Address + {{10-1{1'b0}},1'b1};
        end
        else begin
            OM_write_Address <= OM_write_Address;
        end
    end
end


/*
Reading from OM Address
*/
always@(posedge clk) begin
    if(reset) begin
        OM_read_Address <= 0;
    end
    else begin
        if(read_OM2DM) begin
            OM_read_Address <= OM_read_Address + {{10-1{1'b0}}, 1'b1};
        end
        else begin
            OM_read_Address <= OM_read_Address;
        end
    end
end

/*
Evaluating the last layer
*/
always@(posedge clk) begin
    if(reset_in || done) begin
        prediction_count <= 4'b0;
        last_data <= 0;
        prediction <= 0;
    end
    else begin
        if(OM_Addr_rd_6) begin
            prediction_count <= prediction_count + 1;
            if(data_out > last_data) begin
                last_data <= data_out;
                prediction <= prediction_count;
            end
            else begin
                last_data <= last_data;
                prediction <= prediction;
            end
        end
    end
end

/*
LED light up after prediciton
*/
always@(posedge clk) begin
    if(reset_in || done) begin
        led0 <= 0;
        led1 <= 0;
        led2 <= 0;
        led3 <= 0;
        led4 <= 0;
        led5 <= 0;
        led6 <= 0;
        led7 <= 0;
        led8 <= 0;
        led9 <= 0;
    end
    else begin
        if(currentState == state_15 || currentState == state_0 || currentState == state_16) begin
            case(prediction) 
                4'b0000 : begin
                    led0 <= 1;
                end
                4'b0001 : begin
                    led1 <= 1;
                end
                4'b0010 : begin
                    led2 <= 1;
                end
                4'b0011 : begin
                    led3 <= 1;
                end
                4'b0100 : begin
                    led4 <= 1;
                end
                4'b0101 : begin
                    led5 <= 1;
                end 
                4'b0110 : begin
                    led6 <= 1;
                end
                4'b0111 : begin
                    led7 <= 1;
                end
                4'b1000 : begin
                    led8 <= 1;
                end
                4'b1001 : begin
                    led9 <= 1;
                end
                
                default : begin
                    led0 <= 1;
                    led1 <= 1;
                    led2 <= 1;
                    led3 <= 1;
                    led4 <= 1;
                    led5 <= 1;
                    led6 <= 1;
                    led7 <= 1;
                    led8 <= 1;
                    led9 <= 1;
                end
            endcase
        end
        else begin
            led0 <= 0;
            led1 <= 0;
            led2 <= 0;
            led3 <= 0;
            led4 <= 0;
            led5 <= 0;
            led6 <= 0;
            led7 <= 0;
            led8 <= 0;
            led9 <= 0;
        end
    end
end


/*
State-Machine
*/
always@(posedge clk) begin
    if(reset) begin
        currentState <= state_0;
        read_WM <= 0;
        start <= 0;
        OM_Addr_rd <= 0;
        read_OM2DM <= 0;
        reset_chip <= 1;
        fifo_read <= 0;
        layer2_N_conditional <= 0;
    end
    else begin
        case(currentState)
            state_0 :  begin
                read_WM <= 0;
                reset_chip <= 0;
                start <= 0;
                OM_Addr_rd <= 0;
                fifo_read <= 0;
                read_OM2DM <= 0;
                layer2_N_conditional <= 0;
                if(load_WM || dataAddress_fifo_1 == 10'd784) begin
//                    if(test_mode) begin
//                        currentState <= state_1;
//                    end
//                    else begin
                        currentState <= DM_read;
//                    end
                end
                else begin
                    currentState <= state_0;
                end        
            end
            
            DM_read : begin
                if(dataAddress <= 784-1) begin
                    fifo_read <= 1;
                    currentState <= DM_read;
                end
                else begin
                    fifo_read <= 0;
                    currentState <= wait_state;
                end
            end
            
            wait_state : begin
                currentState <= state_1;
            end
            
            state_1 : begin
                if(WM_PM_Address < (layer1_N*f_1)+No_of_parameter-1) begin
                    read_WM <= 1;
                    currentState <= state_1;
                end
                else begin
                    read_WM <= 0;
                    currentState <= state_2;
                end
            end
            
            state_2 : begin
                start <= 1;
                currentState <= state_3;
            end
            
            state_3 : begin
                if(!done) begin
                    currentState <= state_3;
                end
                else begin
                    currentState <= state_4;
                    start <= 0;
                end
            end
            
            state_4 : begin
                if(OM_Address_a < layer1_N-1) begin
//                    if(test_3)begin
//                        OM_Addr_rd <= 1;
//                        currentState <= state_4;
//                    end
//                    else begin
//                        OM_Addr_rd <= 0;
//                        currentState <= state_4;
//                    end
                    OM_Addr_rd <= 1;
                    currentState <= state_4;
                end
                else begin
                    OM_Addr_rd <= 0;
                    if(!OM_Addr_rd_5) begin
                        reset_chip <= 1;
                        currentState <= state_5;
                    end
                    else begin
                        currentState <= state_4;
                    end
                end
            end 
            
            state_5 : begin
                reset_chip <= 0;
                if(OM_read_Address < OM_write_Address-1) begin
                    read_OM2DM <= 1;
                    currentState <= state_5;
                end
                else begin
                    read_OM2DM <= 0;
                    if(!read_OM2DM_3) begin
                        currentState <= state_6;
                    end
                    else begin
                        currentState <= state_5;
                    end
                end
            end
            
            state_6 : begin
                if(WM_PM_Address < (layer1_N*f_1)+No_of_parameter + (layer2_N*f_2)+No_of_parameter - 1) begin
                    read_WM <= 1;
                    currentState <= state_6;
                end
                else begin
                    read_WM <= 0;
                    currentState <= state_7;
                end
            end
            
            state_7 : begin
                start <= 1;
                currentState <= state_8;
            end
            
            state_8 : begin
                if(!done) begin
                    currentState <= state_8;
                end
                else begin
                    currentState <= state_9;
                    start <= 0;
                end
            end
            
            state_9 : begin
                layer2_N_conditional <= layer2_N;
                if(OM_Address_a < layer1_N + layer2_N-1) begin
                    OM_Addr_rd <= 1;
                    currentState <= state_9;
                end
                else begin
                    OM_Addr_rd <= 0;
                    if(!OM_Addr_rd_5) begin
                        reset_chip <= 1;
                        currentState <= state_10;
                    end
                    else begin
                        currentState <= state_9;
                    end
                end
            end
            
            state_10 : begin
                reset_chip <= 0;
                if(OM_read_Address < OM_write_Address-1) begin
                    read_OM2DM <= 1;
                    currentState <= state_10;
                end
                else begin
                    read_OM2DM <= 0;
                    if(!read_OM2DM_3) begin
                        currentState <= state_11;
                    end
                    else begin
                        currentState <= state_10;
                    end
                end
            end
            
            state_11 : begin
                layer2_N_conditional <= 0;
                if(WM_PM_Address < (layer1_N*f_1)+No_of_parameter + (layer2_N*f_2)+No_of_parameter + (layer3_N*f_3)+No_of_parameter - 1) begin
                    read_WM <= 1;
                    currentState <= state_11;
                end
                else begin
                    read_WM <= 0;
                    currentState <= state_12;
                end
            end
            
            state_12 : begin
                start <= 1;
                currentState <= state_13;
            end
            
            state_13 : begin
                if(!done) begin
                    currentState <= state_13;
                end
                else begin
                    currentState <= state_14;
                    start <= 0;
                end
            end
            
            
            state_14 : begin
                if(OM_Address_a < layer1_N + layer2_N + layer3_N-1 ) begin
                    OM_Addr_rd <= 1;
                    currentState <= state_14;
//                    if(test_3)begin
//                        OM_Addr_rd <= 1;
//                        currentState <= state_9;
//                    end
//                    else begin
//                        OM_Addr_rd <= 0;
//                        currentState <= state_9;
//                    end
                end
                else begin
                    OM_Addr_rd <= 0;
                    if(prediction_count >= layer3_N-7) begin
                        currentState <= state_15;
                    end
                    else begin
                        currentState <= state_14;
                    end
                end
            end
            
            state_15 : begin
                currentState <= state_16;
            end
            
            state_16 : begin
                currentState <= state_16;
            end
            
            
        endcase
    end
end


////////////////////7-segment stff///////////////////////////
reg [32-1:0] display;

always@(posedge clk) begin
    if(reset) begin
        display <= 32'b0;  
    end
    else begin
        case(test_input) 
            2'b00 : begin
                display <= {prediction, prediction, prediction, prediction,6'b0,dataAddress_fifo_1};
            end
            2'b01 : begin
                display <= {chip_State,6'b0,currentState,3'b0,6'b0,dataAddress};
            end
            2'b10 : begin
                display <= {12'b0, prediction, 8'b0, last_data}; //parameters[62:47];
            end
            2'b11 : begin
                display <= {8'b0, data_out_1, 6'b0, OM_write_Address}; //parameters[4:0];
            end
            default : begin
                display <= {8{4'he}};
            end
        endcase
    end
end

(*keep = "true"*) reg [15:0] flag;
(*keep = "true"*) reg [2:0] set;

always @(posedge clk) begin
  flag <= flag + 1;
end

always @(posedge clk) begin
     if(flag > 16'd0 && flag < 16'd8192) begin
        set <= 3'd0;
     end
     else if (flag > 16'd8192 && flag < 16'd16384 )  begin
        set <= 3'd1;
     end
     else if(flag > 16'd16384 && flag < 16'd24576)  begin
        set <= 3'd2;
     end 
    else if(flag > 16'd24576 && flag < 16'd32768)  begin
        set <= 3'd3;
     end 
    else if(flag > 16'd32768 && flag < 16'd40960)  begin
        set <= 3'd4;
     end 
    else if(flag > 16'd40960 && flag < 16'd49152)  begin
        set <= 3'd5;
     end 
    else if(flag > 16'd49152 && flag < 16'd57344)  begin
        set <= 3'd6;
     end 
     else begin
        set <= 3'd7;
     end
end


always @(posedge clk) begin
    if(set == 3'b000) begin
          count1 <= display[3:0];
          AN <= 8'b11111110;
    end
    else if(set == 3'b001) begin
          count1 <= display[7:4];
          AN <= 8'b11111101;
    end
    else if(set == 3'b010) begin
          count1 <= display[11:8];
          AN <= 8'b11111011;
    end
    else if(set == 3'b011) begin
          count1 <= display[15:12];
          AN <= 8'b11110111;
    end
    else if(set == 3'b100) begin
          count1 <= display[19:16];
          AN <= 8'b11101111;
    end
    else if(set == 3'b101) begin
          count1 <= display[23:20];
          AN <= 8'b11011111;
    end
    else if(set == 3'b110) begin
          count1 <= display[27:24];
          AN <= 8'b10111111;
    end
    else begin
          count1 <= display[31:28];
          AN <= 8'b01111111;
    end
end


/*DEBUG logic*/
//always@(posedge clk) begin
//    if(reset) begin
//        read_WM <= 0;
//    end
//    else begin
//        if(test_3) begin
//            read_WM <= 1;
//        end
//        else begin
//            read_WM <= 0;
//        end
//    end
//end


//always@(*) begin
//    if(reset) begin
//        data_in = 0;
//        address_in = 0;
//    end
//    else begin
//        if(currentState == state_0) begin
//            data_in = rx_byte;
//            address_in = {{2'b10},{3'b000},dataAddress};
//        end
//        else if(read_WM_1) begin
//            data_in = WM_PM_chip_data;
//            address_in = WM_PM_chip_Address;
//        end
//        else if(OM_Addr_rd_1) begin
//            address_in = OM_address_out;
//            data_in = 0;
//        end
//        else if(read_OM2DM_1) begin
//            data_in = OM_chip_data;
//            address_in = {{2'b10},{3'b000},OM_read_Address_1};
//        end
//        else begin
//            data_in = data_in;
//            address_in = address_in;
//        end
//    end
//end





endmodule

`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06/02/2019 08:32:17 PM
// Design Name: 
// Module Name: rdp_256X32_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module dual_port_memory_tb    #(
                        parameter dataWidth = 8,
                        parameter addrW = 3
    );
    
    reg clkA = 0, clkB = 0;
    reg [dataWidth-1:0] data_inA;
    reg [addrW-1:0] addrA, addrB;
    reg wrA, rdB, reset;
    wire [dataWidth-1:0] data_outB;
    
    integer period = 10;
    
    dual_port_memory #(
                    .dataWidth(dataWidth),
                    .addrW(addrW)
                )uut(
                    .clkA(clkA),
                    .clkB(clkB),
                    .reset(reset),
                    .data_inA(data_inA),
                    .addrA(addrA),
                    .addrB(addrB),
                    .wrA(wrA),
                    .rdB(rdB),
                    .data_outB(data_outB)
                );
    
    initial begin
        forever #(period/2) clkA = ~clkA;
    end
    
    initial begin
        forever #(period/2) clkB = ~clkB;
    end
    
    initial begin
        reset = 1'b1;
        
        #(period);
        reset = 1'b0;
        
        #(5*period);
        wrA = 1'b0;
        rdB = 1'b0;
        
        #(period);
        wrA = 1'b1;
        addrA = 0;
        data_inA = 4;
        
        #(period);
        addrA = 1;
        data_inA = 3;
        
        #(period);
        wrA = 1'b0;
        rdB = 1'b1;
        addrB = 0;
        
        #(period);
        addrB = 1;
    end
endmodule

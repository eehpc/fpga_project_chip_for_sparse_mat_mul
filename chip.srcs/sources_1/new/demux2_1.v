`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06/06/2019 11:59:27 PM
// Design Name: 
// Module Name: demux2_1
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module demux2_1(
                input i,
                input sel,
                output o0,
                output o1
            );
    assign o0 = !sel ? i : 1'b0;
    assign o1 = sel ? i : 1'b0;
endmodule

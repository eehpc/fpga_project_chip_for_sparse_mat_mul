`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: EEHPC
// Engineer: Nitheesh Manjunath
// 
// Create Date: 06/10/2019 06:12:22 PM
// Design Name: 
// Module Name: chip_top
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments: added shift, as dataWidth but goes in 4'b wide. This propogates to MAC unit.
// read_test_1 which is used to read the memories after writing. This is to debug the data.
//////////////////////////////////////////////////////////////////////////////////


module chip_core#(
                parameter dataWidth = 8,
                parameter weight_addrW = 13,
                parameter data_addrW = 10,
                parameter out_addrW = 10,
                parameter paramWidth = 80,
                parameter PE = 16
               )(
                input wire clk,
                input wire reset,
                input wire start,
                input wire write,
                input wire [dataWidth-1:0] data_in,
                input wire [weight_addrW+1:0] address_in,
                input wire cen_OM_rd,
                output reg [dataWidth-1:0] data_out,
                output reg done
                );
    

wire [3:0] en;

//reg [1:0] tmp_en;
(*keep = "true" *)reg [paramWidth-1:0] parameters;
(*keep = "true" *)reg cen_DM_wr, cen_WM_wr, cen_OM_rd_internal, cen_OM_rd_internal1, read_test;
(*keep = "true" *)reg [dataWidth-1:0] data_in_internal, data_in_internal1;
wire [dataWidth-1:0] data_out_internal;
wire done_internal, read_test_1;
(*keep = "true" *)reg [weight_addrW+1:0]address_in_internal, address_in_internal1;
(*keep = "true" *)reg reset_asyn;

PE_wrapper_16_PE_IO_pin_edit #(
                            .dataWidth(dataWidth),
                            .weight_addrW(weight_addrW),
                            .data_addrW(data_addrW),
                            .out_addrW(out_addrW),
                            .PE(PE)
                            )PE_wrapper_16(
                            .clk(clk),
                            .reset(reset_asyn),
                            .N(parameters[paramWidth-1:paramWidth-16]), 
                            .M(parameters[(paramWidth-16)-1:(paramWidth-16*2)]),
                            .f(parameters[(paramWidth-16*2)-1:(paramWidth-16*3)]),
                            .r(parameters[(paramWidth-16*3)-1:(paramWidth-16*4)]),
                            .M_div_N(parameters[(paramWidth-16*4)-1: 8]),
                            .shift(parameters[5-1:0]),
                            .rd_wr(write),
                            .cen_DM(cen_DM_wr),
                            .cen_WM(cen_WM_wr),
                            .read_test(read_test_1),
                            .weightAddr(address_in_internal[weight_addrW-1:0]),
                            .dataAddr(address_in_internal[data_addrW-1:0]),
                            .outAddr(address_in_internal[out_addrW-1:0]),
                            .cen_outMem(cen_OM_rd_internal),
                            .weightMem_in(data_in_internal),
                            .dataMem_in(data_in_internal),
                            .start(start),
                            .outMem_data(data_out_internal),
                            .done(done_internal)
                            );
                            
lvl_1_dec decoder(
           .i(address_in_internal1[weight_addrW+1:weight_addrW]), // address_in
           .en(1'b1),
           .out(en)
           );
           

/*
|address_in_internal[MSB:MSB-1]|4'b en|Signals Enabled|
|        00                    | 0001 |   test_mode   |
|        01                    | 0010 |   cen_WM_wr   |
|        10                    | 0100 |   cen_DM_wr   |
|        11                    | 1000 |   parameters  |
*/   

assign read_test_1 = cen_OM_rd ? 0 : read_test;

always@(posedge clk or negedge reset) begin
    if(!reset) begin
        reset_asyn <= 1;
    end
    else begin
        reset_asyn <= 0;
    end
end


always@(reset_asyn,en) begin
    if(reset_asyn) begin
        cen_DM_wr = 1'b0;
        cen_WM_wr = 1'b0;
        read_test = 1'b0;
    end
    else begin
        case(en)
            4'b1000 : begin
                cen_DM_wr = 1'b0;
                cen_WM_wr = 1'b0;
                read_test = 1'b0;
            end
            4'b0100 : begin
                cen_DM_wr = 1'b1;
                cen_WM_wr = 1'b0;
                read_test = 1'b0;
            end
            4'b0010 : begin
                cen_WM_wr = 1'b1;
                cen_DM_wr = 1'b0;
                read_test = 1'b0;
            end
            4'b0001 : begin
                cen_DM_wr = 1'b0;
                cen_WM_wr = 1'b0;
                read_test = 1'b1;
            end
            
            default : begin
                cen_DM_wr = 1'b0;
                cen_WM_wr = 1'b0;
                read_test = 1'b0;
            end
        endcase
    end
end


always@(posedge clk) begin
    if(reset_asyn) begin
        parameters <= 0;
    end
    else begin
        if(address_in[weight_addrW+1:weight_addrW] == 2'b11) begin
            parameters <= {parameters[(paramWidth-8)-1:0],data_in};
        end
        else begin
            parameters <= parameters;
        end
    end
end

always@(posedge clk) begin
    if(reset_asyn) begin
        data_in_internal <= 0;
        address_in_internal <= 0;
        data_in_internal1 <= 0;
        address_in_internal1 <= 0;
        cen_OM_rd_internal1 <= 0;
        cen_OM_rd_internal <= 0;
        data_out <= 0;
    end
    else begin
        data_in_internal1 <= data_in;
        data_in_internal <= data_in_internal1;
        address_in_internal1 <= address_in;
        address_in_internal <= address_in_internal1;
        cen_OM_rd_internal1 <= cen_OM_rd;
        cen_OM_rd_internal <= cen_OM_rd_internal1;
        data_out <= data_out_internal;
        done <= done_internal;
    end
end

endmodule

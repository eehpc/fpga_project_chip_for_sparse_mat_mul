`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06/12/2019 06:00:44 PM
// Design Name: 
// Module Name: rdp_256X32_edit
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module rdp_256X32_edit  #(
                    parameter dataWidth = 32,
                    parameter addrW = 8
                    )(
                    input wire clk,
                    input wire [dataWidth-1:0] data_inA,
                    input wire [addrW-1:0] addrA, addrB,
                    input wire wrA, rdB,
                    output reg [dataWidth-1:0] data_outB
    );
    
    localparam RAM_DEPTH = 1 << addrW;
    
    reg  [dataWidth-1:0] r_2p [RAM_DEPTH-1:0] ;
    reg [addrW-1:0] addrB_reg;
    reg rdB_reg;
    always@(posedge clk) begin
        addrB_reg <= addrB;
        rdB_reg <= rdB;
    end
    always @(posedge clk) begin: portA
        if(wrA) begin
            r_2p[addrA] <= data_inA;
        end
    end
    
    always @(posedge clk) begin: portB
        if(rdB_reg) begin
            data_outB <= r_2p[addrB_reg];
        end
    end
endmodule


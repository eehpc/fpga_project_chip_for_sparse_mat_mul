`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: EEHPC
// Engineer: Nitheesh Manjunath
// 
// Create Date: 06/06/2019 07:38:01 PM
// Design Name: 
// Module Name: lvl_1_dec
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module lvl_1_dec(
                input wire [1:0] i,
                input wire en,
                
                output wire [3:0]out
    );
    
    wire [1:0] temp;
    
    dec2_1 inst0( 
                .i(i[1]),
                .en(en),
                .out(temp));
                
    dec2_1 inst1(
                .i(i[0]),
                .en(temp[0]),
                .out(out[1:0]));
                
    dec2_1 inst2(
                .i(i[0]),
                .en(temp[1]),
                .out(out[3:2]));
                
endmodule

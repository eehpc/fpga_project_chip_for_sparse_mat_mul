`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: EEHPC-UMBC
// Engineer: Nitheesh Manjunath
// 
// Create Date: 02/10/2020 02:35:48 PM
// Design Name: 
// Module Name: FPGA_TOP_2
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module FPGA_TOP_2#(
        baud_rate = 9600,
        sys_clk_freq = 67200000,
        dataWidth = 8,
        addrW = 15,
        layer1_N = 784,
        f_1 = 8,
        layer2_N = 16,
        f_2 = 784,
        No_of_parameter = 10
        )(
        input clk,
        input reset, 
        input rx,
        input address_in_en,
        input data_in_en,
        input load_WM,
        input read,
        input [1:0] test_input, 
        input [1:0] memory_test,
        input clear_addr_counter,
        input clear_data_counter,
        
        output tx,
        //output done,
        output [1:0] State,
        output is_receiving,
        output is_transmitting,
        output recv_error,
        output error,
        output rd_done,
        
        //7-Segment
        (*keep = "true"*) output reg [7:0] AN,
        output CA,
        output CB,
        output CC,
        output CD,
        output CE,
        output CF,
        output CG,
        
        output reg led0,
        output reg led1,
        output reg led2,
        output reg led3,
        output reg led4,
        output reg led5,
        output reg led6,
        output reg led7,
        output reg led8,
        output reg led9
        );      
    
localparam dataAddrW = 10;
localparam [4-1:0] state_0 = 4'b0000,
                   state_1 = 4'b0001,
                   state_2 = 4'b0010,
                   state_3 = 4'b0011,
                   state_4 = 4'b0100,
                   state_5 = 4'b0101,
                   state_6 = 4'b0110,
                   state_7 = 4'b0111,
                   state_8 = 4'b1000,
                   state_9 = 4'b1001,
                   state_10 = 4'b1010;
//Registers
reg [4-1:0] currentState;
reg read_WM, start, read_WM_1, write, readOM2DM_1, received_1, OM_Addr_rd, OM_Addr_rd_1, OM_Addr_rd_2, OM_Addr_rd_3, OM_Addr_rd_4, OM_Addr_rd_5;
reg [addrW-1:0] WM_PM_Address;
reg [dataAddrW-1:0] dataAddress;
reg [dataWidth-1:0] data_in;
reg [14:0] address_in;
reg [10-1:0] OM_Address_a, OM_write_Address, OM_read_Address, OM_read_Address_1;
reg read_OM2DM, read_OM2DM_1, read_OM2DM_2, read_WM_2;
reg [4-1:0] prediction, prediction_count;
reg [dataWidth-1:0] last_data;
reg reset_chip;
//Wires
wire [14:0] WM_PM_chip_Address;
wire [dataWidth-1:0] WM_PM_chip_data, rx_byte, OM_chip_data;
wire done, received;
wire [1:0] chip_State;
wire [addrW-1:0] OM_address_out;
wire [dataWidth-1:0] data_out;

chip_top_fpga chip(
    .clk(clk),
    .reset(reset | reset_chip),
    .start(start),
    .write(write),
    .data_in(data_in),
    .address_in(address_in),
    .cen_OM_rd(OM_Addr_rd_1 | OM_Addr_rd_2 | OM_Addr_rd_3 | OM_Addr_rd_4),
    .data_out(data_out),
    .currentState(chip_State),
    .done(done)
    );
                                
rdp#(
    .dataWidth(15),
    .addrW(addrW),
    .memFile("wm_addr_test.mem")
    )WM_PM_address(
    .clk(clk),
    .data_inA(),
    .addrA(),
    .addrB(WM_PM_Address), 
    .wrA(1'b0),
    .rdB(read_WM), 
    .data_outB(WM_PM_chip_Address)
    );
                 
rdp#(
    .dataWidth(dataWidth),
    .addrW(addrW),
    .memFile("wm_data_test.mem")
    )WM_PM_data(
    .clk(clk),
    .data_inA(),
    .addrA(),
    .addrB(WM_PM_Address),
    .wrA(1'b0),
    .rdB(read_WM),
    .data_outB(WM_PM_chip_data)
    );
    
rdp#(
    .dataWidth(dataWidth),
    .addrW(10),
    .memFile("")
    )OM(
    .clk(clk),
    .data_inA(data_out),
    .addrA(OM_write_Address),
    .addrB(OM_read_Address),
    .wrA(OM_Addr_rd_5),
    .rdB(read_OM2DM), 
    .data_outB(OM_chip_data)
    );
    
rdp#(
    .dataWidth(15),
    .addrW(10),
    .memFile("om_addr_test.mem")
    )OM_address(
    .clk(clk),
    .data_inA(),
    .addrA(),
    .addrB(OM_Address_a), 
    .wrA(1'b0),
    .rdB(OM_Addr_rd), 
    .data_outB(OM_address_out)
    );
              
uart#(
    .baud_rate(baud_rate),            // default is 9600
    .sys_clk_freq(sys_clk_freq)       // default is 100000000
    )
    uart_instance(
    .clk(clk),                        // The master clock for this module
    .rst(reset),                      // Synchronous reset
    .rx(rx),                          // Incoming serial line
    .tx(tx),                          // Outgoing serial line
    .transmit(),              // Signal to transmit
    .tx_byte(),                // Byte to transmit       
    .received(received),              // Indicated that a byte has been received
    .rx_byte(rx_byte),                // Byte received
    .is_receiving(is_receiving),      // Low when receive line is idle
    .is_transmitting(is_transmitting),// Low when transmit line is idle
    .recv_error(recv_error)           // Indicates error in receiving packet.
    );

//seven_segment seven_segment_inst(
//    .clk(clk),
//    .SW(count1),
//    .CA(CA),
//    .CB(CB),
//    .CC(CC),
//    .CD(CD),
//    .CE(CE),
//    .CF(CF),
//    .CG(CG)
//    );



//Writing to DM on Chip

always@(posedge clk) begin
    if(reset) begin
        dataAddress <= 0;
    end
    else begin
        if(received) begin
            dataAddress <= dataAddress + {{dataAddrW-1{1'b0}}, 1'b1};
        end
        else begin
            dataAddress <= dataAddress;
        end
    end
end

always@(*) begin
    if(reset) begin
        write = 0;
    end
    else begin
        if(read_WM_1 || read_WM_2) begin
            write = 1;
        end
        else if(readOM2DM_1) begin
            write = 1;
        end
        else if(currentState == state_0) begin
            write = received || received_1;
        end
        else if(read_OM2DM_1 || read_OM2DM_2) begin
            write = 1;
        end
        else begin
            write = 0;
        end
    end
end

always@(*) begin
    if(reset) begin
        data_in = 0;
        address_in = 0;
    end
    else begin
        if(currentState == state_0) begin
            data_in = rx_byte;
            address_in = {{2'b10},{3'b000},dataAddress};
        end
        else if(read_WM_1) begin
            data_in = WM_PM_chip_data;
            address_in = WM_PM_chip_Address;
        end
        else if(OM_Addr_rd_1) begin
            address_in = OM_address_out;
        end
        else if(read_OM2DM_1) begin
            data_in = OM_chip_data;
            address_in = {{2'b10},{3'b000},OM_read_Address_1};
        end
    end
end


//Reading from WM
always@(posedge clk) begin
    if(reset) begin
        WM_PM_Address <= 0;
    end
    else begin
        if(read_WM) begin
            WM_PM_Address <= WM_PM_Address + {{addrW-1{1'b0}},1'b1};
        end
        else begin
            WM_PM_Address <= WM_PM_Address;
        end
    end
end

//Reading OM_Address
always@(posedge clk) begin
    if(reset) begin
        OM_Address_a <= 0;
    end
    else begin
        if(OM_Addr_rd) begin
            OM_Address_a <= OM_Address_a + {{10-1{1'b0}},1'b1};
        end
        else begin
            OM_Address_a <= OM_Address_a;
        end
    end 
end

//Pipeline
always@(posedge clk) begin
    if(reset) begin
        read_WM_1 <= 0;
        received_1 <= 0;
        OM_Addr_rd_1 <= 0;
        OM_Addr_rd_2 <= 0;
        OM_Addr_rd_3 <= 0; 
        OM_Addr_rd_4 <= 0;
        OM_Addr_rd_5 <= 0;
        read_OM2DM_1 <= 0;
        read_OM2DM_2 <= 0;
        OM_read_Address_1 <= 0;
        read_WM_2 <= 0;
    end
    else begin
        read_WM_1 <= read_WM;
        read_WM_2 <= read_WM_1;
        received_1 <= received;
        OM_Addr_rd_1 <= OM_Addr_rd;
        OM_Addr_rd_2 <=  OM_Addr_rd_1;
        OM_Addr_rd_3 <= OM_Addr_rd_2;
        OM_Addr_rd_4 <= OM_Addr_rd_3;
        OM_Addr_rd_5 <= OM_Addr_rd_4;
        read_OM2DM_1 <= read_OM2DM;
        read_OM2DM_2 <= read_OM2DM_1;
        OM_read_Address_1 <= OM_read_Address;
    end
end

/*
Writing into OM
*/
always@(posedge clk) begin
    if(reset) begin
        OM_write_Address <= 0;
    end
    else begin
        if(OM_Addr_rd_5) begin
            OM_write_Address <= OM_write_Address + {{10-1{1'b0}},1'b1};
        end
        else begin
            OM_write_Address <= OM_write_Address;
        end
    end
end


/*
Reading from OM
*/
always@(posedge clk) begin
    if(reset) begin
        OM_read_Address <= 0;
    end
    else begin
        if(read_OM2DM) begin
            OM_read_Address <= OM_read_Address + {{10-1{1'b0}}, 1'b1};
        end
        else begin
            OM_read_Address <= OM_read_Address;
        end
    end
end

/*
Evaluating the last layer
*/
always@(posedge clk) begin
    if(reset || done) begin
        prediction_count <= 4'b0;
        last_data <= 0;
        prediction <= 0;
    end
    else begin
        if(OM_Addr_rd_5) begin
            prediction_count <= prediction_count + 1;
            if(data_out >= last_data) begin
                last_data <= data_out;
                prediction <= prediction_count;
            end
            else begin
                last_data <= last_data;
                prediction <= prediction;
            end
        end
    end
end

/*
LED light up after prediciton
*/

always@(posedge clk) begin
    if(reset) begin
        led0 <= 0;
        led1 <= 0;
        led2 <= 0;
        led3 <= 0;
        led4 <= 0;
        led5 <= 0;
        led6 <= 0;
        led7 <= 0;
        led8 <= 0;
        led9 <= 0;
    end
    else begin
        if(currentState == state_10) begin
            case(prediction) 
                4'b0000 : begin
                    led0 <= 1;
                end
                4'b0001 : begin
                    led1 <= 1;
                end
                4'b0010 : begin
                    led2 <= 1;
                end
                4'b0011 : begin
                    led3 <= 1;
                end
                4'b0100 : begin
                    led4 <= 1;
                end
                4'b0101 : begin
                    led5 <= 1;
                end 
                4'b0110 : begin
                    led6 <= 1;
                end
                4'b0111 : begin
                    led7 <= 1;
                end
                4'b1000 : begin
                    led8 <= 1;
                end
                4'b1001 : begin
                    led9 <= 1;
                end
                
                default : begin
                    led0 <= 0;
                    led1 <= 0;
                    led2 <= 0;
                    led3 <= 0;
                    led4 <= 0;
                    led5 <= 0;
                    led6 <= 0;
                    led7 <= 0;
                    led8 <= 0;
                    led9 <= 0;
                end
            endcase
        end
        else begin
            led0 <= 0;
            led1 <= 0;
            led2 <= 0;
            led3 <= 0;
            led4 <= 0;
            led5 <= 0;
            led6 <= 0;
            led7 <= 0;
            led8 <= 0;
            led9 <= 0;
        end
    end
end





always@(posedge clk) begin
    if(reset) begin
        currentState <= state_0;
        read_WM <= 0;
        start <= 0;
        OM_Addr_rd <= 0;
        read_OM2DM <= 0;
        reset_chip <= 1;
    end
    else begin
        case(currentState)
            state_0 :  begin
                read_WM <= 0;
                reset_chip <= 0;
                start <= 0;
                OM_Addr_rd <= 0;
                read_OM2DM <= 0;
                if(load_WM) begin
                    currentState <= state_1;
                end
                else begin
                    currentState <= state_0;
                end        
            end
            
            state_1 : begin
                if(WM_PM_Address < (layer1_N*f_1)+No_of_parameter-1) begin
                    read_WM <= 1;
                    currentState <= state_1;
                end
                else begin
                    read_WM <= 0;
                    currentState <= state_2;
                end
            end
            
            state_2 : begin
                start <= 1;
                currentState <= state_3;
            end
            
            state_3 : begin
                if(!done) begin
                    currentState <= state_3;
                end
                else begin
                    currentState <= state_4;
                    start <= 0;
                end
            end
            
            state_4 : begin
                if(OM_Address_a < layer1_N-1) begin
                    OM_Addr_rd <= 1;
                    currentState <= state_4;
                end
                else begin
                    OM_Addr_rd <= 0;
                    if(!OM_Addr_rd_5) begin
                        reset_chip <= 1;
                        currentState <= state_5;
                    end
                    else begin
                        currentState <= state_4;
                    end
                end
            end 
            
            state_5 : begin
                reset_chip <= 0;
                if(OM_read_Address < OM_write_Address-1) begin
                    read_OM2DM <= 1;
                    currentState <= state_5;
                end
                else begin
                    read_OM2DM <= 0;
                    currentState <= state_6;
                end
            end
            
            state_6 : begin
                if(WM_PM_Address < (layer1_N*f_1)+No_of_parameter + (layer2_N*f_2)+No_of_parameter - 1) begin
                    read_WM <= 1;
                    currentState <= state_6;
                end
                else begin
                    read_WM <= 0;
                    currentState <= state_7;
                end
            end
            
            state_7 : begin
                start <= 1;
                currentState <= state_8;
            end
            
            state_8 : begin
                if(!done) begin
                    currentState <= state_8;
                end
                else begin
                    currentState <= state_9;
                    start <= 0;
                end
            end
            
            state_9 : begin
                if(OM_Address_a < layer1_N + layer2_N-1) begin
                    OM_Addr_rd <= 1;
                    currentState <= state_9;
                end
                else begin
                    OM_Addr_rd <= 0;
                    if(prediction_count >= layer2_N) begin
                        currentState <= state_10;
                    end
                    else begin
                        currentState <= state_9;
                    end
                end
            end
            
            state_10 : begin
                
            end
            
        endcase
    end
end


endmodule

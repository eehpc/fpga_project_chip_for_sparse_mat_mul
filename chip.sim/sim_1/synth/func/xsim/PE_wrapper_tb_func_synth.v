// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
// Date        : Tue Jun  4 13:54:58 2019
// Host        : DESKTOP-NKOTRQL running 64-bit major release  (build 9200)
// Command     : write_verilog -mode funcsim -nolib -force -file
//               C:/Users/nitheesh/Desktop/RA/chip/chip.sim/sim_1/synth/func/xsim/PE_wrapper_tb_func_synth.v
// Design      : PE_wrapper
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7vx485tffg1761-2
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module MAC
   (out,
    \weightMem_out_internal_reg[31] ,
    \dataMem_out_internal_reg[31] ,
    begin_ops_internal2_reg,
    end_ops_internal2_reg,
    reset_IBUF,
    clk_IBUF_BUFG);
  output [31:0]out;
  input [31:0]\weightMem_out_internal_reg[31] ;
  input [31:0]\dataMem_out_internal_reg[31] ;
  input begin_ops_internal2_reg;
  input end_ops_internal2_reg;
  input reset_IBUF;
  input clk_IBUF_BUFG;

  wire \/i__n_0 ;
  wire begin_ops_internal2_reg;
  wire clk_IBUF_BUFG;
  (* RTL_KEEP = "true" *) wire [1:0]currentState;
  wire \currentState[0]_i_1__0_n_0 ;
  wire \currentState[1]_i_1__0_n_0 ;
  wire [31:0]\dataMem_out_internal_reg[31] ;
  wire \dataOut[11]_i_2_n_0 ;
  wire \dataOut[11]_i_3_n_0 ;
  wire \dataOut[11]_i_4_n_0 ;
  wire \dataOut[11]_i_5_n_0 ;
  wire \dataOut[15]_i_2_n_0 ;
  wire \dataOut[15]_i_3_n_0 ;
  wire \dataOut[15]_i_4_n_0 ;
  wire \dataOut[15]_i_5_n_0 ;
  wire \dataOut[19]_i_2_n_0 ;
  wire \dataOut[19]_i_3_n_0 ;
  wire \dataOut[19]_i_4_n_0 ;
  wire \dataOut[19]_i_5_n_0 ;
  wire \dataOut[23]_i_2_n_0 ;
  wire \dataOut[23]_i_3_n_0 ;
  wire \dataOut[23]_i_4_n_0 ;
  wire \dataOut[23]_i_5_n_0 ;
  wire \dataOut[27]_i_2_n_0 ;
  wire \dataOut[27]_i_3_n_0 ;
  wire \dataOut[27]_i_4_n_0 ;
  wire \dataOut[27]_i_5_n_0 ;
  wire \dataOut[31]_i_2_n_0 ;
  wire \dataOut[31]_i_3_n_0 ;
  wire \dataOut[31]_i_4_n_0 ;
  wire \dataOut[31]_i_5_n_0 ;
  wire \dataOut[35]_i_2_n_0 ;
  wire \dataOut[35]_i_3_n_0 ;
  wire \dataOut[35]_i_4_n_0 ;
  wire \dataOut[35]_i_5_n_0 ;
  wire \dataOut[39]_i_2_n_0 ;
  wire \dataOut[39]_i_3_n_0 ;
  wire \dataOut[39]_i_4_n_0 ;
  wire \dataOut[39]_i_5_n_0 ;
  wire \dataOut[3]_i_2_n_0 ;
  wire \dataOut[3]_i_3_n_0 ;
  wire \dataOut[3]_i_4_n_0 ;
  wire \dataOut[3]_i_5_n_0 ;
  wire \dataOut[43]_i_2_n_0 ;
  wire \dataOut[43]_i_3_n_0 ;
  wire \dataOut[43]_i_4_n_0 ;
  wire \dataOut[43]_i_5_n_0 ;
  wire \dataOut[47]_i_2_n_0 ;
  wire \dataOut[47]_i_3_n_0 ;
  wire \dataOut[47]_i_4_n_0 ;
  wire \dataOut[47]_i_5_n_0 ;
  wire \dataOut[51]_i_2_n_0 ;
  wire \dataOut[51]_i_3_n_0 ;
  wire \dataOut[51]_i_4_n_0 ;
  wire \dataOut[51]_i_5_n_0 ;
  wire \dataOut[55]_i_2_n_0 ;
  wire \dataOut[55]_i_3_n_0 ;
  wire \dataOut[55]_i_4_n_0 ;
  wire \dataOut[55]_i_5_n_0 ;
  wire \dataOut[59]_i_2_n_0 ;
  wire \dataOut[59]_i_3_n_0 ;
  wire \dataOut[59]_i_4_n_0 ;
  wire \dataOut[59]_i_5_n_0 ;
  wire \dataOut[63]_i_2_n_0 ;
  wire \dataOut[63]_i_3_n_0 ;
  wire \dataOut[63]_i_4_n_0 ;
  wire \dataOut[63]_i_5_n_0 ;
  wire \dataOut[64]_i_1_n_0 ;
  wire \dataOut[7]_i_2_n_0 ;
  wire \dataOut[7]_i_3_n_0 ;
  wire \dataOut[7]_i_4_n_0 ;
  wire \dataOut[7]_i_5_n_0 ;
  (* RTL_KEEP = "true" *) wire [64:32]dataOut_orig;
  wire \dataOut_reg[11]_i_1_n_0 ;
  wire \dataOut_reg[11]_i_1_n_1 ;
  wire \dataOut_reg[11]_i_1_n_2 ;
  wire \dataOut_reg[11]_i_1_n_3 ;
  wire \dataOut_reg[11]_i_1_n_4 ;
  wire \dataOut_reg[11]_i_1_n_5 ;
  wire \dataOut_reg[11]_i_1_n_6 ;
  wire \dataOut_reg[11]_i_1_n_7 ;
  wire \dataOut_reg[15]_i_1_n_0 ;
  wire \dataOut_reg[15]_i_1_n_1 ;
  wire \dataOut_reg[15]_i_1_n_2 ;
  wire \dataOut_reg[15]_i_1_n_3 ;
  wire \dataOut_reg[15]_i_1_n_4 ;
  wire \dataOut_reg[15]_i_1_n_5 ;
  wire \dataOut_reg[15]_i_1_n_6 ;
  wire \dataOut_reg[15]_i_1_n_7 ;
  wire \dataOut_reg[19]_i_1_n_0 ;
  wire \dataOut_reg[19]_i_1_n_1 ;
  wire \dataOut_reg[19]_i_1_n_2 ;
  wire \dataOut_reg[19]_i_1_n_3 ;
  wire \dataOut_reg[19]_i_1_n_4 ;
  wire \dataOut_reg[19]_i_1_n_5 ;
  wire \dataOut_reg[19]_i_1_n_6 ;
  wire \dataOut_reg[19]_i_1_n_7 ;
  wire \dataOut_reg[23]_i_1_n_0 ;
  wire \dataOut_reg[23]_i_1_n_1 ;
  wire \dataOut_reg[23]_i_1_n_2 ;
  wire \dataOut_reg[23]_i_1_n_3 ;
  wire \dataOut_reg[23]_i_1_n_4 ;
  wire \dataOut_reg[23]_i_1_n_5 ;
  wire \dataOut_reg[23]_i_1_n_6 ;
  wire \dataOut_reg[23]_i_1_n_7 ;
  wire \dataOut_reg[27]_i_1_n_0 ;
  wire \dataOut_reg[27]_i_1_n_1 ;
  wire \dataOut_reg[27]_i_1_n_2 ;
  wire \dataOut_reg[27]_i_1_n_3 ;
  wire \dataOut_reg[27]_i_1_n_4 ;
  wire \dataOut_reg[27]_i_1_n_5 ;
  wire \dataOut_reg[27]_i_1_n_6 ;
  wire \dataOut_reg[27]_i_1_n_7 ;
  wire \dataOut_reg[31]_i_1_n_0 ;
  wire \dataOut_reg[31]_i_1_n_1 ;
  wire \dataOut_reg[31]_i_1_n_2 ;
  wire \dataOut_reg[31]_i_1_n_3 ;
  wire \dataOut_reg[31]_i_1_n_4 ;
  wire \dataOut_reg[31]_i_1_n_5 ;
  wire \dataOut_reg[31]_i_1_n_6 ;
  wire \dataOut_reg[31]_i_1_n_7 ;
  wire \dataOut_reg[35]_i_1_n_0 ;
  wire \dataOut_reg[35]_i_1_n_1 ;
  wire \dataOut_reg[35]_i_1_n_2 ;
  wire \dataOut_reg[35]_i_1_n_3 ;
  wire \dataOut_reg[35]_i_1_n_4 ;
  wire \dataOut_reg[35]_i_1_n_5 ;
  wire \dataOut_reg[35]_i_1_n_6 ;
  wire \dataOut_reg[35]_i_1_n_7 ;
  wire \dataOut_reg[39]_i_1_n_0 ;
  wire \dataOut_reg[39]_i_1_n_1 ;
  wire \dataOut_reg[39]_i_1_n_2 ;
  wire \dataOut_reg[39]_i_1_n_3 ;
  wire \dataOut_reg[39]_i_1_n_4 ;
  wire \dataOut_reg[39]_i_1_n_5 ;
  wire \dataOut_reg[39]_i_1_n_6 ;
  wire \dataOut_reg[39]_i_1_n_7 ;
  wire \dataOut_reg[3]_i_1_n_0 ;
  wire \dataOut_reg[3]_i_1_n_1 ;
  wire \dataOut_reg[3]_i_1_n_2 ;
  wire \dataOut_reg[3]_i_1_n_3 ;
  wire \dataOut_reg[3]_i_1_n_4 ;
  wire \dataOut_reg[3]_i_1_n_5 ;
  wire \dataOut_reg[3]_i_1_n_6 ;
  wire \dataOut_reg[3]_i_1_n_7 ;
  wire \dataOut_reg[43]_i_1_n_0 ;
  wire \dataOut_reg[43]_i_1_n_1 ;
  wire \dataOut_reg[43]_i_1_n_2 ;
  wire \dataOut_reg[43]_i_1_n_3 ;
  wire \dataOut_reg[43]_i_1_n_4 ;
  wire \dataOut_reg[43]_i_1_n_5 ;
  wire \dataOut_reg[43]_i_1_n_6 ;
  wire \dataOut_reg[43]_i_1_n_7 ;
  wire \dataOut_reg[47]_i_1_n_0 ;
  wire \dataOut_reg[47]_i_1_n_1 ;
  wire \dataOut_reg[47]_i_1_n_2 ;
  wire \dataOut_reg[47]_i_1_n_3 ;
  wire \dataOut_reg[47]_i_1_n_4 ;
  wire \dataOut_reg[47]_i_1_n_5 ;
  wire \dataOut_reg[47]_i_1_n_6 ;
  wire \dataOut_reg[47]_i_1_n_7 ;
  wire \dataOut_reg[51]_i_1_n_0 ;
  wire \dataOut_reg[51]_i_1_n_1 ;
  wire \dataOut_reg[51]_i_1_n_2 ;
  wire \dataOut_reg[51]_i_1_n_3 ;
  wire \dataOut_reg[51]_i_1_n_4 ;
  wire \dataOut_reg[51]_i_1_n_5 ;
  wire \dataOut_reg[51]_i_1_n_6 ;
  wire \dataOut_reg[51]_i_1_n_7 ;
  wire \dataOut_reg[55]_i_1_n_0 ;
  wire \dataOut_reg[55]_i_1_n_1 ;
  wire \dataOut_reg[55]_i_1_n_2 ;
  wire \dataOut_reg[55]_i_1_n_3 ;
  wire \dataOut_reg[55]_i_1_n_4 ;
  wire \dataOut_reg[55]_i_1_n_5 ;
  wire \dataOut_reg[55]_i_1_n_6 ;
  wire \dataOut_reg[55]_i_1_n_7 ;
  wire \dataOut_reg[59]_i_1_n_0 ;
  wire \dataOut_reg[59]_i_1_n_1 ;
  wire \dataOut_reg[59]_i_1_n_2 ;
  wire \dataOut_reg[59]_i_1_n_3 ;
  wire \dataOut_reg[59]_i_1_n_4 ;
  wire \dataOut_reg[59]_i_1_n_5 ;
  wire \dataOut_reg[59]_i_1_n_6 ;
  wire \dataOut_reg[59]_i_1_n_7 ;
  wire \dataOut_reg[63]_i_1_n_0 ;
  wire \dataOut_reg[63]_i_1_n_1 ;
  wire \dataOut_reg[63]_i_1_n_2 ;
  wire \dataOut_reg[63]_i_1_n_3 ;
  wire \dataOut_reg[63]_i_1_n_4 ;
  wire \dataOut_reg[63]_i_1_n_5 ;
  wire \dataOut_reg[63]_i_1_n_6 ;
  wire \dataOut_reg[63]_i_1_n_7 ;
  wire \dataOut_reg[64]_i_2_n_7 ;
  wire \dataOut_reg[7]_i_1_n_0 ;
  wire \dataOut_reg[7]_i_1_n_1 ;
  wire \dataOut_reg[7]_i_1_n_2 ;
  wire \dataOut_reg[7]_i_1_n_3 ;
  wire \dataOut_reg[7]_i_1_n_4 ;
  wire \dataOut_reg[7]_i_1_n_5 ;
  wire \dataOut_reg[7]_i_1_n_6 ;
  wire \dataOut_reg[7]_i_1_n_7 ;
  wire end_ops_internal2_reg;
  (* RTL_KEEP = "true" *) wire [31:0]out;
  (* RTL_KEEP = "true" *) wire [63:0]prodVal;
  wire prodVal0__0_n_100;
  wire prodVal0__0_n_101;
  wire prodVal0__0_n_102;
  wire prodVal0__0_n_103;
  wire prodVal0__0_n_104;
  wire prodVal0__0_n_105;
  wire prodVal0__0_n_76;
  wire prodVal0__0_n_77;
  wire prodVal0__0_n_78;
  wire prodVal0__0_n_79;
  wire prodVal0__0_n_80;
  wire prodVal0__0_n_81;
  wire prodVal0__0_n_82;
  wire prodVal0__0_n_83;
  wire prodVal0__0_n_84;
  wire prodVal0__0_n_85;
  wire prodVal0__0_n_86;
  wire prodVal0__0_n_87;
  wire prodVal0__0_n_88;
  wire prodVal0__0_n_89;
  wire prodVal0__0_n_90;
  wire prodVal0__0_n_91;
  wire prodVal0__0_n_92;
  wire prodVal0__0_n_93;
  wire prodVal0__0_n_94;
  wire prodVal0__0_n_95;
  wire prodVal0__0_n_96;
  wire prodVal0__0_n_97;
  wire prodVal0__0_n_98;
  wire prodVal0__0_n_99;
  wire prodVal0__1_n_100;
  wire prodVal0__1_n_101;
  wire prodVal0__1_n_102;
  wire prodVal0__1_n_103;
  wire prodVal0__1_n_104;
  wire prodVal0__1_n_105;
  wire prodVal0__1_n_106;
  wire prodVal0__1_n_107;
  wire prodVal0__1_n_108;
  wire prodVal0__1_n_109;
  wire prodVal0__1_n_110;
  wire prodVal0__1_n_111;
  wire prodVal0__1_n_112;
  wire prodVal0__1_n_113;
  wire prodVal0__1_n_114;
  wire prodVal0__1_n_115;
  wire prodVal0__1_n_116;
  wire prodVal0__1_n_117;
  wire prodVal0__1_n_118;
  wire prodVal0__1_n_119;
  wire prodVal0__1_n_120;
  wire prodVal0__1_n_121;
  wire prodVal0__1_n_122;
  wire prodVal0__1_n_123;
  wire prodVal0__1_n_124;
  wire prodVal0__1_n_125;
  wire prodVal0__1_n_126;
  wire prodVal0__1_n_127;
  wire prodVal0__1_n_128;
  wire prodVal0__1_n_129;
  wire prodVal0__1_n_130;
  wire prodVal0__1_n_131;
  wire prodVal0__1_n_132;
  wire prodVal0__1_n_133;
  wire prodVal0__1_n_134;
  wire prodVal0__1_n_135;
  wire prodVal0__1_n_136;
  wire prodVal0__1_n_137;
  wire prodVal0__1_n_138;
  wire prodVal0__1_n_139;
  wire prodVal0__1_n_140;
  wire prodVal0__1_n_141;
  wire prodVal0__1_n_142;
  wire prodVal0__1_n_143;
  wire prodVal0__1_n_144;
  wire prodVal0__1_n_145;
  wire prodVal0__1_n_146;
  wire prodVal0__1_n_147;
  wire prodVal0__1_n_148;
  wire prodVal0__1_n_149;
  wire prodVal0__1_n_150;
  wire prodVal0__1_n_151;
  wire prodVal0__1_n_152;
  wire prodVal0__1_n_153;
  wire prodVal0__1_n_24;
  wire prodVal0__1_n_25;
  wire prodVal0__1_n_26;
  wire prodVal0__1_n_27;
  wire prodVal0__1_n_28;
  wire prodVal0__1_n_29;
  wire prodVal0__1_n_30;
  wire prodVal0__1_n_31;
  wire prodVal0__1_n_32;
  wire prodVal0__1_n_33;
  wire prodVal0__1_n_34;
  wire prodVal0__1_n_35;
  wire prodVal0__1_n_36;
  wire prodVal0__1_n_37;
  wire prodVal0__1_n_38;
  wire prodVal0__1_n_39;
  wire prodVal0__1_n_40;
  wire prodVal0__1_n_41;
  wire prodVal0__1_n_42;
  wire prodVal0__1_n_43;
  wire prodVal0__1_n_44;
  wire prodVal0__1_n_45;
  wire prodVal0__1_n_46;
  wire prodVal0__1_n_47;
  wire prodVal0__1_n_48;
  wire prodVal0__1_n_49;
  wire prodVal0__1_n_50;
  wire prodVal0__1_n_51;
  wire prodVal0__1_n_52;
  wire prodVal0__1_n_53;
  wire prodVal0__1_n_58;
  wire prodVal0__1_n_59;
  wire prodVal0__1_n_60;
  wire prodVal0__1_n_61;
  wire prodVal0__1_n_62;
  wire prodVal0__1_n_63;
  wire prodVal0__1_n_64;
  wire prodVal0__1_n_65;
  wire prodVal0__1_n_66;
  wire prodVal0__1_n_67;
  wire prodVal0__1_n_68;
  wire prodVal0__1_n_69;
  wire prodVal0__1_n_70;
  wire prodVal0__1_n_71;
  wire prodVal0__1_n_72;
  wire prodVal0__1_n_73;
  wire prodVal0__1_n_74;
  wire prodVal0__1_n_75;
  wire prodVal0__1_n_76;
  wire prodVal0__1_n_77;
  wire prodVal0__1_n_78;
  wire prodVal0__1_n_79;
  wire prodVal0__1_n_80;
  wire prodVal0__1_n_81;
  wire prodVal0__1_n_82;
  wire prodVal0__1_n_83;
  wire prodVal0__1_n_84;
  wire prodVal0__1_n_85;
  wire prodVal0__1_n_86;
  wire prodVal0__1_n_87;
  wire prodVal0__1_n_88;
  wire prodVal0__1_n_89;
  wire prodVal0__1_n_90;
  wire prodVal0__1_n_91;
  wire prodVal0__1_n_92;
  wire prodVal0__1_n_93;
  wire prodVal0__1_n_94;
  wire prodVal0__1_n_95;
  wire prodVal0__1_n_96;
  wire prodVal0__1_n_97;
  wire prodVal0__1_n_98;
  wire prodVal0__1_n_99;
  wire prodVal0__2_n_100;
  wire prodVal0__2_n_101;
  wire prodVal0__2_n_102;
  wire prodVal0__2_n_103;
  wire prodVal0__2_n_104;
  wire prodVal0__2_n_105;
  wire prodVal0__2_n_59;
  wire prodVal0__2_n_60;
  wire prodVal0__2_n_61;
  wire prodVal0__2_n_62;
  wire prodVal0__2_n_63;
  wire prodVal0__2_n_64;
  wire prodVal0__2_n_65;
  wire prodVal0__2_n_66;
  wire prodVal0__2_n_67;
  wire prodVal0__2_n_68;
  wire prodVal0__2_n_69;
  wire prodVal0__2_n_70;
  wire prodVal0__2_n_71;
  wire prodVal0__2_n_72;
  wire prodVal0__2_n_73;
  wire prodVal0__2_n_74;
  wire prodVal0__2_n_75;
  wire prodVal0__2_n_76;
  wire prodVal0__2_n_77;
  wire prodVal0__2_n_78;
  wire prodVal0__2_n_79;
  wire prodVal0__2_n_80;
  wire prodVal0__2_n_81;
  wire prodVal0__2_n_82;
  wire prodVal0__2_n_83;
  wire prodVal0__2_n_84;
  wire prodVal0__2_n_85;
  wire prodVal0__2_n_86;
  wire prodVal0__2_n_87;
  wire prodVal0__2_n_88;
  wire prodVal0__2_n_89;
  wire prodVal0__2_n_90;
  wire prodVal0__2_n_91;
  wire prodVal0__2_n_92;
  wire prodVal0__2_n_93;
  wire prodVal0__2_n_94;
  wire prodVal0__2_n_95;
  wire prodVal0__2_n_96;
  wire prodVal0__2_n_97;
  wire prodVal0__2_n_98;
  wire prodVal0__2_n_99;
  wire [63:16]prodVal0__3;
  wire prodVal0_carry__0_i_1_n_0;
  wire prodVal0_carry__0_i_2_n_0;
  wire prodVal0_carry__0_i_3_n_0;
  wire prodVal0_carry__0_i_4_n_0;
  wire prodVal0_carry__0_n_0;
  wire prodVal0_carry__0_n_1;
  wire prodVal0_carry__0_n_2;
  wire prodVal0_carry__0_n_3;
  wire prodVal0_carry__10_i_1_n_0;
  wire prodVal0_carry__10_i_2_n_0;
  wire prodVal0_carry__10_i_3_n_0;
  wire prodVal0_carry__10_i_4_n_0;
  wire prodVal0_carry__10_n_1;
  wire prodVal0_carry__10_n_2;
  wire prodVal0_carry__10_n_3;
  wire prodVal0_carry__1_i_1_n_0;
  wire prodVal0_carry__1_i_2_n_0;
  wire prodVal0_carry__1_i_3_n_0;
  wire prodVal0_carry__1_i_4_n_0;
  wire prodVal0_carry__1_n_0;
  wire prodVal0_carry__1_n_1;
  wire prodVal0_carry__1_n_2;
  wire prodVal0_carry__1_n_3;
  wire prodVal0_carry__2_i_1_n_0;
  wire prodVal0_carry__2_i_2_n_0;
  wire prodVal0_carry__2_i_3_n_0;
  wire prodVal0_carry__2_i_4_n_0;
  wire prodVal0_carry__2_n_0;
  wire prodVal0_carry__2_n_1;
  wire prodVal0_carry__2_n_2;
  wire prodVal0_carry__2_n_3;
  wire prodVal0_carry__3_i_1_n_0;
  wire prodVal0_carry__3_i_2_n_0;
  wire prodVal0_carry__3_i_3_n_0;
  wire prodVal0_carry__3_i_4_n_0;
  wire prodVal0_carry__3_n_0;
  wire prodVal0_carry__3_n_1;
  wire prodVal0_carry__3_n_2;
  wire prodVal0_carry__3_n_3;
  wire prodVal0_carry__4_i_1_n_0;
  wire prodVal0_carry__4_i_2_n_0;
  wire prodVal0_carry__4_i_3_n_0;
  wire prodVal0_carry__4_i_4_n_0;
  wire prodVal0_carry__4_n_0;
  wire prodVal0_carry__4_n_1;
  wire prodVal0_carry__4_n_2;
  wire prodVal0_carry__4_n_3;
  wire prodVal0_carry__5_i_1_n_0;
  wire prodVal0_carry__5_i_2_n_0;
  wire prodVal0_carry__5_i_3_n_0;
  wire prodVal0_carry__5_i_4_n_0;
  wire prodVal0_carry__5_n_0;
  wire prodVal0_carry__5_n_1;
  wire prodVal0_carry__5_n_2;
  wire prodVal0_carry__5_n_3;
  wire prodVal0_carry__6_i_1_n_0;
  wire prodVal0_carry__6_i_2_n_0;
  wire prodVal0_carry__6_i_3_n_0;
  wire prodVal0_carry__6_i_4_n_0;
  wire prodVal0_carry__6_n_0;
  wire prodVal0_carry__6_n_1;
  wire prodVal0_carry__6_n_2;
  wire prodVal0_carry__6_n_3;
  wire prodVal0_carry__7_i_1_n_0;
  wire prodVal0_carry__7_i_2_n_0;
  wire prodVal0_carry__7_i_3_n_0;
  wire prodVal0_carry__7_i_4_n_0;
  wire prodVal0_carry__7_n_0;
  wire prodVal0_carry__7_n_1;
  wire prodVal0_carry__7_n_2;
  wire prodVal0_carry__7_n_3;
  wire prodVal0_carry__8_i_1_n_0;
  wire prodVal0_carry__8_i_2_n_0;
  wire prodVal0_carry__8_i_3_n_0;
  wire prodVal0_carry__8_i_4_n_0;
  wire prodVal0_carry__8_n_0;
  wire prodVal0_carry__8_n_1;
  wire prodVal0_carry__8_n_2;
  wire prodVal0_carry__8_n_3;
  wire prodVal0_carry__9_i_1_n_0;
  wire prodVal0_carry__9_i_2_n_0;
  wire prodVal0_carry__9_i_3_n_0;
  wire prodVal0_carry__9_i_4_n_0;
  wire prodVal0_carry__9_n_0;
  wire prodVal0_carry__9_n_1;
  wire prodVal0_carry__9_n_2;
  wire prodVal0_carry__9_n_3;
  wire prodVal0_carry_i_1_n_0;
  wire prodVal0_carry_i_2_n_0;
  wire prodVal0_carry_i_3_n_0;
  wire prodVal0_carry_n_0;
  wire prodVal0_carry_n_1;
  wire prodVal0_carry_n_2;
  wire prodVal0_carry_n_3;
  wire prodVal0_n_100;
  wire prodVal0_n_101;
  wire prodVal0_n_102;
  wire prodVal0_n_103;
  wire prodVal0_n_104;
  wire prodVal0_n_105;
  wire prodVal0_n_106;
  wire prodVal0_n_107;
  wire prodVal0_n_108;
  wire prodVal0_n_109;
  wire prodVal0_n_110;
  wire prodVal0_n_111;
  wire prodVal0_n_112;
  wire prodVal0_n_113;
  wire prodVal0_n_114;
  wire prodVal0_n_115;
  wire prodVal0_n_116;
  wire prodVal0_n_117;
  wire prodVal0_n_118;
  wire prodVal0_n_119;
  wire prodVal0_n_120;
  wire prodVal0_n_121;
  wire prodVal0_n_122;
  wire prodVal0_n_123;
  wire prodVal0_n_124;
  wire prodVal0_n_125;
  wire prodVal0_n_126;
  wire prodVal0_n_127;
  wire prodVal0_n_128;
  wire prodVal0_n_129;
  wire prodVal0_n_130;
  wire prodVal0_n_131;
  wire prodVal0_n_132;
  wire prodVal0_n_133;
  wire prodVal0_n_134;
  wire prodVal0_n_135;
  wire prodVal0_n_136;
  wire prodVal0_n_137;
  wire prodVal0_n_138;
  wire prodVal0_n_139;
  wire prodVal0_n_140;
  wire prodVal0_n_141;
  wire prodVal0_n_142;
  wire prodVal0_n_143;
  wire prodVal0_n_144;
  wire prodVal0_n_145;
  wire prodVal0_n_146;
  wire prodVal0_n_147;
  wire prodVal0_n_148;
  wire prodVal0_n_149;
  wire prodVal0_n_150;
  wire prodVal0_n_151;
  wire prodVal0_n_152;
  wire prodVal0_n_153;
  wire prodVal0_n_58;
  wire prodVal0_n_59;
  wire prodVal0_n_60;
  wire prodVal0_n_61;
  wire prodVal0_n_62;
  wire prodVal0_n_63;
  wire prodVal0_n_64;
  wire prodVal0_n_65;
  wire prodVal0_n_66;
  wire prodVal0_n_67;
  wire prodVal0_n_68;
  wire prodVal0_n_69;
  wire prodVal0_n_70;
  wire prodVal0_n_71;
  wire prodVal0_n_72;
  wire prodVal0_n_73;
  wire prodVal0_n_74;
  wire prodVal0_n_75;
  wire prodVal0_n_76;
  wire prodVal0_n_77;
  wire prodVal0_n_78;
  wire prodVal0_n_79;
  wire prodVal0_n_80;
  wire prodVal0_n_81;
  wire prodVal0_n_82;
  wire prodVal0_n_83;
  wire prodVal0_n_84;
  wire prodVal0_n_85;
  wire prodVal0_n_86;
  wire prodVal0_n_87;
  wire prodVal0_n_88;
  wire prodVal0_n_89;
  wire prodVal0_n_90;
  wire prodVal0_n_91;
  wire prodVal0_n_92;
  wire prodVal0_n_93;
  wire prodVal0_n_94;
  wire prodVal0_n_95;
  wire prodVal0_n_96;
  wire prodVal0_n_97;
  wire prodVal0_n_98;
  wire prodVal0_n_99;
  wire \prodVal[0]_i_1_n_0 ;
  wire \prodVal[10]_i_1_n_0 ;
  wire \prodVal[11]_i_1_n_0 ;
  wire \prodVal[12]_i_1_n_0 ;
  wire \prodVal[13]_i_1_n_0 ;
  wire \prodVal[14]_i_1_n_0 ;
  wire \prodVal[15]_i_1_n_0 ;
  wire \prodVal[16]_i_1_n_0 ;
  wire \prodVal[17]_i_1_n_0 ;
  wire \prodVal[18]_i_1_n_0 ;
  wire \prodVal[19]_i_1_n_0 ;
  wire \prodVal[1]_i_1_n_0 ;
  wire \prodVal[20]_i_1_n_0 ;
  wire \prodVal[21]_i_1_n_0 ;
  wire \prodVal[22]_i_1_n_0 ;
  wire \prodVal[23]_i_1_n_0 ;
  wire \prodVal[24]_i_1_n_0 ;
  wire \prodVal[25]_i_1_n_0 ;
  wire \prodVal[26]_i_1_n_0 ;
  wire \prodVal[27]_i_1_n_0 ;
  wire \prodVal[28]_i_1_n_0 ;
  wire \prodVal[29]_i_1_n_0 ;
  wire \prodVal[2]_i_1_n_0 ;
  wire \prodVal[30]_i_1_n_0 ;
  wire \prodVal[31]_i_1_n_0 ;
  wire \prodVal[32]_i_1_n_0 ;
  wire \prodVal[33]_i_1_n_0 ;
  wire \prodVal[34]_i_1_n_0 ;
  wire \prodVal[35]_i_1_n_0 ;
  wire \prodVal[36]_i_1_n_0 ;
  wire \prodVal[37]_i_1_n_0 ;
  wire \prodVal[38]_i_1_n_0 ;
  wire \prodVal[39]_i_1_n_0 ;
  wire \prodVal[3]_i_1_n_0 ;
  wire \prodVal[40]_i_1_n_0 ;
  wire \prodVal[41]_i_1_n_0 ;
  wire \prodVal[42]_i_1_n_0 ;
  wire \prodVal[43]_i_1_n_0 ;
  wire \prodVal[44]_i_1_n_0 ;
  wire \prodVal[45]_i_1_n_0 ;
  wire \prodVal[46]_i_1_n_0 ;
  wire \prodVal[47]_i_1_n_0 ;
  wire \prodVal[48]_i_1_n_0 ;
  wire \prodVal[49]_i_1_n_0 ;
  wire \prodVal[4]_i_1_n_0 ;
  wire \prodVal[50]_i_1_n_0 ;
  wire \prodVal[51]_i_1_n_0 ;
  wire \prodVal[52]_i_1_n_0 ;
  wire \prodVal[53]_i_1_n_0 ;
  wire \prodVal[54]_i_1_n_0 ;
  wire \prodVal[55]_i_1_n_0 ;
  wire \prodVal[56]_i_1_n_0 ;
  wire \prodVal[57]_i_1_n_0 ;
  wire \prodVal[58]_i_1_n_0 ;
  wire \prodVal[59]_i_1_n_0 ;
  wire \prodVal[5]_i_1_n_0 ;
  wire \prodVal[60]_i_1_n_0 ;
  wire \prodVal[61]_i_1_n_0 ;
  wire \prodVal[62]_i_1_n_0 ;
  wire \prodVal[63]_i_1_n_0 ;
  wire \prodVal[6]_i_1_n_0 ;
  wire \prodVal[7]_i_1_n_0 ;
  wire \prodVal[8]_i_1_n_0 ;
  wire \prodVal[9]_i_1_n_0 ;
  wire reset_IBUF;
  (* RTL_KEEP = "true" *) wire [64:0]sumVal;
  wire \sumVal[0]_i_1_n_0 ;
  wire \sumVal[10]_i_1_n_0 ;
  wire \sumVal[11]_i_1_n_0 ;
  wire \sumVal[12]_i_1_n_0 ;
  wire \sumVal[13]_i_1_n_0 ;
  wire \sumVal[14]_i_1_n_0 ;
  wire \sumVal[15]_i_1_n_0 ;
  wire \sumVal[16]_i_1_n_0 ;
  wire \sumVal[17]_i_1_n_0 ;
  wire \sumVal[18]_i_1_n_0 ;
  wire \sumVal[19]_i_1_n_0 ;
  wire \sumVal[1]_i_1_n_0 ;
  wire \sumVal[20]_i_1_n_0 ;
  wire \sumVal[21]_i_1_n_0 ;
  wire \sumVal[22]_i_1_n_0 ;
  wire \sumVal[23]_i_1_n_0 ;
  wire \sumVal[24]_i_1_n_0 ;
  wire \sumVal[25]_i_1_n_0 ;
  wire \sumVal[26]_i_1_n_0 ;
  wire \sumVal[27]_i_1_n_0 ;
  wire \sumVal[28]_i_1_n_0 ;
  wire \sumVal[29]_i_1_n_0 ;
  wire \sumVal[2]_i_1_n_0 ;
  wire \sumVal[30]_i_1_n_0 ;
  wire \sumVal[31]_i_1_n_0 ;
  wire \sumVal[32]_i_1_n_0 ;
  wire \sumVal[33]_i_1_n_0 ;
  wire \sumVal[34]_i_1_n_0 ;
  wire \sumVal[35]_i_1_n_0 ;
  wire \sumVal[36]_i_1_n_0 ;
  wire \sumVal[37]_i_1_n_0 ;
  wire \sumVal[38]_i_1_n_0 ;
  wire \sumVal[39]_i_1_n_0 ;
  wire \sumVal[3]_i_1_n_0 ;
  wire \sumVal[40]_i_1_n_0 ;
  wire \sumVal[41]_i_1_n_0 ;
  wire \sumVal[42]_i_1_n_0 ;
  wire \sumVal[43]_i_1_n_0 ;
  wire \sumVal[44]_i_1_n_0 ;
  wire \sumVal[45]_i_1_n_0 ;
  wire \sumVal[46]_i_1_n_0 ;
  wire \sumVal[47]_i_1_n_0 ;
  wire \sumVal[48]_i_1_n_0 ;
  wire \sumVal[49]_i_1_n_0 ;
  wire \sumVal[4]_i_1_n_0 ;
  wire \sumVal[50]_i_1_n_0 ;
  wire \sumVal[51]_i_1_n_0 ;
  wire \sumVal[52]_i_1_n_0 ;
  wire \sumVal[53]_i_1_n_0 ;
  wire \sumVal[54]_i_1_n_0 ;
  wire \sumVal[55]_i_1_n_0 ;
  wire \sumVal[56]_i_1_n_0 ;
  wire \sumVal[57]_i_1_n_0 ;
  wire \sumVal[58]_i_1_n_0 ;
  wire \sumVal[59]_i_1_n_0 ;
  wire \sumVal[5]_i_1_n_0 ;
  wire \sumVal[60]_i_1_n_0 ;
  wire \sumVal[61]_i_1_n_0 ;
  wire \sumVal[62]_i_1_n_0 ;
  wire \sumVal[63]_i_1_n_0 ;
  wire \sumVal[64]_i_1_n_0 ;
  wire \sumVal[6]_i_1_n_0 ;
  wire \sumVal[7]_i_1_n_0 ;
  wire \sumVal[8]_i_1_n_0 ;
  wire \sumVal[9]_i_1_n_0 ;
  wire [31:0]\weightMem_out_internal_reg[31] ;
  wire [3:0]\NLW_dataOut_reg[64]_i_2_CO_UNCONNECTED ;
  wire [3:1]\NLW_dataOut_reg[64]_i_2_O_UNCONNECTED ;
  wire NLW_prodVal0_CARRYCASCOUT_UNCONNECTED;
  wire NLW_prodVal0_MULTSIGNOUT_UNCONNECTED;
  wire NLW_prodVal0_OVERFLOW_UNCONNECTED;
  wire NLW_prodVal0_PATTERNBDETECT_UNCONNECTED;
  wire NLW_prodVal0_PATTERNDETECT_UNCONNECTED;
  wire NLW_prodVal0_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_prodVal0_ACOUT_UNCONNECTED;
  wire [17:0]NLW_prodVal0_BCOUT_UNCONNECTED;
  wire [3:0]NLW_prodVal0_CARRYOUT_UNCONNECTED;
  wire NLW_prodVal0__0_CARRYCASCOUT_UNCONNECTED;
  wire NLW_prodVal0__0_MULTSIGNOUT_UNCONNECTED;
  wire NLW_prodVal0__0_OVERFLOW_UNCONNECTED;
  wire NLW_prodVal0__0_PATTERNBDETECT_UNCONNECTED;
  wire NLW_prodVal0__0_PATTERNDETECT_UNCONNECTED;
  wire NLW_prodVal0__0_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_prodVal0__0_ACOUT_UNCONNECTED;
  wire [17:0]NLW_prodVal0__0_BCOUT_UNCONNECTED;
  wire [3:0]NLW_prodVal0__0_CARRYOUT_UNCONNECTED;
  wire [47:30]NLW_prodVal0__0_P_UNCONNECTED;
  wire [47:0]NLW_prodVal0__0_PCOUT_UNCONNECTED;
  wire NLW_prodVal0__1_CARRYCASCOUT_UNCONNECTED;
  wire NLW_prodVal0__1_MULTSIGNOUT_UNCONNECTED;
  wire NLW_prodVal0__1_OVERFLOW_UNCONNECTED;
  wire NLW_prodVal0__1_PATTERNBDETECT_UNCONNECTED;
  wire NLW_prodVal0__1_PATTERNDETECT_UNCONNECTED;
  wire NLW_prodVal0__1_UNDERFLOW_UNCONNECTED;
  wire [17:0]NLW_prodVal0__1_BCOUT_UNCONNECTED;
  wire [3:0]NLW_prodVal0__1_CARRYOUT_UNCONNECTED;
  wire NLW_prodVal0__2_CARRYCASCOUT_UNCONNECTED;
  wire NLW_prodVal0__2_MULTSIGNOUT_UNCONNECTED;
  wire NLW_prodVal0__2_OVERFLOW_UNCONNECTED;
  wire NLW_prodVal0__2_PATTERNBDETECT_UNCONNECTED;
  wire NLW_prodVal0__2_PATTERNDETECT_UNCONNECTED;
  wire NLW_prodVal0__2_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_prodVal0__2_ACOUT_UNCONNECTED;
  wire [17:0]NLW_prodVal0__2_BCOUT_UNCONNECTED;
  wire [3:0]NLW_prodVal0__2_CARRYOUT_UNCONNECTED;
  wire [47:47]NLW_prodVal0__2_P_UNCONNECTED;
  wire [47:0]NLW_prodVal0__2_PCOUT_UNCONNECTED;
  wire [3:3]NLW_prodVal0_carry__10_CO_UNCONNECTED;

  LUT2 #(
    .INIT(4'h8)) 
    \/i_ 
       (.I0(end_ops_internal2_reg),
        .I1(currentState[0]),
        .O(\/i__n_0 ));
  LUT3 #(
    .INIT(8'h2E)) 
    \currentState[0]_i_1__0 
       (.I0(begin_ops_internal2_reg),
        .I1(currentState[0]),
        .I2(end_ops_internal2_reg),
        .O(\currentState[0]_i_1__0_n_0 ));
  LUT2 #(
    .INIT(4'h7)) 
    \currentState[1]_i_1__0 
       (.I0(currentState[0]),
        .I1(currentState[1]),
        .O(\currentState[1]_i_1__0_n_0 ));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \currentState_reg[0] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\currentState[0]_i_1__0_n_0 ),
        .Q(currentState[0]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \currentState_reg[1] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\/i__n_0 ),
        .Q(currentState[1]),
        .R(reset_IBUF));
  LUT2 #(
    .INIT(4'h6)) 
    \dataOut[11]_i_2 
       (.I0(sumVal[11]),
        .I1(prodVal[11]),
        .O(\dataOut[11]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \dataOut[11]_i_3 
       (.I0(sumVal[10]),
        .I1(prodVal[10]),
        .O(\dataOut[11]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \dataOut[11]_i_4 
       (.I0(sumVal[9]),
        .I1(prodVal[9]),
        .O(\dataOut[11]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \dataOut[11]_i_5 
       (.I0(sumVal[8]),
        .I1(prodVal[8]),
        .O(\dataOut[11]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \dataOut[15]_i_2 
       (.I0(sumVal[15]),
        .I1(prodVal[15]),
        .O(\dataOut[15]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \dataOut[15]_i_3 
       (.I0(sumVal[14]),
        .I1(prodVal[14]),
        .O(\dataOut[15]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \dataOut[15]_i_4 
       (.I0(sumVal[13]),
        .I1(prodVal[13]),
        .O(\dataOut[15]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \dataOut[15]_i_5 
       (.I0(sumVal[12]),
        .I1(prodVal[12]),
        .O(\dataOut[15]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \dataOut[19]_i_2 
       (.I0(sumVal[19]),
        .I1(prodVal[19]),
        .O(\dataOut[19]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \dataOut[19]_i_3 
       (.I0(sumVal[18]),
        .I1(prodVal[18]),
        .O(\dataOut[19]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \dataOut[19]_i_4 
       (.I0(sumVal[17]),
        .I1(prodVal[17]),
        .O(\dataOut[19]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \dataOut[19]_i_5 
       (.I0(sumVal[16]),
        .I1(prodVal[16]),
        .O(\dataOut[19]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \dataOut[23]_i_2 
       (.I0(sumVal[23]),
        .I1(prodVal[23]),
        .O(\dataOut[23]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \dataOut[23]_i_3 
       (.I0(sumVal[22]),
        .I1(prodVal[22]),
        .O(\dataOut[23]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \dataOut[23]_i_4 
       (.I0(sumVal[21]),
        .I1(prodVal[21]),
        .O(\dataOut[23]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \dataOut[23]_i_5 
       (.I0(sumVal[20]),
        .I1(prodVal[20]),
        .O(\dataOut[23]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \dataOut[27]_i_2 
       (.I0(sumVal[27]),
        .I1(prodVal[27]),
        .O(\dataOut[27]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \dataOut[27]_i_3 
       (.I0(sumVal[26]),
        .I1(prodVal[26]),
        .O(\dataOut[27]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \dataOut[27]_i_4 
       (.I0(sumVal[25]),
        .I1(prodVal[25]),
        .O(\dataOut[27]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \dataOut[27]_i_5 
       (.I0(sumVal[24]),
        .I1(prodVal[24]),
        .O(\dataOut[27]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \dataOut[31]_i_2 
       (.I0(sumVal[31]),
        .I1(prodVal[31]),
        .O(\dataOut[31]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \dataOut[31]_i_3 
       (.I0(sumVal[30]),
        .I1(prodVal[30]),
        .O(\dataOut[31]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \dataOut[31]_i_4 
       (.I0(sumVal[29]),
        .I1(prodVal[29]),
        .O(\dataOut[31]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \dataOut[31]_i_5 
       (.I0(sumVal[28]),
        .I1(prodVal[28]),
        .O(\dataOut[31]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \dataOut[35]_i_2 
       (.I0(sumVal[35]),
        .I1(prodVal[35]),
        .O(\dataOut[35]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \dataOut[35]_i_3 
       (.I0(sumVal[34]),
        .I1(prodVal[34]),
        .O(\dataOut[35]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \dataOut[35]_i_4 
       (.I0(sumVal[33]),
        .I1(prodVal[33]),
        .O(\dataOut[35]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \dataOut[35]_i_5 
       (.I0(sumVal[32]),
        .I1(prodVal[32]),
        .O(\dataOut[35]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \dataOut[39]_i_2 
       (.I0(sumVal[39]),
        .I1(prodVal[39]),
        .O(\dataOut[39]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \dataOut[39]_i_3 
       (.I0(sumVal[38]),
        .I1(prodVal[38]),
        .O(\dataOut[39]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \dataOut[39]_i_4 
       (.I0(sumVal[37]),
        .I1(prodVal[37]),
        .O(\dataOut[39]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \dataOut[39]_i_5 
       (.I0(sumVal[36]),
        .I1(prodVal[36]),
        .O(\dataOut[39]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \dataOut[3]_i_2 
       (.I0(sumVal[3]),
        .I1(prodVal[3]),
        .O(\dataOut[3]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \dataOut[3]_i_3 
       (.I0(sumVal[2]),
        .I1(prodVal[2]),
        .O(\dataOut[3]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \dataOut[3]_i_4 
       (.I0(sumVal[1]),
        .I1(prodVal[1]),
        .O(\dataOut[3]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \dataOut[3]_i_5 
       (.I0(sumVal[0]),
        .I1(prodVal[0]),
        .O(\dataOut[3]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \dataOut[43]_i_2 
       (.I0(sumVal[43]),
        .I1(prodVal[43]),
        .O(\dataOut[43]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \dataOut[43]_i_3 
       (.I0(sumVal[42]),
        .I1(prodVal[42]),
        .O(\dataOut[43]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \dataOut[43]_i_4 
       (.I0(sumVal[41]),
        .I1(prodVal[41]),
        .O(\dataOut[43]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \dataOut[43]_i_5 
       (.I0(sumVal[40]),
        .I1(prodVal[40]),
        .O(\dataOut[43]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \dataOut[47]_i_2 
       (.I0(sumVal[47]),
        .I1(prodVal[47]),
        .O(\dataOut[47]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \dataOut[47]_i_3 
       (.I0(sumVal[46]),
        .I1(prodVal[46]),
        .O(\dataOut[47]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \dataOut[47]_i_4 
       (.I0(sumVal[45]),
        .I1(prodVal[45]),
        .O(\dataOut[47]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \dataOut[47]_i_5 
       (.I0(sumVal[44]),
        .I1(prodVal[44]),
        .O(\dataOut[47]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \dataOut[51]_i_2 
       (.I0(sumVal[51]),
        .I1(prodVal[51]),
        .O(\dataOut[51]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \dataOut[51]_i_3 
       (.I0(sumVal[50]),
        .I1(prodVal[50]),
        .O(\dataOut[51]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \dataOut[51]_i_4 
       (.I0(sumVal[49]),
        .I1(prodVal[49]),
        .O(\dataOut[51]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \dataOut[51]_i_5 
       (.I0(sumVal[48]),
        .I1(prodVal[48]),
        .O(\dataOut[51]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \dataOut[55]_i_2 
       (.I0(sumVal[55]),
        .I1(prodVal[55]),
        .O(\dataOut[55]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \dataOut[55]_i_3 
       (.I0(sumVal[54]),
        .I1(prodVal[54]),
        .O(\dataOut[55]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \dataOut[55]_i_4 
       (.I0(sumVal[53]),
        .I1(prodVal[53]),
        .O(\dataOut[55]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \dataOut[55]_i_5 
       (.I0(sumVal[52]),
        .I1(prodVal[52]),
        .O(\dataOut[55]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \dataOut[59]_i_2 
       (.I0(sumVal[59]),
        .I1(prodVal[59]),
        .O(\dataOut[59]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \dataOut[59]_i_3 
       (.I0(sumVal[58]),
        .I1(prodVal[58]),
        .O(\dataOut[59]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \dataOut[59]_i_4 
       (.I0(sumVal[57]),
        .I1(prodVal[57]),
        .O(\dataOut[59]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \dataOut[59]_i_5 
       (.I0(sumVal[56]),
        .I1(prodVal[56]),
        .O(\dataOut[59]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \dataOut[63]_i_2 
       (.I0(sumVal[63]),
        .I1(prodVal[63]),
        .O(\dataOut[63]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \dataOut[63]_i_3 
       (.I0(sumVal[62]),
        .I1(prodVal[62]),
        .O(\dataOut[63]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \dataOut[63]_i_4 
       (.I0(sumVal[61]),
        .I1(prodVal[61]),
        .O(\dataOut[63]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \dataOut[63]_i_5 
       (.I0(sumVal[60]),
        .I1(prodVal[60]),
        .O(\dataOut[63]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \dataOut[64]_i_1 
       (.I0(currentState[1]),
        .I1(currentState[0]),
        .O(\dataOut[64]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \dataOut[7]_i_2 
       (.I0(sumVal[7]),
        .I1(prodVal[7]),
        .O(\dataOut[7]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \dataOut[7]_i_3 
       (.I0(sumVal[6]),
        .I1(prodVal[6]),
        .O(\dataOut[7]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \dataOut[7]_i_4 
       (.I0(sumVal[5]),
        .I1(prodVal[5]),
        .O(\dataOut[7]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \dataOut[7]_i_5 
       (.I0(sumVal[4]),
        .I1(prodVal[4]),
        .O(\dataOut[7]_i_5_n_0 ));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataOut_reg[0] 
       (.C(clk_IBUF_BUFG),
        .CE(\dataOut[64]_i_1_n_0 ),
        .D(\dataOut_reg[3]_i_1_n_7 ),
        .Q(out[0]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataOut_reg[10] 
       (.C(clk_IBUF_BUFG),
        .CE(\dataOut[64]_i_1_n_0 ),
        .D(\dataOut_reg[11]_i_1_n_5 ),
        .Q(out[10]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataOut_reg[11] 
       (.C(clk_IBUF_BUFG),
        .CE(\dataOut[64]_i_1_n_0 ),
        .D(\dataOut_reg[11]_i_1_n_4 ),
        .Q(out[11]),
        .R(reset_IBUF));
  CARRY4 \dataOut_reg[11]_i_1 
       (.CI(\dataOut_reg[7]_i_1_n_0 ),
        .CO({\dataOut_reg[11]_i_1_n_0 ,\dataOut_reg[11]_i_1_n_1 ,\dataOut_reg[11]_i_1_n_2 ,\dataOut_reg[11]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(sumVal[11:8]),
        .O({\dataOut_reg[11]_i_1_n_4 ,\dataOut_reg[11]_i_1_n_5 ,\dataOut_reg[11]_i_1_n_6 ,\dataOut_reg[11]_i_1_n_7 }),
        .S({\dataOut[11]_i_2_n_0 ,\dataOut[11]_i_3_n_0 ,\dataOut[11]_i_4_n_0 ,\dataOut[11]_i_5_n_0 }));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataOut_reg[12] 
       (.C(clk_IBUF_BUFG),
        .CE(\dataOut[64]_i_1_n_0 ),
        .D(\dataOut_reg[15]_i_1_n_7 ),
        .Q(out[12]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataOut_reg[13] 
       (.C(clk_IBUF_BUFG),
        .CE(\dataOut[64]_i_1_n_0 ),
        .D(\dataOut_reg[15]_i_1_n_6 ),
        .Q(out[13]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataOut_reg[14] 
       (.C(clk_IBUF_BUFG),
        .CE(\dataOut[64]_i_1_n_0 ),
        .D(\dataOut_reg[15]_i_1_n_5 ),
        .Q(out[14]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataOut_reg[15] 
       (.C(clk_IBUF_BUFG),
        .CE(\dataOut[64]_i_1_n_0 ),
        .D(\dataOut_reg[15]_i_1_n_4 ),
        .Q(out[15]),
        .R(reset_IBUF));
  CARRY4 \dataOut_reg[15]_i_1 
       (.CI(\dataOut_reg[11]_i_1_n_0 ),
        .CO({\dataOut_reg[15]_i_1_n_0 ,\dataOut_reg[15]_i_1_n_1 ,\dataOut_reg[15]_i_1_n_2 ,\dataOut_reg[15]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(sumVal[15:12]),
        .O({\dataOut_reg[15]_i_1_n_4 ,\dataOut_reg[15]_i_1_n_5 ,\dataOut_reg[15]_i_1_n_6 ,\dataOut_reg[15]_i_1_n_7 }),
        .S({\dataOut[15]_i_2_n_0 ,\dataOut[15]_i_3_n_0 ,\dataOut[15]_i_4_n_0 ,\dataOut[15]_i_5_n_0 }));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataOut_reg[16] 
       (.C(clk_IBUF_BUFG),
        .CE(\dataOut[64]_i_1_n_0 ),
        .D(\dataOut_reg[19]_i_1_n_7 ),
        .Q(out[16]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataOut_reg[17] 
       (.C(clk_IBUF_BUFG),
        .CE(\dataOut[64]_i_1_n_0 ),
        .D(\dataOut_reg[19]_i_1_n_6 ),
        .Q(out[17]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataOut_reg[18] 
       (.C(clk_IBUF_BUFG),
        .CE(\dataOut[64]_i_1_n_0 ),
        .D(\dataOut_reg[19]_i_1_n_5 ),
        .Q(out[18]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataOut_reg[19] 
       (.C(clk_IBUF_BUFG),
        .CE(\dataOut[64]_i_1_n_0 ),
        .D(\dataOut_reg[19]_i_1_n_4 ),
        .Q(out[19]),
        .R(reset_IBUF));
  CARRY4 \dataOut_reg[19]_i_1 
       (.CI(\dataOut_reg[15]_i_1_n_0 ),
        .CO({\dataOut_reg[19]_i_1_n_0 ,\dataOut_reg[19]_i_1_n_1 ,\dataOut_reg[19]_i_1_n_2 ,\dataOut_reg[19]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(sumVal[19:16]),
        .O({\dataOut_reg[19]_i_1_n_4 ,\dataOut_reg[19]_i_1_n_5 ,\dataOut_reg[19]_i_1_n_6 ,\dataOut_reg[19]_i_1_n_7 }),
        .S({\dataOut[19]_i_2_n_0 ,\dataOut[19]_i_3_n_0 ,\dataOut[19]_i_4_n_0 ,\dataOut[19]_i_5_n_0 }));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataOut_reg[1] 
       (.C(clk_IBUF_BUFG),
        .CE(\dataOut[64]_i_1_n_0 ),
        .D(\dataOut_reg[3]_i_1_n_6 ),
        .Q(out[1]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataOut_reg[20] 
       (.C(clk_IBUF_BUFG),
        .CE(\dataOut[64]_i_1_n_0 ),
        .D(\dataOut_reg[23]_i_1_n_7 ),
        .Q(out[20]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataOut_reg[21] 
       (.C(clk_IBUF_BUFG),
        .CE(\dataOut[64]_i_1_n_0 ),
        .D(\dataOut_reg[23]_i_1_n_6 ),
        .Q(out[21]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataOut_reg[22] 
       (.C(clk_IBUF_BUFG),
        .CE(\dataOut[64]_i_1_n_0 ),
        .D(\dataOut_reg[23]_i_1_n_5 ),
        .Q(out[22]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataOut_reg[23] 
       (.C(clk_IBUF_BUFG),
        .CE(\dataOut[64]_i_1_n_0 ),
        .D(\dataOut_reg[23]_i_1_n_4 ),
        .Q(out[23]),
        .R(reset_IBUF));
  CARRY4 \dataOut_reg[23]_i_1 
       (.CI(\dataOut_reg[19]_i_1_n_0 ),
        .CO({\dataOut_reg[23]_i_1_n_0 ,\dataOut_reg[23]_i_1_n_1 ,\dataOut_reg[23]_i_1_n_2 ,\dataOut_reg[23]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(sumVal[23:20]),
        .O({\dataOut_reg[23]_i_1_n_4 ,\dataOut_reg[23]_i_1_n_5 ,\dataOut_reg[23]_i_1_n_6 ,\dataOut_reg[23]_i_1_n_7 }),
        .S({\dataOut[23]_i_2_n_0 ,\dataOut[23]_i_3_n_0 ,\dataOut[23]_i_4_n_0 ,\dataOut[23]_i_5_n_0 }));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataOut_reg[24] 
       (.C(clk_IBUF_BUFG),
        .CE(\dataOut[64]_i_1_n_0 ),
        .D(\dataOut_reg[27]_i_1_n_7 ),
        .Q(out[24]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataOut_reg[25] 
       (.C(clk_IBUF_BUFG),
        .CE(\dataOut[64]_i_1_n_0 ),
        .D(\dataOut_reg[27]_i_1_n_6 ),
        .Q(out[25]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataOut_reg[26] 
       (.C(clk_IBUF_BUFG),
        .CE(\dataOut[64]_i_1_n_0 ),
        .D(\dataOut_reg[27]_i_1_n_5 ),
        .Q(out[26]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataOut_reg[27] 
       (.C(clk_IBUF_BUFG),
        .CE(\dataOut[64]_i_1_n_0 ),
        .D(\dataOut_reg[27]_i_1_n_4 ),
        .Q(out[27]),
        .R(reset_IBUF));
  CARRY4 \dataOut_reg[27]_i_1 
       (.CI(\dataOut_reg[23]_i_1_n_0 ),
        .CO({\dataOut_reg[27]_i_1_n_0 ,\dataOut_reg[27]_i_1_n_1 ,\dataOut_reg[27]_i_1_n_2 ,\dataOut_reg[27]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(sumVal[27:24]),
        .O({\dataOut_reg[27]_i_1_n_4 ,\dataOut_reg[27]_i_1_n_5 ,\dataOut_reg[27]_i_1_n_6 ,\dataOut_reg[27]_i_1_n_7 }),
        .S({\dataOut[27]_i_2_n_0 ,\dataOut[27]_i_3_n_0 ,\dataOut[27]_i_4_n_0 ,\dataOut[27]_i_5_n_0 }));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataOut_reg[28] 
       (.C(clk_IBUF_BUFG),
        .CE(\dataOut[64]_i_1_n_0 ),
        .D(\dataOut_reg[31]_i_1_n_7 ),
        .Q(out[28]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataOut_reg[29] 
       (.C(clk_IBUF_BUFG),
        .CE(\dataOut[64]_i_1_n_0 ),
        .D(\dataOut_reg[31]_i_1_n_6 ),
        .Q(out[29]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataOut_reg[2] 
       (.C(clk_IBUF_BUFG),
        .CE(\dataOut[64]_i_1_n_0 ),
        .D(\dataOut_reg[3]_i_1_n_5 ),
        .Q(out[2]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataOut_reg[30] 
       (.C(clk_IBUF_BUFG),
        .CE(\dataOut[64]_i_1_n_0 ),
        .D(\dataOut_reg[31]_i_1_n_5 ),
        .Q(out[30]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataOut_reg[31] 
       (.C(clk_IBUF_BUFG),
        .CE(\dataOut[64]_i_1_n_0 ),
        .D(\dataOut_reg[31]_i_1_n_4 ),
        .Q(out[31]),
        .R(reset_IBUF));
  CARRY4 \dataOut_reg[31]_i_1 
       (.CI(\dataOut_reg[27]_i_1_n_0 ),
        .CO({\dataOut_reg[31]_i_1_n_0 ,\dataOut_reg[31]_i_1_n_1 ,\dataOut_reg[31]_i_1_n_2 ,\dataOut_reg[31]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(sumVal[31:28]),
        .O({\dataOut_reg[31]_i_1_n_4 ,\dataOut_reg[31]_i_1_n_5 ,\dataOut_reg[31]_i_1_n_6 ,\dataOut_reg[31]_i_1_n_7 }),
        .S({\dataOut[31]_i_2_n_0 ,\dataOut[31]_i_3_n_0 ,\dataOut[31]_i_4_n_0 ,\dataOut[31]_i_5_n_0 }));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataOut_reg[32] 
       (.C(clk_IBUF_BUFG),
        .CE(\dataOut[64]_i_1_n_0 ),
        .D(\dataOut_reg[35]_i_1_n_7 ),
        .Q(dataOut_orig[32]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataOut_reg[33] 
       (.C(clk_IBUF_BUFG),
        .CE(\dataOut[64]_i_1_n_0 ),
        .D(\dataOut_reg[35]_i_1_n_6 ),
        .Q(dataOut_orig[33]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataOut_reg[34] 
       (.C(clk_IBUF_BUFG),
        .CE(\dataOut[64]_i_1_n_0 ),
        .D(\dataOut_reg[35]_i_1_n_5 ),
        .Q(dataOut_orig[34]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataOut_reg[35] 
       (.C(clk_IBUF_BUFG),
        .CE(\dataOut[64]_i_1_n_0 ),
        .D(\dataOut_reg[35]_i_1_n_4 ),
        .Q(dataOut_orig[35]),
        .R(reset_IBUF));
  CARRY4 \dataOut_reg[35]_i_1 
       (.CI(\dataOut_reg[31]_i_1_n_0 ),
        .CO({\dataOut_reg[35]_i_1_n_0 ,\dataOut_reg[35]_i_1_n_1 ,\dataOut_reg[35]_i_1_n_2 ,\dataOut_reg[35]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(sumVal[35:32]),
        .O({\dataOut_reg[35]_i_1_n_4 ,\dataOut_reg[35]_i_1_n_5 ,\dataOut_reg[35]_i_1_n_6 ,\dataOut_reg[35]_i_1_n_7 }),
        .S({\dataOut[35]_i_2_n_0 ,\dataOut[35]_i_3_n_0 ,\dataOut[35]_i_4_n_0 ,\dataOut[35]_i_5_n_0 }));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataOut_reg[36] 
       (.C(clk_IBUF_BUFG),
        .CE(\dataOut[64]_i_1_n_0 ),
        .D(\dataOut_reg[39]_i_1_n_7 ),
        .Q(dataOut_orig[36]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataOut_reg[37] 
       (.C(clk_IBUF_BUFG),
        .CE(\dataOut[64]_i_1_n_0 ),
        .D(\dataOut_reg[39]_i_1_n_6 ),
        .Q(dataOut_orig[37]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataOut_reg[38] 
       (.C(clk_IBUF_BUFG),
        .CE(\dataOut[64]_i_1_n_0 ),
        .D(\dataOut_reg[39]_i_1_n_5 ),
        .Q(dataOut_orig[38]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataOut_reg[39] 
       (.C(clk_IBUF_BUFG),
        .CE(\dataOut[64]_i_1_n_0 ),
        .D(\dataOut_reg[39]_i_1_n_4 ),
        .Q(dataOut_orig[39]),
        .R(reset_IBUF));
  CARRY4 \dataOut_reg[39]_i_1 
       (.CI(\dataOut_reg[35]_i_1_n_0 ),
        .CO({\dataOut_reg[39]_i_1_n_0 ,\dataOut_reg[39]_i_1_n_1 ,\dataOut_reg[39]_i_1_n_2 ,\dataOut_reg[39]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(sumVal[39:36]),
        .O({\dataOut_reg[39]_i_1_n_4 ,\dataOut_reg[39]_i_1_n_5 ,\dataOut_reg[39]_i_1_n_6 ,\dataOut_reg[39]_i_1_n_7 }),
        .S({\dataOut[39]_i_2_n_0 ,\dataOut[39]_i_3_n_0 ,\dataOut[39]_i_4_n_0 ,\dataOut[39]_i_5_n_0 }));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataOut_reg[3] 
       (.C(clk_IBUF_BUFG),
        .CE(\dataOut[64]_i_1_n_0 ),
        .D(\dataOut_reg[3]_i_1_n_4 ),
        .Q(out[3]),
        .R(reset_IBUF));
  CARRY4 \dataOut_reg[3]_i_1 
       (.CI(1'b0),
        .CO({\dataOut_reg[3]_i_1_n_0 ,\dataOut_reg[3]_i_1_n_1 ,\dataOut_reg[3]_i_1_n_2 ,\dataOut_reg[3]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(sumVal[3:0]),
        .O({\dataOut_reg[3]_i_1_n_4 ,\dataOut_reg[3]_i_1_n_5 ,\dataOut_reg[3]_i_1_n_6 ,\dataOut_reg[3]_i_1_n_7 }),
        .S({\dataOut[3]_i_2_n_0 ,\dataOut[3]_i_3_n_0 ,\dataOut[3]_i_4_n_0 ,\dataOut[3]_i_5_n_0 }));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataOut_reg[40] 
       (.C(clk_IBUF_BUFG),
        .CE(\dataOut[64]_i_1_n_0 ),
        .D(\dataOut_reg[43]_i_1_n_7 ),
        .Q(dataOut_orig[40]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataOut_reg[41] 
       (.C(clk_IBUF_BUFG),
        .CE(\dataOut[64]_i_1_n_0 ),
        .D(\dataOut_reg[43]_i_1_n_6 ),
        .Q(dataOut_orig[41]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataOut_reg[42] 
       (.C(clk_IBUF_BUFG),
        .CE(\dataOut[64]_i_1_n_0 ),
        .D(\dataOut_reg[43]_i_1_n_5 ),
        .Q(dataOut_orig[42]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataOut_reg[43] 
       (.C(clk_IBUF_BUFG),
        .CE(\dataOut[64]_i_1_n_0 ),
        .D(\dataOut_reg[43]_i_1_n_4 ),
        .Q(dataOut_orig[43]),
        .R(reset_IBUF));
  CARRY4 \dataOut_reg[43]_i_1 
       (.CI(\dataOut_reg[39]_i_1_n_0 ),
        .CO({\dataOut_reg[43]_i_1_n_0 ,\dataOut_reg[43]_i_1_n_1 ,\dataOut_reg[43]_i_1_n_2 ,\dataOut_reg[43]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(sumVal[43:40]),
        .O({\dataOut_reg[43]_i_1_n_4 ,\dataOut_reg[43]_i_1_n_5 ,\dataOut_reg[43]_i_1_n_6 ,\dataOut_reg[43]_i_1_n_7 }),
        .S({\dataOut[43]_i_2_n_0 ,\dataOut[43]_i_3_n_0 ,\dataOut[43]_i_4_n_0 ,\dataOut[43]_i_5_n_0 }));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataOut_reg[44] 
       (.C(clk_IBUF_BUFG),
        .CE(\dataOut[64]_i_1_n_0 ),
        .D(\dataOut_reg[47]_i_1_n_7 ),
        .Q(dataOut_orig[44]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataOut_reg[45] 
       (.C(clk_IBUF_BUFG),
        .CE(\dataOut[64]_i_1_n_0 ),
        .D(\dataOut_reg[47]_i_1_n_6 ),
        .Q(dataOut_orig[45]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataOut_reg[46] 
       (.C(clk_IBUF_BUFG),
        .CE(\dataOut[64]_i_1_n_0 ),
        .D(\dataOut_reg[47]_i_1_n_5 ),
        .Q(dataOut_orig[46]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataOut_reg[47] 
       (.C(clk_IBUF_BUFG),
        .CE(\dataOut[64]_i_1_n_0 ),
        .D(\dataOut_reg[47]_i_1_n_4 ),
        .Q(dataOut_orig[47]),
        .R(reset_IBUF));
  CARRY4 \dataOut_reg[47]_i_1 
       (.CI(\dataOut_reg[43]_i_1_n_0 ),
        .CO({\dataOut_reg[47]_i_1_n_0 ,\dataOut_reg[47]_i_1_n_1 ,\dataOut_reg[47]_i_1_n_2 ,\dataOut_reg[47]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(sumVal[47:44]),
        .O({\dataOut_reg[47]_i_1_n_4 ,\dataOut_reg[47]_i_1_n_5 ,\dataOut_reg[47]_i_1_n_6 ,\dataOut_reg[47]_i_1_n_7 }),
        .S({\dataOut[47]_i_2_n_0 ,\dataOut[47]_i_3_n_0 ,\dataOut[47]_i_4_n_0 ,\dataOut[47]_i_5_n_0 }));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataOut_reg[48] 
       (.C(clk_IBUF_BUFG),
        .CE(\dataOut[64]_i_1_n_0 ),
        .D(\dataOut_reg[51]_i_1_n_7 ),
        .Q(dataOut_orig[48]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataOut_reg[49] 
       (.C(clk_IBUF_BUFG),
        .CE(\dataOut[64]_i_1_n_0 ),
        .D(\dataOut_reg[51]_i_1_n_6 ),
        .Q(dataOut_orig[49]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataOut_reg[4] 
       (.C(clk_IBUF_BUFG),
        .CE(\dataOut[64]_i_1_n_0 ),
        .D(\dataOut_reg[7]_i_1_n_7 ),
        .Q(out[4]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataOut_reg[50] 
       (.C(clk_IBUF_BUFG),
        .CE(\dataOut[64]_i_1_n_0 ),
        .D(\dataOut_reg[51]_i_1_n_5 ),
        .Q(dataOut_orig[50]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataOut_reg[51] 
       (.C(clk_IBUF_BUFG),
        .CE(\dataOut[64]_i_1_n_0 ),
        .D(\dataOut_reg[51]_i_1_n_4 ),
        .Q(dataOut_orig[51]),
        .R(reset_IBUF));
  CARRY4 \dataOut_reg[51]_i_1 
       (.CI(\dataOut_reg[47]_i_1_n_0 ),
        .CO({\dataOut_reg[51]_i_1_n_0 ,\dataOut_reg[51]_i_1_n_1 ,\dataOut_reg[51]_i_1_n_2 ,\dataOut_reg[51]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(sumVal[51:48]),
        .O({\dataOut_reg[51]_i_1_n_4 ,\dataOut_reg[51]_i_1_n_5 ,\dataOut_reg[51]_i_1_n_6 ,\dataOut_reg[51]_i_1_n_7 }),
        .S({\dataOut[51]_i_2_n_0 ,\dataOut[51]_i_3_n_0 ,\dataOut[51]_i_4_n_0 ,\dataOut[51]_i_5_n_0 }));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataOut_reg[52] 
       (.C(clk_IBUF_BUFG),
        .CE(\dataOut[64]_i_1_n_0 ),
        .D(\dataOut_reg[55]_i_1_n_7 ),
        .Q(dataOut_orig[52]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataOut_reg[53] 
       (.C(clk_IBUF_BUFG),
        .CE(\dataOut[64]_i_1_n_0 ),
        .D(\dataOut_reg[55]_i_1_n_6 ),
        .Q(dataOut_orig[53]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataOut_reg[54] 
       (.C(clk_IBUF_BUFG),
        .CE(\dataOut[64]_i_1_n_0 ),
        .D(\dataOut_reg[55]_i_1_n_5 ),
        .Q(dataOut_orig[54]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataOut_reg[55] 
       (.C(clk_IBUF_BUFG),
        .CE(\dataOut[64]_i_1_n_0 ),
        .D(\dataOut_reg[55]_i_1_n_4 ),
        .Q(dataOut_orig[55]),
        .R(reset_IBUF));
  CARRY4 \dataOut_reg[55]_i_1 
       (.CI(\dataOut_reg[51]_i_1_n_0 ),
        .CO({\dataOut_reg[55]_i_1_n_0 ,\dataOut_reg[55]_i_1_n_1 ,\dataOut_reg[55]_i_1_n_2 ,\dataOut_reg[55]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(sumVal[55:52]),
        .O({\dataOut_reg[55]_i_1_n_4 ,\dataOut_reg[55]_i_1_n_5 ,\dataOut_reg[55]_i_1_n_6 ,\dataOut_reg[55]_i_1_n_7 }),
        .S({\dataOut[55]_i_2_n_0 ,\dataOut[55]_i_3_n_0 ,\dataOut[55]_i_4_n_0 ,\dataOut[55]_i_5_n_0 }));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataOut_reg[56] 
       (.C(clk_IBUF_BUFG),
        .CE(\dataOut[64]_i_1_n_0 ),
        .D(\dataOut_reg[59]_i_1_n_7 ),
        .Q(dataOut_orig[56]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataOut_reg[57] 
       (.C(clk_IBUF_BUFG),
        .CE(\dataOut[64]_i_1_n_0 ),
        .D(\dataOut_reg[59]_i_1_n_6 ),
        .Q(dataOut_orig[57]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataOut_reg[58] 
       (.C(clk_IBUF_BUFG),
        .CE(\dataOut[64]_i_1_n_0 ),
        .D(\dataOut_reg[59]_i_1_n_5 ),
        .Q(dataOut_orig[58]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataOut_reg[59] 
       (.C(clk_IBUF_BUFG),
        .CE(\dataOut[64]_i_1_n_0 ),
        .D(\dataOut_reg[59]_i_1_n_4 ),
        .Q(dataOut_orig[59]),
        .R(reset_IBUF));
  CARRY4 \dataOut_reg[59]_i_1 
       (.CI(\dataOut_reg[55]_i_1_n_0 ),
        .CO({\dataOut_reg[59]_i_1_n_0 ,\dataOut_reg[59]_i_1_n_1 ,\dataOut_reg[59]_i_1_n_2 ,\dataOut_reg[59]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(sumVal[59:56]),
        .O({\dataOut_reg[59]_i_1_n_4 ,\dataOut_reg[59]_i_1_n_5 ,\dataOut_reg[59]_i_1_n_6 ,\dataOut_reg[59]_i_1_n_7 }),
        .S({\dataOut[59]_i_2_n_0 ,\dataOut[59]_i_3_n_0 ,\dataOut[59]_i_4_n_0 ,\dataOut[59]_i_5_n_0 }));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataOut_reg[5] 
       (.C(clk_IBUF_BUFG),
        .CE(\dataOut[64]_i_1_n_0 ),
        .D(\dataOut_reg[7]_i_1_n_6 ),
        .Q(out[5]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataOut_reg[60] 
       (.C(clk_IBUF_BUFG),
        .CE(\dataOut[64]_i_1_n_0 ),
        .D(\dataOut_reg[63]_i_1_n_7 ),
        .Q(dataOut_orig[60]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataOut_reg[61] 
       (.C(clk_IBUF_BUFG),
        .CE(\dataOut[64]_i_1_n_0 ),
        .D(\dataOut_reg[63]_i_1_n_6 ),
        .Q(dataOut_orig[61]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataOut_reg[62] 
       (.C(clk_IBUF_BUFG),
        .CE(\dataOut[64]_i_1_n_0 ),
        .D(\dataOut_reg[63]_i_1_n_5 ),
        .Q(dataOut_orig[62]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataOut_reg[63] 
       (.C(clk_IBUF_BUFG),
        .CE(\dataOut[64]_i_1_n_0 ),
        .D(\dataOut_reg[63]_i_1_n_4 ),
        .Q(dataOut_orig[63]),
        .R(reset_IBUF));
  CARRY4 \dataOut_reg[63]_i_1 
       (.CI(\dataOut_reg[59]_i_1_n_0 ),
        .CO({\dataOut_reg[63]_i_1_n_0 ,\dataOut_reg[63]_i_1_n_1 ,\dataOut_reg[63]_i_1_n_2 ,\dataOut_reg[63]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(sumVal[63:60]),
        .O({\dataOut_reg[63]_i_1_n_4 ,\dataOut_reg[63]_i_1_n_5 ,\dataOut_reg[63]_i_1_n_6 ,\dataOut_reg[63]_i_1_n_7 }),
        .S({\dataOut[63]_i_2_n_0 ,\dataOut[63]_i_3_n_0 ,\dataOut[63]_i_4_n_0 ,\dataOut[63]_i_5_n_0 }));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataOut_reg[64] 
       (.C(clk_IBUF_BUFG),
        .CE(\dataOut[64]_i_1_n_0 ),
        .D(\dataOut_reg[64]_i_2_n_7 ),
        .Q(dataOut_orig[64]),
        .R(reset_IBUF));
  CARRY4 \dataOut_reg[64]_i_2 
       (.CI(\dataOut_reg[63]_i_1_n_0 ),
        .CO(\NLW_dataOut_reg[64]_i_2_CO_UNCONNECTED [3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_dataOut_reg[64]_i_2_O_UNCONNECTED [3:1],\dataOut_reg[64]_i_2_n_7 }),
        .S({1'b0,1'b0,1'b0,sumVal[64]}));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataOut_reg[6] 
       (.C(clk_IBUF_BUFG),
        .CE(\dataOut[64]_i_1_n_0 ),
        .D(\dataOut_reg[7]_i_1_n_5 ),
        .Q(out[6]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataOut_reg[7] 
       (.C(clk_IBUF_BUFG),
        .CE(\dataOut[64]_i_1_n_0 ),
        .D(\dataOut_reg[7]_i_1_n_4 ),
        .Q(out[7]),
        .R(reset_IBUF));
  CARRY4 \dataOut_reg[7]_i_1 
       (.CI(\dataOut_reg[3]_i_1_n_0 ),
        .CO({\dataOut_reg[7]_i_1_n_0 ,\dataOut_reg[7]_i_1_n_1 ,\dataOut_reg[7]_i_1_n_2 ,\dataOut_reg[7]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(sumVal[7:4]),
        .O({\dataOut_reg[7]_i_1_n_4 ,\dataOut_reg[7]_i_1_n_5 ,\dataOut_reg[7]_i_1_n_6 ,\dataOut_reg[7]_i_1_n_7 }),
        .S({\dataOut[7]_i_2_n_0 ,\dataOut[7]_i_3_n_0 ,\dataOut[7]_i_4_n_0 ,\dataOut[7]_i_5_n_0 }));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataOut_reg[8] 
       (.C(clk_IBUF_BUFG),
        .CE(\dataOut[64]_i_1_n_0 ),
        .D(\dataOut_reg[11]_i_1_n_7 ),
        .Q(out[8]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataOut_reg[9] 
       (.C(clk_IBUF_BUFG),
        .CE(\dataOut[64]_i_1_n_0 ),
        .D(\dataOut_reg[11]_i_1_n_6 ),
        .Q(out[9]),
        .R(reset_IBUF));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-10 {cell *THIS*} {string 16x18 4}}" *) 
  DSP48E1 #(
    .ACASCREG(0),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(0),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(0),
    .BREG(0),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(0),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    prodVal0
       (.A({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,\dataMem_out_internal_reg[31] [16:0]}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_prodVal0_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({1'b0,1'b0,1'b0,\weightMem_out_internal_reg[31] [31:17]}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_prodVal0_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_prodVal0_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_prodVal0_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(1'b0),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(1'b0),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(1'b0),
        .CLK(1'b0),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_prodVal0_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b0,1'b0,1'b0,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_prodVal0_OVERFLOW_UNCONNECTED),
        .P({prodVal0_n_58,prodVal0_n_59,prodVal0_n_60,prodVal0_n_61,prodVal0_n_62,prodVal0_n_63,prodVal0_n_64,prodVal0_n_65,prodVal0_n_66,prodVal0_n_67,prodVal0_n_68,prodVal0_n_69,prodVal0_n_70,prodVal0_n_71,prodVal0_n_72,prodVal0_n_73,prodVal0_n_74,prodVal0_n_75,prodVal0_n_76,prodVal0_n_77,prodVal0_n_78,prodVal0_n_79,prodVal0_n_80,prodVal0_n_81,prodVal0_n_82,prodVal0_n_83,prodVal0_n_84,prodVal0_n_85,prodVal0_n_86,prodVal0_n_87,prodVal0_n_88,prodVal0_n_89,prodVal0_n_90,prodVal0_n_91,prodVal0_n_92,prodVal0_n_93,prodVal0_n_94,prodVal0_n_95,prodVal0_n_96,prodVal0_n_97,prodVal0_n_98,prodVal0_n_99,prodVal0_n_100,prodVal0_n_101,prodVal0_n_102,prodVal0_n_103,prodVal0_n_104,prodVal0_n_105}),
        .PATTERNBDETECT(NLW_prodVal0_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_prodVal0_PATTERNDETECT_UNCONNECTED),
        .PCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCOUT({prodVal0_n_106,prodVal0_n_107,prodVal0_n_108,prodVal0_n_109,prodVal0_n_110,prodVal0_n_111,prodVal0_n_112,prodVal0_n_113,prodVal0_n_114,prodVal0_n_115,prodVal0_n_116,prodVal0_n_117,prodVal0_n_118,prodVal0_n_119,prodVal0_n_120,prodVal0_n_121,prodVal0_n_122,prodVal0_n_123,prodVal0_n_124,prodVal0_n_125,prodVal0_n_126,prodVal0_n_127,prodVal0_n_128,prodVal0_n_129,prodVal0_n_130,prodVal0_n_131,prodVal0_n_132,prodVal0_n_133,prodVal0_n_134,prodVal0_n_135,prodVal0_n_136,prodVal0_n_137,prodVal0_n_138,prodVal0_n_139,prodVal0_n_140,prodVal0_n_141,prodVal0_n_142,prodVal0_n_143,prodVal0_n_144,prodVal0_n_145,prodVal0_n_146,prodVal0_n_147,prodVal0_n_148,prodVal0_n_149,prodVal0_n_150,prodVal0_n_151,prodVal0_n_152,prodVal0_n_153}),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(1'b0),
        .UNDERFLOW(NLW_prodVal0_UNDERFLOW_UNCONNECTED));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-10 {cell *THIS*} {string 16x16 4}}" *) 
  DSP48E1 #(
    .ACASCREG(0),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(0),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(0),
    .BREG(0),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(0),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    prodVal0__0
       (.A({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,\weightMem_out_internal_reg[31] [31:17]}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_prodVal0__0_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({1'b0,1'b0,1'b0,\dataMem_out_internal_reg[31] [31:17]}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_prodVal0__0_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_prodVal0__0_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_prodVal0__0_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(1'b0),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(1'b0),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(1'b0),
        .CLK(1'b0),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_prodVal0__0_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b1,1'b0,1'b1,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_prodVal0__0_OVERFLOW_UNCONNECTED),
        .P({NLW_prodVal0__0_P_UNCONNECTED[47:30],prodVal0__0_n_76,prodVal0__0_n_77,prodVal0__0_n_78,prodVal0__0_n_79,prodVal0__0_n_80,prodVal0__0_n_81,prodVal0__0_n_82,prodVal0__0_n_83,prodVal0__0_n_84,prodVal0__0_n_85,prodVal0__0_n_86,prodVal0__0_n_87,prodVal0__0_n_88,prodVal0__0_n_89,prodVal0__0_n_90,prodVal0__0_n_91,prodVal0__0_n_92,prodVal0__0_n_93,prodVal0__0_n_94,prodVal0__0_n_95,prodVal0__0_n_96,prodVal0__0_n_97,prodVal0__0_n_98,prodVal0__0_n_99,prodVal0__0_n_100,prodVal0__0_n_101,prodVal0__0_n_102,prodVal0__0_n_103,prodVal0__0_n_104,prodVal0__0_n_105}),
        .PATTERNBDETECT(NLW_prodVal0__0_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_prodVal0__0_PATTERNDETECT_UNCONNECTED),
        .PCIN({prodVal0_n_106,prodVal0_n_107,prodVal0_n_108,prodVal0_n_109,prodVal0_n_110,prodVal0_n_111,prodVal0_n_112,prodVal0_n_113,prodVal0_n_114,prodVal0_n_115,prodVal0_n_116,prodVal0_n_117,prodVal0_n_118,prodVal0_n_119,prodVal0_n_120,prodVal0_n_121,prodVal0_n_122,prodVal0_n_123,prodVal0_n_124,prodVal0_n_125,prodVal0_n_126,prodVal0_n_127,prodVal0_n_128,prodVal0_n_129,prodVal0_n_130,prodVal0_n_131,prodVal0_n_132,prodVal0_n_133,prodVal0_n_134,prodVal0_n_135,prodVal0_n_136,prodVal0_n_137,prodVal0_n_138,prodVal0_n_139,prodVal0_n_140,prodVal0_n_141,prodVal0_n_142,prodVal0_n_143,prodVal0_n_144,prodVal0_n_145,prodVal0_n_146,prodVal0_n_147,prodVal0_n_148,prodVal0_n_149,prodVal0_n_150,prodVal0_n_151,prodVal0_n_152,prodVal0_n_153}),
        .PCOUT(NLW_prodVal0__0_PCOUT_UNCONNECTED[47:0]),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(1'b0),
        .UNDERFLOW(NLW_prodVal0__0_UNDERFLOW_UNCONNECTED));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-10 {cell *THIS*} {string 18x18 4}}" *) 
  DSP48E1 #(
    .ACASCREG(0),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(0),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(0),
    .BREG(0),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(0),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    prodVal0__1
       (.A({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,\weightMem_out_internal_reg[31] [16:0]}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT({prodVal0__1_n_24,prodVal0__1_n_25,prodVal0__1_n_26,prodVal0__1_n_27,prodVal0__1_n_28,prodVal0__1_n_29,prodVal0__1_n_30,prodVal0__1_n_31,prodVal0__1_n_32,prodVal0__1_n_33,prodVal0__1_n_34,prodVal0__1_n_35,prodVal0__1_n_36,prodVal0__1_n_37,prodVal0__1_n_38,prodVal0__1_n_39,prodVal0__1_n_40,prodVal0__1_n_41,prodVal0__1_n_42,prodVal0__1_n_43,prodVal0__1_n_44,prodVal0__1_n_45,prodVal0__1_n_46,prodVal0__1_n_47,prodVal0__1_n_48,prodVal0__1_n_49,prodVal0__1_n_50,prodVal0__1_n_51,prodVal0__1_n_52,prodVal0__1_n_53}),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({1'b0,\dataMem_out_internal_reg[31] [16:0]}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_prodVal0__1_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_prodVal0__1_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_prodVal0__1_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(1'b0),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(1'b0),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(1'b0),
        .CLK(1'b0),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_prodVal0__1_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b0,1'b0,1'b0,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_prodVal0__1_OVERFLOW_UNCONNECTED),
        .P({prodVal0__1_n_58,prodVal0__1_n_59,prodVal0__1_n_60,prodVal0__1_n_61,prodVal0__1_n_62,prodVal0__1_n_63,prodVal0__1_n_64,prodVal0__1_n_65,prodVal0__1_n_66,prodVal0__1_n_67,prodVal0__1_n_68,prodVal0__1_n_69,prodVal0__1_n_70,prodVal0__1_n_71,prodVal0__1_n_72,prodVal0__1_n_73,prodVal0__1_n_74,prodVal0__1_n_75,prodVal0__1_n_76,prodVal0__1_n_77,prodVal0__1_n_78,prodVal0__1_n_79,prodVal0__1_n_80,prodVal0__1_n_81,prodVal0__1_n_82,prodVal0__1_n_83,prodVal0__1_n_84,prodVal0__1_n_85,prodVal0__1_n_86,prodVal0__1_n_87,prodVal0__1_n_88,prodVal0__1_n_89,prodVal0__1_n_90,prodVal0__1_n_91,prodVal0__1_n_92,prodVal0__1_n_93,prodVal0__1_n_94,prodVal0__1_n_95,prodVal0__1_n_96,prodVal0__1_n_97,prodVal0__1_n_98,prodVal0__1_n_99,prodVal0__1_n_100,prodVal0__1_n_101,prodVal0__1_n_102,prodVal0__1_n_103,prodVal0__1_n_104,prodVal0__1_n_105}),
        .PATTERNBDETECT(NLW_prodVal0__1_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_prodVal0__1_PATTERNDETECT_UNCONNECTED),
        .PCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCOUT({prodVal0__1_n_106,prodVal0__1_n_107,prodVal0__1_n_108,prodVal0__1_n_109,prodVal0__1_n_110,prodVal0__1_n_111,prodVal0__1_n_112,prodVal0__1_n_113,prodVal0__1_n_114,prodVal0__1_n_115,prodVal0__1_n_116,prodVal0__1_n_117,prodVal0__1_n_118,prodVal0__1_n_119,prodVal0__1_n_120,prodVal0__1_n_121,prodVal0__1_n_122,prodVal0__1_n_123,prodVal0__1_n_124,prodVal0__1_n_125,prodVal0__1_n_126,prodVal0__1_n_127,prodVal0__1_n_128,prodVal0__1_n_129,prodVal0__1_n_130,prodVal0__1_n_131,prodVal0__1_n_132,prodVal0__1_n_133,prodVal0__1_n_134,prodVal0__1_n_135,prodVal0__1_n_136,prodVal0__1_n_137,prodVal0__1_n_138,prodVal0__1_n_139,prodVal0__1_n_140,prodVal0__1_n_141,prodVal0__1_n_142,prodVal0__1_n_143,prodVal0__1_n_144,prodVal0__1_n_145,prodVal0__1_n_146,prodVal0__1_n_147,prodVal0__1_n_148,prodVal0__1_n_149,prodVal0__1_n_150,prodVal0__1_n_151,prodVal0__1_n_152,prodVal0__1_n_153}),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(1'b0),
        .UNDERFLOW(NLW_prodVal0__1_UNDERFLOW_UNCONNECTED));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-10 {cell *THIS*} {string 18x16 4}}" *) 
  DSP48E1 #(
    .ACASCREG(0),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(0),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("CASCADE"),
    .BCASCREG(0),
    .BREG(0),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(0),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    prodVal0__2
       (.A({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACIN({prodVal0__1_n_24,prodVal0__1_n_25,prodVal0__1_n_26,prodVal0__1_n_27,prodVal0__1_n_28,prodVal0__1_n_29,prodVal0__1_n_30,prodVal0__1_n_31,prodVal0__1_n_32,prodVal0__1_n_33,prodVal0__1_n_34,prodVal0__1_n_35,prodVal0__1_n_36,prodVal0__1_n_37,prodVal0__1_n_38,prodVal0__1_n_39,prodVal0__1_n_40,prodVal0__1_n_41,prodVal0__1_n_42,prodVal0__1_n_43,prodVal0__1_n_44,prodVal0__1_n_45,prodVal0__1_n_46,prodVal0__1_n_47,prodVal0__1_n_48,prodVal0__1_n_49,prodVal0__1_n_50,prodVal0__1_n_51,prodVal0__1_n_52,prodVal0__1_n_53}),
        .ACOUT(NLW_prodVal0__2_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({1'b0,1'b0,1'b0,\dataMem_out_internal_reg[31] [31:17]}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_prodVal0__2_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_prodVal0__2_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_prodVal0__2_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(1'b0),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(1'b0),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(1'b0),
        .CLK(1'b0),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_prodVal0__2_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b1,1'b0,1'b1,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_prodVal0__2_OVERFLOW_UNCONNECTED),
        .P({NLW_prodVal0__2_P_UNCONNECTED[47],prodVal0__2_n_59,prodVal0__2_n_60,prodVal0__2_n_61,prodVal0__2_n_62,prodVal0__2_n_63,prodVal0__2_n_64,prodVal0__2_n_65,prodVal0__2_n_66,prodVal0__2_n_67,prodVal0__2_n_68,prodVal0__2_n_69,prodVal0__2_n_70,prodVal0__2_n_71,prodVal0__2_n_72,prodVal0__2_n_73,prodVal0__2_n_74,prodVal0__2_n_75,prodVal0__2_n_76,prodVal0__2_n_77,prodVal0__2_n_78,prodVal0__2_n_79,prodVal0__2_n_80,prodVal0__2_n_81,prodVal0__2_n_82,prodVal0__2_n_83,prodVal0__2_n_84,prodVal0__2_n_85,prodVal0__2_n_86,prodVal0__2_n_87,prodVal0__2_n_88,prodVal0__2_n_89,prodVal0__2_n_90,prodVal0__2_n_91,prodVal0__2_n_92,prodVal0__2_n_93,prodVal0__2_n_94,prodVal0__2_n_95,prodVal0__2_n_96,prodVal0__2_n_97,prodVal0__2_n_98,prodVal0__2_n_99,prodVal0__2_n_100,prodVal0__2_n_101,prodVal0__2_n_102,prodVal0__2_n_103,prodVal0__2_n_104,prodVal0__2_n_105}),
        .PATTERNBDETECT(NLW_prodVal0__2_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_prodVal0__2_PATTERNDETECT_UNCONNECTED),
        .PCIN({prodVal0__1_n_106,prodVal0__1_n_107,prodVal0__1_n_108,prodVal0__1_n_109,prodVal0__1_n_110,prodVal0__1_n_111,prodVal0__1_n_112,prodVal0__1_n_113,prodVal0__1_n_114,prodVal0__1_n_115,prodVal0__1_n_116,prodVal0__1_n_117,prodVal0__1_n_118,prodVal0__1_n_119,prodVal0__1_n_120,prodVal0__1_n_121,prodVal0__1_n_122,prodVal0__1_n_123,prodVal0__1_n_124,prodVal0__1_n_125,prodVal0__1_n_126,prodVal0__1_n_127,prodVal0__1_n_128,prodVal0__1_n_129,prodVal0__1_n_130,prodVal0__1_n_131,prodVal0__1_n_132,prodVal0__1_n_133,prodVal0__1_n_134,prodVal0__1_n_135,prodVal0__1_n_136,prodVal0__1_n_137,prodVal0__1_n_138,prodVal0__1_n_139,prodVal0__1_n_140,prodVal0__1_n_141,prodVal0__1_n_142,prodVal0__1_n_143,prodVal0__1_n_144,prodVal0__1_n_145,prodVal0__1_n_146,prodVal0__1_n_147,prodVal0__1_n_148,prodVal0__1_n_149,prodVal0__1_n_150,prodVal0__1_n_151,prodVal0__1_n_152,prodVal0__1_n_153}),
        .PCOUT(NLW_prodVal0__2_PCOUT_UNCONNECTED[47:0]),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(1'b0),
        .UNDERFLOW(NLW_prodVal0__2_UNDERFLOW_UNCONNECTED));
  CARRY4 prodVal0_carry
       (.CI(1'b0),
        .CO({prodVal0_carry_n_0,prodVal0_carry_n_1,prodVal0_carry_n_2,prodVal0_carry_n_3}),
        .CYINIT(1'b0),
        .DI({prodVal0__2_n_103,prodVal0__2_n_104,prodVal0__2_n_105,1'b0}),
        .O(prodVal0__3[19:16]),
        .S({prodVal0_carry_i_1_n_0,prodVal0_carry_i_2_n_0,prodVal0_carry_i_3_n_0,prodVal0__1_n_89}));
  CARRY4 prodVal0_carry__0
       (.CI(prodVal0_carry_n_0),
        .CO({prodVal0_carry__0_n_0,prodVal0_carry__0_n_1,prodVal0_carry__0_n_2,prodVal0_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({prodVal0__2_n_99,prodVal0__2_n_100,prodVal0__2_n_101,prodVal0__2_n_102}),
        .O(prodVal0__3[23:20]),
        .S({prodVal0_carry__0_i_1_n_0,prodVal0_carry__0_i_2_n_0,prodVal0_carry__0_i_3_n_0,prodVal0_carry__0_i_4_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    prodVal0_carry__0_i_1
       (.I0(prodVal0__2_n_99),
        .I1(prodVal0_n_99),
        .O(prodVal0_carry__0_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    prodVal0_carry__0_i_2
       (.I0(prodVal0__2_n_100),
        .I1(prodVal0_n_100),
        .O(prodVal0_carry__0_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    prodVal0_carry__0_i_3
       (.I0(prodVal0__2_n_101),
        .I1(prodVal0_n_101),
        .O(prodVal0_carry__0_i_3_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    prodVal0_carry__0_i_4
       (.I0(prodVal0__2_n_102),
        .I1(prodVal0_n_102),
        .O(prodVal0_carry__0_i_4_n_0));
  CARRY4 prodVal0_carry__1
       (.CI(prodVal0_carry__0_n_0),
        .CO({prodVal0_carry__1_n_0,prodVal0_carry__1_n_1,prodVal0_carry__1_n_2,prodVal0_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI({prodVal0__2_n_95,prodVal0__2_n_96,prodVal0__2_n_97,prodVal0__2_n_98}),
        .O(prodVal0__3[27:24]),
        .S({prodVal0_carry__1_i_1_n_0,prodVal0_carry__1_i_2_n_0,prodVal0_carry__1_i_3_n_0,prodVal0_carry__1_i_4_n_0}));
  CARRY4 prodVal0_carry__10
       (.CI(prodVal0_carry__9_n_0),
        .CO({NLW_prodVal0_carry__10_CO_UNCONNECTED[3],prodVal0_carry__10_n_1,prodVal0_carry__10_n_2,prodVal0_carry__10_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,prodVal0__2_n_60,prodVal0__2_n_61,prodVal0__2_n_62}),
        .O(prodVal0__3[63:60]),
        .S({prodVal0_carry__10_i_1_n_0,prodVal0_carry__10_i_2_n_0,prodVal0_carry__10_i_3_n_0,prodVal0_carry__10_i_4_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    prodVal0_carry__10_i_1
       (.I0(prodVal0__2_n_59),
        .I1(prodVal0__0_n_76),
        .O(prodVal0_carry__10_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    prodVal0_carry__10_i_2
       (.I0(prodVal0__2_n_60),
        .I1(prodVal0__0_n_77),
        .O(prodVal0_carry__10_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    prodVal0_carry__10_i_3
       (.I0(prodVal0__2_n_61),
        .I1(prodVal0__0_n_78),
        .O(prodVal0_carry__10_i_3_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    prodVal0_carry__10_i_4
       (.I0(prodVal0__2_n_62),
        .I1(prodVal0__0_n_79),
        .O(prodVal0_carry__10_i_4_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    prodVal0_carry__1_i_1
       (.I0(prodVal0__2_n_95),
        .I1(prodVal0_n_95),
        .O(prodVal0_carry__1_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    prodVal0_carry__1_i_2
       (.I0(prodVal0__2_n_96),
        .I1(prodVal0_n_96),
        .O(prodVal0_carry__1_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    prodVal0_carry__1_i_3
       (.I0(prodVal0__2_n_97),
        .I1(prodVal0_n_97),
        .O(prodVal0_carry__1_i_3_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    prodVal0_carry__1_i_4
       (.I0(prodVal0__2_n_98),
        .I1(prodVal0_n_98),
        .O(prodVal0_carry__1_i_4_n_0));
  CARRY4 prodVal0_carry__2
       (.CI(prodVal0_carry__1_n_0),
        .CO({prodVal0_carry__2_n_0,prodVal0_carry__2_n_1,prodVal0_carry__2_n_2,prodVal0_carry__2_n_3}),
        .CYINIT(1'b0),
        .DI({prodVal0__2_n_91,prodVal0__2_n_92,prodVal0__2_n_93,prodVal0__2_n_94}),
        .O(prodVal0__3[31:28]),
        .S({prodVal0_carry__2_i_1_n_0,prodVal0_carry__2_i_2_n_0,prodVal0_carry__2_i_3_n_0,prodVal0_carry__2_i_4_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    prodVal0_carry__2_i_1
       (.I0(prodVal0__2_n_91),
        .I1(prodVal0_n_91),
        .O(prodVal0_carry__2_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    prodVal0_carry__2_i_2
       (.I0(prodVal0__2_n_92),
        .I1(prodVal0_n_92),
        .O(prodVal0_carry__2_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    prodVal0_carry__2_i_3
       (.I0(prodVal0__2_n_93),
        .I1(prodVal0_n_93),
        .O(prodVal0_carry__2_i_3_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    prodVal0_carry__2_i_4
       (.I0(prodVal0__2_n_94),
        .I1(prodVal0_n_94),
        .O(prodVal0_carry__2_i_4_n_0));
  CARRY4 prodVal0_carry__3
       (.CI(prodVal0_carry__2_n_0),
        .CO({prodVal0_carry__3_n_0,prodVal0_carry__3_n_1,prodVal0_carry__3_n_2,prodVal0_carry__3_n_3}),
        .CYINIT(1'b0),
        .DI({prodVal0__2_n_87,prodVal0__2_n_88,prodVal0__2_n_89,prodVal0__2_n_90}),
        .O(prodVal0__3[35:32]),
        .S({prodVal0_carry__3_i_1_n_0,prodVal0_carry__3_i_2_n_0,prodVal0_carry__3_i_3_n_0,prodVal0_carry__3_i_4_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    prodVal0_carry__3_i_1
       (.I0(prodVal0__2_n_87),
        .I1(prodVal0__0_n_104),
        .O(prodVal0_carry__3_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    prodVal0_carry__3_i_2
       (.I0(prodVal0__2_n_88),
        .I1(prodVal0__0_n_105),
        .O(prodVal0_carry__3_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    prodVal0_carry__3_i_3
       (.I0(prodVal0__2_n_89),
        .I1(prodVal0_n_89),
        .O(prodVal0_carry__3_i_3_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    prodVal0_carry__3_i_4
       (.I0(prodVal0__2_n_90),
        .I1(prodVal0_n_90),
        .O(prodVal0_carry__3_i_4_n_0));
  CARRY4 prodVal0_carry__4
       (.CI(prodVal0_carry__3_n_0),
        .CO({prodVal0_carry__4_n_0,prodVal0_carry__4_n_1,prodVal0_carry__4_n_2,prodVal0_carry__4_n_3}),
        .CYINIT(1'b0),
        .DI({prodVal0__2_n_83,prodVal0__2_n_84,prodVal0__2_n_85,prodVal0__2_n_86}),
        .O(prodVal0__3[39:36]),
        .S({prodVal0_carry__4_i_1_n_0,prodVal0_carry__4_i_2_n_0,prodVal0_carry__4_i_3_n_0,prodVal0_carry__4_i_4_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    prodVal0_carry__4_i_1
       (.I0(prodVal0__2_n_83),
        .I1(prodVal0__0_n_100),
        .O(prodVal0_carry__4_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    prodVal0_carry__4_i_2
       (.I0(prodVal0__2_n_84),
        .I1(prodVal0__0_n_101),
        .O(prodVal0_carry__4_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    prodVal0_carry__4_i_3
       (.I0(prodVal0__2_n_85),
        .I1(prodVal0__0_n_102),
        .O(prodVal0_carry__4_i_3_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    prodVal0_carry__4_i_4
       (.I0(prodVal0__2_n_86),
        .I1(prodVal0__0_n_103),
        .O(prodVal0_carry__4_i_4_n_0));
  CARRY4 prodVal0_carry__5
       (.CI(prodVal0_carry__4_n_0),
        .CO({prodVal0_carry__5_n_0,prodVal0_carry__5_n_1,prodVal0_carry__5_n_2,prodVal0_carry__5_n_3}),
        .CYINIT(1'b0),
        .DI({prodVal0__2_n_79,prodVal0__2_n_80,prodVal0__2_n_81,prodVal0__2_n_82}),
        .O(prodVal0__3[43:40]),
        .S({prodVal0_carry__5_i_1_n_0,prodVal0_carry__5_i_2_n_0,prodVal0_carry__5_i_3_n_0,prodVal0_carry__5_i_4_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    prodVal0_carry__5_i_1
       (.I0(prodVal0__2_n_79),
        .I1(prodVal0__0_n_96),
        .O(prodVal0_carry__5_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    prodVal0_carry__5_i_2
       (.I0(prodVal0__2_n_80),
        .I1(prodVal0__0_n_97),
        .O(prodVal0_carry__5_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    prodVal0_carry__5_i_3
       (.I0(prodVal0__2_n_81),
        .I1(prodVal0__0_n_98),
        .O(prodVal0_carry__5_i_3_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    prodVal0_carry__5_i_4
       (.I0(prodVal0__2_n_82),
        .I1(prodVal0__0_n_99),
        .O(prodVal0_carry__5_i_4_n_0));
  CARRY4 prodVal0_carry__6
       (.CI(prodVal0_carry__5_n_0),
        .CO({prodVal0_carry__6_n_0,prodVal0_carry__6_n_1,prodVal0_carry__6_n_2,prodVal0_carry__6_n_3}),
        .CYINIT(1'b0),
        .DI({prodVal0__2_n_75,prodVal0__2_n_76,prodVal0__2_n_77,prodVal0__2_n_78}),
        .O(prodVal0__3[47:44]),
        .S({prodVal0_carry__6_i_1_n_0,prodVal0_carry__6_i_2_n_0,prodVal0_carry__6_i_3_n_0,prodVal0_carry__6_i_4_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    prodVal0_carry__6_i_1
       (.I0(prodVal0__2_n_75),
        .I1(prodVal0__0_n_92),
        .O(prodVal0_carry__6_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    prodVal0_carry__6_i_2
       (.I0(prodVal0__2_n_76),
        .I1(prodVal0__0_n_93),
        .O(prodVal0_carry__6_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    prodVal0_carry__6_i_3
       (.I0(prodVal0__2_n_77),
        .I1(prodVal0__0_n_94),
        .O(prodVal0_carry__6_i_3_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    prodVal0_carry__6_i_4
       (.I0(prodVal0__2_n_78),
        .I1(prodVal0__0_n_95),
        .O(prodVal0_carry__6_i_4_n_0));
  CARRY4 prodVal0_carry__7
       (.CI(prodVal0_carry__6_n_0),
        .CO({prodVal0_carry__7_n_0,prodVal0_carry__7_n_1,prodVal0_carry__7_n_2,prodVal0_carry__7_n_3}),
        .CYINIT(1'b0),
        .DI({prodVal0__2_n_71,prodVal0__2_n_72,prodVal0__2_n_73,prodVal0__2_n_74}),
        .O(prodVal0__3[51:48]),
        .S({prodVal0_carry__7_i_1_n_0,prodVal0_carry__7_i_2_n_0,prodVal0_carry__7_i_3_n_0,prodVal0_carry__7_i_4_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    prodVal0_carry__7_i_1
       (.I0(prodVal0__2_n_71),
        .I1(prodVal0__0_n_88),
        .O(prodVal0_carry__7_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    prodVal0_carry__7_i_2
       (.I0(prodVal0__2_n_72),
        .I1(prodVal0__0_n_89),
        .O(prodVal0_carry__7_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    prodVal0_carry__7_i_3
       (.I0(prodVal0__2_n_73),
        .I1(prodVal0__0_n_90),
        .O(prodVal0_carry__7_i_3_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    prodVal0_carry__7_i_4
       (.I0(prodVal0__2_n_74),
        .I1(prodVal0__0_n_91),
        .O(prodVal0_carry__7_i_4_n_0));
  CARRY4 prodVal0_carry__8
       (.CI(prodVal0_carry__7_n_0),
        .CO({prodVal0_carry__8_n_0,prodVal0_carry__8_n_1,prodVal0_carry__8_n_2,prodVal0_carry__8_n_3}),
        .CYINIT(1'b0),
        .DI({prodVal0__2_n_67,prodVal0__2_n_68,prodVal0__2_n_69,prodVal0__2_n_70}),
        .O(prodVal0__3[55:52]),
        .S({prodVal0_carry__8_i_1_n_0,prodVal0_carry__8_i_2_n_0,prodVal0_carry__8_i_3_n_0,prodVal0_carry__8_i_4_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    prodVal0_carry__8_i_1
       (.I0(prodVal0__2_n_67),
        .I1(prodVal0__0_n_84),
        .O(prodVal0_carry__8_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    prodVal0_carry__8_i_2
       (.I0(prodVal0__2_n_68),
        .I1(prodVal0__0_n_85),
        .O(prodVal0_carry__8_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    prodVal0_carry__8_i_3
       (.I0(prodVal0__2_n_69),
        .I1(prodVal0__0_n_86),
        .O(prodVal0_carry__8_i_3_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    prodVal0_carry__8_i_4
       (.I0(prodVal0__2_n_70),
        .I1(prodVal0__0_n_87),
        .O(prodVal0_carry__8_i_4_n_0));
  CARRY4 prodVal0_carry__9
       (.CI(prodVal0_carry__8_n_0),
        .CO({prodVal0_carry__9_n_0,prodVal0_carry__9_n_1,prodVal0_carry__9_n_2,prodVal0_carry__9_n_3}),
        .CYINIT(1'b0),
        .DI({prodVal0__2_n_63,prodVal0__2_n_64,prodVal0__2_n_65,prodVal0__2_n_66}),
        .O(prodVal0__3[59:56]),
        .S({prodVal0_carry__9_i_1_n_0,prodVal0_carry__9_i_2_n_0,prodVal0_carry__9_i_3_n_0,prodVal0_carry__9_i_4_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    prodVal0_carry__9_i_1
       (.I0(prodVal0__2_n_63),
        .I1(prodVal0__0_n_80),
        .O(prodVal0_carry__9_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    prodVal0_carry__9_i_2
       (.I0(prodVal0__2_n_64),
        .I1(prodVal0__0_n_81),
        .O(prodVal0_carry__9_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    prodVal0_carry__9_i_3
       (.I0(prodVal0__2_n_65),
        .I1(prodVal0__0_n_82),
        .O(prodVal0_carry__9_i_3_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    prodVal0_carry__9_i_4
       (.I0(prodVal0__2_n_66),
        .I1(prodVal0__0_n_83),
        .O(prodVal0_carry__9_i_4_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    prodVal0_carry_i_1
       (.I0(prodVal0__2_n_103),
        .I1(prodVal0_n_103),
        .O(prodVal0_carry_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    prodVal0_carry_i_2
       (.I0(prodVal0__2_n_104),
        .I1(prodVal0_n_104),
        .O(prodVal0_carry_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    prodVal0_carry_i_3
       (.I0(prodVal0__2_n_105),
        .I1(prodVal0_n_105),
        .O(prodVal0_carry_i_3_n_0));
  LUT5 #(
    .INIT(32'hFFB80088)) 
    \prodVal[0]_i_1 
       (.I0(prodVal[0]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(prodVal0__1_n_105),
        .O(\prodVal[0]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFB80088)) 
    \prodVal[10]_i_1 
       (.I0(prodVal[10]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(prodVal0__1_n_95),
        .O(\prodVal[10]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFB80088)) 
    \prodVal[11]_i_1 
       (.I0(prodVal[11]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(prodVal0__1_n_94),
        .O(\prodVal[11]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFB80088)) 
    \prodVal[12]_i_1 
       (.I0(prodVal[12]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(prodVal0__1_n_93),
        .O(\prodVal[12]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFB80088)) 
    \prodVal[13]_i_1 
       (.I0(prodVal[13]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(prodVal0__1_n_92),
        .O(\prodVal[13]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFB80088)) 
    \prodVal[14]_i_1 
       (.I0(prodVal[14]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(prodVal0__1_n_91),
        .O(\prodVal[14]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFB80088)) 
    \prodVal[15]_i_1 
       (.I0(prodVal[15]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(prodVal0__1_n_90),
        .O(\prodVal[15]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFB80088)) 
    \prodVal[16]_i_1 
       (.I0(prodVal[16]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(prodVal0__3[16]),
        .O(\prodVal[16]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFB80088)) 
    \prodVal[17]_i_1 
       (.I0(prodVal[17]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(prodVal0__3[17]),
        .O(\prodVal[17]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFB80088)) 
    \prodVal[18]_i_1 
       (.I0(prodVal[18]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(prodVal0__3[18]),
        .O(\prodVal[18]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFB80088)) 
    \prodVal[19]_i_1 
       (.I0(prodVal[19]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(prodVal0__3[19]),
        .O(\prodVal[19]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFB80088)) 
    \prodVal[1]_i_1 
       (.I0(prodVal[1]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(prodVal0__1_n_104),
        .O(\prodVal[1]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFB80088)) 
    \prodVal[20]_i_1 
       (.I0(prodVal[20]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(prodVal0__3[20]),
        .O(\prodVal[20]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFB80088)) 
    \prodVal[21]_i_1 
       (.I0(prodVal[21]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(prodVal0__3[21]),
        .O(\prodVal[21]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFB80088)) 
    \prodVal[22]_i_1 
       (.I0(prodVal[22]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(prodVal0__3[22]),
        .O(\prodVal[22]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFB80088)) 
    \prodVal[23]_i_1 
       (.I0(prodVal[23]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(prodVal0__3[23]),
        .O(\prodVal[23]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFB80088)) 
    \prodVal[24]_i_1 
       (.I0(prodVal[24]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(prodVal0__3[24]),
        .O(\prodVal[24]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFB80088)) 
    \prodVal[25]_i_1 
       (.I0(prodVal[25]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(prodVal0__3[25]),
        .O(\prodVal[25]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFB80088)) 
    \prodVal[26]_i_1 
       (.I0(prodVal[26]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(prodVal0__3[26]),
        .O(\prodVal[26]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFB80088)) 
    \prodVal[27]_i_1 
       (.I0(prodVal[27]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(prodVal0__3[27]),
        .O(\prodVal[27]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFB80088)) 
    \prodVal[28]_i_1 
       (.I0(prodVal[28]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(prodVal0__3[28]),
        .O(\prodVal[28]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFB80088)) 
    \prodVal[29]_i_1 
       (.I0(prodVal[29]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(prodVal0__3[29]),
        .O(\prodVal[29]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFB80088)) 
    \prodVal[2]_i_1 
       (.I0(prodVal[2]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(prodVal0__1_n_103),
        .O(\prodVal[2]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFB80088)) 
    \prodVal[30]_i_1 
       (.I0(prodVal[30]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(prodVal0__3[30]),
        .O(\prodVal[30]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFB80088)) 
    \prodVal[31]_i_1 
       (.I0(prodVal[31]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(prodVal0__3[31]),
        .O(\prodVal[31]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFB80088)) 
    \prodVal[32]_i_1 
       (.I0(prodVal[32]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(prodVal0__3[32]),
        .O(\prodVal[32]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFB80088)) 
    \prodVal[33]_i_1 
       (.I0(prodVal[33]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(prodVal0__3[33]),
        .O(\prodVal[33]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFB80088)) 
    \prodVal[34]_i_1 
       (.I0(prodVal[34]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(prodVal0__3[34]),
        .O(\prodVal[34]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFB80088)) 
    \prodVal[35]_i_1 
       (.I0(prodVal[35]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(prodVal0__3[35]),
        .O(\prodVal[35]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFB80088)) 
    \prodVal[36]_i_1 
       (.I0(prodVal[36]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(prodVal0__3[36]),
        .O(\prodVal[36]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFB80088)) 
    \prodVal[37]_i_1 
       (.I0(prodVal[37]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(prodVal0__3[37]),
        .O(\prodVal[37]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFB80088)) 
    \prodVal[38]_i_1 
       (.I0(prodVal[38]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(prodVal0__3[38]),
        .O(\prodVal[38]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFB80088)) 
    \prodVal[39]_i_1 
       (.I0(prodVal[39]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(prodVal0__3[39]),
        .O(\prodVal[39]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFB80088)) 
    \prodVal[3]_i_1 
       (.I0(prodVal[3]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(prodVal0__1_n_102),
        .O(\prodVal[3]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFB80088)) 
    \prodVal[40]_i_1 
       (.I0(prodVal[40]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(prodVal0__3[40]),
        .O(\prodVal[40]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFB80088)) 
    \prodVal[41]_i_1 
       (.I0(prodVal[41]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(prodVal0__3[41]),
        .O(\prodVal[41]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFB80088)) 
    \prodVal[42]_i_1 
       (.I0(prodVal[42]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(prodVal0__3[42]),
        .O(\prodVal[42]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFB80088)) 
    \prodVal[43]_i_1 
       (.I0(prodVal[43]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(prodVal0__3[43]),
        .O(\prodVal[43]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFB80088)) 
    \prodVal[44]_i_1 
       (.I0(prodVal[44]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(prodVal0__3[44]),
        .O(\prodVal[44]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFB80088)) 
    \prodVal[45]_i_1 
       (.I0(prodVal[45]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(prodVal0__3[45]),
        .O(\prodVal[45]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFB80088)) 
    \prodVal[46]_i_1 
       (.I0(prodVal[46]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(prodVal0__3[46]),
        .O(\prodVal[46]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFB80088)) 
    \prodVal[47]_i_1 
       (.I0(prodVal[47]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(prodVal0__3[47]),
        .O(\prodVal[47]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFB80088)) 
    \prodVal[48]_i_1 
       (.I0(prodVal[48]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(prodVal0__3[48]),
        .O(\prodVal[48]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFB80088)) 
    \prodVal[49]_i_1 
       (.I0(prodVal[49]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(prodVal0__3[49]),
        .O(\prodVal[49]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFB80088)) 
    \prodVal[4]_i_1 
       (.I0(prodVal[4]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(prodVal0__1_n_101),
        .O(\prodVal[4]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFB80088)) 
    \prodVal[50]_i_1 
       (.I0(prodVal[50]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(prodVal0__3[50]),
        .O(\prodVal[50]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFB80088)) 
    \prodVal[51]_i_1 
       (.I0(prodVal[51]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(prodVal0__3[51]),
        .O(\prodVal[51]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFB80088)) 
    \prodVal[52]_i_1 
       (.I0(prodVal[52]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(prodVal0__3[52]),
        .O(\prodVal[52]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFB80088)) 
    \prodVal[53]_i_1 
       (.I0(prodVal[53]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(prodVal0__3[53]),
        .O(\prodVal[53]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFB80088)) 
    \prodVal[54]_i_1 
       (.I0(prodVal[54]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(prodVal0__3[54]),
        .O(\prodVal[54]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFB80088)) 
    \prodVal[55]_i_1 
       (.I0(prodVal[55]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(prodVal0__3[55]),
        .O(\prodVal[55]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFB80088)) 
    \prodVal[56]_i_1 
       (.I0(prodVal[56]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(prodVal0__3[56]),
        .O(\prodVal[56]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFB80088)) 
    \prodVal[57]_i_1 
       (.I0(prodVal[57]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(prodVal0__3[57]),
        .O(\prodVal[57]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFB80088)) 
    \prodVal[58]_i_1 
       (.I0(prodVal[58]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(prodVal0__3[58]),
        .O(\prodVal[58]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFB80088)) 
    \prodVal[59]_i_1 
       (.I0(prodVal[59]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(prodVal0__3[59]),
        .O(\prodVal[59]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFB80088)) 
    \prodVal[5]_i_1 
       (.I0(prodVal[5]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(prodVal0__1_n_100),
        .O(\prodVal[5]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFB80088)) 
    \prodVal[60]_i_1 
       (.I0(prodVal[60]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(prodVal0__3[60]),
        .O(\prodVal[60]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFB80088)) 
    \prodVal[61]_i_1 
       (.I0(prodVal[61]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(prodVal0__3[61]),
        .O(\prodVal[61]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFB80088)) 
    \prodVal[62]_i_1 
       (.I0(prodVal[62]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(prodVal0__3[62]),
        .O(\prodVal[62]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFB80088)) 
    \prodVal[63]_i_1 
       (.I0(prodVal[63]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(prodVal0__3[63]),
        .O(\prodVal[63]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFB80088)) 
    \prodVal[6]_i_1 
       (.I0(prodVal[6]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(prodVal0__1_n_99),
        .O(\prodVal[6]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFB80088)) 
    \prodVal[7]_i_1 
       (.I0(prodVal[7]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(prodVal0__1_n_98),
        .O(\prodVal[7]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFB80088)) 
    \prodVal[8]_i_1 
       (.I0(prodVal[8]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(prodVal0__1_n_97),
        .O(\prodVal[8]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFB80088)) 
    \prodVal[9]_i_1 
       (.I0(prodVal[9]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(prodVal0__1_n_96),
        .O(\prodVal[9]_i_1_n_0 ));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \prodVal_reg[0] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\prodVal[0]_i_1_n_0 ),
        .Q(prodVal[0]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \prodVal_reg[10] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\prodVal[10]_i_1_n_0 ),
        .Q(prodVal[10]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \prodVal_reg[11] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\prodVal[11]_i_1_n_0 ),
        .Q(prodVal[11]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \prodVal_reg[12] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\prodVal[12]_i_1_n_0 ),
        .Q(prodVal[12]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \prodVal_reg[13] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\prodVal[13]_i_1_n_0 ),
        .Q(prodVal[13]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \prodVal_reg[14] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\prodVal[14]_i_1_n_0 ),
        .Q(prodVal[14]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \prodVal_reg[15] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\prodVal[15]_i_1_n_0 ),
        .Q(prodVal[15]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \prodVal_reg[16] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\prodVal[16]_i_1_n_0 ),
        .Q(prodVal[16]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \prodVal_reg[17] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\prodVal[17]_i_1_n_0 ),
        .Q(prodVal[17]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \prodVal_reg[18] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\prodVal[18]_i_1_n_0 ),
        .Q(prodVal[18]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \prodVal_reg[19] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\prodVal[19]_i_1_n_0 ),
        .Q(prodVal[19]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \prodVal_reg[1] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\prodVal[1]_i_1_n_0 ),
        .Q(prodVal[1]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \prodVal_reg[20] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\prodVal[20]_i_1_n_0 ),
        .Q(prodVal[20]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \prodVal_reg[21] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\prodVal[21]_i_1_n_0 ),
        .Q(prodVal[21]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \prodVal_reg[22] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\prodVal[22]_i_1_n_0 ),
        .Q(prodVal[22]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \prodVal_reg[23] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\prodVal[23]_i_1_n_0 ),
        .Q(prodVal[23]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \prodVal_reg[24] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\prodVal[24]_i_1_n_0 ),
        .Q(prodVal[24]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \prodVal_reg[25] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\prodVal[25]_i_1_n_0 ),
        .Q(prodVal[25]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \prodVal_reg[26] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\prodVal[26]_i_1_n_0 ),
        .Q(prodVal[26]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \prodVal_reg[27] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\prodVal[27]_i_1_n_0 ),
        .Q(prodVal[27]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \prodVal_reg[28] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\prodVal[28]_i_1_n_0 ),
        .Q(prodVal[28]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \prodVal_reg[29] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\prodVal[29]_i_1_n_0 ),
        .Q(prodVal[29]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \prodVal_reg[2] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\prodVal[2]_i_1_n_0 ),
        .Q(prodVal[2]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \prodVal_reg[30] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\prodVal[30]_i_1_n_0 ),
        .Q(prodVal[30]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \prodVal_reg[31] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\prodVal[31]_i_1_n_0 ),
        .Q(prodVal[31]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \prodVal_reg[32] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\prodVal[32]_i_1_n_0 ),
        .Q(prodVal[32]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \prodVal_reg[33] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\prodVal[33]_i_1_n_0 ),
        .Q(prodVal[33]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \prodVal_reg[34] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\prodVal[34]_i_1_n_0 ),
        .Q(prodVal[34]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \prodVal_reg[35] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\prodVal[35]_i_1_n_0 ),
        .Q(prodVal[35]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \prodVal_reg[36] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\prodVal[36]_i_1_n_0 ),
        .Q(prodVal[36]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \prodVal_reg[37] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\prodVal[37]_i_1_n_0 ),
        .Q(prodVal[37]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \prodVal_reg[38] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\prodVal[38]_i_1_n_0 ),
        .Q(prodVal[38]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \prodVal_reg[39] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\prodVal[39]_i_1_n_0 ),
        .Q(prodVal[39]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \prodVal_reg[3] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\prodVal[3]_i_1_n_0 ),
        .Q(prodVal[3]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \prodVal_reg[40] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\prodVal[40]_i_1_n_0 ),
        .Q(prodVal[40]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \prodVal_reg[41] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\prodVal[41]_i_1_n_0 ),
        .Q(prodVal[41]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \prodVal_reg[42] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\prodVal[42]_i_1_n_0 ),
        .Q(prodVal[42]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \prodVal_reg[43] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\prodVal[43]_i_1_n_0 ),
        .Q(prodVal[43]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \prodVal_reg[44] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\prodVal[44]_i_1_n_0 ),
        .Q(prodVal[44]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \prodVal_reg[45] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\prodVal[45]_i_1_n_0 ),
        .Q(prodVal[45]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \prodVal_reg[46] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\prodVal[46]_i_1_n_0 ),
        .Q(prodVal[46]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \prodVal_reg[47] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\prodVal[47]_i_1_n_0 ),
        .Q(prodVal[47]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \prodVal_reg[48] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\prodVal[48]_i_1_n_0 ),
        .Q(prodVal[48]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \prodVal_reg[49] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\prodVal[49]_i_1_n_0 ),
        .Q(prodVal[49]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \prodVal_reg[4] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\prodVal[4]_i_1_n_0 ),
        .Q(prodVal[4]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \prodVal_reg[50] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\prodVal[50]_i_1_n_0 ),
        .Q(prodVal[50]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \prodVal_reg[51] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\prodVal[51]_i_1_n_0 ),
        .Q(prodVal[51]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \prodVal_reg[52] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\prodVal[52]_i_1_n_0 ),
        .Q(prodVal[52]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \prodVal_reg[53] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\prodVal[53]_i_1_n_0 ),
        .Q(prodVal[53]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \prodVal_reg[54] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\prodVal[54]_i_1_n_0 ),
        .Q(prodVal[54]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \prodVal_reg[55] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\prodVal[55]_i_1_n_0 ),
        .Q(prodVal[55]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \prodVal_reg[56] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\prodVal[56]_i_1_n_0 ),
        .Q(prodVal[56]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \prodVal_reg[57] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\prodVal[57]_i_1_n_0 ),
        .Q(prodVal[57]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \prodVal_reg[58] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\prodVal[58]_i_1_n_0 ),
        .Q(prodVal[58]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \prodVal_reg[59] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\prodVal[59]_i_1_n_0 ),
        .Q(prodVal[59]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \prodVal_reg[5] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\prodVal[5]_i_1_n_0 ),
        .Q(prodVal[5]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \prodVal_reg[60] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\prodVal[60]_i_1_n_0 ),
        .Q(prodVal[60]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \prodVal_reg[61] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\prodVal[61]_i_1_n_0 ),
        .Q(prodVal[61]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \prodVal_reg[62] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\prodVal[62]_i_1_n_0 ),
        .Q(prodVal[62]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \prodVal_reg[63] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\prodVal[63]_i_1_n_0 ),
        .Q(prodVal[63]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \prodVal_reg[6] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\prodVal[6]_i_1_n_0 ),
        .Q(prodVal[6]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \prodVal_reg[7] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\prodVal[7]_i_1_n_0 ),
        .Q(prodVal[7]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \prodVal_reg[8] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\prodVal[8]_i_1_n_0 ),
        .Q(prodVal[8]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \prodVal_reg[9] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\prodVal[9]_i_1_n_0 ),
        .Q(prodVal[9]),
        .R(reset_IBUF));
  LUT5 #(
    .INIT(32'h33B80088)) 
    \sumVal[0]_i_1 
       (.I0(sumVal[0]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(\dataOut_reg[3]_i_1_n_7 ),
        .O(\sumVal[0]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h33B80088)) 
    \sumVal[10]_i_1 
       (.I0(sumVal[10]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(\dataOut_reg[11]_i_1_n_5 ),
        .O(\sumVal[10]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h33B80088)) 
    \sumVal[11]_i_1 
       (.I0(sumVal[11]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(\dataOut_reg[11]_i_1_n_4 ),
        .O(\sumVal[11]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h33B80088)) 
    \sumVal[12]_i_1 
       (.I0(sumVal[12]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(\dataOut_reg[15]_i_1_n_7 ),
        .O(\sumVal[12]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h33B80088)) 
    \sumVal[13]_i_1 
       (.I0(sumVal[13]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(\dataOut_reg[15]_i_1_n_6 ),
        .O(\sumVal[13]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h33B80088)) 
    \sumVal[14]_i_1 
       (.I0(sumVal[14]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(\dataOut_reg[15]_i_1_n_5 ),
        .O(\sumVal[14]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h33B80088)) 
    \sumVal[15]_i_1 
       (.I0(sumVal[15]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(\dataOut_reg[15]_i_1_n_4 ),
        .O(\sumVal[15]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h33B80088)) 
    \sumVal[16]_i_1 
       (.I0(sumVal[16]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(\dataOut_reg[19]_i_1_n_7 ),
        .O(\sumVal[16]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h33B80088)) 
    \sumVal[17]_i_1 
       (.I0(sumVal[17]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(\dataOut_reg[19]_i_1_n_6 ),
        .O(\sumVal[17]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h33B80088)) 
    \sumVal[18]_i_1 
       (.I0(sumVal[18]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(\dataOut_reg[19]_i_1_n_5 ),
        .O(\sumVal[18]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h33B80088)) 
    \sumVal[19]_i_1 
       (.I0(sumVal[19]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(\dataOut_reg[19]_i_1_n_4 ),
        .O(\sumVal[19]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h33B80088)) 
    \sumVal[1]_i_1 
       (.I0(sumVal[1]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(\dataOut_reg[3]_i_1_n_6 ),
        .O(\sumVal[1]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h33B80088)) 
    \sumVal[20]_i_1 
       (.I0(sumVal[20]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(\dataOut_reg[23]_i_1_n_7 ),
        .O(\sumVal[20]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h33B80088)) 
    \sumVal[21]_i_1 
       (.I0(sumVal[21]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(\dataOut_reg[23]_i_1_n_6 ),
        .O(\sumVal[21]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h33B80088)) 
    \sumVal[22]_i_1 
       (.I0(sumVal[22]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(\dataOut_reg[23]_i_1_n_5 ),
        .O(\sumVal[22]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h33B80088)) 
    \sumVal[23]_i_1 
       (.I0(sumVal[23]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(\dataOut_reg[23]_i_1_n_4 ),
        .O(\sumVal[23]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h33B80088)) 
    \sumVal[24]_i_1 
       (.I0(sumVal[24]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(\dataOut_reg[27]_i_1_n_7 ),
        .O(\sumVal[24]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h33B80088)) 
    \sumVal[25]_i_1 
       (.I0(sumVal[25]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(\dataOut_reg[27]_i_1_n_6 ),
        .O(\sumVal[25]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h33B80088)) 
    \sumVal[26]_i_1 
       (.I0(sumVal[26]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(\dataOut_reg[27]_i_1_n_5 ),
        .O(\sumVal[26]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h33B80088)) 
    \sumVal[27]_i_1 
       (.I0(sumVal[27]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(\dataOut_reg[27]_i_1_n_4 ),
        .O(\sumVal[27]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h33B80088)) 
    \sumVal[28]_i_1 
       (.I0(sumVal[28]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(\dataOut_reg[31]_i_1_n_7 ),
        .O(\sumVal[28]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h33B80088)) 
    \sumVal[29]_i_1 
       (.I0(sumVal[29]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(\dataOut_reg[31]_i_1_n_6 ),
        .O(\sumVal[29]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h33B80088)) 
    \sumVal[2]_i_1 
       (.I0(sumVal[2]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(\dataOut_reg[3]_i_1_n_5 ),
        .O(\sumVal[2]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h33B80088)) 
    \sumVal[30]_i_1 
       (.I0(sumVal[30]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(\dataOut_reg[31]_i_1_n_5 ),
        .O(\sumVal[30]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h33B80088)) 
    \sumVal[31]_i_1 
       (.I0(sumVal[31]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(\dataOut_reg[31]_i_1_n_4 ),
        .O(\sumVal[31]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h33B80088)) 
    \sumVal[32]_i_1 
       (.I0(sumVal[32]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(\dataOut_reg[35]_i_1_n_7 ),
        .O(\sumVal[32]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h33B80088)) 
    \sumVal[33]_i_1 
       (.I0(sumVal[33]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(\dataOut_reg[35]_i_1_n_6 ),
        .O(\sumVal[33]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h33B80088)) 
    \sumVal[34]_i_1 
       (.I0(sumVal[34]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(\dataOut_reg[35]_i_1_n_5 ),
        .O(\sumVal[34]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h33B80088)) 
    \sumVal[35]_i_1 
       (.I0(sumVal[35]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(\dataOut_reg[35]_i_1_n_4 ),
        .O(\sumVal[35]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h33B80088)) 
    \sumVal[36]_i_1 
       (.I0(sumVal[36]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(\dataOut_reg[39]_i_1_n_7 ),
        .O(\sumVal[36]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h33B80088)) 
    \sumVal[37]_i_1 
       (.I0(sumVal[37]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(\dataOut_reg[39]_i_1_n_6 ),
        .O(\sumVal[37]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h33B80088)) 
    \sumVal[38]_i_1 
       (.I0(sumVal[38]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(\dataOut_reg[39]_i_1_n_5 ),
        .O(\sumVal[38]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h33B80088)) 
    \sumVal[39]_i_1 
       (.I0(sumVal[39]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(\dataOut_reg[39]_i_1_n_4 ),
        .O(\sumVal[39]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h33B80088)) 
    \sumVal[3]_i_1 
       (.I0(sumVal[3]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(\dataOut_reg[3]_i_1_n_4 ),
        .O(\sumVal[3]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h33B80088)) 
    \sumVal[40]_i_1 
       (.I0(sumVal[40]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(\dataOut_reg[43]_i_1_n_7 ),
        .O(\sumVal[40]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h33B80088)) 
    \sumVal[41]_i_1 
       (.I0(sumVal[41]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(\dataOut_reg[43]_i_1_n_6 ),
        .O(\sumVal[41]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h33B80088)) 
    \sumVal[42]_i_1 
       (.I0(sumVal[42]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(\dataOut_reg[43]_i_1_n_5 ),
        .O(\sumVal[42]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h33B80088)) 
    \sumVal[43]_i_1 
       (.I0(sumVal[43]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(\dataOut_reg[43]_i_1_n_4 ),
        .O(\sumVal[43]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h33B80088)) 
    \sumVal[44]_i_1 
       (.I0(sumVal[44]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(\dataOut_reg[47]_i_1_n_7 ),
        .O(\sumVal[44]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h33B80088)) 
    \sumVal[45]_i_1 
       (.I0(sumVal[45]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(\dataOut_reg[47]_i_1_n_6 ),
        .O(\sumVal[45]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h33B80088)) 
    \sumVal[46]_i_1 
       (.I0(sumVal[46]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(\dataOut_reg[47]_i_1_n_5 ),
        .O(\sumVal[46]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h33B80088)) 
    \sumVal[47]_i_1 
       (.I0(sumVal[47]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(\dataOut_reg[47]_i_1_n_4 ),
        .O(\sumVal[47]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h33B80088)) 
    \sumVal[48]_i_1 
       (.I0(sumVal[48]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(\dataOut_reg[51]_i_1_n_7 ),
        .O(\sumVal[48]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h33B80088)) 
    \sumVal[49]_i_1 
       (.I0(sumVal[49]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(\dataOut_reg[51]_i_1_n_6 ),
        .O(\sumVal[49]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h33B80088)) 
    \sumVal[4]_i_1 
       (.I0(sumVal[4]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(\dataOut_reg[7]_i_1_n_7 ),
        .O(\sumVal[4]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h33B80088)) 
    \sumVal[50]_i_1 
       (.I0(sumVal[50]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(\dataOut_reg[51]_i_1_n_5 ),
        .O(\sumVal[50]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h33B80088)) 
    \sumVal[51]_i_1 
       (.I0(sumVal[51]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(\dataOut_reg[51]_i_1_n_4 ),
        .O(\sumVal[51]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h33B80088)) 
    \sumVal[52]_i_1 
       (.I0(sumVal[52]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(\dataOut_reg[55]_i_1_n_7 ),
        .O(\sumVal[52]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h33B80088)) 
    \sumVal[53]_i_1 
       (.I0(sumVal[53]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(\dataOut_reg[55]_i_1_n_6 ),
        .O(\sumVal[53]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h33B80088)) 
    \sumVal[54]_i_1 
       (.I0(sumVal[54]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(\dataOut_reg[55]_i_1_n_5 ),
        .O(\sumVal[54]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h33B80088)) 
    \sumVal[55]_i_1 
       (.I0(sumVal[55]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(\dataOut_reg[55]_i_1_n_4 ),
        .O(\sumVal[55]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h33B80088)) 
    \sumVal[56]_i_1 
       (.I0(sumVal[56]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(\dataOut_reg[59]_i_1_n_7 ),
        .O(\sumVal[56]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h33B80088)) 
    \sumVal[57]_i_1 
       (.I0(sumVal[57]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(\dataOut_reg[59]_i_1_n_6 ),
        .O(\sumVal[57]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h33B80088)) 
    \sumVal[58]_i_1 
       (.I0(sumVal[58]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(\dataOut_reg[59]_i_1_n_5 ),
        .O(\sumVal[58]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h33B80088)) 
    \sumVal[59]_i_1 
       (.I0(sumVal[59]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(\dataOut_reg[59]_i_1_n_4 ),
        .O(\sumVal[59]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h33B80088)) 
    \sumVal[5]_i_1 
       (.I0(sumVal[5]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(\dataOut_reg[7]_i_1_n_6 ),
        .O(\sumVal[5]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h33B80088)) 
    \sumVal[60]_i_1 
       (.I0(sumVal[60]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(\dataOut_reg[63]_i_1_n_7 ),
        .O(\sumVal[60]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h33B80088)) 
    \sumVal[61]_i_1 
       (.I0(sumVal[61]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(\dataOut_reg[63]_i_1_n_6 ),
        .O(\sumVal[61]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h33B80088)) 
    \sumVal[62]_i_1 
       (.I0(sumVal[62]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(\dataOut_reg[63]_i_1_n_5 ),
        .O(\sumVal[62]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h33B80088)) 
    \sumVal[63]_i_1 
       (.I0(sumVal[63]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(\dataOut_reg[63]_i_1_n_4 ),
        .O(\sumVal[63]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h33B80088)) 
    \sumVal[64]_i_1 
       (.I0(sumVal[64]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(\dataOut_reg[64]_i_2_n_7 ),
        .O(\sumVal[64]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h33B80088)) 
    \sumVal[6]_i_1 
       (.I0(sumVal[6]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(\dataOut_reg[7]_i_1_n_5 ),
        .O(\sumVal[6]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h33B80088)) 
    \sumVal[7]_i_1 
       (.I0(sumVal[7]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(\dataOut_reg[7]_i_1_n_4 ),
        .O(\sumVal[7]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h33B80088)) 
    \sumVal[8]_i_1 
       (.I0(sumVal[8]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(\dataOut_reg[11]_i_1_n_7 ),
        .O(\sumVal[8]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h33B80088)) 
    \sumVal[9]_i_1 
       (.I0(sumVal[9]),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(begin_ops_internal2_reg),
        .I4(\dataOut_reg[11]_i_1_n_6 ),
        .O(\sumVal[9]_i_1_n_0 ));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sumVal_reg[0] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\sumVal[0]_i_1_n_0 ),
        .Q(sumVal[0]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sumVal_reg[10] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\sumVal[10]_i_1_n_0 ),
        .Q(sumVal[10]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sumVal_reg[11] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\sumVal[11]_i_1_n_0 ),
        .Q(sumVal[11]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sumVal_reg[12] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\sumVal[12]_i_1_n_0 ),
        .Q(sumVal[12]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sumVal_reg[13] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\sumVal[13]_i_1_n_0 ),
        .Q(sumVal[13]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sumVal_reg[14] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\sumVal[14]_i_1_n_0 ),
        .Q(sumVal[14]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sumVal_reg[15] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\sumVal[15]_i_1_n_0 ),
        .Q(sumVal[15]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sumVal_reg[16] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\sumVal[16]_i_1_n_0 ),
        .Q(sumVal[16]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sumVal_reg[17] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\sumVal[17]_i_1_n_0 ),
        .Q(sumVal[17]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sumVal_reg[18] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\sumVal[18]_i_1_n_0 ),
        .Q(sumVal[18]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sumVal_reg[19] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\sumVal[19]_i_1_n_0 ),
        .Q(sumVal[19]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sumVal_reg[1] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\sumVal[1]_i_1_n_0 ),
        .Q(sumVal[1]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sumVal_reg[20] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\sumVal[20]_i_1_n_0 ),
        .Q(sumVal[20]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sumVal_reg[21] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\sumVal[21]_i_1_n_0 ),
        .Q(sumVal[21]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sumVal_reg[22] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\sumVal[22]_i_1_n_0 ),
        .Q(sumVal[22]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sumVal_reg[23] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\sumVal[23]_i_1_n_0 ),
        .Q(sumVal[23]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sumVal_reg[24] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\sumVal[24]_i_1_n_0 ),
        .Q(sumVal[24]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sumVal_reg[25] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\sumVal[25]_i_1_n_0 ),
        .Q(sumVal[25]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sumVal_reg[26] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\sumVal[26]_i_1_n_0 ),
        .Q(sumVal[26]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sumVal_reg[27] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\sumVal[27]_i_1_n_0 ),
        .Q(sumVal[27]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sumVal_reg[28] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\sumVal[28]_i_1_n_0 ),
        .Q(sumVal[28]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sumVal_reg[29] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\sumVal[29]_i_1_n_0 ),
        .Q(sumVal[29]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sumVal_reg[2] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\sumVal[2]_i_1_n_0 ),
        .Q(sumVal[2]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sumVal_reg[30] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\sumVal[30]_i_1_n_0 ),
        .Q(sumVal[30]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sumVal_reg[31] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\sumVal[31]_i_1_n_0 ),
        .Q(sumVal[31]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sumVal_reg[32] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\sumVal[32]_i_1_n_0 ),
        .Q(sumVal[32]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sumVal_reg[33] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\sumVal[33]_i_1_n_0 ),
        .Q(sumVal[33]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sumVal_reg[34] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\sumVal[34]_i_1_n_0 ),
        .Q(sumVal[34]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sumVal_reg[35] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\sumVal[35]_i_1_n_0 ),
        .Q(sumVal[35]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sumVal_reg[36] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\sumVal[36]_i_1_n_0 ),
        .Q(sumVal[36]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sumVal_reg[37] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\sumVal[37]_i_1_n_0 ),
        .Q(sumVal[37]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sumVal_reg[38] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\sumVal[38]_i_1_n_0 ),
        .Q(sumVal[38]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sumVal_reg[39] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\sumVal[39]_i_1_n_0 ),
        .Q(sumVal[39]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sumVal_reg[3] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\sumVal[3]_i_1_n_0 ),
        .Q(sumVal[3]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sumVal_reg[40] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\sumVal[40]_i_1_n_0 ),
        .Q(sumVal[40]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sumVal_reg[41] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\sumVal[41]_i_1_n_0 ),
        .Q(sumVal[41]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sumVal_reg[42] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\sumVal[42]_i_1_n_0 ),
        .Q(sumVal[42]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sumVal_reg[43] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\sumVal[43]_i_1_n_0 ),
        .Q(sumVal[43]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sumVal_reg[44] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\sumVal[44]_i_1_n_0 ),
        .Q(sumVal[44]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sumVal_reg[45] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\sumVal[45]_i_1_n_0 ),
        .Q(sumVal[45]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sumVal_reg[46] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\sumVal[46]_i_1_n_0 ),
        .Q(sumVal[46]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sumVal_reg[47] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\sumVal[47]_i_1_n_0 ),
        .Q(sumVal[47]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sumVal_reg[48] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\sumVal[48]_i_1_n_0 ),
        .Q(sumVal[48]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sumVal_reg[49] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\sumVal[49]_i_1_n_0 ),
        .Q(sumVal[49]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sumVal_reg[4] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\sumVal[4]_i_1_n_0 ),
        .Q(sumVal[4]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sumVal_reg[50] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\sumVal[50]_i_1_n_0 ),
        .Q(sumVal[50]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sumVal_reg[51] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\sumVal[51]_i_1_n_0 ),
        .Q(sumVal[51]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sumVal_reg[52] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\sumVal[52]_i_1_n_0 ),
        .Q(sumVal[52]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sumVal_reg[53] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\sumVal[53]_i_1_n_0 ),
        .Q(sumVal[53]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sumVal_reg[54] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\sumVal[54]_i_1_n_0 ),
        .Q(sumVal[54]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sumVal_reg[55] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\sumVal[55]_i_1_n_0 ),
        .Q(sumVal[55]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sumVal_reg[56] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\sumVal[56]_i_1_n_0 ),
        .Q(sumVal[56]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sumVal_reg[57] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\sumVal[57]_i_1_n_0 ),
        .Q(sumVal[57]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sumVal_reg[58] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\sumVal[58]_i_1_n_0 ),
        .Q(sumVal[58]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sumVal_reg[59] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\sumVal[59]_i_1_n_0 ),
        .Q(sumVal[59]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sumVal_reg[5] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\sumVal[5]_i_1_n_0 ),
        .Q(sumVal[5]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sumVal_reg[60] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\sumVal[60]_i_1_n_0 ),
        .Q(sumVal[60]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sumVal_reg[61] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\sumVal[61]_i_1_n_0 ),
        .Q(sumVal[61]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sumVal_reg[62] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\sumVal[62]_i_1_n_0 ),
        .Q(sumVal[62]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sumVal_reg[63] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\sumVal[63]_i_1_n_0 ),
        .Q(sumVal[63]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sumVal_reg[64] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\sumVal[64]_i_1_n_0 ),
        .Q(sumVal[64]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sumVal_reg[6] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\sumVal[6]_i_1_n_0 ),
        .Q(sumVal[6]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sumVal_reg[7] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\sumVal[7]_i_1_n_0 ),
        .Q(sumVal[7]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sumVal_reg[8] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\sumVal[8]_i_1_n_0 ),
        .Q(sumVal[8]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sumVal_reg[9] 
       (.C(clk_IBUF_BUFG),
        .CE(\currentState[1]_i_1__0_n_0 ),
        .D(\sumVal[9]_i_1_n_0 ),
        .Q(sumVal[9]),
        .R(reset_IBUF));
endmodule

(* dataWidth = "32" *) (* data_addrW = "8" *) (* idle = "3'b000" *) 
(* out_addrW = "8" *) (* s1 = "3'b001" *) (* s2 = "3'b010" *) 
(* s3 = "3'b011" *) (* s4 = "3'b100" *) (* s5 = "3'b101" *) 
(* s6 = "3'b110" *) (* weight_addrW = "8" *) 
(* NotValidForBitStream *)
module PE_wrapper
   (clk,
    reset,
    N,
    f,
    r,
    rd_wr,
    weightAddr,
    dataAddr,
    outAddr,
    cen_outMem,
    weightMem_in,
    dataMem_in,
    start,
    outMem_data,
    done);
  input clk;
  input reset;
  input [31:0]N;
  input [31:0]f;
  input [31:0]r;
  input rd_wr;
  input [7:0]weightAddr;
  input [7:0]dataAddr;
  input [7:0]outAddr;
  input cen_outMem;
  input [31:0]weightMem_in;
  input [31:0]dataMem_in;
  input start;
  output [31:0]outMem_data;
  output done;

  wire [3:3]C;
  wire [31:0]MAC_out;
  wire [31:0]N;
  wire [31:0]N_IBUF;
  wire [7:0]PCOUT;
  (* RTL_KEEP = "true" *) wire begin_ops_internal;
  (* RTL_KEEP = "true" *) wire begin_ops_internal1;
  (* RTL_KEEP = "true" *) wire begin_ops_internal2;
  (* RTL_KEEP = "true" *) wire cen_MemR;
  wire cen_MemR_i_1_n_0;
  (* RTL_KEEP = "true" *) wire cen_dataMemWR;
  wire cen_dataMemWR_i_1_n_0;
  wire cen_outMem;
  wire cen_outMem_IBUF;
  (* RTL_KEEP = "true" *) wire cen_weightMemWR;
  wire cen_weightMemWR_i_1_n_0;
  wire cen_weightMemWR_i_2_n_0;
  wire clk;
  wire clk_IBUF;
  wire clk_IBUF_BUFG;
  (* RTL_KEEP = "true" *) wire [2:0]currentState;
  wire currentState2;
  wire currentState28_in;
  wire [31:1]currentState3;
  wire \currentState[0]_i_1_n_0 ;
  wire \currentState[1]_i_1_n_0 ;
  wire \currentState[1]_i_2_n_0 ;
  wire \currentState[2]_i_1_n_0 ;
  wire [7:0]dataAddr;
  wire [4:0]dataAddr_IBUF;
  (* RTL_KEEP = "true" *) wire [7:0]dataAddr_internal;
  wire [7:0]dataAddr_internal1;
  wire [31:0]dataAddr_internal3;
  wire dataAddr_internal4__0_n_100;
  wire dataAddr_internal4__0_n_101;
  wire dataAddr_internal4__0_n_102;
  wire dataAddr_internal4__0_n_103;
  wire dataAddr_internal4__0_n_104;
  wire dataAddr_internal4__0_n_105;
  wire dataAddr_internal4__0_n_58;
  wire dataAddr_internal4__0_n_59;
  wire dataAddr_internal4__0_n_60;
  wire dataAddr_internal4__0_n_61;
  wire dataAddr_internal4__0_n_62;
  wire dataAddr_internal4__0_n_63;
  wire dataAddr_internal4__0_n_64;
  wire dataAddr_internal4__0_n_65;
  wire dataAddr_internal4__0_n_66;
  wire dataAddr_internal4__0_n_67;
  wire dataAddr_internal4__0_n_68;
  wire dataAddr_internal4__0_n_69;
  wire dataAddr_internal4__0_n_70;
  wire dataAddr_internal4__0_n_71;
  wire dataAddr_internal4__0_n_72;
  wire dataAddr_internal4__0_n_73;
  wire dataAddr_internal4__0_n_74;
  wire dataAddr_internal4__0_n_75;
  wire dataAddr_internal4__0_n_76;
  wire dataAddr_internal4__0_n_77;
  wire dataAddr_internal4__0_n_78;
  wire dataAddr_internal4__0_n_79;
  wire dataAddr_internal4__0_n_80;
  wire dataAddr_internal4__0_n_81;
  wire dataAddr_internal4__0_n_82;
  wire dataAddr_internal4__0_n_83;
  wire dataAddr_internal4__0_n_84;
  wire dataAddr_internal4__0_n_85;
  wire dataAddr_internal4__0_n_86;
  wire dataAddr_internal4__0_n_87;
  wire dataAddr_internal4__0_n_88;
  wire dataAddr_internal4__0_n_89;
  wire dataAddr_internal4__0_n_90;
  wire dataAddr_internal4__0_n_91;
  wire dataAddr_internal4__0_n_92;
  wire dataAddr_internal4__0_n_93;
  wire dataAddr_internal4__0_n_94;
  wire dataAddr_internal4__0_n_95;
  wire dataAddr_internal4__0_n_96;
  wire dataAddr_internal4__0_n_97;
  wire dataAddr_internal4__0_n_98;
  wire dataAddr_internal4__0_n_99;
  wire dataAddr_internal4_n_100;
  wire dataAddr_internal4_n_101;
  wire dataAddr_internal4_n_102;
  wire dataAddr_internal4_n_103;
  wire dataAddr_internal4_n_104;
  wire dataAddr_internal4_n_105;
  wire dataAddr_internal4_n_106;
  wire dataAddr_internal4_n_107;
  wire dataAddr_internal4_n_108;
  wire dataAddr_internal4_n_109;
  wire dataAddr_internal4_n_110;
  wire dataAddr_internal4_n_111;
  wire dataAddr_internal4_n_112;
  wire dataAddr_internal4_n_113;
  wire dataAddr_internal4_n_114;
  wire dataAddr_internal4_n_115;
  wire dataAddr_internal4_n_116;
  wire dataAddr_internal4_n_117;
  wire dataAddr_internal4_n_118;
  wire dataAddr_internal4_n_119;
  wire dataAddr_internal4_n_120;
  wire dataAddr_internal4_n_121;
  wire dataAddr_internal4_n_122;
  wire dataAddr_internal4_n_123;
  wire dataAddr_internal4_n_124;
  wire dataAddr_internal4_n_125;
  wire dataAddr_internal4_n_126;
  wire dataAddr_internal4_n_127;
  wire dataAddr_internal4_n_128;
  wire dataAddr_internal4_n_129;
  wire dataAddr_internal4_n_130;
  wire dataAddr_internal4_n_131;
  wire dataAddr_internal4_n_132;
  wire dataAddr_internal4_n_133;
  wire dataAddr_internal4_n_134;
  wire dataAddr_internal4_n_135;
  wire dataAddr_internal4_n_136;
  wire dataAddr_internal4_n_137;
  wire dataAddr_internal4_n_138;
  wire dataAddr_internal4_n_139;
  wire dataAddr_internal4_n_140;
  wire dataAddr_internal4_n_141;
  wire dataAddr_internal4_n_142;
  wire dataAddr_internal4_n_143;
  wire dataAddr_internal4_n_144;
  wire dataAddr_internal4_n_145;
  wire dataAddr_internal4_n_146;
  wire dataAddr_internal4_n_147;
  wire dataAddr_internal4_n_148;
  wire dataAddr_internal4_n_149;
  wire dataAddr_internal4_n_150;
  wire dataAddr_internal4_n_151;
  wire dataAddr_internal4_n_152;
  wire dataAddr_internal4_n_153;
  wire dataAddr_internal4_n_58;
  wire dataAddr_internal4_n_59;
  wire dataAddr_internal4_n_60;
  wire dataAddr_internal4_n_61;
  wire dataAddr_internal4_n_62;
  wire dataAddr_internal4_n_63;
  wire dataAddr_internal4_n_64;
  wire dataAddr_internal4_n_65;
  wire dataAddr_internal4_n_66;
  wire dataAddr_internal4_n_67;
  wire dataAddr_internal4_n_68;
  wire dataAddr_internal4_n_69;
  wire dataAddr_internal4_n_70;
  wire dataAddr_internal4_n_71;
  wire dataAddr_internal4_n_72;
  wire dataAddr_internal4_n_73;
  wire dataAddr_internal4_n_74;
  wire dataAddr_internal4_n_75;
  wire dataAddr_internal4_n_76;
  wire dataAddr_internal4_n_77;
  wire dataAddr_internal4_n_78;
  wire dataAddr_internal4_n_79;
  wire dataAddr_internal4_n_80;
  wire dataAddr_internal4_n_81;
  wire dataAddr_internal4_n_82;
  wire dataAddr_internal4_n_83;
  wire dataAddr_internal4_n_84;
  wire dataAddr_internal4_n_85;
  wire dataAddr_internal4_n_86;
  wire dataAddr_internal4_n_87;
  wire dataAddr_internal4_n_88;
  wire dataAddr_internal4_n_89;
  wire dataAddr_internal4_n_90;
  wire dataAddr_internal4_n_91;
  wire dataAddr_internal4_n_92;
  wire dataAddr_internal4_n_93;
  wire dataAddr_internal4_n_94;
  wire dataAddr_internal4_n_95;
  wire dataAddr_internal4_n_96;
  wire dataAddr_internal4_n_97;
  wire dataAddr_internal4_n_98;
  wire dataAddr_internal4_n_99;
  wire dataAddr_internal_inferred_i_100_n_0;
  wire dataAddr_internal_inferred_i_100_n_1;
  wire dataAddr_internal_inferred_i_100_n_2;
  wire dataAddr_internal_inferred_i_100_n_3;
  wire dataAddr_internal_inferred_i_101_n_0;
  wire dataAddr_internal_inferred_i_101_n_1;
  wire dataAddr_internal_inferred_i_101_n_2;
  wire dataAddr_internal_inferred_i_101_n_3;
  wire dataAddr_internal_inferred_i_102_n_0;
  wire dataAddr_internal_inferred_i_103_n_0;
  wire dataAddr_internal_inferred_i_104_n_0;
  wire dataAddr_internal_inferred_i_105_n_0;
  wire dataAddr_internal_inferred_i_106_n_0;
  wire dataAddr_internal_inferred_i_106_n_1;
  wire dataAddr_internal_inferred_i_106_n_2;
  wire dataAddr_internal_inferred_i_106_n_3;
  wire dataAddr_internal_inferred_i_107_n_0;
  wire dataAddr_internal_inferred_i_107_n_1;
  wire dataAddr_internal_inferred_i_107_n_2;
  wire dataAddr_internal_inferred_i_107_n_3;
  wire dataAddr_internal_inferred_i_108_n_0;
  wire dataAddr_internal_inferred_i_109_n_0;
  wire dataAddr_internal_inferred_i_10_n_0;
  wire dataAddr_internal_inferred_i_10_n_1;
  wire dataAddr_internal_inferred_i_10_n_2;
  wire dataAddr_internal_inferred_i_10_n_3;
  wire dataAddr_internal_inferred_i_110_n_0;
  wire dataAddr_internal_inferred_i_111_n_0;
  wire dataAddr_internal_inferred_i_112_n_0;
  wire dataAddr_internal_inferred_i_112_n_1;
  wire dataAddr_internal_inferred_i_112_n_2;
  wire dataAddr_internal_inferred_i_112_n_3;
  wire dataAddr_internal_inferred_i_113_n_0;
  wire dataAddr_internal_inferred_i_113_n_1;
  wire dataAddr_internal_inferred_i_113_n_2;
  wire dataAddr_internal_inferred_i_113_n_3;
  wire dataAddr_internal_inferred_i_114_n_0;
  wire dataAddr_internal_inferred_i_115_n_0;
  wire dataAddr_internal_inferred_i_116_n_0;
  wire dataAddr_internal_inferred_i_117_n_0;
  wire dataAddr_internal_inferred_i_118_n_0;
  wire dataAddr_internal_inferred_i_118_n_1;
  wire dataAddr_internal_inferred_i_118_n_2;
  wire dataAddr_internal_inferred_i_118_n_3;
  wire dataAddr_internal_inferred_i_119_n_0;
  wire dataAddr_internal_inferred_i_11_n_0;
  wire dataAddr_internal_inferred_i_120_n_0;
  wire dataAddr_internal_inferred_i_121_n_0;
  wire dataAddr_internal_inferred_i_122_n_0;
  wire dataAddr_internal_inferred_i_123_n_0;
  wire dataAddr_internal_inferred_i_124_n_0;
  wire dataAddr_internal_inferred_i_125_n_0;
  wire dataAddr_internal_inferred_i_126_n_0;
  wire dataAddr_internal_inferred_i_127_n_0;
  wire dataAddr_internal_inferred_i_128_n_0;
  wire dataAddr_internal_inferred_i_129_n_0;
  wire dataAddr_internal_inferred_i_12_n_0;
  wire dataAddr_internal_inferred_i_130_n_0;
  wire dataAddr_internal_inferred_i_13_n_0;
  wire dataAddr_internal_inferred_i_14_n_0;
  wire dataAddr_internal_inferred_i_15_n_0;
  wire dataAddr_internal_inferred_i_16_n_0;
  wire dataAddr_internal_inferred_i_17_n_0;
  wire dataAddr_internal_inferred_i_18_n_0;
  wire dataAddr_internal_inferred_i_20_n_0;
  wire dataAddr_internal_inferred_i_20_n_1;
  wire dataAddr_internal_inferred_i_20_n_2;
  wire dataAddr_internal_inferred_i_20_n_3;
  wire dataAddr_internal_inferred_i_21_n_0;
  wire dataAddr_internal_inferred_i_21_n_1;
  wire dataAddr_internal_inferred_i_21_n_2;
  wire dataAddr_internal_inferred_i_21_n_3;
  wire dataAddr_internal_inferred_i_22_n_0;
  wire dataAddr_internal_inferred_i_23_n_0;
  wire dataAddr_internal_inferred_i_23_n_1;
  wire dataAddr_internal_inferred_i_23_n_2;
  wire dataAddr_internal_inferred_i_23_n_3;
  wire dataAddr_internal_inferred_i_23_n_4;
  wire dataAddr_internal_inferred_i_23_n_5;
  wire dataAddr_internal_inferred_i_23_n_6;
  wire dataAddr_internal_inferred_i_25_n_0;
  wire dataAddr_internal_inferred_i_26_n_0;
  wire dataAddr_internal_inferred_i_27_n_0;
  wire dataAddr_internal_inferred_i_28_n_0;
  wire dataAddr_internal_inferred_i_29_n_0;
  wire dataAddr_internal_inferred_i_29_n_1;
  wire dataAddr_internal_inferred_i_29_n_2;
  wire dataAddr_internal_inferred_i_29_n_3;
  wire dataAddr_internal_inferred_i_30_n_0;
  wire dataAddr_internal_inferred_i_30_n_1;
  wire dataAddr_internal_inferred_i_30_n_2;
  wire dataAddr_internal_inferred_i_30_n_3;
  wire dataAddr_internal_inferred_i_30_n_4;
  wire dataAddr_internal_inferred_i_30_n_5;
  wire dataAddr_internal_inferred_i_30_n_6;
  wire dataAddr_internal_inferred_i_30_n_7;
  wire dataAddr_internal_inferred_i_31_n_0;
  wire dataAddr_internal_inferred_i_32_n_0;
  wire dataAddr_internal_inferred_i_33_n_0;
  wire dataAddr_internal_inferred_i_34_n_7;
  wire dataAddr_internal_inferred_i_35_n_0;
  wire dataAddr_internal_inferred_i_36_n_1;
  wire dataAddr_internal_inferred_i_36_n_2;
  wire dataAddr_internal_inferred_i_36_n_3;
  wire dataAddr_internal_inferred_i_36_n_4;
  wire dataAddr_internal_inferred_i_36_n_5;
  wire dataAddr_internal_inferred_i_36_n_6;
  wire dataAddr_internal_inferred_i_36_n_7;
  wire dataAddr_internal_inferred_i_37_n_0;
  wire dataAddr_internal_inferred_i_38_n_0;
  wire dataAddr_internal_inferred_i_39_n_0;
  wire dataAddr_internal_inferred_i_40_n_0;
  wire dataAddr_internal_inferred_i_41_n_0;
  wire dataAddr_internal_inferred_i_41_n_1;
  wire dataAddr_internal_inferred_i_41_n_2;
  wire dataAddr_internal_inferred_i_41_n_3;
  wire dataAddr_internal_inferred_i_41_n_4;
  wire dataAddr_internal_inferred_i_41_n_5;
  wire dataAddr_internal_inferred_i_41_n_6;
  wire dataAddr_internal_inferred_i_41_n_7;
  wire dataAddr_internal_inferred_i_42_n_0;
  wire dataAddr_internal_inferred_i_42_n_1;
  wire dataAddr_internal_inferred_i_42_n_2;
  wire dataAddr_internal_inferred_i_42_n_3;
  wire dataAddr_internal_inferred_i_43_n_1;
  wire dataAddr_internal_inferred_i_43_n_2;
  wire dataAddr_internal_inferred_i_43_n_3;
  wire dataAddr_internal_inferred_i_44_n_0;
  wire dataAddr_internal_inferred_i_45_n_0;
  wire dataAddr_internal_inferred_i_46_n_0;
  wire dataAddr_internal_inferred_i_47_n_0;
  wire dataAddr_internal_inferred_i_48_n_0;
  wire dataAddr_internal_inferred_i_49_n_0;
  wire dataAddr_internal_inferred_i_50_n_0;
  wire dataAddr_internal_inferred_i_51_n_0;
  wire dataAddr_internal_inferred_i_52_n_0;
  wire dataAddr_internal_inferred_i_53_n_0;
  wire dataAddr_internal_inferred_i_54_n_0;
  wire dataAddr_internal_inferred_i_55_n_0;
  wire dataAddr_internal_inferred_i_56_n_0;
  wire dataAddr_internal_inferred_i_57_n_0;
  wire dataAddr_internal_inferred_i_58_n_0;
  wire dataAddr_internal_inferred_i_59_n_0;
  wire dataAddr_internal_inferred_i_60_n_0;
  wire dataAddr_internal_inferred_i_61_n_0;
  wire dataAddr_internal_inferred_i_62_n_0;
  wire dataAddr_internal_inferred_i_63_n_0;
  wire dataAddr_internal_inferred_i_64_n_0;
  wire dataAddr_internal_inferred_i_65_n_0;
  wire dataAddr_internal_inferred_i_66_n_0;
  wire dataAddr_internal_inferred_i_67_n_0;
  wire dataAddr_internal_inferred_i_68_n_0;
  wire dataAddr_internal_inferred_i_69_n_0;
  wire dataAddr_internal_inferred_i_70_n_0;
  wire dataAddr_internal_inferred_i_70_n_1;
  wire dataAddr_internal_inferred_i_70_n_2;
  wire dataAddr_internal_inferred_i_70_n_3;
  wire dataAddr_internal_inferred_i_71_n_0;
  wire dataAddr_internal_inferred_i_71_n_1;
  wire dataAddr_internal_inferred_i_71_n_2;
  wire dataAddr_internal_inferred_i_71_n_3;
  wire dataAddr_internal_inferred_i_72_n_0;
  wire dataAddr_internal_inferred_i_73_n_0;
  wire dataAddr_internal_inferred_i_74_n_0;
  wire dataAddr_internal_inferred_i_75_n_0;
  wire dataAddr_internal_inferred_i_76_n_0;
  wire dataAddr_internal_inferred_i_77_n_7;
  wire dataAddr_internal_inferred_i_78_n_0;
  wire dataAddr_internal_inferred_i_79_n_0;
  wire dataAddr_internal_inferred_i_80_n_0;
  wire dataAddr_internal_inferred_i_81_n_0;
  wire dataAddr_internal_inferred_i_82_n_0;
  wire dataAddr_internal_inferred_i_83_n_0;
  wire dataAddr_internal_inferred_i_84_n_0;
  wire dataAddr_internal_inferred_i_85_n_0;
  wire dataAddr_internal_inferred_i_85_n_1;
  wire dataAddr_internal_inferred_i_85_n_2;
  wire dataAddr_internal_inferred_i_85_n_3;
  wire dataAddr_internal_inferred_i_86_n_0;
  wire dataAddr_internal_inferred_i_86_n_1;
  wire dataAddr_internal_inferred_i_86_n_2;
  wire dataAddr_internal_inferred_i_86_n_3;
  wire dataAddr_internal_inferred_i_87_n_0;
  wire dataAddr_internal_inferred_i_88_n_0;
  wire dataAddr_internal_inferred_i_89_n_0;
  wire dataAddr_internal_inferred_i_90_n_0;
  wire dataAddr_internal_inferred_i_91_n_0;
  wire dataAddr_internal_inferred_i_92_n_0;
  wire dataAddr_internal_inferred_i_92_n_1;
  wire dataAddr_internal_inferred_i_92_n_2;
  wire dataAddr_internal_inferred_i_92_n_3;
  wire dataAddr_internal_inferred_i_93_n_0;
  wire dataAddr_internal_inferred_i_93_n_1;
  wire dataAddr_internal_inferred_i_93_n_2;
  wire dataAddr_internal_inferred_i_93_n_3;
  wire dataAddr_internal_inferred_i_94_n_0;
  wire dataAddr_internal_inferred_i_95_n_0;
  wire dataAddr_internal_inferred_i_96_n_0;
  wire dataAddr_internal_inferred_i_97_n_0;
  wire dataAddr_internal_inferred_i_98_n_0;
  wire dataAddr_internal_inferred_i_99_n_0;
  wire dataAddr_internal_inferred_i_9_n_1;
  wire dataAddr_internal_inferred_i_9_n_2;
  wire dataAddr_internal_inferred_i_9_n_3;
  wire [31:0]dataMem_in;
  wire [31:0]dataMem_in_IBUF;
  wire [31:0]dataMem_out;
  (* RTL_KEEP = "true" *) wire [31:0]dataMem_out_internal;
  wire done;
  (* RTL_KEEP = "true" *) wire done_internal;
  (* RTL_KEEP = "true" *) wire done_internal1;
  (* RTL_KEEP = "true" *) wire done_internal2;
  (* RTL_KEEP = "true" *) wire done_internal3;
  wire done_internal_i_1_n_0;
  wire done_internal_i_2_n_0;
  (* RTL_KEEP = "true" *) wire end_ops_internal;
  (* RTL_KEEP = "true" *) wire end_ops_internal1;
  (* RTL_KEEP = "true" *) wire end_ops_internal2;
  wire [31:1]end_ops_internal2__0;
  (* RTL_KEEP = "true" *) wire end_ops_internal3;
  (* RTL_KEEP = "true" *) wire end_ops_internal4;
  wire end_ops_internal_inferred_i_10_n_0;
  wire end_ops_internal_inferred_i_10_n_1;
  wire end_ops_internal_inferred_i_10_n_2;
  wire end_ops_internal_inferred_i_10_n_3;
  wire end_ops_internal_inferred_i_11_n_0;
  wire end_ops_internal_inferred_i_12_n_0;
  wire end_ops_internal_inferred_i_13_n_0;
  wire end_ops_internal_inferred_i_14_n_0;
  wire end_ops_internal_inferred_i_15_n_2;
  wire end_ops_internal_inferred_i_15_n_3;
  wire end_ops_internal_inferred_i_16_n_0;
  wire end_ops_internal_inferred_i_16_n_1;
  wire end_ops_internal_inferred_i_16_n_2;
  wire end_ops_internal_inferred_i_16_n_3;
  wire end_ops_internal_inferred_i_17_n_0;
  wire end_ops_internal_inferred_i_17_n_1;
  wire end_ops_internal_inferred_i_17_n_2;
  wire end_ops_internal_inferred_i_17_n_3;
  wire end_ops_internal_inferred_i_18_n_0;
  wire end_ops_internal_inferred_i_19_n_0;
  wire end_ops_internal_inferred_i_20_n_0;
  wire end_ops_internal_inferred_i_21_n_0;
  wire end_ops_internal_inferred_i_22_n_0;
  wire end_ops_internal_inferred_i_22_n_1;
  wire end_ops_internal_inferred_i_22_n_2;
  wire end_ops_internal_inferred_i_22_n_3;
  wire end_ops_internal_inferred_i_23_n_0;
  wire end_ops_internal_inferred_i_23_n_1;
  wire end_ops_internal_inferred_i_23_n_2;
  wire end_ops_internal_inferred_i_23_n_3;
  wire end_ops_internal_inferred_i_24_n_0;
  wire end_ops_internal_inferred_i_24_n_1;
  wire end_ops_internal_inferred_i_24_n_2;
  wire end_ops_internal_inferred_i_24_n_3;
  wire end_ops_internal_inferred_i_25_n_0;
  wire end_ops_internal_inferred_i_26_n_0;
  wire end_ops_internal_inferred_i_27_n_0;
  wire end_ops_internal_inferred_i_28_n_0;
  wire end_ops_internal_inferred_i_29_n_0;
  wire end_ops_internal_inferred_i_2_n_0;
  wire end_ops_internal_inferred_i_30_n_0;
  wire end_ops_internal_inferred_i_31_n_0;
  wire end_ops_internal_inferred_i_32_n_0;
  wire end_ops_internal_inferred_i_33_n_0;
  wire end_ops_internal_inferred_i_34_n_0;
  wire end_ops_internal_inferred_i_35_n_0;
  wire end_ops_internal_inferred_i_36_n_0;
  wire end_ops_internal_inferred_i_36_n_1;
  wire end_ops_internal_inferred_i_36_n_2;
  wire end_ops_internal_inferred_i_36_n_3;
  wire end_ops_internal_inferred_i_37_n_0;
  wire end_ops_internal_inferred_i_37_n_1;
  wire end_ops_internal_inferred_i_37_n_2;
  wire end_ops_internal_inferred_i_37_n_3;
  wire end_ops_internal_inferred_i_38_n_0;
  wire end_ops_internal_inferred_i_39_n_0;
  wire end_ops_internal_inferred_i_3_n_1;
  wire end_ops_internal_inferred_i_3_n_2;
  wire end_ops_internal_inferred_i_3_n_3;
  wire end_ops_internal_inferred_i_40_n_0;
  wire end_ops_internal_inferred_i_41_n_0;
  wire end_ops_internal_inferred_i_42_n_0;
  wire end_ops_internal_inferred_i_43_n_0;
  wire end_ops_internal_inferred_i_44_n_0;
  wire end_ops_internal_inferred_i_45_n_0;
  wire end_ops_internal_inferred_i_46_n_0;
  wire end_ops_internal_inferred_i_47_n_0;
  wire end_ops_internal_inferred_i_48_n_0;
  wire end_ops_internal_inferred_i_49_n_0;
  wire end_ops_internal_inferred_i_4_n_0;
  wire end_ops_internal_inferred_i_50_n_0;
  wire end_ops_internal_inferred_i_51_n_0;
  wire end_ops_internal_inferred_i_52_n_0;
  wire end_ops_internal_inferred_i_53_n_0;
  wire end_ops_internal_inferred_i_54_n_0;
  wire end_ops_internal_inferred_i_55_n_0;
  wire end_ops_internal_inferred_i_56_n_0;
  wire end_ops_internal_inferred_i_57_n_0;
  wire end_ops_internal_inferred_i_5_n_0;
  wire end_ops_internal_inferred_i_6_n_0;
  wire end_ops_internal_inferred_i_6_n_1;
  wire end_ops_internal_inferred_i_6_n_2;
  wire end_ops_internal_inferred_i_6_n_3;
  wire end_ops_internal_inferred_i_7_n_0;
  wire end_ops_internal_inferred_i_8_n_0;
  wire end_ops_internal_inferred_i_9_n_0;
  wire [31:0]f;
  wire [31:0]f_IBUF;
  (* RTL_KEEP = "true" *) wire [7:0]hAddr_cnt;
  wire \hAddr_cnt[0]_i_1_n_0 ;
  wire \hAddr_cnt[1]_i_1_n_0 ;
  wire \hAddr_cnt[2]_i_1_n_0 ;
  wire \hAddr_cnt[3]_i_1_n_0 ;
  wire \hAddr_cnt[4]_i_1_n_0 ;
  wire \hAddr_cnt[4]_i_2_n_0 ;
  wire \hAddr_cnt[5]_i_1_n_0 ;
  wire \hAddr_cnt[5]_i_2_n_0 ;
  wire \hAddr_cnt[6]_i_1_n_0 ;
  wire \hAddr_cnt[7]_i_10_n_0 ;
  wire \hAddr_cnt[7]_i_11_n_0 ;
  wire \hAddr_cnt[7]_i_12_n_0 ;
  wire \hAddr_cnt[7]_i_13_n_0 ;
  wire \hAddr_cnt[7]_i_15_n_0 ;
  wire \hAddr_cnt[7]_i_16_n_0 ;
  wire \hAddr_cnt[7]_i_17_n_0 ;
  wire \hAddr_cnt[7]_i_18_n_0 ;
  wire \hAddr_cnt[7]_i_19_n_0 ;
  wire \hAddr_cnt[7]_i_1_n_0 ;
  wire \hAddr_cnt[7]_i_20_n_0 ;
  wire \hAddr_cnt[7]_i_21_n_0 ;
  wire \hAddr_cnt[7]_i_22_n_0 ;
  wire \hAddr_cnt[7]_i_24_n_0 ;
  wire \hAddr_cnt[7]_i_25_n_0 ;
  wire \hAddr_cnt[7]_i_26_n_0 ;
  wire \hAddr_cnt[7]_i_27_n_0 ;
  wire \hAddr_cnt[7]_i_28_n_0 ;
  wire \hAddr_cnt[7]_i_29_n_0 ;
  wire \hAddr_cnt[7]_i_2_n_0 ;
  wire \hAddr_cnt[7]_i_30_n_0 ;
  wire \hAddr_cnt[7]_i_31_n_0 ;
  wire \hAddr_cnt[7]_i_32_n_0 ;
  wire \hAddr_cnt[7]_i_33_n_0 ;
  wire \hAddr_cnt[7]_i_34_n_0 ;
  wire \hAddr_cnt[7]_i_35_n_0 ;
  wire \hAddr_cnt[7]_i_36_n_0 ;
  wire \hAddr_cnt[7]_i_37_n_0 ;
  wire \hAddr_cnt[7]_i_38_n_0 ;
  wire \hAddr_cnt[7]_i_39_n_0 ;
  wire \hAddr_cnt[7]_i_4_n_0 ;
  wire \hAddr_cnt[7]_i_6_n_0 ;
  wire \hAddr_cnt[7]_i_7_n_0 ;
  wire \hAddr_cnt[7]_i_8_n_0 ;
  wire \hAddr_cnt[7]_i_9_n_0 ;
  wire \hAddr_cnt_reg[7]_i_14_n_0 ;
  wire \hAddr_cnt_reg[7]_i_14_n_1 ;
  wire \hAddr_cnt_reg[7]_i_14_n_2 ;
  wire \hAddr_cnt_reg[7]_i_14_n_3 ;
  wire \hAddr_cnt_reg[7]_i_23_n_0 ;
  wire \hAddr_cnt_reg[7]_i_23_n_1 ;
  wire \hAddr_cnt_reg[7]_i_23_n_2 ;
  wire \hAddr_cnt_reg[7]_i_23_n_3 ;
  wire \hAddr_cnt_reg[7]_i_3_n_1 ;
  wire \hAddr_cnt_reg[7]_i_3_n_2 ;
  wire \hAddr_cnt_reg[7]_i_3_n_3 ;
  wire \hAddr_cnt_reg[7]_i_5_n_0 ;
  wire \hAddr_cnt_reg[7]_i_5_n_1 ;
  wire \hAddr_cnt_reg[7]_i_5_n_2 ;
  wire \hAddr_cnt_reg[7]_i_5_n_3 ;
  wire [7:0]outAddr;
  wire [4:0]outAddr_IBUF;
  (* RTL_KEEP = "true" *) wire [7:0]outAddr_internal;
  wire \outAddr_internal[0]_i_1_n_0 ;
  wire \outAddr_internal[1]_i_1_n_0 ;
  wire \outAddr_internal[2]_i_1_n_0 ;
  wire \outAddr_internal[3]_i_1_n_0 ;
  wire \outAddr_internal[4]_i_1_n_0 ;
  wire \outAddr_internal[5]_i_1_n_0 ;
  wire \outAddr_internal[6]_i_1_n_0 ;
  wire \outAddr_internal[7]_i_1_n_0 ;
  wire \outAddr_internal[7]_i_2_n_0 ;
  wire [31:0]outMem_data;
  wire [31:0]outMem_data_OBUF;
  wire [31:0]r;
  wire [31:0]r_IBUF;
  wire rd_wr;
  wire rd_wr_IBUF;
  wire reset;
  wire reset_IBUF;
  wire start;
  wire start_IBUF;
  (* RTL_KEEP = "true" *) wire [7:0]vAddr_cnt;
  wire \vAddr_cnt[0]_i_1_n_0 ;
  wire \vAddr_cnt[1]_i_1_n_0 ;
  wire \vAddr_cnt[2]_i_1_n_0 ;
  wire \vAddr_cnt[3]_i_1_n_0 ;
  wire \vAddr_cnt[3]_i_2_n_0 ;
  wire \vAddr_cnt[4]_i_1_n_0 ;
  wire \vAddr_cnt[4]_i_2_n_0 ;
  wire \vAddr_cnt[5]_i_1_n_0 ;
  wire \vAddr_cnt[5]_i_2_n_0 ;
  wire \vAddr_cnt[6]_i_1_n_0 ;
  wire \vAddr_cnt[7]_i_10_n_0 ;
  wire \vAddr_cnt[7]_i_11_n_0 ;
  wire \vAddr_cnt[7]_i_12_n_0 ;
  wire \vAddr_cnt[7]_i_14_n_0 ;
  wire \vAddr_cnt[7]_i_15_n_0 ;
  wire \vAddr_cnt[7]_i_16_n_0 ;
  wire \vAddr_cnt[7]_i_17_n_0 ;
  wire \vAddr_cnt[7]_i_18_n_0 ;
  wire \vAddr_cnt[7]_i_19_n_0 ;
  wire \vAddr_cnt[7]_i_1_n_0 ;
  wire \vAddr_cnt[7]_i_20_n_0 ;
  wire \vAddr_cnt[7]_i_21_n_0 ;
  wire \vAddr_cnt[7]_i_26_n_0 ;
  wire \vAddr_cnt[7]_i_27_n_0 ;
  wire \vAddr_cnt[7]_i_28_n_0 ;
  wire \vAddr_cnt[7]_i_29_n_0 ;
  wire \vAddr_cnt[7]_i_30_n_0 ;
  wire \vAddr_cnt[7]_i_31_n_0 ;
  wire \vAddr_cnt[7]_i_32_n_0 ;
  wire \vAddr_cnt[7]_i_33_n_0 ;
  wire \vAddr_cnt[7]_i_36_n_0 ;
  wire \vAddr_cnt[7]_i_37_n_0 ;
  wire \vAddr_cnt[7]_i_38_n_0 ;
  wire \vAddr_cnt[7]_i_39_n_0 ;
  wire \vAddr_cnt[7]_i_3_n_0 ;
  wire \vAddr_cnt[7]_i_40_n_0 ;
  wire \vAddr_cnt[7]_i_41_n_0 ;
  wire \vAddr_cnt[7]_i_42_n_0 ;
  wire \vAddr_cnt[7]_i_43_n_0 ;
  wire \vAddr_cnt[7]_i_44_n_0 ;
  wire \vAddr_cnt[7]_i_45_n_0 ;
  wire \vAddr_cnt[7]_i_46_n_0 ;
  wire \vAddr_cnt[7]_i_47_n_0 ;
  wire \vAddr_cnt[7]_i_48_n_0 ;
  wire \vAddr_cnt[7]_i_49_n_0 ;
  wire \vAddr_cnt[7]_i_50_n_0 ;
  wire \vAddr_cnt[7]_i_51_n_0 ;
  wire \vAddr_cnt[7]_i_52_n_0 ;
  wire \vAddr_cnt[7]_i_53_n_0 ;
  wire \vAddr_cnt[7]_i_54_n_0 ;
  wire \vAddr_cnt[7]_i_57_n_0 ;
  wire \vAddr_cnt[7]_i_58_n_0 ;
  wire \vAddr_cnt[7]_i_59_n_0 ;
  wire \vAddr_cnt[7]_i_5_n_0 ;
  wire \vAddr_cnt[7]_i_60_n_0 ;
  wire \vAddr_cnt[7]_i_61_n_0 ;
  wire \vAddr_cnt[7]_i_62_n_0 ;
  wire \vAddr_cnt[7]_i_63_n_0 ;
  wire \vAddr_cnt[7]_i_64_n_0 ;
  wire \vAddr_cnt[7]_i_66_n_0 ;
  wire \vAddr_cnt[7]_i_67_n_0 ;
  wire \vAddr_cnt[7]_i_68_n_0 ;
  wire \vAddr_cnt[7]_i_69_n_0 ;
  wire \vAddr_cnt[7]_i_6_n_0 ;
  wire \vAddr_cnt[7]_i_70_n_0 ;
  wire \vAddr_cnt[7]_i_71_n_0 ;
  wire \vAddr_cnt[7]_i_72_n_0 ;
  wire \vAddr_cnt[7]_i_73_n_0 ;
  wire \vAddr_cnt[7]_i_74_n_0 ;
  wire \vAddr_cnt[7]_i_75_n_0 ;
  wire \vAddr_cnt[7]_i_76_n_0 ;
  wire \vAddr_cnt[7]_i_77_n_0 ;
  wire \vAddr_cnt[7]_i_7_n_0 ;
  wire \vAddr_cnt[7]_i_8_n_0 ;
  wire \vAddr_cnt[7]_i_9_n_0 ;
  wire \vAddr_cnt_reg[7]_i_13_n_0 ;
  wire \vAddr_cnt_reg[7]_i_13_n_1 ;
  wire \vAddr_cnt_reg[7]_i_13_n_2 ;
  wire \vAddr_cnt_reg[7]_i_13_n_3 ;
  wire \vAddr_cnt_reg[7]_i_22_n_2 ;
  wire \vAddr_cnt_reg[7]_i_22_n_3 ;
  wire \vAddr_cnt_reg[7]_i_23_n_0 ;
  wire \vAddr_cnt_reg[7]_i_23_n_1 ;
  wire \vAddr_cnt_reg[7]_i_23_n_2 ;
  wire \vAddr_cnt_reg[7]_i_23_n_3 ;
  wire \vAddr_cnt_reg[7]_i_24_n_0 ;
  wire \vAddr_cnt_reg[7]_i_24_n_1 ;
  wire \vAddr_cnt_reg[7]_i_24_n_2 ;
  wire \vAddr_cnt_reg[7]_i_24_n_3 ;
  wire \vAddr_cnt_reg[7]_i_25_n_0 ;
  wire \vAddr_cnt_reg[7]_i_25_n_1 ;
  wire \vAddr_cnt_reg[7]_i_25_n_2 ;
  wire \vAddr_cnt_reg[7]_i_25_n_3 ;
  wire \vAddr_cnt_reg[7]_i_2_n_1 ;
  wire \vAddr_cnt_reg[7]_i_2_n_2 ;
  wire \vAddr_cnt_reg[7]_i_2_n_3 ;
  wire \vAddr_cnt_reg[7]_i_34_n_0 ;
  wire \vAddr_cnt_reg[7]_i_34_n_1 ;
  wire \vAddr_cnt_reg[7]_i_34_n_2 ;
  wire \vAddr_cnt_reg[7]_i_34_n_3 ;
  wire \vAddr_cnt_reg[7]_i_35_n_0 ;
  wire \vAddr_cnt_reg[7]_i_35_n_1 ;
  wire \vAddr_cnt_reg[7]_i_35_n_2 ;
  wire \vAddr_cnt_reg[7]_i_35_n_3 ;
  wire \vAddr_cnt_reg[7]_i_4_n_0 ;
  wire \vAddr_cnt_reg[7]_i_4_n_1 ;
  wire \vAddr_cnt_reg[7]_i_4_n_2 ;
  wire \vAddr_cnt_reg[7]_i_4_n_3 ;
  wire \vAddr_cnt_reg[7]_i_55_n_0 ;
  wire \vAddr_cnt_reg[7]_i_55_n_1 ;
  wire \vAddr_cnt_reg[7]_i_55_n_2 ;
  wire \vAddr_cnt_reg[7]_i_55_n_3 ;
  wire \vAddr_cnt_reg[7]_i_56_n_0 ;
  wire \vAddr_cnt_reg[7]_i_56_n_1 ;
  wire \vAddr_cnt_reg[7]_i_56_n_2 ;
  wire \vAddr_cnt_reg[7]_i_56_n_3 ;
  wire \vAddr_cnt_reg[7]_i_65_n_0 ;
  wire \vAddr_cnt_reg[7]_i_65_n_1 ;
  wire \vAddr_cnt_reg[7]_i_65_n_2 ;
  wire \vAddr_cnt_reg[7]_i_65_n_3 ;
  wire [7:0]weightAddr;
  wire [4:0]weightAddr_IBUF;
  (* RTL_KEEP = "true" *) wire [7:0]weightAddr_internal;
  wire [7:0]weightAddr_internal0;
  wire [7:0]weightAddr_internal1;
  wire weightAddr_internal_inferred_i_10_n_0;
  wire weightAddr_internal_inferred_i_10_n_1;
  wire weightAddr_internal_inferred_i_10_n_2;
  wire weightAddr_internal_inferred_i_10_n_3;
  wire weightAddr_internal_inferred_i_11_n_0;
  wire weightAddr_internal_inferred_i_12_n_0;
  wire weightAddr_internal_inferred_i_13_n_0;
  wire weightAddr_internal_inferred_i_14_n_0;
  wire weightAddr_internal_inferred_i_15_n_0;
  wire weightAddr_internal_inferred_i_16_n_0;
  wire weightAddr_internal_inferred_i_17_n_0;
  wire weightAddr_internal_inferred_i_18_n_0;
  wire weightAddr_internal_inferred_i_20_n_0;
  wire weightAddr_internal_inferred_i_20_n_1;
  wire weightAddr_internal_inferred_i_20_n_2;
  wire weightAddr_internal_inferred_i_20_n_3;
  wire weightAddr_internal_inferred_i_21_n_0;
  wire weightAddr_internal_inferred_i_21_n_1;
  wire weightAddr_internal_inferred_i_21_n_2;
  wire weightAddr_internal_inferred_i_21_n_3;
  wire weightAddr_internal_inferred_i_21_n_4;
  wire weightAddr_internal_inferred_i_21_n_5;
  wire weightAddr_internal_inferred_i_21_n_6;
  wire weightAddr_internal_inferred_i_21_n_7;
  wire weightAddr_internal_inferred_i_22_n_0;
  wire weightAddr_internal_inferred_i_22_n_1;
  wire weightAddr_internal_inferred_i_22_n_2;
  wire weightAddr_internal_inferred_i_22_n_3;
  wire weightAddr_internal_inferred_i_22_n_4;
  wire weightAddr_internal_inferred_i_23_n_0;
  wire weightAddr_internal_inferred_i_24_n_0;
  wire weightAddr_internal_inferred_i_25_n_1;
  wire weightAddr_internal_inferred_i_25_n_2;
  wire weightAddr_internal_inferred_i_25_n_3;
  wire weightAddr_internal_inferred_i_25_n_4;
  wire weightAddr_internal_inferred_i_25_n_5;
  wire weightAddr_internal_inferred_i_25_n_6;
  wire weightAddr_internal_inferred_i_25_n_7;
  wire weightAddr_internal_inferred_i_26_n_0;
  wire weightAddr_internal_inferred_i_27_n_0;
  wire weightAddr_internal_inferred_i_28_n_0;
  wire weightAddr_internal_inferred_i_30_n_0;
  wire weightAddr_internal_inferred_i_31_n_0;
  wire weightAddr_internal_inferred_i_32_n_0;
  wire weightAddr_internal_inferred_i_33_n_0;
  wire weightAddr_internal_inferred_i_34_n_0;
  wire weightAddr_internal_inferred_i_35_n_0;
  wire weightAddr_internal_inferred_i_36_n_0;
  wire weightAddr_internal_inferred_i_37_n_0;
  wire weightAddr_internal_inferred_i_38_n_0;
  wire weightAddr_internal_inferred_i_39_n_0;
  wire weightAddr_internal_inferred_i_40_n_0;
  wire weightAddr_internal_inferred_i_41_n_0;
  wire weightAddr_internal_inferred_i_42_n_0;
  wire weightAddr_internal_inferred_i_43_n_0;
  wire weightAddr_internal_inferred_i_44_n_7;
  wire weightAddr_internal_inferred_i_45_n_0;
  wire weightAddr_internal_inferred_i_46_n_0;
  wire weightAddr_internal_inferred_i_47_n_0;
  wire weightAddr_internal_inferred_i_48_n_0;
  wire weightAddr_internal_inferred_i_49_n_0;
  wire weightAddr_internal_inferred_i_50_n_0;
  wire weightAddr_internal_inferred_i_51_n_0;
  wire weightAddr_internal_inferred_i_52_n_0;
  wire weightAddr_internal_inferred_i_53_n_0;
  wire weightAddr_internal_inferred_i_54_n_0;
  wire weightAddr_internal_inferred_i_55_n_0;
  wire weightAddr_internal_inferred_i_56_n_0;
  wire weightAddr_internal_inferred_i_57_n_0;
  wire weightAddr_internal_inferred_i_58_n_0;
  wire weightAddr_internal_inferred_i_59_n_0;
  wire weightAddr_internal_inferred_i_60_n_0;
  wire weightAddr_internal_inferred_i_61_n_0;
  wire weightAddr_internal_inferred_i_62_n_0;
  wire weightAddr_internal_inferred_i_9_n_1;
  wire weightAddr_internal_inferred_i_9_n_2;
  wire weightAddr_internal_inferred_i_9_n_3;
  wire [31:0]weightMem_in;
  wire [31:0]weightMem_in_IBUF;
  wire [31:0]weightMem_out;
  (* RTL_KEEP = "true" *) wire [31:0]weightMem_out_internal;
  wire NLW_dataAddr_internal4_CARRYCASCOUT_UNCONNECTED;
  wire NLW_dataAddr_internal4_MULTSIGNOUT_UNCONNECTED;
  wire NLW_dataAddr_internal4_OVERFLOW_UNCONNECTED;
  wire NLW_dataAddr_internal4_PATTERNBDETECT_UNCONNECTED;
  wire NLW_dataAddr_internal4_PATTERNDETECT_UNCONNECTED;
  wire NLW_dataAddr_internal4_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_dataAddr_internal4_ACOUT_UNCONNECTED;
  wire [17:0]NLW_dataAddr_internal4_BCOUT_UNCONNECTED;
  wire [3:0]NLW_dataAddr_internal4_CARRYOUT_UNCONNECTED;
  wire NLW_dataAddr_internal4__0_CARRYCASCOUT_UNCONNECTED;
  wire NLW_dataAddr_internal4__0_MULTSIGNOUT_UNCONNECTED;
  wire NLW_dataAddr_internal4__0_OVERFLOW_UNCONNECTED;
  wire NLW_dataAddr_internal4__0_PATTERNBDETECT_UNCONNECTED;
  wire NLW_dataAddr_internal4__0_PATTERNDETECT_UNCONNECTED;
  wire NLW_dataAddr_internal4__0_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_dataAddr_internal4__0_ACOUT_UNCONNECTED;
  wire [17:0]NLW_dataAddr_internal4__0_BCOUT_UNCONNECTED;
  wire [3:0]NLW_dataAddr_internal4__0_CARRYOUT_UNCONNECTED;
  wire [47:0]NLW_dataAddr_internal4__0_PCOUT_UNCONNECTED;
  wire [3:0]NLW_dataAddr_internal_inferred_i_100_O_UNCONNECTED;
  wire [3:0]NLW_dataAddr_internal_inferred_i_106_O_UNCONNECTED;
  wire [3:0]NLW_dataAddr_internal_inferred_i_112_O_UNCONNECTED;
  wire [3:0]NLW_dataAddr_internal_inferred_i_19_CO_UNCONNECTED;
  wire [3:1]NLW_dataAddr_internal_inferred_i_19_O_UNCONNECTED;
  wire [0:0]NLW_dataAddr_internal_inferred_i_21_O_UNCONNECTED;
  wire [0:0]NLW_dataAddr_internal_inferred_i_23_O_UNCONNECTED;
  wire [3:0]NLW_dataAddr_internal_inferred_i_29_O_UNCONNECTED;
  wire [3:0]NLW_dataAddr_internal_inferred_i_34_CO_UNCONNECTED;
  wire [3:1]NLW_dataAddr_internal_inferred_i_34_O_UNCONNECTED;
  wire [3:3]NLW_dataAddr_internal_inferred_i_36_CO_UNCONNECTED;
  wire [3:0]NLW_dataAddr_internal_inferred_i_42_O_UNCONNECTED;
  wire [3:3]NLW_dataAddr_internal_inferred_i_43_CO_UNCONNECTED;
  wire [3:0]NLW_dataAddr_internal_inferred_i_70_O_UNCONNECTED;
  wire [3:0]NLW_dataAddr_internal_inferred_i_77_CO_UNCONNECTED;
  wire [3:1]NLW_dataAddr_internal_inferred_i_77_O_UNCONNECTED;
  wire [3:0]NLW_dataAddr_internal_inferred_i_85_O_UNCONNECTED;
  wire [3:3]NLW_dataAddr_internal_inferred_i_9_CO_UNCONNECTED;
  wire [3:0]NLW_dataAddr_internal_inferred_i_92_O_UNCONNECTED;
  wire [3:0]NLW_end_ops_internal_inferred_i_10_O_UNCONNECTED;
  wire [3:2]NLW_end_ops_internal_inferred_i_15_CO_UNCONNECTED;
  wire [3:3]NLW_end_ops_internal_inferred_i_15_O_UNCONNECTED;
  wire [3:3]NLW_end_ops_internal_inferred_i_3_CO_UNCONNECTED;
  wire [3:0]NLW_end_ops_internal_inferred_i_3_O_UNCONNECTED;
  wire [3:0]NLW_end_ops_internal_inferred_i_6_O_UNCONNECTED;
  wire [3:0]\NLW_hAddr_cnt_reg[7]_i_14_O_UNCONNECTED ;
  wire [3:0]\NLW_hAddr_cnt_reg[7]_i_23_O_UNCONNECTED ;
  wire [3:0]\NLW_hAddr_cnt_reg[7]_i_3_O_UNCONNECTED ;
  wire [3:0]\NLW_hAddr_cnt_reg[7]_i_5_O_UNCONNECTED ;
  wire [3:0]\NLW_vAddr_cnt_reg[7]_i_13_O_UNCONNECTED ;
  wire [3:0]\NLW_vAddr_cnt_reg[7]_i_2_O_UNCONNECTED ;
  wire [3:2]\NLW_vAddr_cnt_reg[7]_i_22_CO_UNCONNECTED ;
  wire [3:3]\NLW_vAddr_cnt_reg[7]_i_22_O_UNCONNECTED ;
  wire [3:0]\NLW_vAddr_cnt_reg[7]_i_25_O_UNCONNECTED ;
  wire [3:0]\NLW_vAddr_cnt_reg[7]_i_4_O_UNCONNECTED ;
  wire [3:0]NLW_weightAddr_internal_inferred_i_19_CO_UNCONNECTED;
  wire [3:1]NLW_weightAddr_internal_inferred_i_19_O_UNCONNECTED;
  wire [0:0]NLW_weightAddr_internal_inferred_i_20_O_UNCONNECTED;
  wire [3:3]NLW_weightAddr_internal_inferred_i_25_CO_UNCONNECTED;
  wire [3:0]NLW_weightAddr_internal_inferred_i_44_CO_UNCONNECTED;
  wire [3:1]NLW_weightAddr_internal_inferred_i_44_O_UNCONNECTED;
  wire [3:3]NLW_weightAddr_internal_inferred_i_9_CO_UNCONNECTED;

  rdp_256X32 DM
       (.ADDRBWRADDR(dataAddr_IBUF),
        .D(dataMem_out),
        .cen_dataMemWR_reg(cen_dataMemWR),
        .clk_IBUF_BUFG(clk_IBUF_BUFG),
        .dataMem_in_IBUF(dataMem_in_IBUF),
        .out(cen_MemR),
        .\vAddr_cnt_reg[6] (dataAddr_internal[4:0]));
  MAC MU
       (.begin_ops_internal2_reg(begin_ops_internal2),
        .clk_IBUF_BUFG(clk_IBUF_BUFG),
        .\dataMem_out_internal_reg[31] (dataMem_out_internal),
        .end_ops_internal2_reg(end_ops_internal2),
        .out(MAC_out),
        .reset_IBUF(reset_IBUF),
        .\weightMem_out_internal_reg[31] (weightMem_out_internal));
  IBUF \N_IBUF[0]_inst 
       (.I(N[0]),
        .O(N_IBUF[0]));
  IBUF \N_IBUF[10]_inst 
       (.I(N[10]),
        .O(N_IBUF[10]));
  IBUF \N_IBUF[11]_inst 
       (.I(N[11]),
        .O(N_IBUF[11]));
  IBUF \N_IBUF[12]_inst 
       (.I(N[12]),
        .O(N_IBUF[12]));
  IBUF \N_IBUF[13]_inst 
       (.I(N[13]),
        .O(N_IBUF[13]));
  IBUF \N_IBUF[14]_inst 
       (.I(N[14]),
        .O(N_IBUF[14]));
  IBUF \N_IBUF[15]_inst 
       (.I(N[15]),
        .O(N_IBUF[15]));
  IBUF \N_IBUF[16]_inst 
       (.I(N[16]),
        .O(N_IBUF[16]));
  IBUF \N_IBUF[17]_inst 
       (.I(N[17]),
        .O(N_IBUF[17]));
  IBUF \N_IBUF[18]_inst 
       (.I(N[18]),
        .O(N_IBUF[18]));
  IBUF \N_IBUF[19]_inst 
       (.I(N[19]),
        .O(N_IBUF[19]));
  IBUF \N_IBUF[1]_inst 
       (.I(N[1]),
        .O(N_IBUF[1]));
  IBUF \N_IBUF[20]_inst 
       (.I(N[20]),
        .O(N_IBUF[20]));
  IBUF \N_IBUF[21]_inst 
       (.I(N[21]),
        .O(N_IBUF[21]));
  IBUF \N_IBUF[22]_inst 
       (.I(N[22]),
        .O(N_IBUF[22]));
  IBUF \N_IBUF[23]_inst 
       (.I(N[23]),
        .O(N_IBUF[23]));
  IBUF \N_IBUF[24]_inst 
       (.I(N[24]),
        .O(N_IBUF[24]));
  IBUF \N_IBUF[25]_inst 
       (.I(N[25]),
        .O(N_IBUF[25]));
  IBUF \N_IBUF[26]_inst 
       (.I(N[26]),
        .O(N_IBUF[26]));
  IBUF \N_IBUF[27]_inst 
       (.I(N[27]),
        .O(N_IBUF[27]));
  IBUF \N_IBUF[28]_inst 
       (.I(N[28]),
        .O(N_IBUF[28]));
  IBUF \N_IBUF[29]_inst 
       (.I(N[29]),
        .O(N_IBUF[29]));
  IBUF \N_IBUF[2]_inst 
       (.I(N[2]),
        .O(N_IBUF[2]));
  IBUF \N_IBUF[30]_inst 
       (.I(N[30]),
        .O(N_IBUF[30]));
  IBUF \N_IBUF[31]_inst 
       (.I(N[31]),
        .O(N_IBUF[31]));
  IBUF \N_IBUF[3]_inst 
       (.I(N[3]),
        .O(N_IBUF[3]));
  IBUF \N_IBUF[4]_inst 
       (.I(N[4]),
        .O(N_IBUF[4]));
  IBUF \N_IBUF[5]_inst 
       (.I(N[5]),
        .O(N_IBUF[5]));
  IBUF \N_IBUF[6]_inst 
       (.I(N[6]),
        .O(N_IBUF[6]));
  IBUF \N_IBUF[7]_inst 
       (.I(N[7]),
        .O(N_IBUF[7]));
  IBUF \N_IBUF[8]_inst 
       (.I(N[8]),
        .O(N_IBUF[8]));
  IBUF \N_IBUF[9]_inst 
       (.I(N[9]),
        .O(N_IBUF[9]));
  rdp_256X32_0 OM
       (.ADDRARDADDR(outAddr_IBUF),
        .cen_outMem_IBUF(cen_outMem_IBUF),
        .clk_IBUF_BUFG(clk_IBUF_BUFG),
        .\dataOut_reg[31] (MAC_out),
        .end_ops_internal4_reg(end_ops_internal4),
        .out(outAddr_internal[4:0]),
        .outMem_data_OBUF(outMem_data_OBUF));
  rdp_256X32_1 WM
       (.ADDRBWRADDR(weightAddr_IBUF),
        .D(weightMem_out),
        .cen_weightMemWR_reg(cen_weightMemWR),
        .clk_IBUF_BUFG(clk_IBUF_BUFG),
        .\hAddr_cnt_reg[6] (weightAddr_internal[4:0]),
        .out(cen_MemR),
        .weightMem_in_IBUF(weightMem_in_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    begin_ops_internal1_reg
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(begin_ops_internal),
        .Q(begin_ops_internal1),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    begin_ops_internal2_reg
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(begin_ops_internal1),
        .Q(begin_ops_internal2),
        .R(reset_IBUF));
  LUT2 #(
    .INIT(4'h1)) 
    begin_ops_internal_inferred_i_1
       (.I0(reset_IBUF),
        .I1(end_ops_internal_inferred_i_2_n_0),
        .O(begin_ops_internal));
  LUT6 #(
    .INIT(64'hFFFFFFE8000000E8)) 
    cen_MemR_i_1
       (.I0(start_IBUF),
        .I1(cen_MemR),
        .I2(currentState[1]),
        .I3(currentState[2]),
        .I4(currentState[0]),
        .I5(cen_MemR),
        .O(cen_MemR_i_1_n_0));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    cen_MemR_reg
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(cen_MemR_i_1_n_0),
        .Q(cen_MemR),
        .R(reset_IBUF));
  LUT6 #(
    .INIT(64'hEE20FFFFEE200000)) 
    cen_dataMemWR_i_1
       (.I0(start_IBUF),
        .I1(currentState[0]),
        .I2(cen_dataMemWR),
        .I3(rd_wr_IBUF),
        .I4(cen_weightMemWR_i_2_n_0),
        .I5(cen_dataMemWR),
        .O(cen_dataMemWR_i_1_n_0));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    cen_dataMemWR_reg
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(cen_dataMemWR_i_1_n_0),
        .Q(cen_dataMemWR),
        .R(reset_IBUF));
  IBUF cen_outMem_IBUF_inst
       (.I(cen_outMem),
        .O(cen_outMem_IBUF));
  LUT6 #(
    .INIT(64'hEE20FFFFEE200000)) 
    cen_weightMemWR_i_1
       (.I0(start_IBUF),
        .I1(currentState[0]),
        .I2(cen_weightMemWR),
        .I3(rd_wr_IBUF),
        .I4(cen_weightMemWR_i_2_n_0),
        .I5(cen_weightMemWR),
        .O(cen_weightMemWR_i_1_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    cen_weightMemWR_i_2
       (.I0(currentState[2]),
        .I1(currentState[1]),
        .O(cen_weightMemWR_i_2_n_0));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    cen_weightMemWR_reg
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(cen_weightMemWR_i_1_n_0),
        .Q(cen_weightMemWR),
        .R(reset_IBUF));
  BUFG clk_IBUF_BUFG_inst
       (.I(clk_IBUF),
        .O(clk_IBUF_BUFG));
  IBUF clk_IBUF_inst
       (.I(clk),
        .O(clk_IBUF));
  LUT6 #(
    .INIT(64'hFFFF755000007550)) 
    \currentState[0]_i_1 
       (.I0(\currentState[1]_i_2_n_0 ),
        .I1(currentState[0]),
        .I2(currentState[1]),
        .I3(rd_wr_IBUF),
        .I4(currentState[2]),
        .I5(currentState[0]),
        .O(\currentState[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFF04DD000004DD)) 
    \currentState[1]_i_1 
       (.I0(rd_wr_IBUF),
        .I1(currentState[1]),
        .I2(currentState[0]),
        .I3(\currentState[1]_i_2_n_0 ),
        .I4(currentState[2]),
        .I5(currentState[1]),
        .O(\currentState[1]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h110F000F)) 
    \currentState[1]_i_2 
       (.I0(currentState28_in),
        .I1(currentState2),
        .I2(start_IBUF),
        .I3(currentState[0]),
        .I4(currentState[1]),
        .O(\currentState[1]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \currentState[2]_i_1 
       (.I0(currentState[2]),
        .I1(currentState[2]),
        .O(\currentState[2]_i_1_n_0 ));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \currentState_reg[0] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\currentState[0]_i_1_n_0 ),
        .Q(currentState[0]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \currentState_reg[1] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\currentState[1]_i_1_n_0 ),
        .Q(currentState[1]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \currentState_reg[2] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\currentState[2]_i_1_n_0 ),
        .Q(currentState[2]),
        .R(reset_IBUF));
  IBUF \dataAddr_IBUF[0]_inst 
       (.I(dataAddr[0]),
        .O(dataAddr_IBUF[0]));
  IBUF \dataAddr_IBUF[1]_inst 
       (.I(dataAddr[1]),
        .O(dataAddr_IBUF[1]));
  IBUF \dataAddr_IBUF[2]_inst 
       (.I(dataAddr[2]),
        .O(dataAddr_IBUF[2]));
  IBUF \dataAddr_IBUF[3]_inst 
       (.I(dataAddr[3]),
        .O(dataAddr_IBUF[3]));
  IBUF \dataAddr_IBUF[4]_inst 
       (.I(dataAddr[4]),
        .O(dataAddr_IBUF[4]));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-13 {cell *THIS*}}" *) 
  DSP48E1 #(
    .ACASCREG(0),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(0),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(0),
    .BREG(0),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(0),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    dataAddr_internal4
       (.A({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,r_IBUF[16:0]}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_dataAddr_internal4_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,hAddr_cnt}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_dataAddr_internal4_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_dataAddr_internal4_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_dataAddr_internal4_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(1'b0),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(1'b0),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(1'b0),
        .CLK(1'b0),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_dataAddr_internal4_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b0,1'b0,1'b0,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_dataAddr_internal4_OVERFLOW_UNCONNECTED),
        .P({dataAddr_internal4_n_58,dataAddr_internal4_n_59,dataAddr_internal4_n_60,dataAddr_internal4_n_61,dataAddr_internal4_n_62,dataAddr_internal4_n_63,dataAddr_internal4_n_64,dataAddr_internal4_n_65,dataAddr_internal4_n_66,dataAddr_internal4_n_67,dataAddr_internal4_n_68,dataAddr_internal4_n_69,dataAddr_internal4_n_70,dataAddr_internal4_n_71,dataAddr_internal4_n_72,dataAddr_internal4_n_73,dataAddr_internal4_n_74,dataAddr_internal4_n_75,dataAddr_internal4_n_76,dataAddr_internal4_n_77,dataAddr_internal4_n_78,dataAddr_internal4_n_79,dataAddr_internal4_n_80,dataAddr_internal4_n_81,dataAddr_internal4_n_82,dataAddr_internal4_n_83,dataAddr_internal4_n_84,dataAddr_internal4_n_85,dataAddr_internal4_n_86,dataAddr_internal4_n_87,dataAddr_internal4_n_88,dataAddr_internal4_n_89,dataAddr_internal4_n_90,dataAddr_internal4_n_91,dataAddr_internal4_n_92,dataAddr_internal4_n_93,dataAddr_internal4_n_94,dataAddr_internal4_n_95,dataAddr_internal4_n_96,dataAddr_internal4_n_97,dataAddr_internal4_n_98,dataAddr_internal4_n_99,dataAddr_internal4_n_100,dataAddr_internal4_n_101,dataAddr_internal4_n_102,dataAddr_internal4_n_103,dataAddr_internal4_n_104,dataAddr_internal4_n_105}),
        .PATTERNBDETECT(NLW_dataAddr_internal4_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_dataAddr_internal4_PATTERNDETECT_UNCONNECTED),
        .PCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCOUT({dataAddr_internal4_n_106,dataAddr_internal4_n_107,dataAddr_internal4_n_108,dataAddr_internal4_n_109,dataAddr_internal4_n_110,dataAddr_internal4_n_111,dataAddr_internal4_n_112,dataAddr_internal4_n_113,dataAddr_internal4_n_114,dataAddr_internal4_n_115,dataAddr_internal4_n_116,dataAddr_internal4_n_117,dataAddr_internal4_n_118,dataAddr_internal4_n_119,dataAddr_internal4_n_120,dataAddr_internal4_n_121,dataAddr_internal4_n_122,dataAddr_internal4_n_123,dataAddr_internal4_n_124,dataAddr_internal4_n_125,dataAddr_internal4_n_126,dataAddr_internal4_n_127,dataAddr_internal4_n_128,dataAddr_internal4_n_129,dataAddr_internal4_n_130,dataAddr_internal4_n_131,dataAddr_internal4_n_132,dataAddr_internal4_n_133,dataAddr_internal4_n_134,dataAddr_internal4_n_135,dataAddr_internal4_n_136,dataAddr_internal4_n_137,dataAddr_internal4_n_138,dataAddr_internal4_n_139,dataAddr_internal4_n_140,dataAddr_internal4_n_141,dataAddr_internal4_n_142,dataAddr_internal4_n_143,dataAddr_internal4_n_144,dataAddr_internal4_n_145,dataAddr_internal4_n_146,dataAddr_internal4_n_147,dataAddr_internal4_n_148,dataAddr_internal4_n_149,dataAddr_internal4_n_150,dataAddr_internal4_n_151,dataAddr_internal4_n_152,dataAddr_internal4_n_153}),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(1'b0),
        .UNDERFLOW(NLW_dataAddr_internal4_UNDERFLOW_UNCONNECTED));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-13 {cell *THIS*}}" *) 
  DSP48E1 #(
    .ACASCREG(0),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(0),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(0),
    .BREG(0),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(0),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    dataAddr_internal4__0
       (.A({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,r_IBUF[31:17]}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_dataAddr_internal4__0_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,hAddr_cnt}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_dataAddr_internal4__0_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_dataAddr_internal4__0_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_dataAddr_internal4__0_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(1'b0),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(1'b0),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(1'b0),
        .CLK(1'b0),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_dataAddr_internal4__0_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b1,1'b0,1'b1,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_dataAddr_internal4__0_OVERFLOW_UNCONNECTED),
        .P({dataAddr_internal4__0_n_58,dataAddr_internal4__0_n_59,dataAddr_internal4__0_n_60,dataAddr_internal4__0_n_61,dataAddr_internal4__0_n_62,dataAddr_internal4__0_n_63,dataAddr_internal4__0_n_64,dataAddr_internal4__0_n_65,dataAddr_internal4__0_n_66,dataAddr_internal4__0_n_67,dataAddr_internal4__0_n_68,dataAddr_internal4__0_n_69,dataAddr_internal4__0_n_70,dataAddr_internal4__0_n_71,dataAddr_internal4__0_n_72,dataAddr_internal4__0_n_73,dataAddr_internal4__0_n_74,dataAddr_internal4__0_n_75,dataAddr_internal4__0_n_76,dataAddr_internal4__0_n_77,dataAddr_internal4__0_n_78,dataAddr_internal4__0_n_79,dataAddr_internal4__0_n_80,dataAddr_internal4__0_n_81,dataAddr_internal4__0_n_82,dataAddr_internal4__0_n_83,dataAddr_internal4__0_n_84,dataAddr_internal4__0_n_85,dataAddr_internal4__0_n_86,dataAddr_internal4__0_n_87,dataAddr_internal4__0_n_88,dataAddr_internal4__0_n_89,dataAddr_internal4__0_n_90,dataAddr_internal4__0_n_91,dataAddr_internal4__0_n_92,dataAddr_internal4__0_n_93,dataAddr_internal4__0_n_94,dataAddr_internal4__0_n_95,dataAddr_internal4__0_n_96,dataAddr_internal4__0_n_97,dataAddr_internal4__0_n_98,dataAddr_internal4__0_n_99,dataAddr_internal4__0_n_100,dataAddr_internal4__0_n_101,dataAddr_internal4__0_n_102,dataAddr_internal4__0_n_103,dataAddr_internal4__0_n_104,dataAddr_internal4__0_n_105}),
        .PATTERNBDETECT(NLW_dataAddr_internal4__0_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_dataAddr_internal4__0_PATTERNDETECT_UNCONNECTED),
        .PCIN({dataAddr_internal4_n_106,dataAddr_internal4_n_107,dataAddr_internal4_n_108,dataAddr_internal4_n_109,dataAddr_internal4_n_110,dataAddr_internal4_n_111,dataAddr_internal4_n_112,dataAddr_internal4_n_113,dataAddr_internal4_n_114,dataAddr_internal4_n_115,dataAddr_internal4_n_116,dataAddr_internal4_n_117,dataAddr_internal4_n_118,dataAddr_internal4_n_119,dataAddr_internal4_n_120,dataAddr_internal4_n_121,dataAddr_internal4_n_122,dataAddr_internal4_n_123,dataAddr_internal4_n_124,dataAddr_internal4_n_125,dataAddr_internal4_n_126,dataAddr_internal4_n_127,dataAddr_internal4_n_128,dataAddr_internal4_n_129,dataAddr_internal4_n_130,dataAddr_internal4_n_131,dataAddr_internal4_n_132,dataAddr_internal4_n_133,dataAddr_internal4_n_134,dataAddr_internal4_n_135,dataAddr_internal4_n_136,dataAddr_internal4_n_137,dataAddr_internal4_n_138,dataAddr_internal4_n_139,dataAddr_internal4_n_140,dataAddr_internal4_n_141,dataAddr_internal4_n_142,dataAddr_internal4_n_143,dataAddr_internal4_n_144,dataAddr_internal4_n_145,dataAddr_internal4_n_146,dataAddr_internal4_n_147,dataAddr_internal4_n_148,dataAddr_internal4_n_149,dataAddr_internal4_n_150,dataAddr_internal4_n_151,dataAddr_internal4_n_152,dataAddr_internal4_n_153}),
        .PCOUT(NLW_dataAddr_internal4__0_PCOUT_UNCONNECTED[47:0]),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(1'b0),
        .UNDERFLOW(NLW_dataAddr_internal4__0_UNDERFLOW_UNCONNECTED));
  LUT2 #(
    .INIT(4'h2)) 
    dataAddr_internal_inferred_i_1
       (.I0(dataAddr_internal1[7]),
        .I1(reset_IBUF),
        .O(dataAddr_internal[7]));
  CARRY4 dataAddr_internal_inferred_i_10
       (.CI(1'b0),
        .CO({dataAddr_internal_inferred_i_10_n_0,dataAddr_internal_inferred_i_10_n_1,dataAddr_internal_inferred_i_10_n_2,dataAddr_internal_inferred_i_10_n_3}),
        .CYINIT(1'b0),
        .DI(vAddr_cnt[3:0]),
        .O(dataAddr_internal1[3:0]),
        .S({dataAddr_internal_inferred_i_15_n_0,dataAddr_internal_inferred_i_16_n_0,dataAddr_internal_inferred_i_17_n_0,dataAddr_internal_inferred_i_18_n_0}));
  CARRY4 dataAddr_internal_inferred_i_100
       (.CI(dataAddr_internal_inferred_i_106_n_0),
        .CO({dataAddr_internal_inferred_i_100_n_0,dataAddr_internal_inferred_i_100_n_1,dataAddr_internal_inferred_i_100_n_2,dataAddr_internal_inferred_i_100_n_3}),
        .CYINIT(1'b0),
        .DI(dataAddr_internal3[11:8]),
        .O(NLW_dataAddr_internal_inferred_i_100_O_UNCONNECTED[3:0]),
        .S({dataAddr_internal_inferred_i_108_n_0,dataAddr_internal_inferred_i_109_n_0,dataAddr_internal_inferred_i_110_n_0,dataAddr_internal_inferred_i_111_n_0}));
  CARRY4 dataAddr_internal_inferred_i_101
       (.CI(dataAddr_internal_inferred_i_107_n_0),
        .CO({dataAddr_internal_inferred_i_101_n_0,dataAddr_internal_inferred_i_101_n_1,dataAddr_internal_inferred_i_101_n_2,dataAddr_internal_inferred_i_101_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(dataAddr_internal3[15:12]),
        .S({dataAddr_internal4_n_90,dataAddr_internal4_n_91,dataAddr_internal4_n_92,dataAddr_internal4_n_93}));
  LUT2 #(
    .INIT(4'h9)) 
    dataAddr_internal_inferred_i_102
       (.I0(dataAddr_internal3[15]),
        .I1(N_IBUF[15]),
        .O(dataAddr_internal_inferred_i_102_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    dataAddr_internal_inferred_i_103
       (.I0(dataAddr_internal3[14]),
        .I1(N_IBUF[14]),
        .O(dataAddr_internal_inferred_i_103_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    dataAddr_internal_inferred_i_104
       (.I0(dataAddr_internal3[13]),
        .I1(N_IBUF[13]),
        .O(dataAddr_internal_inferred_i_104_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    dataAddr_internal_inferred_i_105
       (.I0(dataAddr_internal3[12]),
        .I1(N_IBUF[12]),
        .O(dataAddr_internal_inferred_i_105_n_0));
  CARRY4 dataAddr_internal_inferred_i_106
       (.CI(dataAddr_internal_inferred_i_112_n_0),
        .CO({dataAddr_internal_inferred_i_106_n_0,dataAddr_internal_inferred_i_106_n_1,dataAddr_internal_inferred_i_106_n_2,dataAddr_internal_inferred_i_106_n_3}),
        .CYINIT(1'b0),
        .DI(dataAddr_internal3[7:4]),
        .O(NLW_dataAddr_internal_inferred_i_106_O_UNCONNECTED[3:0]),
        .S({dataAddr_internal_inferred_i_114_n_0,dataAddr_internal_inferred_i_115_n_0,dataAddr_internal_inferred_i_116_n_0,dataAddr_internal_inferred_i_117_n_0}));
  CARRY4 dataAddr_internal_inferred_i_107
       (.CI(dataAddr_internal_inferred_i_113_n_0),
        .CO({dataAddr_internal_inferred_i_107_n_0,dataAddr_internal_inferred_i_107_n_1,dataAddr_internal_inferred_i_107_n_2,dataAddr_internal_inferred_i_107_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(dataAddr_internal3[11:8]),
        .S({dataAddr_internal4_n_94,dataAddr_internal4_n_95,dataAddr_internal4_n_96,dataAddr_internal4_n_97}));
  LUT2 #(
    .INIT(4'h9)) 
    dataAddr_internal_inferred_i_108
       (.I0(dataAddr_internal3[11]),
        .I1(N_IBUF[11]),
        .O(dataAddr_internal_inferred_i_108_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    dataAddr_internal_inferred_i_109
       (.I0(dataAddr_internal3[10]),
        .I1(N_IBUF[10]),
        .O(dataAddr_internal_inferred_i_109_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    dataAddr_internal_inferred_i_11
       (.I0(vAddr_cnt[7]),
        .I1(PCOUT[7]),
        .O(dataAddr_internal_inferred_i_11_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    dataAddr_internal_inferred_i_110
       (.I0(dataAddr_internal3[9]),
        .I1(N_IBUF[9]),
        .O(dataAddr_internal_inferred_i_110_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    dataAddr_internal_inferred_i_111
       (.I0(dataAddr_internal3[8]),
        .I1(N_IBUF[8]),
        .O(dataAddr_internal_inferred_i_111_n_0));
  CARRY4 dataAddr_internal_inferred_i_112
       (.CI(1'b0),
        .CO({dataAddr_internal_inferred_i_112_n_0,dataAddr_internal_inferred_i_112_n_1,dataAddr_internal_inferred_i_112_n_2,dataAddr_internal_inferred_i_112_n_3}),
        .CYINIT(1'b1),
        .DI(dataAddr_internal3[3:0]),
        .O(NLW_dataAddr_internal_inferred_i_112_O_UNCONNECTED[3:0]),
        .S({dataAddr_internal_inferred_i_119_n_0,dataAddr_internal_inferred_i_120_n_0,dataAddr_internal_inferred_i_121_n_0,dataAddr_internal_inferred_i_122_n_0}));
  CARRY4 dataAddr_internal_inferred_i_113
       (.CI(dataAddr_internal_inferred_i_118_n_0),
        .CO({dataAddr_internal_inferred_i_113_n_0,dataAddr_internal_inferred_i_113_n_1,dataAddr_internal_inferred_i_113_n_2,dataAddr_internal_inferred_i_113_n_3}),
        .CYINIT(1'b0),
        .DI({dataAddr_internal4_n_98,dataAddr_internal4_n_99,dataAddr_internal4_n_100,dataAddr_internal4_n_101}),
        .O(dataAddr_internal3[7:4]),
        .S({dataAddr_internal_inferred_i_123_n_0,dataAddr_internal_inferred_i_124_n_0,dataAddr_internal_inferred_i_125_n_0,dataAddr_internal_inferred_i_126_n_0}));
  LUT2 #(
    .INIT(4'h9)) 
    dataAddr_internal_inferred_i_114
       (.I0(dataAddr_internal3[7]),
        .I1(N_IBUF[7]),
        .O(dataAddr_internal_inferred_i_114_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    dataAddr_internal_inferred_i_115
       (.I0(dataAddr_internal3[6]),
        .I1(N_IBUF[6]),
        .O(dataAddr_internal_inferred_i_115_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    dataAddr_internal_inferred_i_116
       (.I0(dataAddr_internal3[5]),
        .I1(N_IBUF[5]),
        .O(dataAddr_internal_inferred_i_116_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    dataAddr_internal_inferred_i_117
       (.I0(dataAddr_internal3[4]),
        .I1(N_IBUF[4]),
        .O(dataAddr_internal_inferred_i_117_n_0));
  CARRY4 dataAddr_internal_inferred_i_118
       (.CI(1'b0),
        .CO({dataAddr_internal_inferred_i_118_n_0,dataAddr_internal_inferred_i_118_n_1,dataAddr_internal_inferred_i_118_n_2,dataAddr_internal_inferred_i_118_n_3}),
        .CYINIT(1'b0),
        .DI({dataAddr_internal4_n_102,dataAddr_internal4_n_103,dataAddr_internal4_n_104,dataAddr_internal4_n_105}),
        .O(dataAddr_internal3[3:0]),
        .S({dataAddr_internal_inferred_i_127_n_0,dataAddr_internal_inferred_i_128_n_0,dataAddr_internal_inferred_i_129_n_0,dataAddr_internal_inferred_i_130_n_0}));
  LUT2 #(
    .INIT(4'h9)) 
    dataAddr_internal_inferred_i_119
       (.I0(dataAddr_internal3[3]),
        .I1(N_IBUF[3]),
        .O(dataAddr_internal_inferred_i_119_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    dataAddr_internal_inferred_i_12
       (.I0(vAddr_cnt[6]),
        .I1(PCOUT[6]),
        .O(dataAddr_internal_inferred_i_12_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    dataAddr_internal_inferred_i_120
       (.I0(dataAddr_internal3[2]),
        .I1(N_IBUF[2]),
        .O(dataAddr_internal_inferred_i_120_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    dataAddr_internal_inferred_i_121
       (.I0(dataAddr_internal3[1]),
        .I1(N_IBUF[1]),
        .O(dataAddr_internal_inferred_i_121_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    dataAddr_internal_inferred_i_122
       (.I0(dataAddr_internal3[0]),
        .I1(N_IBUF[0]),
        .O(dataAddr_internal_inferred_i_122_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    dataAddr_internal_inferred_i_123
       (.I0(dataAddr_internal4_n_98),
        .I1(vAddr_cnt[7]),
        .O(dataAddr_internal_inferred_i_123_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    dataAddr_internal_inferred_i_124
       (.I0(dataAddr_internal4_n_99),
        .I1(vAddr_cnt[6]),
        .O(dataAddr_internal_inferred_i_124_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    dataAddr_internal_inferred_i_125
       (.I0(dataAddr_internal4_n_100),
        .I1(vAddr_cnt[5]),
        .O(dataAddr_internal_inferred_i_125_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    dataAddr_internal_inferred_i_126
       (.I0(dataAddr_internal4_n_101),
        .I1(vAddr_cnt[4]),
        .O(dataAddr_internal_inferred_i_126_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    dataAddr_internal_inferred_i_127
       (.I0(dataAddr_internal4_n_102),
        .I1(vAddr_cnt[3]),
        .O(dataAddr_internal_inferred_i_127_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    dataAddr_internal_inferred_i_128
       (.I0(dataAddr_internal4_n_103),
        .I1(vAddr_cnt[2]),
        .O(dataAddr_internal_inferred_i_128_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    dataAddr_internal_inferred_i_129
       (.I0(dataAddr_internal4_n_104),
        .I1(vAddr_cnt[1]),
        .O(dataAddr_internal_inferred_i_129_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    dataAddr_internal_inferred_i_13
       (.I0(vAddr_cnt[5]),
        .I1(PCOUT[5]),
        .O(dataAddr_internal_inferred_i_13_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    dataAddr_internal_inferred_i_130
       (.I0(dataAddr_internal4_n_105),
        .I1(vAddr_cnt[0]),
        .O(dataAddr_internal_inferred_i_130_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    dataAddr_internal_inferred_i_14
       (.I0(vAddr_cnt[4]),
        .I1(PCOUT[4]),
        .O(dataAddr_internal_inferred_i_14_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    dataAddr_internal_inferred_i_15
       (.I0(vAddr_cnt[3]),
        .I1(PCOUT[3]),
        .O(dataAddr_internal_inferred_i_15_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    dataAddr_internal_inferred_i_16
       (.I0(vAddr_cnt[2]),
        .I1(PCOUT[2]),
        .O(dataAddr_internal_inferred_i_16_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    dataAddr_internal_inferred_i_17
       (.I0(vAddr_cnt[1]),
        .I1(PCOUT[1]),
        .O(dataAddr_internal_inferred_i_17_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    dataAddr_internal_inferred_i_18
       (.I0(vAddr_cnt[0]),
        .I1(PCOUT[0]),
        .O(dataAddr_internal_inferred_i_18_n_0));
  CARRY4 dataAddr_internal_inferred_i_19
       (.CI(dataAddr_internal_inferred_i_20_n_0),
        .CO(NLW_dataAddr_internal_inferred_i_19_CO_UNCONNECTED[3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({NLW_dataAddr_internal_inferred_i_19_O_UNCONNECTED[3:1],PCOUT[7]}),
        .S({1'b0,1'b0,1'b0,dataAddr_internal_inferred_i_22_n_0}));
  LUT2 #(
    .INIT(4'h2)) 
    dataAddr_internal_inferred_i_2
       (.I0(dataAddr_internal1[6]),
        .I1(reset_IBUF),
        .O(dataAddr_internal[6]));
  CARRY4 dataAddr_internal_inferred_i_20
       (.CI(dataAddr_internal_inferred_i_21_n_0),
        .CO({dataAddr_internal_inferred_i_20_n_0,dataAddr_internal_inferred_i_20_n_1,dataAddr_internal_inferred_i_20_n_2,dataAddr_internal_inferred_i_20_n_3}),
        .CYINIT(1'b0),
        .DI({dataAddr_internal_inferred_i_23_n_4,dataAddr_internal_inferred_i_23_n_5,dataAddr_internal_inferred_i_23_n_6,C}),
        .O(PCOUT[6:3]),
        .S({dataAddr_internal_inferred_i_25_n_0,dataAddr_internal_inferred_i_26_n_0,dataAddr_internal_inferred_i_27_n_0,dataAddr_internal_inferred_i_28_n_0}));
  CARRY4 dataAddr_internal_inferred_i_21
       (.CI(1'b0),
        .CO({dataAddr_internal_inferred_i_21_n_0,dataAddr_internal_inferred_i_21_n_1,dataAddr_internal_inferred_i_21_n_2,dataAddr_internal_inferred_i_21_n_3}),
        .CYINIT(dataAddr_internal_inferred_i_29_n_0),
        .DI({dataAddr_internal_inferred_i_30_n_5,dataAddr_internal_inferred_i_30_n_6,dataAddr_internal_inferred_i_30_n_7,1'b0}),
        .O({PCOUT[2:0],NLW_dataAddr_internal_inferred_i_21_O_UNCONNECTED[0]}),
        .S({dataAddr_internal_inferred_i_31_n_0,dataAddr_internal_inferred_i_32_n_0,dataAddr_internal_inferred_i_33_n_0,1'b1}));
  LUT3 #(
    .INIT(8'h9A)) 
    dataAddr_internal_inferred_i_22
       (.I0(dataAddr_internal_inferred_i_34_n_7),
        .I1(N_IBUF[7]),
        .I2(dataAddr_internal_inferred_i_29_n_0),
        .O(dataAddr_internal_inferred_i_22_n_0));
  CARRY4 dataAddr_internal_inferred_i_23
       (.CI(1'b0),
        .CO({dataAddr_internal_inferred_i_23_n_0,dataAddr_internal_inferred_i_23_n_1,dataAddr_internal_inferred_i_23_n_2,dataAddr_internal_inferred_i_23_n_3}),
        .CYINIT(1'b0),
        .DI({dataAddr_internal_inferred_i_35_n_0,dataAddr_internal_inferred_i_36_n_6,dataAddr_internal_inferred_i_36_n_7,dataAddr_internal_inferred_i_30_n_4}),
        .O({dataAddr_internal_inferred_i_23_n_4,dataAddr_internal_inferred_i_23_n_5,dataAddr_internal_inferred_i_23_n_6,NLW_dataAddr_internal_inferred_i_23_O_UNCONNECTED[0]}),
        .S({dataAddr_internal_inferred_i_37_n_0,dataAddr_internal_inferred_i_38_n_0,dataAddr_internal_inferred_i_39_n_0,dataAddr_internal_inferred_i_40_n_0}));
  LUT2 #(
    .INIT(4'h2)) 
    dataAddr_internal_inferred_i_24
       (.I0(dataAddr_internal_inferred_i_29_n_0),
        .I1(N_IBUF[3]),
        .O(C));
  LUT3 #(
    .INIT(8'hB4)) 
    dataAddr_internal_inferred_i_25
       (.I0(N_IBUF[6]),
        .I1(dataAddr_internal_inferred_i_29_n_0),
        .I2(dataAddr_internal_inferred_i_23_n_4),
        .O(dataAddr_internal_inferred_i_25_n_0));
  LUT3 #(
    .INIT(8'hB4)) 
    dataAddr_internal_inferred_i_26
       (.I0(N_IBUF[5]),
        .I1(dataAddr_internal_inferred_i_29_n_0),
        .I2(dataAddr_internal_inferred_i_23_n_5),
        .O(dataAddr_internal_inferred_i_26_n_0));
  LUT3 #(
    .INIT(8'hB4)) 
    dataAddr_internal_inferred_i_27
       (.I0(N_IBUF[4]),
        .I1(dataAddr_internal_inferred_i_29_n_0),
        .I2(dataAddr_internal_inferred_i_23_n_6),
        .O(dataAddr_internal_inferred_i_27_n_0));
  LUT4 #(
    .INIT(16'h4BB4)) 
    dataAddr_internal_inferred_i_28
       (.I0(N_IBUF[3]),
        .I1(dataAddr_internal_inferred_i_29_n_0),
        .I2(dataAddr_internal_inferred_i_41_n_7),
        .I3(dataAddr_internal_inferred_i_30_n_4),
        .O(dataAddr_internal_inferred_i_28_n_0));
  CARRY4 dataAddr_internal_inferred_i_29
       (.CI(dataAddr_internal_inferred_i_42_n_0),
        .CO({dataAddr_internal_inferred_i_29_n_0,dataAddr_internal_inferred_i_29_n_1,dataAddr_internal_inferred_i_29_n_2,dataAddr_internal_inferred_i_29_n_3}),
        .CYINIT(1'b0),
        .DI(dataAddr_internal3[31:28]),
        .O(NLW_dataAddr_internal_inferred_i_29_O_UNCONNECTED[3:0]),
        .S({dataAddr_internal_inferred_i_44_n_0,dataAddr_internal_inferred_i_45_n_0,dataAddr_internal_inferred_i_46_n_0,dataAddr_internal_inferred_i_47_n_0}));
  LUT2 #(
    .INIT(4'h2)) 
    dataAddr_internal_inferred_i_3
       (.I0(dataAddr_internal1[5]),
        .I1(reset_IBUF),
        .O(dataAddr_internal[5]));
  CARRY4 dataAddr_internal_inferred_i_30
       (.CI(1'b0),
        .CO({dataAddr_internal_inferred_i_30_n_0,dataAddr_internal_inferred_i_30_n_1,dataAddr_internal_inferred_i_30_n_2,dataAddr_internal_inferred_i_30_n_3}),
        .CYINIT(1'b0),
        .DI({dataAddr_internal_inferred_i_48_n_0,dataAddr_internal_inferred_i_49_n_0,dataAddr_internal_inferred_i_50_n_0,1'b0}),
        .O({dataAddr_internal_inferred_i_30_n_4,dataAddr_internal_inferred_i_30_n_5,dataAddr_internal_inferred_i_30_n_6,dataAddr_internal_inferred_i_30_n_7}),
        .S({dataAddr_internal_inferred_i_51_n_0,dataAddr_internal_inferred_i_52_n_0,dataAddr_internal_inferred_i_53_n_0,dataAddr_internal_inferred_i_54_n_0}));
  LUT3 #(
    .INIT(8'hB4)) 
    dataAddr_internal_inferred_i_31
       (.I0(N_IBUF[2]),
        .I1(dataAddr_internal_inferred_i_29_n_0),
        .I2(dataAddr_internal_inferred_i_30_n_5),
        .O(dataAddr_internal_inferred_i_31_n_0));
  LUT3 #(
    .INIT(8'hB4)) 
    dataAddr_internal_inferred_i_32
       (.I0(N_IBUF[1]),
        .I1(dataAddr_internal_inferred_i_29_n_0),
        .I2(dataAddr_internal_inferred_i_30_n_6),
        .O(dataAddr_internal_inferred_i_32_n_0));
  LUT3 #(
    .INIT(8'hB4)) 
    dataAddr_internal_inferred_i_33
       (.I0(N_IBUF[0]),
        .I1(dataAddr_internal_inferred_i_29_n_0),
        .I2(dataAddr_internal_inferred_i_30_n_7),
        .O(dataAddr_internal_inferred_i_33_n_0));
  CARRY4 dataAddr_internal_inferred_i_34
       (.CI(dataAddr_internal_inferred_i_23_n_0),
        .CO(NLW_dataAddr_internal_inferred_i_34_CO_UNCONNECTED[3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({NLW_dataAddr_internal_inferred_i_34_O_UNCONNECTED[3:1],dataAddr_internal_inferred_i_34_n_7}),
        .S({1'b0,1'b0,1'b0,dataAddr_internal_inferred_i_55_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    dataAddr_internal_inferred_i_35
       (.I0(dataAddr_internal_inferred_i_36_n_5),
        .I1(dataAddr_internal_inferred_i_41_n_4),
        .O(dataAddr_internal_inferred_i_35_n_0));
  CARRY4 dataAddr_internal_inferred_i_36
       (.CI(dataAddr_internal_inferred_i_30_n_0),
        .CO({NLW_dataAddr_internal_inferred_i_36_CO_UNCONNECTED[3],dataAddr_internal_inferred_i_36_n_1,dataAddr_internal_inferred_i_36_n_2,dataAddr_internal_inferred_i_36_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,dataAddr_internal_inferred_i_56_n_0,dataAddr_internal_inferred_i_57_n_0,dataAddr_internal_inferred_i_58_n_0}),
        .O({dataAddr_internal_inferred_i_36_n_4,dataAddr_internal_inferred_i_36_n_5,dataAddr_internal_inferred_i_36_n_6,dataAddr_internal_inferred_i_36_n_7}),
        .S({dataAddr_internal_inferred_i_59_n_0,dataAddr_internal_inferred_i_60_n_0,dataAddr_internal_inferred_i_61_n_0,dataAddr_internal_inferred_i_62_n_0}));
  LUT4 #(
    .INIT(16'h9666)) 
    dataAddr_internal_inferred_i_37
       (.I0(dataAddr_internal_inferred_i_41_n_4),
        .I1(dataAddr_internal_inferred_i_36_n_5),
        .I2(r_IBUF[6]),
        .I3(hAddr_cnt[0]),
        .O(dataAddr_internal_inferred_i_37_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    dataAddr_internal_inferred_i_38
       (.I0(dataAddr_internal_inferred_i_36_n_6),
        .I1(dataAddr_internal_inferred_i_41_n_5),
        .O(dataAddr_internal_inferred_i_38_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    dataAddr_internal_inferred_i_39
       (.I0(dataAddr_internal_inferred_i_36_n_7),
        .I1(dataAddr_internal_inferred_i_41_n_6),
        .O(dataAddr_internal_inferred_i_39_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    dataAddr_internal_inferred_i_4
       (.I0(dataAddr_internal1[4]),
        .I1(reset_IBUF),
        .O(dataAddr_internal[4]));
  LUT2 #(
    .INIT(4'h6)) 
    dataAddr_internal_inferred_i_40
       (.I0(dataAddr_internal_inferred_i_30_n_4),
        .I1(dataAddr_internal_inferred_i_41_n_7),
        .O(dataAddr_internal_inferred_i_40_n_0));
  CARRY4 dataAddr_internal_inferred_i_41
       (.CI(1'b0),
        .CO({dataAddr_internal_inferred_i_41_n_0,dataAddr_internal_inferred_i_41_n_1,dataAddr_internal_inferred_i_41_n_2,dataAddr_internal_inferred_i_41_n_3}),
        .CYINIT(1'b0),
        .DI({dataAddr_internal_inferred_i_63_n_0,dataAddr_internal_inferred_i_64_n_0,dataAddr_internal_inferred_i_65_n_0,1'b0}),
        .O({dataAddr_internal_inferred_i_41_n_4,dataAddr_internal_inferred_i_41_n_5,dataAddr_internal_inferred_i_41_n_6,dataAddr_internal_inferred_i_41_n_7}),
        .S({dataAddr_internal_inferred_i_66_n_0,dataAddr_internal_inferred_i_67_n_0,dataAddr_internal_inferred_i_68_n_0,dataAddr_internal_inferred_i_69_n_0}));
  CARRY4 dataAddr_internal_inferred_i_42
       (.CI(dataAddr_internal_inferred_i_70_n_0),
        .CO({dataAddr_internal_inferred_i_42_n_0,dataAddr_internal_inferred_i_42_n_1,dataAddr_internal_inferred_i_42_n_2,dataAddr_internal_inferred_i_42_n_3}),
        .CYINIT(1'b0),
        .DI(dataAddr_internal3[27:24]),
        .O(NLW_dataAddr_internal_inferred_i_42_O_UNCONNECTED[3:0]),
        .S({dataAddr_internal_inferred_i_72_n_0,dataAddr_internal_inferred_i_73_n_0,dataAddr_internal_inferred_i_74_n_0,dataAddr_internal_inferred_i_75_n_0}));
  CARRY4 dataAddr_internal_inferred_i_43
       (.CI(dataAddr_internal_inferred_i_71_n_0),
        .CO({NLW_dataAddr_internal_inferred_i_43_CO_UNCONNECTED[3],dataAddr_internal_inferred_i_43_n_1,dataAddr_internal_inferred_i_43_n_2,dataAddr_internal_inferred_i_43_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(dataAddr_internal3[31:28]),
        .S({dataAddr_internal4__0_n_91,dataAddr_internal4__0_n_92,dataAddr_internal4__0_n_93,dataAddr_internal4__0_n_94}));
  LUT2 #(
    .INIT(4'h9)) 
    dataAddr_internal_inferred_i_44
       (.I0(dataAddr_internal3[31]),
        .I1(N_IBUF[31]),
        .O(dataAddr_internal_inferred_i_44_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    dataAddr_internal_inferred_i_45
       (.I0(dataAddr_internal3[30]),
        .I1(N_IBUF[30]),
        .O(dataAddr_internal_inferred_i_45_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    dataAddr_internal_inferred_i_46
       (.I0(dataAddr_internal3[29]),
        .I1(N_IBUF[29]),
        .O(dataAddr_internal_inferred_i_46_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    dataAddr_internal_inferred_i_47
       (.I0(dataAddr_internal3[28]),
        .I1(N_IBUF[28]),
        .O(dataAddr_internal_inferred_i_47_n_0));
  LUT6 #(
    .INIT(64'h8777788878887888)) 
    dataAddr_internal_inferred_i_48
       (.I0(r_IBUF[0]),
        .I1(hAddr_cnt[3]),
        .I2(hAddr_cnt[2]),
        .I3(r_IBUF[1]),
        .I4(hAddr_cnt[1]),
        .I5(r_IBUF[2]),
        .O(dataAddr_internal_inferred_i_48_n_0));
  LUT4 #(
    .INIT(16'h7888)) 
    dataAddr_internal_inferred_i_49
       (.I0(r_IBUF[1]),
        .I1(hAddr_cnt[1]),
        .I2(r_IBUF[2]),
        .I3(hAddr_cnt[0]),
        .O(dataAddr_internal_inferred_i_49_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    dataAddr_internal_inferred_i_5
       (.I0(dataAddr_internal1[3]),
        .I1(reset_IBUF),
        .O(dataAddr_internal[3]));
  LUT2 #(
    .INIT(4'h8)) 
    dataAddr_internal_inferred_i_50
       (.I0(hAddr_cnt[0]),
        .I1(r_IBUF[1]),
        .O(dataAddr_internal_inferred_i_50_n_0));
  LUT6 #(
    .INIT(64'h6A953F3F6A6AC0C0)) 
    dataAddr_internal_inferred_i_51
       (.I0(hAddr_cnt[2]),
        .I1(hAddr_cnt[3]),
        .I2(r_IBUF[0]),
        .I3(hAddr_cnt[0]),
        .I4(r_IBUF[1]),
        .I5(dataAddr_internal_inferred_i_76_n_0),
        .O(dataAddr_internal_inferred_i_51_n_0));
  LUT6 #(
    .INIT(64'h8777788878887888)) 
    dataAddr_internal_inferred_i_52
       (.I0(hAddr_cnt[0]),
        .I1(r_IBUF[2]),
        .I2(hAddr_cnt[1]),
        .I3(r_IBUF[1]),
        .I4(r_IBUF[0]),
        .I5(hAddr_cnt[2]),
        .O(dataAddr_internal_inferred_i_52_n_0));
  LUT4 #(
    .INIT(16'h7888)) 
    dataAddr_internal_inferred_i_53
       (.I0(r_IBUF[0]),
        .I1(hAddr_cnt[1]),
        .I2(r_IBUF[1]),
        .I3(hAddr_cnt[0]),
        .O(dataAddr_internal_inferred_i_53_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    dataAddr_internal_inferred_i_54
       (.I0(hAddr_cnt[0]),
        .I1(r_IBUF[0]),
        .O(dataAddr_internal_inferred_i_54_n_0));
  LUT5 #(
    .INIT(32'h69999666)) 
    dataAddr_internal_inferred_i_55
       (.I0(dataAddr_internal_inferred_i_77_n_7),
        .I1(dataAddr_internal_inferred_i_36_n_4),
        .I2(hAddr_cnt[0]),
        .I3(r_IBUF[7]),
        .I4(dataAddr_internal_inferred_i_78_n_0),
        .O(dataAddr_internal_inferred_i_55_n_0));
  LUT6 #(
    .INIT(64'hF888800080008000)) 
    dataAddr_internal_inferred_i_56
       (.I0(r_IBUF[2]),
        .I1(hAddr_cnt[3]),
        .I2(r_IBUF[1]),
        .I3(hAddr_cnt[4]),
        .I4(hAddr_cnt[5]),
        .I5(r_IBUF[0]),
        .O(dataAddr_internal_inferred_i_56_n_0));
  LUT6 #(
    .INIT(64'hF888800080008000)) 
    dataAddr_internal_inferred_i_57
       (.I0(r_IBUF[2]),
        .I1(hAddr_cnt[2]),
        .I2(r_IBUF[1]),
        .I3(hAddr_cnt[3]),
        .I4(hAddr_cnt[4]),
        .I5(r_IBUF[0]),
        .O(dataAddr_internal_inferred_i_57_n_0));
  LUT6 #(
    .INIT(64'hF888800080008000)) 
    dataAddr_internal_inferred_i_58
       (.I0(r_IBUF[2]),
        .I1(hAddr_cnt[1]),
        .I2(r_IBUF[1]),
        .I3(hAddr_cnt[2]),
        .I4(hAddr_cnt[3]),
        .I5(r_IBUF[0]),
        .O(dataAddr_internal_inferred_i_58_n_0));
  LUT6 #(
    .INIT(64'hEA808080157F7F7F)) 
    dataAddr_internal_inferred_i_59
       (.I0(dataAddr_internal_inferred_i_79_n_0),
        .I1(hAddr_cnt[5]),
        .I2(r_IBUF[1]),
        .I3(hAddr_cnt[4]),
        .I4(r_IBUF[2]),
        .I5(dataAddr_internal_inferred_i_80_n_0),
        .O(dataAddr_internal_inferred_i_59_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    dataAddr_internal_inferred_i_6
       (.I0(dataAddr_internal1[2]),
        .I1(reset_IBUF),
        .O(dataAddr_internal[2]));
  LUT6 #(
    .INIT(64'h6999966696669666)) 
    dataAddr_internal_inferred_i_60
       (.I0(dataAddr_internal_inferred_i_56_n_0),
        .I1(dataAddr_internal_inferred_i_81_n_0),
        .I2(r_IBUF[1]),
        .I3(hAddr_cnt[5]),
        .I4(hAddr_cnt[6]),
        .I5(r_IBUF[0]),
        .O(dataAddr_internal_inferred_i_60_n_0));
  LUT6 #(
    .INIT(64'h6A95956A956A956A)) 
    dataAddr_internal_inferred_i_61
       (.I0(dataAddr_internal_inferred_i_57_n_0),
        .I1(r_IBUF[2]),
        .I2(hAddr_cnt[3]),
        .I3(dataAddr_internal_inferred_i_82_n_0),
        .I4(hAddr_cnt[5]),
        .I5(r_IBUF[0]),
        .O(dataAddr_internal_inferred_i_61_n_0));
  LUT6 #(
    .INIT(64'h6A95956A956A956A)) 
    dataAddr_internal_inferred_i_62
       (.I0(dataAddr_internal_inferred_i_58_n_0),
        .I1(r_IBUF[2]),
        .I2(hAddr_cnt[2]),
        .I3(dataAddr_internal_inferred_i_83_n_0),
        .I4(hAddr_cnt[4]),
        .I5(r_IBUF[0]),
        .O(dataAddr_internal_inferred_i_62_n_0));
  LUT6 #(
    .INIT(64'h8777788878887888)) 
    dataAddr_internal_inferred_i_63
       (.I0(r_IBUF[3]),
        .I1(hAddr_cnt[3]),
        .I2(hAddr_cnt[2]),
        .I3(r_IBUF[4]),
        .I4(hAddr_cnt[1]),
        .I5(r_IBUF[5]),
        .O(dataAddr_internal_inferred_i_63_n_0));
  LUT4 #(
    .INIT(16'h7888)) 
    dataAddr_internal_inferred_i_64
       (.I0(r_IBUF[4]),
        .I1(hAddr_cnt[1]),
        .I2(r_IBUF[5]),
        .I3(hAddr_cnt[0]),
        .O(dataAddr_internal_inferred_i_64_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    dataAddr_internal_inferred_i_65
       (.I0(hAddr_cnt[0]),
        .I1(r_IBUF[4]),
        .O(dataAddr_internal_inferred_i_65_n_0));
  LUT6 #(
    .INIT(64'h6A953F3F6A6AC0C0)) 
    dataAddr_internal_inferred_i_66
       (.I0(hAddr_cnt[2]),
        .I1(hAddr_cnt[3]),
        .I2(r_IBUF[3]),
        .I3(hAddr_cnt[0]),
        .I4(r_IBUF[4]),
        .I5(dataAddr_internal_inferred_i_84_n_0),
        .O(dataAddr_internal_inferred_i_66_n_0));
  LUT6 #(
    .INIT(64'h8777788878887888)) 
    dataAddr_internal_inferred_i_67
       (.I0(hAddr_cnt[0]),
        .I1(r_IBUF[5]),
        .I2(hAddr_cnt[1]),
        .I3(r_IBUF[4]),
        .I4(r_IBUF[3]),
        .I5(hAddr_cnt[2]),
        .O(dataAddr_internal_inferred_i_67_n_0));
  LUT4 #(
    .INIT(16'h7888)) 
    dataAddr_internal_inferred_i_68
       (.I0(r_IBUF[3]),
        .I1(hAddr_cnt[1]),
        .I2(r_IBUF[4]),
        .I3(hAddr_cnt[0]),
        .O(dataAddr_internal_inferred_i_68_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    dataAddr_internal_inferred_i_69
       (.I0(hAddr_cnt[0]),
        .I1(r_IBUF[3]),
        .O(dataAddr_internal_inferred_i_69_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    dataAddr_internal_inferred_i_7
       (.I0(dataAddr_internal1[1]),
        .I1(reset_IBUF),
        .O(dataAddr_internal[1]));
  CARRY4 dataAddr_internal_inferred_i_70
       (.CI(dataAddr_internal_inferred_i_85_n_0),
        .CO({dataAddr_internal_inferred_i_70_n_0,dataAddr_internal_inferred_i_70_n_1,dataAddr_internal_inferred_i_70_n_2,dataAddr_internal_inferred_i_70_n_3}),
        .CYINIT(1'b0),
        .DI(dataAddr_internal3[23:20]),
        .O(NLW_dataAddr_internal_inferred_i_70_O_UNCONNECTED[3:0]),
        .S({dataAddr_internal_inferred_i_87_n_0,dataAddr_internal_inferred_i_88_n_0,dataAddr_internal_inferred_i_89_n_0,dataAddr_internal_inferred_i_90_n_0}));
  CARRY4 dataAddr_internal_inferred_i_71
       (.CI(dataAddr_internal_inferred_i_86_n_0),
        .CO({dataAddr_internal_inferred_i_71_n_0,dataAddr_internal_inferred_i_71_n_1,dataAddr_internal_inferred_i_71_n_2,dataAddr_internal_inferred_i_71_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(dataAddr_internal3[27:24]),
        .S({dataAddr_internal4__0_n_95,dataAddr_internal4__0_n_96,dataAddr_internal4__0_n_97,dataAddr_internal4__0_n_98}));
  LUT2 #(
    .INIT(4'h9)) 
    dataAddr_internal_inferred_i_72
       (.I0(dataAddr_internal3[27]),
        .I1(N_IBUF[27]),
        .O(dataAddr_internal_inferred_i_72_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    dataAddr_internal_inferred_i_73
       (.I0(dataAddr_internal3[26]),
        .I1(N_IBUF[26]),
        .O(dataAddr_internal_inferred_i_73_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    dataAddr_internal_inferred_i_74
       (.I0(dataAddr_internal3[25]),
        .I1(N_IBUF[25]),
        .O(dataAddr_internal_inferred_i_74_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    dataAddr_internal_inferred_i_75
       (.I0(dataAddr_internal3[24]),
        .I1(N_IBUF[24]),
        .O(dataAddr_internal_inferred_i_75_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    dataAddr_internal_inferred_i_76
       (.I0(hAddr_cnt[1]),
        .I1(r_IBUF[2]),
        .O(dataAddr_internal_inferred_i_76_n_0));
  CARRY4 dataAddr_internal_inferred_i_77
       (.CI(dataAddr_internal_inferred_i_41_n_0),
        .CO(NLW_dataAddr_internal_inferred_i_77_CO_UNCONNECTED[3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({NLW_dataAddr_internal_inferred_i_77_O_UNCONNECTED[3:1],dataAddr_internal_inferred_i_77_n_7}),
        .S({1'b0,1'b0,1'b0,dataAddr_internal_inferred_i_91_n_0}));
  LUT4 #(
    .INIT(16'h7888)) 
    dataAddr_internal_inferred_i_78
       (.I0(r_IBUF[6]),
        .I1(hAddr_cnt[1]),
        .I2(dataAddr_internal_inferred_i_41_n_4),
        .I3(dataAddr_internal_inferred_i_36_n_5),
        .O(dataAddr_internal_inferred_i_78_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    dataAddr_internal_inferred_i_79
       (.I0(hAddr_cnt[6]),
        .I1(r_IBUF[0]),
        .O(dataAddr_internal_inferred_i_79_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    dataAddr_internal_inferred_i_8
       (.I0(dataAddr_internal1[0]),
        .I1(reset_IBUF),
        .O(dataAddr_internal[0]));
  LUT6 #(
    .INIT(64'h7888877787778777)) 
    dataAddr_internal_inferred_i_80
       (.I0(r_IBUF[0]),
        .I1(hAddr_cnt[7]),
        .I2(hAddr_cnt[6]),
        .I3(r_IBUF[1]),
        .I4(hAddr_cnt[5]),
        .I5(r_IBUF[2]),
        .O(dataAddr_internal_inferred_i_80_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    dataAddr_internal_inferred_i_81
       (.I0(hAddr_cnt[4]),
        .I1(r_IBUF[2]),
        .O(dataAddr_internal_inferred_i_81_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    dataAddr_internal_inferred_i_82
       (.I0(hAddr_cnt[4]),
        .I1(r_IBUF[1]),
        .O(dataAddr_internal_inferred_i_82_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    dataAddr_internal_inferred_i_83
       (.I0(hAddr_cnt[3]),
        .I1(r_IBUF[1]),
        .O(dataAddr_internal_inferred_i_83_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    dataAddr_internal_inferred_i_84
       (.I0(hAddr_cnt[1]),
        .I1(r_IBUF[5]),
        .O(dataAddr_internal_inferred_i_84_n_0));
  CARRY4 dataAddr_internal_inferred_i_85
       (.CI(dataAddr_internal_inferred_i_92_n_0),
        .CO({dataAddr_internal_inferred_i_85_n_0,dataAddr_internal_inferred_i_85_n_1,dataAddr_internal_inferred_i_85_n_2,dataAddr_internal_inferred_i_85_n_3}),
        .CYINIT(1'b0),
        .DI(dataAddr_internal3[19:16]),
        .O(NLW_dataAddr_internal_inferred_i_85_O_UNCONNECTED[3:0]),
        .S({dataAddr_internal_inferred_i_94_n_0,dataAddr_internal_inferred_i_95_n_0,dataAddr_internal_inferred_i_96_n_0,dataAddr_internal_inferred_i_97_n_0}));
  CARRY4 dataAddr_internal_inferred_i_86
       (.CI(dataAddr_internal_inferred_i_93_n_0),
        .CO({dataAddr_internal_inferred_i_86_n_0,dataAddr_internal_inferred_i_86_n_1,dataAddr_internal_inferred_i_86_n_2,dataAddr_internal_inferred_i_86_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(dataAddr_internal3[23:20]),
        .S({dataAddr_internal4__0_n_99,dataAddr_internal4__0_n_100,dataAddr_internal4__0_n_101,dataAddr_internal4__0_n_102}));
  LUT2 #(
    .INIT(4'h9)) 
    dataAddr_internal_inferred_i_87
       (.I0(dataAddr_internal3[23]),
        .I1(N_IBUF[23]),
        .O(dataAddr_internal_inferred_i_87_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    dataAddr_internal_inferred_i_88
       (.I0(dataAddr_internal3[22]),
        .I1(N_IBUF[22]),
        .O(dataAddr_internal_inferred_i_88_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    dataAddr_internal_inferred_i_89
       (.I0(dataAddr_internal3[21]),
        .I1(N_IBUF[21]),
        .O(dataAddr_internal_inferred_i_89_n_0));
  CARRY4 dataAddr_internal_inferred_i_9
       (.CI(dataAddr_internal_inferred_i_10_n_0),
        .CO({NLW_dataAddr_internal_inferred_i_9_CO_UNCONNECTED[3],dataAddr_internal_inferred_i_9_n_1,dataAddr_internal_inferred_i_9_n_2,dataAddr_internal_inferred_i_9_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,vAddr_cnt[6:4]}),
        .O(dataAddr_internal1[7:4]),
        .S({dataAddr_internal_inferred_i_11_n_0,dataAddr_internal_inferred_i_12_n_0,dataAddr_internal_inferred_i_13_n_0,dataAddr_internal_inferred_i_14_n_0}));
  LUT2 #(
    .INIT(4'h9)) 
    dataAddr_internal_inferred_i_90
       (.I0(dataAddr_internal3[20]),
        .I1(N_IBUF[20]),
        .O(dataAddr_internal_inferred_i_90_n_0));
  LUT6 #(
    .INIT(64'hEA808080157F7F7F)) 
    dataAddr_internal_inferred_i_91
       (.I0(dataAddr_internal_inferred_i_98_n_0),
        .I1(hAddr_cnt[2]),
        .I2(r_IBUF[4]),
        .I3(hAddr_cnt[1]),
        .I4(r_IBUF[5]),
        .I5(dataAddr_internal_inferred_i_99_n_0),
        .O(dataAddr_internal_inferred_i_91_n_0));
  CARRY4 dataAddr_internal_inferred_i_92
       (.CI(dataAddr_internal_inferred_i_100_n_0),
        .CO({dataAddr_internal_inferred_i_92_n_0,dataAddr_internal_inferred_i_92_n_1,dataAddr_internal_inferred_i_92_n_2,dataAddr_internal_inferred_i_92_n_3}),
        .CYINIT(1'b0),
        .DI(dataAddr_internal3[15:12]),
        .O(NLW_dataAddr_internal_inferred_i_92_O_UNCONNECTED[3:0]),
        .S({dataAddr_internal_inferred_i_102_n_0,dataAddr_internal_inferred_i_103_n_0,dataAddr_internal_inferred_i_104_n_0,dataAddr_internal_inferred_i_105_n_0}));
  CARRY4 dataAddr_internal_inferred_i_93
       (.CI(dataAddr_internal_inferred_i_101_n_0),
        .CO({dataAddr_internal_inferred_i_93_n_0,dataAddr_internal_inferred_i_93_n_1,dataAddr_internal_inferred_i_93_n_2,dataAddr_internal_inferred_i_93_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(dataAddr_internal3[19:16]),
        .S({dataAddr_internal4__0_n_103,dataAddr_internal4__0_n_104,dataAddr_internal4__0_n_105,dataAddr_internal4_n_89}));
  LUT2 #(
    .INIT(4'h9)) 
    dataAddr_internal_inferred_i_94
       (.I0(dataAddr_internal3[19]),
        .I1(N_IBUF[19]),
        .O(dataAddr_internal_inferred_i_94_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    dataAddr_internal_inferred_i_95
       (.I0(dataAddr_internal3[18]),
        .I1(N_IBUF[18]),
        .O(dataAddr_internal_inferred_i_95_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    dataAddr_internal_inferred_i_96
       (.I0(dataAddr_internal3[17]),
        .I1(N_IBUF[17]),
        .O(dataAddr_internal_inferred_i_96_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    dataAddr_internal_inferred_i_97
       (.I0(dataAddr_internal3[16]),
        .I1(N_IBUF[16]),
        .O(dataAddr_internal_inferred_i_97_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    dataAddr_internal_inferred_i_98
       (.I0(hAddr_cnt[3]),
        .I1(r_IBUF[3]),
        .O(dataAddr_internal_inferred_i_98_n_0));
  LUT6 #(
    .INIT(64'h7888877787778777)) 
    dataAddr_internal_inferred_i_99
       (.I0(r_IBUF[3]),
        .I1(hAddr_cnt[4]),
        .I2(hAddr_cnt[3]),
        .I3(r_IBUF[4]),
        .I4(hAddr_cnt[2]),
        .I5(r_IBUF[5]),
        .O(dataAddr_internal_inferred_i_99_n_0));
  IBUF \dataMem_in_IBUF[0]_inst 
       (.I(dataMem_in[0]),
        .O(dataMem_in_IBUF[0]));
  IBUF \dataMem_in_IBUF[10]_inst 
       (.I(dataMem_in[10]),
        .O(dataMem_in_IBUF[10]));
  IBUF \dataMem_in_IBUF[11]_inst 
       (.I(dataMem_in[11]),
        .O(dataMem_in_IBUF[11]));
  IBUF \dataMem_in_IBUF[12]_inst 
       (.I(dataMem_in[12]),
        .O(dataMem_in_IBUF[12]));
  IBUF \dataMem_in_IBUF[13]_inst 
       (.I(dataMem_in[13]),
        .O(dataMem_in_IBUF[13]));
  IBUF \dataMem_in_IBUF[14]_inst 
       (.I(dataMem_in[14]),
        .O(dataMem_in_IBUF[14]));
  IBUF \dataMem_in_IBUF[15]_inst 
       (.I(dataMem_in[15]),
        .O(dataMem_in_IBUF[15]));
  IBUF \dataMem_in_IBUF[16]_inst 
       (.I(dataMem_in[16]),
        .O(dataMem_in_IBUF[16]));
  IBUF \dataMem_in_IBUF[17]_inst 
       (.I(dataMem_in[17]),
        .O(dataMem_in_IBUF[17]));
  IBUF \dataMem_in_IBUF[18]_inst 
       (.I(dataMem_in[18]),
        .O(dataMem_in_IBUF[18]));
  IBUF \dataMem_in_IBUF[19]_inst 
       (.I(dataMem_in[19]),
        .O(dataMem_in_IBUF[19]));
  IBUF \dataMem_in_IBUF[1]_inst 
       (.I(dataMem_in[1]),
        .O(dataMem_in_IBUF[1]));
  IBUF \dataMem_in_IBUF[20]_inst 
       (.I(dataMem_in[20]),
        .O(dataMem_in_IBUF[20]));
  IBUF \dataMem_in_IBUF[21]_inst 
       (.I(dataMem_in[21]),
        .O(dataMem_in_IBUF[21]));
  IBUF \dataMem_in_IBUF[22]_inst 
       (.I(dataMem_in[22]),
        .O(dataMem_in_IBUF[22]));
  IBUF \dataMem_in_IBUF[23]_inst 
       (.I(dataMem_in[23]),
        .O(dataMem_in_IBUF[23]));
  IBUF \dataMem_in_IBUF[24]_inst 
       (.I(dataMem_in[24]),
        .O(dataMem_in_IBUF[24]));
  IBUF \dataMem_in_IBUF[25]_inst 
       (.I(dataMem_in[25]),
        .O(dataMem_in_IBUF[25]));
  IBUF \dataMem_in_IBUF[26]_inst 
       (.I(dataMem_in[26]),
        .O(dataMem_in_IBUF[26]));
  IBUF \dataMem_in_IBUF[27]_inst 
       (.I(dataMem_in[27]),
        .O(dataMem_in_IBUF[27]));
  IBUF \dataMem_in_IBUF[28]_inst 
       (.I(dataMem_in[28]),
        .O(dataMem_in_IBUF[28]));
  IBUF \dataMem_in_IBUF[29]_inst 
       (.I(dataMem_in[29]),
        .O(dataMem_in_IBUF[29]));
  IBUF \dataMem_in_IBUF[2]_inst 
       (.I(dataMem_in[2]),
        .O(dataMem_in_IBUF[2]));
  IBUF \dataMem_in_IBUF[30]_inst 
       (.I(dataMem_in[30]),
        .O(dataMem_in_IBUF[30]));
  IBUF \dataMem_in_IBUF[31]_inst 
       (.I(dataMem_in[31]),
        .O(dataMem_in_IBUF[31]));
  IBUF \dataMem_in_IBUF[3]_inst 
       (.I(dataMem_in[3]),
        .O(dataMem_in_IBUF[3]));
  IBUF \dataMem_in_IBUF[4]_inst 
       (.I(dataMem_in[4]),
        .O(dataMem_in_IBUF[4]));
  IBUF \dataMem_in_IBUF[5]_inst 
       (.I(dataMem_in[5]),
        .O(dataMem_in_IBUF[5]));
  IBUF \dataMem_in_IBUF[6]_inst 
       (.I(dataMem_in[6]),
        .O(dataMem_in_IBUF[6]));
  IBUF \dataMem_in_IBUF[7]_inst 
       (.I(dataMem_in[7]),
        .O(dataMem_in_IBUF[7]));
  IBUF \dataMem_in_IBUF[8]_inst 
       (.I(dataMem_in[8]),
        .O(dataMem_in_IBUF[8]));
  IBUF \dataMem_in_IBUF[9]_inst 
       (.I(dataMem_in[9]),
        .O(dataMem_in_IBUF[9]));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataMem_out_internal_reg[0] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(dataMem_out[0]),
        .Q(dataMem_out_internal[0]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataMem_out_internal_reg[10] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(dataMem_out[10]),
        .Q(dataMem_out_internal[10]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataMem_out_internal_reg[11] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(dataMem_out[11]),
        .Q(dataMem_out_internal[11]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataMem_out_internal_reg[12] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(dataMem_out[12]),
        .Q(dataMem_out_internal[12]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataMem_out_internal_reg[13] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(dataMem_out[13]),
        .Q(dataMem_out_internal[13]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataMem_out_internal_reg[14] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(dataMem_out[14]),
        .Q(dataMem_out_internal[14]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataMem_out_internal_reg[15] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(dataMem_out[15]),
        .Q(dataMem_out_internal[15]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataMem_out_internal_reg[16] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(dataMem_out[16]),
        .Q(dataMem_out_internal[16]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataMem_out_internal_reg[17] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(dataMem_out[17]),
        .Q(dataMem_out_internal[17]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataMem_out_internal_reg[18] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(dataMem_out[18]),
        .Q(dataMem_out_internal[18]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataMem_out_internal_reg[19] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(dataMem_out[19]),
        .Q(dataMem_out_internal[19]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataMem_out_internal_reg[1] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(dataMem_out[1]),
        .Q(dataMem_out_internal[1]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataMem_out_internal_reg[20] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(dataMem_out[20]),
        .Q(dataMem_out_internal[20]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataMem_out_internal_reg[21] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(dataMem_out[21]),
        .Q(dataMem_out_internal[21]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataMem_out_internal_reg[22] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(dataMem_out[22]),
        .Q(dataMem_out_internal[22]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataMem_out_internal_reg[23] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(dataMem_out[23]),
        .Q(dataMem_out_internal[23]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataMem_out_internal_reg[24] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(dataMem_out[24]),
        .Q(dataMem_out_internal[24]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataMem_out_internal_reg[25] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(dataMem_out[25]),
        .Q(dataMem_out_internal[25]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataMem_out_internal_reg[26] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(dataMem_out[26]),
        .Q(dataMem_out_internal[26]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataMem_out_internal_reg[27] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(dataMem_out[27]),
        .Q(dataMem_out_internal[27]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataMem_out_internal_reg[28] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(dataMem_out[28]),
        .Q(dataMem_out_internal[28]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataMem_out_internal_reg[29] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(dataMem_out[29]),
        .Q(dataMem_out_internal[29]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataMem_out_internal_reg[2] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(dataMem_out[2]),
        .Q(dataMem_out_internal[2]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataMem_out_internal_reg[30] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(dataMem_out[30]),
        .Q(dataMem_out_internal[30]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataMem_out_internal_reg[31] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(dataMem_out[31]),
        .Q(dataMem_out_internal[31]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataMem_out_internal_reg[3] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(dataMem_out[3]),
        .Q(dataMem_out_internal[3]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataMem_out_internal_reg[4] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(dataMem_out[4]),
        .Q(dataMem_out_internal[4]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataMem_out_internal_reg[5] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(dataMem_out[5]),
        .Q(dataMem_out_internal[5]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataMem_out_internal_reg[6] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(dataMem_out[6]),
        .Q(dataMem_out_internal[6]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataMem_out_internal_reg[7] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(dataMem_out[7]),
        .Q(dataMem_out_internal[7]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataMem_out_internal_reg[8] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(dataMem_out[8]),
        .Q(dataMem_out_internal[8]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \dataMem_out_internal_reg[9] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(dataMem_out[9]),
        .Q(dataMem_out_internal[9]),
        .R(reset_IBUF));
  OBUF done_OBUF_inst
       (.I(done_internal3),
        .O(done));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    done_internal1_reg
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(done_internal),
        .Q(done_internal1),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    done_internal2_reg
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(done_internal1),
        .Q(done_internal2),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    done_internal3_reg
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(done_internal2),
        .Q(done_internal3),
        .R(reset_IBUF));
  LUT6 #(
    .INIT(64'hCCA8CCFFCCA8CC00)) 
    done_internal_i_1
       (.I0(currentState[1]),
        .I1(done_internal),
        .I2(done_internal_i_2_n_0),
        .I3(reset_IBUF),
        .I4(\hAddr_cnt[7]_i_1_n_0 ),
        .I5(done_internal),
        .O(done_internal_i_1_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    done_internal_i_2
       (.I0(currentState28_in),
        .I1(currentState2),
        .O(done_internal_i_2_n_0));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    done_internal_reg
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(done_internal_i_1_n_0),
        .Q(done_internal),
        .R(1'b0));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    end_ops_internal1_reg
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(end_ops_internal),
        .Q(end_ops_internal1),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    end_ops_internal2_reg
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(end_ops_internal1),
        .Q(end_ops_internal2),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    end_ops_internal3_reg
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(end_ops_internal2),
        .Q(end_ops_internal3),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    end_ops_internal4_reg
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(end_ops_internal3),
        .Q(end_ops_internal4),
        .R(reset_IBUF));
  LUT6 #(
    .INIT(64'h2020202020000000)) 
    end_ops_internal_inferred_i_1
       (.I0(end_ops_internal_inferred_i_2_n_0),
        .I1(reset_IBUF),
        .I2(end_ops_internal_inferred_i_3_n_1),
        .I3(currentState[1]),
        .I4(currentState[0]),
        .I5(currentState[2]),
        .O(end_ops_internal));
  CARRY4 end_ops_internal_inferred_i_10
       (.CI(1'b0),
        .CO({end_ops_internal_inferred_i_10_n_0,end_ops_internal_inferred_i_10_n_1,end_ops_internal_inferred_i_10_n_2,end_ops_internal_inferred_i_10_n_3}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_end_ops_internal_inferred_i_10_O_UNCONNECTED[3:0]),
        .S({end_ops_internal_inferred_i_18_n_0,end_ops_internal_inferred_i_19_n_0,end_ops_internal_inferred_i_20_n_0,end_ops_internal_inferred_i_21_n_0}));
  LUT3 #(
    .INIT(8'h01)) 
    end_ops_internal_inferred_i_11
       (.I0(end_ops_internal2__0[23]),
        .I1(end_ops_internal2__0[22]),
        .I2(end_ops_internal2__0[21]),
        .O(end_ops_internal_inferred_i_11_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    end_ops_internal_inferred_i_12
       (.I0(end_ops_internal2__0[19]),
        .I1(end_ops_internal2__0[18]),
        .I2(end_ops_internal2__0[20]),
        .O(end_ops_internal_inferred_i_12_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    end_ops_internal_inferred_i_13
       (.I0(end_ops_internal2__0[17]),
        .I1(end_ops_internal2__0[16]),
        .I2(end_ops_internal2__0[15]),
        .O(end_ops_internal_inferred_i_13_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    end_ops_internal_inferred_i_14
       (.I0(end_ops_internal2__0[13]),
        .I1(end_ops_internal2__0[12]),
        .I2(end_ops_internal2__0[14]),
        .O(end_ops_internal_inferred_i_14_n_0));
  CARRY4 end_ops_internal_inferred_i_15
       (.CI(end_ops_internal_inferred_i_16_n_0),
        .CO({NLW_end_ops_internal_inferred_i_15_CO_UNCONNECTED[3:2],end_ops_internal_inferred_i_15_n_2,end_ops_internal_inferred_i_15_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,f_IBUF[30:29]}),
        .O({NLW_end_ops_internal_inferred_i_15_O_UNCONNECTED[3],end_ops_internal2__0[31:29]}),
        .S({1'b0,end_ops_internal_inferred_i_25_n_0,end_ops_internal_inferred_i_26_n_0,end_ops_internal_inferred_i_27_n_0}));
  CARRY4 end_ops_internal_inferred_i_16
       (.CI(end_ops_internal_inferred_i_17_n_0),
        .CO({end_ops_internal_inferred_i_16_n_0,end_ops_internal_inferred_i_16_n_1,end_ops_internal_inferred_i_16_n_2,end_ops_internal_inferred_i_16_n_3}),
        .CYINIT(1'b0),
        .DI(f_IBUF[28:25]),
        .O(end_ops_internal2__0[28:25]),
        .S({end_ops_internal_inferred_i_28_n_0,end_ops_internal_inferred_i_29_n_0,end_ops_internal_inferred_i_30_n_0,end_ops_internal_inferred_i_31_n_0}));
  CARRY4 end_ops_internal_inferred_i_17
       (.CI(end_ops_internal_inferred_i_22_n_0),
        .CO({end_ops_internal_inferred_i_17_n_0,end_ops_internal_inferred_i_17_n_1,end_ops_internal_inferred_i_17_n_2,end_ops_internal_inferred_i_17_n_3}),
        .CYINIT(1'b0),
        .DI(f_IBUF[24:21]),
        .O(end_ops_internal2__0[24:21]),
        .S({end_ops_internal_inferred_i_32_n_0,end_ops_internal_inferred_i_33_n_0,end_ops_internal_inferred_i_34_n_0,end_ops_internal_inferred_i_35_n_0}));
  LUT3 #(
    .INIT(8'h01)) 
    end_ops_internal_inferred_i_18
       (.I0(end_ops_internal2__0[11]),
        .I1(end_ops_internal2__0[10]),
        .I2(end_ops_internal2__0[9]),
        .O(end_ops_internal_inferred_i_18_n_0));
  LUT5 #(
    .INIT(32'h00009009)) 
    end_ops_internal_inferred_i_19
       (.I0(end_ops_internal2__0[6]),
        .I1(hAddr_cnt[6]),
        .I2(end_ops_internal2__0[7]),
        .I3(hAddr_cnt[7]),
        .I4(end_ops_internal2__0[8]),
        .O(end_ops_internal_inferred_i_19_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    end_ops_internal_inferred_i_2
       (.I0(end_ops_internal_inferred_i_4_n_0),
        .I1(end_ops_internal_inferred_i_5_n_0),
        .I2(hAddr_cnt[1]),
        .I3(hAddr_cnt[0]),
        .I4(hAddr_cnt[3]),
        .I5(hAddr_cnt[2]),
        .O(end_ops_internal_inferred_i_2_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    end_ops_internal_inferred_i_20
       (.I0(end_ops_internal2__0[4]),
        .I1(hAddr_cnt[4]),
        .I2(end_ops_internal2__0[5]),
        .I3(hAddr_cnt[5]),
        .I4(hAddr_cnt[3]),
        .I5(end_ops_internal2__0[3]),
        .O(end_ops_internal_inferred_i_20_n_0));
  LUT6 #(
    .INIT(64'h0990000000000990)) 
    end_ops_internal_inferred_i_21
       (.I0(end_ops_internal2__0[1]),
        .I1(hAddr_cnt[1]),
        .I2(f_IBUF[0]),
        .I3(hAddr_cnt[0]),
        .I4(hAddr_cnt[2]),
        .I5(end_ops_internal2__0[2]),
        .O(end_ops_internal_inferred_i_21_n_0));
  CARRY4 end_ops_internal_inferred_i_22
       (.CI(end_ops_internal_inferred_i_23_n_0),
        .CO({end_ops_internal_inferred_i_22_n_0,end_ops_internal_inferred_i_22_n_1,end_ops_internal_inferred_i_22_n_2,end_ops_internal_inferred_i_22_n_3}),
        .CYINIT(1'b0),
        .DI(f_IBUF[20:17]),
        .O(end_ops_internal2__0[20:17]),
        .S({end_ops_internal_inferred_i_38_n_0,end_ops_internal_inferred_i_39_n_0,end_ops_internal_inferred_i_40_n_0,end_ops_internal_inferred_i_41_n_0}));
  CARRY4 end_ops_internal_inferred_i_23
       (.CI(end_ops_internal_inferred_i_24_n_0),
        .CO({end_ops_internal_inferred_i_23_n_0,end_ops_internal_inferred_i_23_n_1,end_ops_internal_inferred_i_23_n_2,end_ops_internal_inferred_i_23_n_3}),
        .CYINIT(1'b0),
        .DI(f_IBUF[16:13]),
        .O(end_ops_internal2__0[16:13]),
        .S({end_ops_internal_inferred_i_42_n_0,end_ops_internal_inferred_i_43_n_0,end_ops_internal_inferred_i_44_n_0,end_ops_internal_inferred_i_45_n_0}));
  CARRY4 end_ops_internal_inferred_i_24
       (.CI(end_ops_internal_inferred_i_36_n_0),
        .CO({end_ops_internal_inferred_i_24_n_0,end_ops_internal_inferred_i_24_n_1,end_ops_internal_inferred_i_24_n_2,end_ops_internal_inferred_i_24_n_3}),
        .CYINIT(1'b0),
        .DI(f_IBUF[12:9]),
        .O(end_ops_internal2__0[12:9]),
        .S({end_ops_internal_inferred_i_46_n_0,end_ops_internal_inferred_i_47_n_0,end_ops_internal_inferred_i_48_n_0,end_ops_internal_inferred_i_49_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    end_ops_internal_inferred_i_25
       (.I0(f_IBUF[31]),
        .O(end_ops_internal_inferred_i_25_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    end_ops_internal_inferred_i_26
       (.I0(f_IBUF[30]),
        .O(end_ops_internal_inferred_i_26_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    end_ops_internal_inferred_i_27
       (.I0(f_IBUF[29]),
        .O(end_ops_internal_inferred_i_27_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    end_ops_internal_inferred_i_28
       (.I0(f_IBUF[28]),
        .O(end_ops_internal_inferred_i_28_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    end_ops_internal_inferred_i_29
       (.I0(f_IBUF[27]),
        .O(end_ops_internal_inferred_i_29_n_0));
  CARRY4 end_ops_internal_inferred_i_3
       (.CI(end_ops_internal_inferred_i_6_n_0),
        .CO({NLW_end_ops_internal_inferred_i_3_CO_UNCONNECTED[3],end_ops_internal_inferred_i_3_n_1,end_ops_internal_inferred_i_3_n_2,end_ops_internal_inferred_i_3_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_end_ops_internal_inferred_i_3_O_UNCONNECTED[3:0]),
        .S({1'b0,end_ops_internal_inferred_i_7_n_0,end_ops_internal_inferred_i_8_n_0,end_ops_internal_inferred_i_9_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    end_ops_internal_inferred_i_30
       (.I0(f_IBUF[26]),
        .O(end_ops_internal_inferred_i_30_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    end_ops_internal_inferred_i_31
       (.I0(f_IBUF[25]),
        .O(end_ops_internal_inferred_i_31_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    end_ops_internal_inferred_i_32
       (.I0(f_IBUF[24]),
        .O(end_ops_internal_inferred_i_32_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    end_ops_internal_inferred_i_33
       (.I0(f_IBUF[23]),
        .O(end_ops_internal_inferred_i_33_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    end_ops_internal_inferred_i_34
       (.I0(f_IBUF[22]),
        .O(end_ops_internal_inferred_i_34_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    end_ops_internal_inferred_i_35
       (.I0(f_IBUF[21]),
        .O(end_ops_internal_inferred_i_35_n_0));
  CARRY4 end_ops_internal_inferred_i_36
       (.CI(end_ops_internal_inferred_i_37_n_0),
        .CO({end_ops_internal_inferred_i_36_n_0,end_ops_internal_inferred_i_36_n_1,end_ops_internal_inferred_i_36_n_2,end_ops_internal_inferred_i_36_n_3}),
        .CYINIT(1'b0),
        .DI(f_IBUF[8:5]),
        .O(end_ops_internal2__0[8:5]),
        .S({end_ops_internal_inferred_i_50_n_0,end_ops_internal_inferred_i_51_n_0,end_ops_internal_inferred_i_52_n_0,end_ops_internal_inferred_i_53_n_0}));
  CARRY4 end_ops_internal_inferred_i_37
       (.CI(1'b0),
        .CO({end_ops_internal_inferred_i_37_n_0,end_ops_internal_inferred_i_37_n_1,end_ops_internal_inferred_i_37_n_2,end_ops_internal_inferred_i_37_n_3}),
        .CYINIT(f_IBUF[0]),
        .DI(f_IBUF[4:1]),
        .O(end_ops_internal2__0[4:1]),
        .S({end_ops_internal_inferred_i_54_n_0,end_ops_internal_inferred_i_55_n_0,end_ops_internal_inferred_i_56_n_0,end_ops_internal_inferred_i_57_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    end_ops_internal_inferred_i_38
       (.I0(f_IBUF[20]),
        .O(end_ops_internal_inferred_i_38_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    end_ops_internal_inferred_i_39
       (.I0(f_IBUF[19]),
        .O(end_ops_internal_inferred_i_39_n_0));
  LUT3 #(
    .INIT(8'h07)) 
    end_ops_internal_inferred_i_4
       (.I0(currentState[1]),
        .I1(currentState[0]),
        .I2(currentState[2]),
        .O(end_ops_internal_inferred_i_4_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    end_ops_internal_inferred_i_40
       (.I0(f_IBUF[18]),
        .O(end_ops_internal_inferred_i_40_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    end_ops_internal_inferred_i_41
       (.I0(f_IBUF[17]),
        .O(end_ops_internal_inferred_i_41_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    end_ops_internal_inferred_i_42
       (.I0(f_IBUF[16]),
        .O(end_ops_internal_inferred_i_42_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    end_ops_internal_inferred_i_43
       (.I0(f_IBUF[15]),
        .O(end_ops_internal_inferred_i_43_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    end_ops_internal_inferred_i_44
       (.I0(f_IBUF[14]),
        .O(end_ops_internal_inferred_i_44_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    end_ops_internal_inferred_i_45
       (.I0(f_IBUF[13]),
        .O(end_ops_internal_inferred_i_45_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    end_ops_internal_inferred_i_46
       (.I0(f_IBUF[12]),
        .O(end_ops_internal_inferred_i_46_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    end_ops_internal_inferred_i_47
       (.I0(f_IBUF[11]),
        .O(end_ops_internal_inferred_i_47_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    end_ops_internal_inferred_i_48
       (.I0(f_IBUF[10]),
        .O(end_ops_internal_inferred_i_48_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    end_ops_internal_inferred_i_49
       (.I0(f_IBUF[9]),
        .O(end_ops_internal_inferred_i_49_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    end_ops_internal_inferred_i_5
       (.I0(hAddr_cnt[5]),
        .I1(hAddr_cnt[4]),
        .I2(hAddr_cnt[7]),
        .I3(hAddr_cnt[6]),
        .O(end_ops_internal_inferred_i_5_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    end_ops_internal_inferred_i_50
       (.I0(f_IBUF[8]),
        .O(end_ops_internal_inferred_i_50_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    end_ops_internal_inferred_i_51
       (.I0(f_IBUF[7]),
        .O(end_ops_internal_inferred_i_51_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    end_ops_internal_inferred_i_52
       (.I0(f_IBUF[6]),
        .O(end_ops_internal_inferred_i_52_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    end_ops_internal_inferred_i_53
       (.I0(f_IBUF[5]),
        .O(end_ops_internal_inferred_i_53_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    end_ops_internal_inferred_i_54
       (.I0(f_IBUF[4]),
        .O(end_ops_internal_inferred_i_54_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    end_ops_internal_inferred_i_55
       (.I0(f_IBUF[3]),
        .O(end_ops_internal_inferred_i_55_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    end_ops_internal_inferred_i_56
       (.I0(f_IBUF[2]),
        .O(end_ops_internal_inferred_i_56_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    end_ops_internal_inferred_i_57
       (.I0(f_IBUF[1]),
        .O(end_ops_internal_inferred_i_57_n_0));
  CARRY4 end_ops_internal_inferred_i_6
       (.CI(end_ops_internal_inferred_i_10_n_0),
        .CO({end_ops_internal_inferred_i_6_n_0,end_ops_internal_inferred_i_6_n_1,end_ops_internal_inferred_i_6_n_2,end_ops_internal_inferred_i_6_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_end_ops_internal_inferred_i_6_O_UNCONNECTED[3:0]),
        .S({end_ops_internal_inferred_i_11_n_0,end_ops_internal_inferred_i_12_n_0,end_ops_internal_inferred_i_13_n_0,end_ops_internal_inferred_i_14_n_0}));
  LUT2 #(
    .INIT(4'h1)) 
    end_ops_internal_inferred_i_7
       (.I0(end_ops_internal2__0[30]),
        .I1(end_ops_internal2__0[31]),
        .O(end_ops_internal_inferred_i_7_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    end_ops_internal_inferred_i_8
       (.I0(end_ops_internal2__0[29]),
        .I1(end_ops_internal2__0[28]),
        .I2(end_ops_internal2__0[27]),
        .O(end_ops_internal_inferred_i_8_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    end_ops_internal_inferred_i_9
       (.I0(end_ops_internal2__0[25]),
        .I1(end_ops_internal2__0[24]),
        .I2(end_ops_internal2__0[26]),
        .O(end_ops_internal_inferred_i_9_n_0));
  IBUF \f_IBUF[0]_inst 
       (.I(f[0]),
        .O(f_IBUF[0]));
  IBUF \f_IBUF[10]_inst 
       (.I(f[10]),
        .O(f_IBUF[10]));
  IBUF \f_IBUF[11]_inst 
       (.I(f[11]),
        .O(f_IBUF[11]));
  IBUF \f_IBUF[12]_inst 
       (.I(f[12]),
        .O(f_IBUF[12]));
  IBUF \f_IBUF[13]_inst 
       (.I(f[13]),
        .O(f_IBUF[13]));
  IBUF \f_IBUF[14]_inst 
       (.I(f[14]),
        .O(f_IBUF[14]));
  IBUF \f_IBUF[15]_inst 
       (.I(f[15]),
        .O(f_IBUF[15]));
  IBUF \f_IBUF[16]_inst 
       (.I(f[16]),
        .O(f_IBUF[16]));
  IBUF \f_IBUF[17]_inst 
       (.I(f[17]),
        .O(f_IBUF[17]));
  IBUF \f_IBUF[18]_inst 
       (.I(f[18]),
        .O(f_IBUF[18]));
  IBUF \f_IBUF[19]_inst 
       (.I(f[19]),
        .O(f_IBUF[19]));
  IBUF \f_IBUF[1]_inst 
       (.I(f[1]),
        .O(f_IBUF[1]));
  IBUF \f_IBUF[20]_inst 
       (.I(f[20]),
        .O(f_IBUF[20]));
  IBUF \f_IBUF[21]_inst 
       (.I(f[21]),
        .O(f_IBUF[21]));
  IBUF \f_IBUF[22]_inst 
       (.I(f[22]),
        .O(f_IBUF[22]));
  IBUF \f_IBUF[23]_inst 
       (.I(f[23]),
        .O(f_IBUF[23]));
  IBUF \f_IBUF[24]_inst 
       (.I(f[24]),
        .O(f_IBUF[24]));
  IBUF \f_IBUF[25]_inst 
       (.I(f[25]),
        .O(f_IBUF[25]));
  IBUF \f_IBUF[26]_inst 
       (.I(f[26]),
        .O(f_IBUF[26]));
  IBUF \f_IBUF[27]_inst 
       (.I(f[27]),
        .O(f_IBUF[27]));
  IBUF \f_IBUF[28]_inst 
       (.I(f[28]),
        .O(f_IBUF[28]));
  IBUF \f_IBUF[29]_inst 
       (.I(f[29]),
        .O(f_IBUF[29]));
  IBUF \f_IBUF[2]_inst 
       (.I(f[2]),
        .O(f_IBUF[2]));
  IBUF \f_IBUF[30]_inst 
       (.I(f[30]),
        .O(f_IBUF[30]));
  IBUF \f_IBUF[31]_inst 
       (.I(f[31]),
        .O(f_IBUF[31]));
  IBUF \f_IBUF[3]_inst 
       (.I(f[3]),
        .O(f_IBUF[3]));
  IBUF \f_IBUF[4]_inst 
       (.I(f[4]),
        .O(f_IBUF[4]));
  IBUF \f_IBUF[5]_inst 
       (.I(f[5]),
        .O(f_IBUF[5]));
  IBUF \f_IBUF[6]_inst 
       (.I(f[6]),
        .O(f_IBUF[6]));
  IBUF \f_IBUF[7]_inst 
       (.I(f[7]),
        .O(f_IBUF[7]));
  IBUF \f_IBUF[8]_inst 
       (.I(f[8]),
        .O(f_IBUF[8]));
  IBUF \f_IBUF[9]_inst 
       (.I(f[9]),
        .O(f_IBUF[9]));
  LUT3 #(
    .INIT(8'h40)) 
    \hAddr_cnt[0]_i_1 
       (.I0(hAddr_cnt[0]),
        .I1(currentState[1]),
        .I2(currentState2),
        .O(\hAddr_cnt[0]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h6000)) 
    \hAddr_cnt[1]_i_1 
       (.I0(hAddr_cnt[0]),
        .I1(hAddr_cnt[1]),
        .I2(currentState2),
        .I3(currentState[1]),
        .O(\hAddr_cnt[1]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h78000000)) 
    \hAddr_cnt[2]_i_1 
       (.I0(hAddr_cnt[1]),
        .I1(hAddr_cnt[0]),
        .I2(hAddr_cnt[2]),
        .I3(currentState2),
        .I4(currentState[1]),
        .O(\hAddr_cnt[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h7F80000000000000)) 
    \hAddr_cnt[3]_i_1 
       (.I0(hAddr_cnt[2]),
        .I1(hAddr_cnt[0]),
        .I2(hAddr_cnt[1]),
        .I3(hAddr_cnt[3]),
        .I4(currentState2),
        .I5(currentState[1]),
        .O(\hAddr_cnt[3]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h000000007FFF8000)) 
    \hAddr_cnt[4]_i_1 
       (.I0(hAddr_cnt[3]),
        .I1(hAddr_cnt[1]),
        .I2(hAddr_cnt[0]),
        .I3(hAddr_cnt[2]),
        .I4(hAddr_cnt[4]),
        .I5(\hAddr_cnt[4]_i_2_n_0 ),
        .O(\hAddr_cnt[4]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h7)) 
    \hAddr_cnt[4]_i_2 
       (.I0(currentState2),
        .I1(currentState[1]),
        .O(\hAddr_cnt[4]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h9000)) 
    \hAddr_cnt[5]_i_1 
       (.I0(\hAddr_cnt[5]_i_2_n_0 ),
        .I1(hAddr_cnt[5]),
        .I2(currentState2),
        .I3(currentState[1]),
        .O(\hAddr_cnt[5]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h7FFFFFFF)) 
    \hAddr_cnt[5]_i_2 
       (.I0(hAddr_cnt[3]),
        .I1(hAddr_cnt[1]),
        .I2(hAddr_cnt[0]),
        .I3(hAddr_cnt[2]),
        .I4(hAddr_cnt[4]),
        .O(\hAddr_cnt[5]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h9000)) 
    \hAddr_cnt[6]_i_1 
       (.I0(\hAddr_cnt[7]_i_4_n_0 ),
        .I1(hAddr_cnt[6]),
        .I2(currentState2),
        .I3(currentState[1]),
        .O(\hAddr_cnt[6]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h41)) 
    \hAddr_cnt[7]_i_1 
       (.I0(currentState[2]),
        .I1(currentState[0]),
        .I2(currentState[1]),
        .O(\hAddr_cnt[7]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \hAddr_cnt[7]_i_10 
       (.I0(end_ops_internal2__0[30]),
        .I1(end_ops_internal2__0[31]),
        .O(\hAddr_cnt[7]_i_10_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \hAddr_cnt[7]_i_11 
       (.I0(end_ops_internal2__0[28]),
        .I1(end_ops_internal2__0[29]),
        .O(\hAddr_cnt[7]_i_11_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \hAddr_cnt[7]_i_12 
       (.I0(end_ops_internal2__0[26]),
        .I1(end_ops_internal2__0[27]),
        .O(\hAddr_cnt[7]_i_12_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \hAddr_cnt[7]_i_13 
       (.I0(end_ops_internal2__0[24]),
        .I1(end_ops_internal2__0[25]),
        .O(\hAddr_cnt[7]_i_13_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \hAddr_cnt[7]_i_15 
       (.I0(end_ops_internal2__0[23]),
        .I1(end_ops_internal2__0[22]),
        .O(\hAddr_cnt[7]_i_15_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \hAddr_cnt[7]_i_16 
       (.I0(end_ops_internal2__0[21]),
        .I1(end_ops_internal2__0[20]),
        .O(\hAddr_cnt[7]_i_16_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \hAddr_cnt[7]_i_17 
       (.I0(end_ops_internal2__0[19]),
        .I1(end_ops_internal2__0[18]),
        .O(\hAddr_cnt[7]_i_17_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \hAddr_cnt[7]_i_18 
       (.I0(end_ops_internal2__0[17]),
        .I1(end_ops_internal2__0[16]),
        .O(\hAddr_cnt[7]_i_18_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \hAddr_cnt[7]_i_19 
       (.I0(end_ops_internal2__0[22]),
        .I1(end_ops_internal2__0[23]),
        .O(\hAddr_cnt[7]_i_19_n_0 ));
  LUT5 #(
    .INIT(32'h80880800)) 
    \hAddr_cnt[7]_i_2 
       (.I0(currentState2),
        .I1(currentState[1]),
        .I2(\hAddr_cnt[7]_i_4_n_0 ),
        .I3(hAddr_cnt[6]),
        .I4(hAddr_cnt[7]),
        .O(\hAddr_cnt[7]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \hAddr_cnt[7]_i_20 
       (.I0(end_ops_internal2__0[20]),
        .I1(end_ops_internal2__0[21]),
        .O(\hAddr_cnt[7]_i_20_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \hAddr_cnt[7]_i_21 
       (.I0(end_ops_internal2__0[18]),
        .I1(end_ops_internal2__0[19]),
        .O(\hAddr_cnt[7]_i_21_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \hAddr_cnt[7]_i_22 
       (.I0(end_ops_internal2__0[16]),
        .I1(end_ops_internal2__0[17]),
        .O(\hAddr_cnt[7]_i_22_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \hAddr_cnt[7]_i_24 
       (.I0(end_ops_internal2__0[15]),
        .I1(end_ops_internal2__0[14]),
        .O(\hAddr_cnt[7]_i_24_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \hAddr_cnt[7]_i_25 
       (.I0(end_ops_internal2__0[13]),
        .I1(end_ops_internal2__0[12]),
        .O(\hAddr_cnt[7]_i_25_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \hAddr_cnt[7]_i_26 
       (.I0(end_ops_internal2__0[11]),
        .I1(end_ops_internal2__0[10]),
        .O(\hAddr_cnt[7]_i_26_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \hAddr_cnt[7]_i_27 
       (.I0(end_ops_internal2__0[9]),
        .I1(end_ops_internal2__0[8]),
        .O(\hAddr_cnt[7]_i_27_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \hAddr_cnt[7]_i_28 
       (.I0(end_ops_internal2__0[14]),
        .I1(end_ops_internal2__0[15]),
        .O(\hAddr_cnt[7]_i_28_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \hAddr_cnt[7]_i_29 
       (.I0(end_ops_internal2__0[12]),
        .I1(end_ops_internal2__0[13]),
        .O(\hAddr_cnt[7]_i_29_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \hAddr_cnt[7]_i_30 
       (.I0(end_ops_internal2__0[10]),
        .I1(end_ops_internal2__0[11]),
        .O(\hAddr_cnt[7]_i_30_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \hAddr_cnt[7]_i_31 
       (.I0(end_ops_internal2__0[8]),
        .I1(end_ops_internal2__0[9]),
        .O(\hAddr_cnt[7]_i_31_n_0 ));
  LUT4 #(
    .INIT(16'h22B2)) 
    \hAddr_cnt[7]_i_32 
       (.I0(end_ops_internal2__0[7]),
        .I1(hAddr_cnt[7]),
        .I2(end_ops_internal2__0[6]),
        .I3(hAddr_cnt[6]),
        .O(\hAddr_cnt[7]_i_32_n_0 ));
  LUT4 #(
    .INIT(16'h22B2)) 
    \hAddr_cnt[7]_i_33 
       (.I0(end_ops_internal2__0[5]),
        .I1(hAddr_cnt[5]),
        .I2(end_ops_internal2__0[4]),
        .I3(hAddr_cnt[4]),
        .O(\hAddr_cnt[7]_i_33_n_0 ));
  LUT4 #(
    .INIT(16'h22B2)) 
    \hAddr_cnt[7]_i_34 
       (.I0(end_ops_internal2__0[3]),
        .I1(hAddr_cnt[3]),
        .I2(end_ops_internal2__0[2]),
        .I3(hAddr_cnt[2]),
        .O(\hAddr_cnt[7]_i_34_n_0 ));
  LUT4 #(
    .INIT(16'h222B)) 
    \hAddr_cnt[7]_i_35 
       (.I0(end_ops_internal2__0[1]),
        .I1(hAddr_cnt[1]),
        .I2(f_IBUF[0]),
        .I3(hAddr_cnt[0]),
        .O(\hAddr_cnt[7]_i_35_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \hAddr_cnt[7]_i_36 
       (.I0(hAddr_cnt[7]),
        .I1(end_ops_internal2__0[7]),
        .I2(hAddr_cnt[6]),
        .I3(end_ops_internal2__0[6]),
        .O(\hAddr_cnt[7]_i_36_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \hAddr_cnt[7]_i_37 
       (.I0(hAddr_cnt[5]),
        .I1(end_ops_internal2__0[5]),
        .I2(hAddr_cnt[4]),
        .I3(end_ops_internal2__0[4]),
        .O(\hAddr_cnt[7]_i_37_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \hAddr_cnt[7]_i_38 
       (.I0(hAddr_cnt[3]),
        .I1(end_ops_internal2__0[3]),
        .I2(hAddr_cnt[2]),
        .I3(end_ops_internal2__0[2]),
        .O(\hAddr_cnt[7]_i_38_n_0 ));
  LUT4 #(
    .INIT(16'h6006)) 
    \hAddr_cnt[7]_i_39 
       (.I0(hAddr_cnt[0]),
        .I1(f_IBUF[0]),
        .I2(hAddr_cnt[1]),
        .I3(end_ops_internal2__0[1]),
        .O(\hAddr_cnt[7]_i_39_n_0 ));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
    \hAddr_cnt[7]_i_4 
       (.I0(hAddr_cnt[4]),
        .I1(hAddr_cnt[2]),
        .I2(hAddr_cnt[0]),
        .I3(hAddr_cnt[1]),
        .I4(hAddr_cnt[3]),
        .I5(hAddr_cnt[5]),
        .O(\hAddr_cnt[7]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \hAddr_cnt[7]_i_6 
       (.I0(end_ops_internal2__0[31]),
        .I1(end_ops_internal2__0[30]),
        .O(\hAddr_cnt[7]_i_6_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \hAddr_cnt[7]_i_7 
       (.I0(end_ops_internal2__0[29]),
        .I1(end_ops_internal2__0[28]),
        .O(\hAddr_cnt[7]_i_7_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \hAddr_cnt[7]_i_8 
       (.I0(end_ops_internal2__0[27]),
        .I1(end_ops_internal2__0[26]),
        .O(\hAddr_cnt[7]_i_8_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \hAddr_cnt[7]_i_9 
       (.I0(end_ops_internal2__0[25]),
        .I1(end_ops_internal2__0[24]),
        .O(\hAddr_cnt[7]_i_9_n_0 ));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \hAddr_cnt_reg[0] 
       (.C(clk_IBUF_BUFG),
        .CE(\hAddr_cnt[7]_i_1_n_0 ),
        .D(\hAddr_cnt[0]_i_1_n_0 ),
        .Q(hAddr_cnt[0]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \hAddr_cnt_reg[1] 
       (.C(clk_IBUF_BUFG),
        .CE(\hAddr_cnt[7]_i_1_n_0 ),
        .D(\hAddr_cnt[1]_i_1_n_0 ),
        .Q(hAddr_cnt[1]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \hAddr_cnt_reg[2] 
       (.C(clk_IBUF_BUFG),
        .CE(\hAddr_cnt[7]_i_1_n_0 ),
        .D(\hAddr_cnt[2]_i_1_n_0 ),
        .Q(hAddr_cnt[2]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \hAddr_cnt_reg[3] 
       (.C(clk_IBUF_BUFG),
        .CE(\hAddr_cnt[7]_i_1_n_0 ),
        .D(\hAddr_cnt[3]_i_1_n_0 ),
        .Q(hAddr_cnt[3]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \hAddr_cnt_reg[4] 
       (.C(clk_IBUF_BUFG),
        .CE(\hAddr_cnt[7]_i_1_n_0 ),
        .D(\hAddr_cnt[4]_i_1_n_0 ),
        .Q(hAddr_cnt[4]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \hAddr_cnt_reg[5] 
       (.C(clk_IBUF_BUFG),
        .CE(\hAddr_cnt[7]_i_1_n_0 ),
        .D(\hAddr_cnt[5]_i_1_n_0 ),
        .Q(hAddr_cnt[5]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \hAddr_cnt_reg[6] 
       (.C(clk_IBUF_BUFG),
        .CE(\hAddr_cnt[7]_i_1_n_0 ),
        .D(\hAddr_cnt[6]_i_1_n_0 ),
        .Q(hAddr_cnt[6]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \hAddr_cnt_reg[7] 
       (.C(clk_IBUF_BUFG),
        .CE(\hAddr_cnt[7]_i_1_n_0 ),
        .D(\hAddr_cnt[7]_i_2_n_0 ),
        .Q(hAddr_cnt[7]),
        .R(reset_IBUF));
  CARRY4 \hAddr_cnt_reg[7]_i_14 
       (.CI(\hAddr_cnt_reg[7]_i_23_n_0 ),
        .CO({\hAddr_cnt_reg[7]_i_14_n_0 ,\hAddr_cnt_reg[7]_i_14_n_1 ,\hAddr_cnt_reg[7]_i_14_n_2 ,\hAddr_cnt_reg[7]_i_14_n_3 }),
        .CYINIT(1'b0),
        .DI({\hAddr_cnt[7]_i_24_n_0 ,\hAddr_cnt[7]_i_25_n_0 ,\hAddr_cnt[7]_i_26_n_0 ,\hAddr_cnt[7]_i_27_n_0 }),
        .O(\NLW_hAddr_cnt_reg[7]_i_14_O_UNCONNECTED [3:0]),
        .S({\hAddr_cnt[7]_i_28_n_0 ,\hAddr_cnt[7]_i_29_n_0 ,\hAddr_cnt[7]_i_30_n_0 ,\hAddr_cnt[7]_i_31_n_0 }));
  CARRY4 \hAddr_cnt_reg[7]_i_23 
       (.CI(1'b0),
        .CO({\hAddr_cnt_reg[7]_i_23_n_0 ,\hAddr_cnt_reg[7]_i_23_n_1 ,\hAddr_cnt_reg[7]_i_23_n_2 ,\hAddr_cnt_reg[7]_i_23_n_3 }),
        .CYINIT(1'b0),
        .DI({\hAddr_cnt[7]_i_32_n_0 ,\hAddr_cnt[7]_i_33_n_0 ,\hAddr_cnt[7]_i_34_n_0 ,\hAddr_cnt[7]_i_35_n_0 }),
        .O(\NLW_hAddr_cnt_reg[7]_i_23_O_UNCONNECTED [3:0]),
        .S({\hAddr_cnt[7]_i_36_n_0 ,\hAddr_cnt[7]_i_37_n_0 ,\hAddr_cnt[7]_i_38_n_0 ,\hAddr_cnt[7]_i_39_n_0 }));
  CARRY4 \hAddr_cnt_reg[7]_i_3 
       (.CI(\hAddr_cnt_reg[7]_i_5_n_0 ),
        .CO({currentState2,\hAddr_cnt_reg[7]_i_3_n_1 ,\hAddr_cnt_reg[7]_i_3_n_2 ,\hAddr_cnt_reg[7]_i_3_n_3 }),
        .CYINIT(1'b0),
        .DI({\hAddr_cnt[7]_i_6_n_0 ,\hAddr_cnt[7]_i_7_n_0 ,\hAddr_cnt[7]_i_8_n_0 ,\hAddr_cnt[7]_i_9_n_0 }),
        .O(\NLW_hAddr_cnt_reg[7]_i_3_O_UNCONNECTED [3:0]),
        .S({\hAddr_cnt[7]_i_10_n_0 ,\hAddr_cnt[7]_i_11_n_0 ,\hAddr_cnt[7]_i_12_n_0 ,\hAddr_cnt[7]_i_13_n_0 }));
  CARRY4 \hAddr_cnt_reg[7]_i_5 
       (.CI(\hAddr_cnt_reg[7]_i_14_n_0 ),
        .CO({\hAddr_cnt_reg[7]_i_5_n_0 ,\hAddr_cnt_reg[7]_i_5_n_1 ,\hAddr_cnt_reg[7]_i_5_n_2 ,\hAddr_cnt_reg[7]_i_5_n_3 }),
        .CYINIT(1'b0),
        .DI({\hAddr_cnt[7]_i_15_n_0 ,\hAddr_cnt[7]_i_16_n_0 ,\hAddr_cnt[7]_i_17_n_0 ,\hAddr_cnt[7]_i_18_n_0 }),
        .O(\NLW_hAddr_cnt_reg[7]_i_5_O_UNCONNECTED [3:0]),
        .S({\hAddr_cnt[7]_i_19_n_0 ,\hAddr_cnt[7]_i_20_n_0 ,\hAddr_cnt[7]_i_21_n_0 ,\hAddr_cnt[7]_i_22_n_0 }));
  IBUF \outAddr_IBUF[0]_inst 
       (.I(outAddr[0]),
        .O(outAddr_IBUF[0]));
  IBUF \outAddr_IBUF[1]_inst 
       (.I(outAddr[1]),
        .O(outAddr_IBUF[1]));
  IBUF \outAddr_IBUF[2]_inst 
       (.I(outAddr[2]),
        .O(outAddr_IBUF[2]));
  IBUF \outAddr_IBUF[3]_inst 
       (.I(outAddr[3]),
        .O(outAddr_IBUF[3]));
  IBUF \outAddr_IBUF[4]_inst 
       (.I(outAddr[4]),
        .O(outAddr_IBUF[4]));
  LUT1 #(
    .INIT(2'h1)) 
    \outAddr_internal[0]_i_1 
       (.I0(outAddr_internal[0]),
        .O(\outAddr_internal[0]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \outAddr_internal[1]_i_1 
       (.I0(outAddr_internal[0]),
        .I1(outAddr_internal[1]),
        .O(\outAddr_internal[1]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h78)) 
    \outAddr_internal[2]_i_1 
       (.I0(outAddr_internal[0]),
        .I1(outAddr_internal[1]),
        .I2(outAddr_internal[2]),
        .O(\outAddr_internal[2]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h7F80)) 
    \outAddr_internal[3]_i_1 
       (.I0(outAddr_internal[1]),
        .I1(outAddr_internal[0]),
        .I2(outAddr_internal[2]),
        .I3(outAddr_internal[3]),
        .O(\outAddr_internal[3]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \outAddr_internal[4]_i_1 
       (.I0(outAddr_internal[2]),
        .I1(outAddr_internal[0]),
        .I2(outAddr_internal[1]),
        .I3(outAddr_internal[3]),
        .I4(outAddr_internal[4]),
        .O(\outAddr_internal[4]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \outAddr_internal[5]_i_1 
       (.I0(outAddr_internal[3]),
        .I1(outAddr_internal[1]),
        .I2(outAddr_internal[0]),
        .I3(outAddr_internal[2]),
        .I4(outAddr_internal[4]),
        .I5(outAddr_internal[5]),
        .O(\outAddr_internal[5]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \outAddr_internal[6]_i_1 
       (.I0(\outAddr_internal[7]_i_2_n_0 ),
        .I1(outAddr_internal[6]),
        .O(\outAddr_internal[6]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h78)) 
    \outAddr_internal[7]_i_1 
       (.I0(\outAddr_internal[7]_i_2_n_0 ),
        .I1(outAddr_internal[6]),
        .I2(outAddr_internal[7]),
        .O(\outAddr_internal[7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \outAddr_internal[7]_i_2 
       (.I0(outAddr_internal[5]),
        .I1(outAddr_internal[3]),
        .I2(outAddr_internal[1]),
        .I3(outAddr_internal[0]),
        .I4(outAddr_internal[2]),
        .I5(outAddr_internal[4]),
        .O(\outAddr_internal[7]_i_2_n_0 ));
  (* KEEP = "yes" *) 
  FDSE #(
    .INIT(1'b1)) 
    \outAddr_internal_reg[0] 
       (.C(clk_IBUF_BUFG),
        .CE(end_ops_internal3),
        .D(\outAddr_internal[0]_i_1_n_0 ),
        .Q(outAddr_internal[0]),
        .S(reset_IBUF));
  (* KEEP = "yes" *) 
  FDSE #(
    .INIT(1'b1)) 
    \outAddr_internal_reg[1] 
       (.C(clk_IBUF_BUFG),
        .CE(end_ops_internal3),
        .D(\outAddr_internal[1]_i_1_n_0 ),
        .Q(outAddr_internal[1]),
        .S(reset_IBUF));
  (* KEEP = "yes" *) 
  FDSE #(
    .INIT(1'b1)) 
    \outAddr_internal_reg[2] 
       (.C(clk_IBUF_BUFG),
        .CE(end_ops_internal3),
        .D(\outAddr_internal[2]_i_1_n_0 ),
        .Q(outAddr_internal[2]),
        .S(reset_IBUF));
  (* KEEP = "yes" *) 
  FDSE #(
    .INIT(1'b1)) 
    \outAddr_internal_reg[3] 
       (.C(clk_IBUF_BUFG),
        .CE(end_ops_internal3),
        .D(\outAddr_internal[3]_i_1_n_0 ),
        .Q(outAddr_internal[3]),
        .S(reset_IBUF));
  (* KEEP = "yes" *) 
  FDSE #(
    .INIT(1'b1)) 
    \outAddr_internal_reg[4] 
       (.C(clk_IBUF_BUFG),
        .CE(end_ops_internal3),
        .D(\outAddr_internal[4]_i_1_n_0 ),
        .Q(outAddr_internal[4]),
        .S(reset_IBUF));
  (* KEEP = "yes" *) 
  FDSE #(
    .INIT(1'b1)) 
    \outAddr_internal_reg[5] 
       (.C(clk_IBUF_BUFG),
        .CE(end_ops_internal3),
        .D(\outAddr_internal[5]_i_1_n_0 ),
        .Q(outAddr_internal[5]),
        .S(reset_IBUF));
  (* KEEP = "yes" *) 
  FDSE #(
    .INIT(1'b1)) 
    \outAddr_internal_reg[6] 
       (.C(clk_IBUF_BUFG),
        .CE(end_ops_internal3),
        .D(\outAddr_internal[6]_i_1_n_0 ),
        .Q(outAddr_internal[6]),
        .S(reset_IBUF));
  (* KEEP = "yes" *) 
  FDSE #(
    .INIT(1'b1)) 
    \outAddr_internal_reg[7] 
       (.C(clk_IBUF_BUFG),
        .CE(end_ops_internal3),
        .D(\outAddr_internal[7]_i_1_n_0 ),
        .Q(outAddr_internal[7]),
        .S(reset_IBUF));
  OBUF \outMem_data_OBUF[0]_inst 
       (.I(outMem_data_OBUF[0]),
        .O(outMem_data[0]));
  OBUF \outMem_data_OBUF[10]_inst 
       (.I(outMem_data_OBUF[10]),
        .O(outMem_data[10]));
  OBUF \outMem_data_OBUF[11]_inst 
       (.I(outMem_data_OBUF[11]),
        .O(outMem_data[11]));
  OBUF \outMem_data_OBUF[12]_inst 
       (.I(outMem_data_OBUF[12]),
        .O(outMem_data[12]));
  OBUF \outMem_data_OBUF[13]_inst 
       (.I(outMem_data_OBUF[13]),
        .O(outMem_data[13]));
  OBUF \outMem_data_OBUF[14]_inst 
       (.I(outMem_data_OBUF[14]),
        .O(outMem_data[14]));
  OBUF \outMem_data_OBUF[15]_inst 
       (.I(outMem_data_OBUF[15]),
        .O(outMem_data[15]));
  OBUF \outMem_data_OBUF[16]_inst 
       (.I(outMem_data_OBUF[16]),
        .O(outMem_data[16]));
  OBUF \outMem_data_OBUF[17]_inst 
       (.I(outMem_data_OBUF[17]),
        .O(outMem_data[17]));
  OBUF \outMem_data_OBUF[18]_inst 
       (.I(outMem_data_OBUF[18]),
        .O(outMem_data[18]));
  OBUF \outMem_data_OBUF[19]_inst 
       (.I(outMem_data_OBUF[19]),
        .O(outMem_data[19]));
  OBUF \outMem_data_OBUF[1]_inst 
       (.I(outMem_data_OBUF[1]),
        .O(outMem_data[1]));
  OBUF \outMem_data_OBUF[20]_inst 
       (.I(outMem_data_OBUF[20]),
        .O(outMem_data[20]));
  OBUF \outMem_data_OBUF[21]_inst 
       (.I(outMem_data_OBUF[21]),
        .O(outMem_data[21]));
  OBUF \outMem_data_OBUF[22]_inst 
       (.I(outMem_data_OBUF[22]),
        .O(outMem_data[22]));
  OBUF \outMem_data_OBUF[23]_inst 
       (.I(outMem_data_OBUF[23]),
        .O(outMem_data[23]));
  OBUF \outMem_data_OBUF[24]_inst 
       (.I(outMem_data_OBUF[24]),
        .O(outMem_data[24]));
  OBUF \outMem_data_OBUF[25]_inst 
       (.I(outMem_data_OBUF[25]),
        .O(outMem_data[25]));
  OBUF \outMem_data_OBUF[26]_inst 
       (.I(outMem_data_OBUF[26]),
        .O(outMem_data[26]));
  OBUF \outMem_data_OBUF[27]_inst 
       (.I(outMem_data_OBUF[27]),
        .O(outMem_data[27]));
  OBUF \outMem_data_OBUF[28]_inst 
       (.I(outMem_data_OBUF[28]),
        .O(outMem_data[28]));
  OBUF \outMem_data_OBUF[29]_inst 
       (.I(outMem_data_OBUF[29]),
        .O(outMem_data[29]));
  OBUF \outMem_data_OBUF[2]_inst 
       (.I(outMem_data_OBUF[2]),
        .O(outMem_data[2]));
  OBUF \outMem_data_OBUF[30]_inst 
       (.I(outMem_data_OBUF[30]),
        .O(outMem_data[30]));
  OBUF \outMem_data_OBUF[31]_inst 
       (.I(outMem_data_OBUF[31]),
        .O(outMem_data[31]));
  OBUF \outMem_data_OBUF[3]_inst 
       (.I(outMem_data_OBUF[3]),
        .O(outMem_data[3]));
  OBUF \outMem_data_OBUF[4]_inst 
       (.I(outMem_data_OBUF[4]),
        .O(outMem_data[4]));
  OBUF \outMem_data_OBUF[5]_inst 
       (.I(outMem_data_OBUF[5]),
        .O(outMem_data[5]));
  OBUF \outMem_data_OBUF[6]_inst 
       (.I(outMem_data_OBUF[6]),
        .O(outMem_data[6]));
  OBUF \outMem_data_OBUF[7]_inst 
       (.I(outMem_data_OBUF[7]),
        .O(outMem_data[7]));
  OBUF \outMem_data_OBUF[8]_inst 
       (.I(outMem_data_OBUF[8]),
        .O(outMem_data[8]));
  OBUF \outMem_data_OBUF[9]_inst 
       (.I(outMem_data_OBUF[9]),
        .O(outMem_data[9]));
  IBUF \r_IBUF[0]_inst 
       (.I(r[0]),
        .O(r_IBUF[0]));
  IBUF \r_IBUF[10]_inst 
       (.I(r[10]),
        .O(r_IBUF[10]));
  IBUF \r_IBUF[11]_inst 
       (.I(r[11]),
        .O(r_IBUF[11]));
  IBUF \r_IBUF[12]_inst 
       (.I(r[12]),
        .O(r_IBUF[12]));
  IBUF \r_IBUF[13]_inst 
       (.I(r[13]),
        .O(r_IBUF[13]));
  IBUF \r_IBUF[14]_inst 
       (.I(r[14]),
        .O(r_IBUF[14]));
  IBUF \r_IBUF[15]_inst 
       (.I(r[15]),
        .O(r_IBUF[15]));
  IBUF \r_IBUF[16]_inst 
       (.I(r[16]),
        .O(r_IBUF[16]));
  IBUF \r_IBUF[17]_inst 
       (.I(r[17]),
        .O(r_IBUF[17]));
  IBUF \r_IBUF[18]_inst 
       (.I(r[18]),
        .O(r_IBUF[18]));
  IBUF \r_IBUF[19]_inst 
       (.I(r[19]),
        .O(r_IBUF[19]));
  IBUF \r_IBUF[1]_inst 
       (.I(r[1]),
        .O(r_IBUF[1]));
  IBUF \r_IBUF[20]_inst 
       (.I(r[20]),
        .O(r_IBUF[20]));
  IBUF \r_IBUF[21]_inst 
       (.I(r[21]),
        .O(r_IBUF[21]));
  IBUF \r_IBUF[22]_inst 
       (.I(r[22]),
        .O(r_IBUF[22]));
  IBUF \r_IBUF[23]_inst 
       (.I(r[23]),
        .O(r_IBUF[23]));
  IBUF \r_IBUF[24]_inst 
       (.I(r[24]),
        .O(r_IBUF[24]));
  IBUF \r_IBUF[25]_inst 
       (.I(r[25]),
        .O(r_IBUF[25]));
  IBUF \r_IBUF[26]_inst 
       (.I(r[26]),
        .O(r_IBUF[26]));
  IBUF \r_IBUF[27]_inst 
       (.I(r[27]),
        .O(r_IBUF[27]));
  IBUF \r_IBUF[28]_inst 
       (.I(r[28]),
        .O(r_IBUF[28]));
  IBUF \r_IBUF[29]_inst 
       (.I(r[29]),
        .O(r_IBUF[29]));
  IBUF \r_IBUF[2]_inst 
       (.I(r[2]),
        .O(r_IBUF[2]));
  IBUF \r_IBUF[30]_inst 
       (.I(r[30]),
        .O(r_IBUF[30]));
  IBUF \r_IBUF[31]_inst 
       (.I(r[31]),
        .O(r_IBUF[31]));
  IBUF \r_IBUF[3]_inst 
       (.I(r[3]),
        .O(r_IBUF[3]));
  IBUF \r_IBUF[4]_inst 
       (.I(r[4]),
        .O(r_IBUF[4]));
  IBUF \r_IBUF[5]_inst 
       (.I(r[5]),
        .O(r_IBUF[5]));
  IBUF \r_IBUF[6]_inst 
       (.I(r[6]),
        .O(r_IBUF[6]));
  IBUF \r_IBUF[7]_inst 
       (.I(r[7]),
        .O(r_IBUF[7]));
  IBUF \r_IBUF[8]_inst 
       (.I(r[8]),
        .O(r_IBUF[8]));
  IBUF \r_IBUF[9]_inst 
       (.I(r[9]),
        .O(r_IBUF[9]));
  IBUF rd_wr_IBUF_inst
       (.I(rd_wr),
        .O(rd_wr_IBUF));
  IBUF reset_IBUF_inst
       (.I(reset),
        .O(reset_IBUF));
  IBUF start_IBUF_inst
       (.I(start),
        .O(start_IBUF));
  LUT4 #(
    .INIT(16'hC008)) 
    \vAddr_cnt[0]_i_1 
       (.I0(currentState28_in),
        .I1(currentState[1]),
        .I2(vAddr_cnt[0]),
        .I3(currentState2),
        .O(\vAddr_cnt[0]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hC0C80800)) 
    \vAddr_cnt[1]_i_1 
       (.I0(currentState28_in),
        .I1(currentState[1]),
        .I2(currentState2),
        .I3(vAddr_cnt[0]),
        .I4(vAddr_cnt[1]),
        .O(\vAddr_cnt[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAA000008888000)) 
    \vAddr_cnt[2]_i_1 
       (.I0(currentState[1]),
        .I1(currentState28_in),
        .I2(vAddr_cnt[1]),
        .I3(vAddr_cnt[0]),
        .I4(vAddr_cnt[2]),
        .I5(currentState2),
        .O(\vAddr_cnt[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAA00002AAA8000)) 
    \vAddr_cnt[3]_i_1 
       (.I0(\vAddr_cnt[3]_i_2_n_0 ),
        .I1(vAddr_cnt[2]),
        .I2(vAddr_cnt[0]),
        .I3(vAddr_cnt[1]),
        .I4(vAddr_cnt[3]),
        .I5(currentState2),
        .O(\vAddr_cnt[3]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'hA8)) 
    \vAddr_cnt[3]_i_2 
       (.I0(currentState[1]),
        .I1(currentState2),
        .I2(currentState28_in),
        .O(\vAddr_cnt[3]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hA8008820)) 
    \vAddr_cnt[4]_i_1 
       (.I0(currentState[1]),
        .I1(currentState2),
        .I2(currentState28_in),
        .I3(vAddr_cnt[4]),
        .I4(\vAddr_cnt[4]_i_2_n_0 ),
        .O(\vAddr_cnt[4]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h7FFF)) 
    \vAddr_cnt[4]_i_2 
       (.I0(vAddr_cnt[2]),
        .I1(vAddr_cnt[0]),
        .I2(vAddr_cnt[1]),
        .I3(vAddr_cnt[3]),
        .O(\vAddr_cnt[4]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hA8880020)) 
    \vAddr_cnt[5]_i_1 
       (.I0(currentState[1]),
        .I1(currentState2),
        .I2(currentState28_in),
        .I3(\vAddr_cnt[5]_i_2_n_0 ),
        .I4(vAddr_cnt[5]),
        .O(\vAddr_cnt[5]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h7FFFFFFF)) 
    \vAddr_cnt[5]_i_2 
       (.I0(vAddr_cnt[3]),
        .I1(vAddr_cnt[1]),
        .I2(vAddr_cnt[0]),
        .I3(vAddr_cnt[2]),
        .I4(vAddr_cnt[4]),
        .O(\vAddr_cnt[5]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hA8A00008)) 
    \vAddr_cnt[6]_i_1 
       (.I0(currentState[1]),
        .I1(currentState28_in),
        .I2(currentState2),
        .I3(\vAddr_cnt[7]_i_3_n_0 ),
        .I4(vAddr_cnt[6]),
        .O(\vAddr_cnt[6]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h8880888082808880)) 
    \vAddr_cnt[7]_i_1 
       (.I0(currentState[1]),
        .I1(vAddr_cnt[7]),
        .I2(currentState2),
        .I3(currentState28_in),
        .I4(vAddr_cnt[6]),
        .I5(\vAddr_cnt[7]_i_3_n_0 ),
        .O(\vAddr_cnt[7]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \vAddr_cnt[7]_i_10 
       (.I0(currentState3[28]),
        .I1(currentState3[29]),
        .O(\vAddr_cnt[7]_i_10_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \vAddr_cnt[7]_i_11 
       (.I0(currentState3[26]),
        .I1(currentState3[27]),
        .O(\vAddr_cnt[7]_i_11_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \vAddr_cnt[7]_i_12 
       (.I0(currentState3[24]),
        .I1(currentState3[25]),
        .O(\vAddr_cnt[7]_i_12_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \vAddr_cnt[7]_i_14 
       (.I0(currentState3[23]),
        .I1(currentState3[22]),
        .O(\vAddr_cnt[7]_i_14_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \vAddr_cnt[7]_i_15 
       (.I0(currentState3[21]),
        .I1(currentState3[20]),
        .O(\vAddr_cnt[7]_i_15_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \vAddr_cnt[7]_i_16 
       (.I0(currentState3[19]),
        .I1(currentState3[18]),
        .O(\vAddr_cnt[7]_i_16_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \vAddr_cnt[7]_i_17 
       (.I0(currentState3[17]),
        .I1(currentState3[16]),
        .O(\vAddr_cnt[7]_i_17_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \vAddr_cnt[7]_i_18 
       (.I0(currentState3[22]),
        .I1(currentState3[23]),
        .O(\vAddr_cnt[7]_i_18_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \vAddr_cnt[7]_i_19 
       (.I0(currentState3[20]),
        .I1(currentState3[21]),
        .O(\vAddr_cnt[7]_i_19_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \vAddr_cnt[7]_i_20 
       (.I0(currentState3[18]),
        .I1(currentState3[19]),
        .O(\vAddr_cnt[7]_i_20_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \vAddr_cnt[7]_i_21 
       (.I0(currentState3[16]),
        .I1(currentState3[17]),
        .O(\vAddr_cnt[7]_i_21_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \vAddr_cnt[7]_i_26 
       (.I0(currentState3[15]),
        .I1(currentState3[14]),
        .O(\vAddr_cnt[7]_i_26_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \vAddr_cnt[7]_i_27 
       (.I0(currentState3[13]),
        .I1(currentState3[12]),
        .O(\vAddr_cnt[7]_i_27_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \vAddr_cnt[7]_i_28 
       (.I0(currentState3[11]),
        .I1(currentState3[10]),
        .O(\vAddr_cnt[7]_i_28_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \vAddr_cnt[7]_i_29 
       (.I0(currentState3[9]),
        .I1(currentState3[8]),
        .O(\vAddr_cnt[7]_i_29_n_0 ));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
    \vAddr_cnt[7]_i_3 
       (.I0(vAddr_cnt[4]),
        .I1(vAddr_cnt[2]),
        .I2(vAddr_cnt[0]),
        .I3(vAddr_cnt[1]),
        .I4(vAddr_cnt[3]),
        .I5(vAddr_cnt[5]),
        .O(\vAddr_cnt[7]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \vAddr_cnt[7]_i_30 
       (.I0(currentState3[14]),
        .I1(currentState3[15]),
        .O(\vAddr_cnt[7]_i_30_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \vAddr_cnt[7]_i_31 
       (.I0(currentState3[12]),
        .I1(currentState3[13]),
        .O(\vAddr_cnt[7]_i_31_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \vAddr_cnt[7]_i_32 
       (.I0(currentState3[10]),
        .I1(currentState3[11]),
        .O(\vAddr_cnt[7]_i_32_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \vAddr_cnt[7]_i_33 
       (.I0(currentState3[8]),
        .I1(currentState3[9]),
        .O(\vAddr_cnt[7]_i_33_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \vAddr_cnt[7]_i_36 
       (.I0(N_IBUF[31]),
        .O(\vAddr_cnt[7]_i_36_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \vAddr_cnt[7]_i_37 
       (.I0(N_IBUF[30]),
        .O(\vAddr_cnt[7]_i_37_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \vAddr_cnt[7]_i_38 
       (.I0(N_IBUF[29]),
        .O(\vAddr_cnt[7]_i_38_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \vAddr_cnt[7]_i_39 
       (.I0(N_IBUF[28]),
        .O(\vAddr_cnt[7]_i_39_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \vAddr_cnt[7]_i_40 
       (.I0(N_IBUF[27]),
        .O(\vAddr_cnt[7]_i_40_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \vAddr_cnt[7]_i_41 
       (.I0(N_IBUF[26]),
        .O(\vAddr_cnt[7]_i_41_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \vAddr_cnt[7]_i_42 
       (.I0(N_IBUF[25]),
        .O(\vAddr_cnt[7]_i_42_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \vAddr_cnt[7]_i_43 
       (.I0(N_IBUF[24]),
        .O(\vAddr_cnt[7]_i_43_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \vAddr_cnt[7]_i_44 
       (.I0(N_IBUF[23]),
        .O(\vAddr_cnt[7]_i_44_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \vAddr_cnt[7]_i_45 
       (.I0(N_IBUF[22]),
        .O(\vAddr_cnt[7]_i_45_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \vAddr_cnt[7]_i_46 
       (.I0(N_IBUF[21]),
        .O(\vAddr_cnt[7]_i_46_n_0 ));
  LUT4 #(
    .INIT(16'h22B2)) 
    \vAddr_cnt[7]_i_47 
       (.I0(currentState3[7]),
        .I1(vAddr_cnt[7]),
        .I2(currentState3[6]),
        .I3(vAddr_cnt[6]),
        .O(\vAddr_cnt[7]_i_47_n_0 ));
  LUT4 #(
    .INIT(16'h22B2)) 
    \vAddr_cnt[7]_i_48 
       (.I0(currentState3[5]),
        .I1(vAddr_cnt[5]),
        .I2(currentState3[4]),
        .I3(vAddr_cnt[4]),
        .O(\vAddr_cnt[7]_i_48_n_0 ));
  LUT4 #(
    .INIT(16'h22B2)) 
    \vAddr_cnt[7]_i_49 
       (.I0(currentState3[3]),
        .I1(vAddr_cnt[3]),
        .I2(currentState3[2]),
        .I3(vAddr_cnt[2]),
        .O(\vAddr_cnt[7]_i_49_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \vAddr_cnt[7]_i_5 
       (.I0(currentState3[31]),
        .I1(currentState3[30]),
        .O(\vAddr_cnt[7]_i_5_n_0 ));
  LUT4 #(
    .INIT(16'h222B)) 
    \vAddr_cnt[7]_i_50 
       (.I0(currentState3[1]),
        .I1(vAddr_cnt[1]),
        .I2(N_IBUF[0]),
        .I3(vAddr_cnt[0]),
        .O(\vAddr_cnt[7]_i_50_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \vAddr_cnt[7]_i_51 
       (.I0(vAddr_cnt[7]),
        .I1(currentState3[7]),
        .I2(vAddr_cnt[6]),
        .I3(currentState3[6]),
        .O(\vAddr_cnt[7]_i_51_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \vAddr_cnt[7]_i_52 
       (.I0(vAddr_cnt[5]),
        .I1(currentState3[5]),
        .I2(vAddr_cnt[4]),
        .I3(currentState3[4]),
        .O(\vAddr_cnt[7]_i_52_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \vAddr_cnt[7]_i_53 
       (.I0(vAddr_cnt[3]),
        .I1(currentState3[3]),
        .I2(vAddr_cnt[2]),
        .I3(currentState3[2]),
        .O(\vAddr_cnt[7]_i_53_n_0 ));
  LUT4 #(
    .INIT(16'h6006)) 
    \vAddr_cnt[7]_i_54 
       (.I0(vAddr_cnt[0]),
        .I1(N_IBUF[0]),
        .I2(vAddr_cnt[1]),
        .I3(currentState3[1]),
        .O(\vAddr_cnt[7]_i_54_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \vAddr_cnt[7]_i_57 
       (.I0(N_IBUF[20]),
        .O(\vAddr_cnt[7]_i_57_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \vAddr_cnt[7]_i_58 
       (.I0(N_IBUF[19]),
        .O(\vAddr_cnt[7]_i_58_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \vAddr_cnt[7]_i_59 
       (.I0(N_IBUF[18]),
        .O(\vAddr_cnt[7]_i_59_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \vAddr_cnt[7]_i_6 
       (.I0(currentState3[29]),
        .I1(currentState3[28]),
        .O(\vAddr_cnt[7]_i_6_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \vAddr_cnt[7]_i_60 
       (.I0(N_IBUF[17]),
        .O(\vAddr_cnt[7]_i_60_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \vAddr_cnt[7]_i_61 
       (.I0(N_IBUF[16]),
        .O(\vAddr_cnt[7]_i_61_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \vAddr_cnt[7]_i_62 
       (.I0(N_IBUF[15]),
        .O(\vAddr_cnt[7]_i_62_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \vAddr_cnt[7]_i_63 
       (.I0(N_IBUF[14]),
        .O(\vAddr_cnt[7]_i_63_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \vAddr_cnt[7]_i_64 
       (.I0(N_IBUF[13]),
        .O(\vAddr_cnt[7]_i_64_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \vAddr_cnt[7]_i_66 
       (.I0(N_IBUF[12]),
        .O(\vAddr_cnt[7]_i_66_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \vAddr_cnt[7]_i_67 
       (.I0(N_IBUF[11]),
        .O(\vAddr_cnt[7]_i_67_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \vAddr_cnt[7]_i_68 
       (.I0(N_IBUF[10]),
        .O(\vAddr_cnt[7]_i_68_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \vAddr_cnt[7]_i_69 
       (.I0(N_IBUF[9]),
        .O(\vAddr_cnt[7]_i_69_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \vAddr_cnt[7]_i_7 
       (.I0(currentState3[27]),
        .I1(currentState3[26]),
        .O(\vAddr_cnt[7]_i_7_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \vAddr_cnt[7]_i_70 
       (.I0(N_IBUF[8]),
        .O(\vAddr_cnt[7]_i_70_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \vAddr_cnt[7]_i_71 
       (.I0(N_IBUF[7]),
        .O(\vAddr_cnt[7]_i_71_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \vAddr_cnt[7]_i_72 
       (.I0(N_IBUF[6]),
        .O(\vAddr_cnt[7]_i_72_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \vAddr_cnt[7]_i_73 
       (.I0(N_IBUF[5]),
        .O(\vAddr_cnt[7]_i_73_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \vAddr_cnt[7]_i_74 
       (.I0(N_IBUF[4]),
        .O(\vAddr_cnt[7]_i_74_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \vAddr_cnt[7]_i_75 
       (.I0(N_IBUF[3]),
        .O(\vAddr_cnt[7]_i_75_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \vAddr_cnt[7]_i_76 
       (.I0(N_IBUF[2]),
        .O(\vAddr_cnt[7]_i_76_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \vAddr_cnt[7]_i_77 
       (.I0(N_IBUF[1]),
        .O(\vAddr_cnt[7]_i_77_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \vAddr_cnt[7]_i_8 
       (.I0(currentState3[25]),
        .I1(currentState3[24]),
        .O(\vAddr_cnt[7]_i_8_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \vAddr_cnt[7]_i_9 
       (.I0(currentState3[30]),
        .I1(currentState3[31]),
        .O(\vAddr_cnt[7]_i_9_n_0 ));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \vAddr_cnt_reg[0] 
       (.C(clk_IBUF_BUFG),
        .CE(\hAddr_cnt[7]_i_1_n_0 ),
        .D(\vAddr_cnt[0]_i_1_n_0 ),
        .Q(vAddr_cnt[0]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \vAddr_cnt_reg[1] 
       (.C(clk_IBUF_BUFG),
        .CE(\hAddr_cnt[7]_i_1_n_0 ),
        .D(\vAddr_cnt[1]_i_1_n_0 ),
        .Q(vAddr_cnt[1]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \vAddr_cnt_reg[2] 
       (.C(clk_IBUF_BUFG),
        .CE(\hAddr_cnt[7]_i_1_n_0 ),
        .D(\vAddr_cnt[2]_i_1_n_0 ),
        .Q(vAddr_cnt[2]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \vAddr_cnt_reg[3] 
       (.C(clk_IBUF_BUFG),
        .CE(\hAddr_cnt[7]_i_1_n_0 ),
        .D(\vAddr_cnt[3]_i_1_n_0 ),
        .Q(vAddr_cnt[3]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \vAddr_cnt_reg[4] 
       (.C(clk_IBUF_BUFG),
        .CE(\hAddr_cnt[7]_i_1_n_0 ),
        .D(\vAddr_cnt[4]_i_1_n_0 ),
        .Q(vAddr_cnt[4]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \vAddr_cnt_reg[5] 
       (.C(clk_IBUF_BUFG),
        .CE(\hAddr_cnt[7]_i_1_n_0 ),
        .D(\vAddr_cnt[5]_i_1_n_0 ),
        .Q(vAddr_cnt[5]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \vAddr_cnt_reg[6] 
       (.C(clk_IBUF_BUFG),
        .CE(\hAddr_cnt[7]_i_1_n_0 ),
        .D(\vAddr_cnt[6]_i_1_n_0 ),
        .Q(vAddr_cnt[6]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \vAddr_cnt_reg[7] 
       (.C(clk_IBUF_BUFG),
        .CE(\hAddr_cnt[7]_i_1_n_0 ),
        .D(\vAddr_cnt[7]_i_1_n_0 ),
        .Q(vAddr_cnt[7]),
        .R(reset_IBUF));
  CARRY4 \vAddr_cnt_reg[7]_i_13 
       (.CI(\vAddr_cnt_reg[7]_i_25_n_0 ),
        .CO({\vAddr_cnt_reg[7]_i_13_n_0 ,\vAddr_cnt_reg[7]_i_13_n_1 ,\vAddr_cnt_reg[7]_i_13_n_2 ,\vAddr_cnt_reg[7]_i_13_n_3 }),
        .CYINIT(1'b0),
        .DI({\vAddr_cnt[7]_i_26_n_0 ,\vAddr_cnt[7]_i_27_n_0 ,\vAddr_cnt[7]_i_28_n_0 ,\vAddr_cnt[7]_i_29_n_0 }),
        .O(\NLW_vAddr_cnt_reg[7]_i_13_O_UNCONNECTED [3:0]),
        .S({\vAddr_cnt[7]_i_30_n_0 ,\vAddr_cnt[7]_i_31_n_0 ,\vAddr_cnt[7]_i_32_n_0 ,\vAddr_cnt[7]_i_33_n_0 }));
  CARRY4 \vAddr_cnt_reg[7]_i_2 
       (.CI(\vAddr_cnt_reg[7]_i_4_n_0 ),
        .CO({currentState28_in,\vAddr_cnt_reg[7]_i_2_n_1 ,\vAddr_cnt_reg[7]_i_2_n_2 ,\vAddr_cnt_reg[7]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({\vAddr_cnt[7]_i_5_n_0 ,\vAddr_cnt[7]_i_6_n_0 ,\vAddr_cnt[7]_i_7_n_0 ,\vAddr_cnt[7]_i_8_n_0 }),
        .O(\NLW_vAddr_cnt_reg[7]_i_2_O_UNCONNECTED [3:0]),
        .S({\vAddr_cnt[7]_i_9_n_0 ,\vAddr_cnt[7]_i_10_n_0 ,\vAddr_cnt[7]_i_11_n_0 ,\vAddr_cnt[7]_i_12_n_0 }));
  CARRY4 \vAddr_cnt_reg[7]_i_22 
       (.CI(\vAddr_cnt_reg[7]_i_23_n_0 ),
        .CO({\NLW_vAddr_cnt_reg[7]_i_22_CO_UNCONNECTED [3:2],\vAddr_cnt_reg[7]_i_22_n_2 ,\vAddr_cnt_reg[7]_i_22_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,N_IBUF[30:29]}),
        .O({\NLW_vAddr_cnt_reg[7]_i_22_O_UNCONNECTED [3],currentState3[31:29]}),
        .S({1'b0,\vAddr_cnt[7]_i_36_n_0 ,\vAddr_cnt[7]_i_37_n_0 ,\vAddr_cnt[7]_i_38_n_0 }));
  CARRY4 \vAddr_cnt_reg[7]_i_23 
       (.CI(\vAddr_cnt_reg[7]_i_24_n_0 ),
        .CO({\vAddr_cnt_reg[7]_i_23_n_0 ,\vAddr_cnt_reg[7]_i_23_n_1 ,\vAddr_cnt_reg[7]_i_23_n_2 ,\vAddr_cnt_reg[7]_i_23_n_3 }),
        .CYINIT(1'b0),
        .DI(N_IBUF[28:25]),
        .O(currentState3[28:25]),
        .S({\vAddr_cnt[7]_i_39_n_0 ,\vAddr_cnt[7]_i_40_n_0 ,\vAddr_cnt[7]_i_41_n_0 ,\vAddr_cnt[7]_i_42_n_0 }));
  CARRY4 \vAddr_cnt_reg[7]_i_24 
       (.CI(\vAddr_cnt_reg[7]_i_34_n_0 ),
        .CO({\vAddr_cnt_reg[7]_i_24_n_0 ,\vAddr_cnt_reg[7]_i_24_n_1 ,\vAddr_cnt_reg[7]_i_24_n_2 ,\vAddr_cnt_reg[7]_i_24_n_3 }),
        .CYINIT(1'b0),
        .DI(N_IBUF[24:21]),
        .O(currentState3[24:21]),
        .S({\vAddr_cnt[7]_i_43_n_0 ,\vAddr_cnt[7]_i_44_n_0 ,\vAddr_cnt[7]_i_45_n_0 ,\vAddr_cnt[7]_i_46_n_0 }));
  CARRY4 \vAddr_cnt_reg[7]_i_25 
       (.CI(1'b0),
        .CO({\vAddr_cnt_reg[7]_i_25_n_0 ,\vAddr_cnt_reg[7]_i_25_n_1 ,\vAddr_cnt_reg[7]_i_25_n_2 ,\vAddr_cnt_reg[7]_i_25_n_3 }),
        .CYINIT(1'b0),
        .DI({\vAddr_cnt[7]_i_47_n_0 ,\vAddr_cnt[7]_i_48_n_0 ,\vAddr_cnt[7]_i_49_n_0 ,\vAddr_cnt[7]_i_50_n_0 }),
        .O(\NLW_vAddr_cnt_reg[7]_i_25_O_UNCONNECTED [3:0]),
        .S({\vAddr_cnt[7]_i_51_n_0 ,\vAddr_cnt[7]_i_52_n_0 ,\vAddr_cnt[7]_i_53_n_0 ,\vAddr_cnt[7]_i_54_n_0 }));
  CARRY4 \vAddr_cnt_reg[7]_i_34 
       (.CI(\vAddr_cnt_reg[7]_i_35_n_0 ),
        .CO({\vAddr_cnt_reg[7]_i_34_n_0 ,\vAddr_cnt_reg[7]_i_34_n_1 ,\vAddr_cnt_reg[7]_i_34_n_2 ,\vAddr_cnt_reg[7]_i_34_n_3 }),
        .CYINIT(1'b0),
        .DI(N_IBUF[20:17]),
        .O(currentState3[20:17]),
        .S({\vAddr_cnt[7]_i_57_n_0 ,\vAddr_cnt[7]_i_58_n_0 ,\vAddr_cnt[7]_i_59_n_0 ,\vAddr_cnt[7]_i_60_n_0 }));
  CARRY4 \vAddr_cnt_reg[7]_i_35 
       (.CI(\vAddr_cnt_reg[7]_i_55_n_0 ),
        .CO({\vAddr_cnt_reg[7]_i_35_n_0 ,\vAddr_cnt_reg[7]_i_35_n_1 ,\vAddr_cnt_reg[7]_i_35_n_2 ,\vAddr_cnt_reg[7]_i_35_n_3 }),
        .CYINIT(1'b0),
        .DI(N_IBUF[16:13]),
        .O(currentState3[16:13]),
        .S({\vAddr_cnt[7]_i_61_n_0 ,\vAddr_cnt[7]_i_62_n_0 ,\vAddr_cnt[7]_i_63_n_0 ,\vAddr_cnt[7]_i_64_n_0 }));
  CARRY4 \vAddr_cnt_reg[7]_i_4 
       (.CI(\vAddr_cnt_reg[7]_i_13_n_0 ),
        .CO({\vAddr_cnt_reg[7]_i_4_n_0 ,\vAddr_cnt_reg[7]_i_4_n_1 ,\vAddr_cnt_reg[7]_i_4_n_2 ,\vAddr_cnt_reg[7]_i_4_n_3 }),
        .CYINIT(1'b0),
        .DI({\vAddr_cnt[7]_i_14_n_0 ,\vAddr_cnt[7]_i_15_n_0 ,\vAddr_cnt[7]_i_16_n_0 ,\vAddr_cnt[7]_i_17_n_0 }),
        .O(\NLW_vAddr_cnt_reg[7]_i_4_O_UNCONNECTED [3:0]),
        .S({\vAddr_cnt[7]_i_18_n_0 ,\vAddr_cnt[7]_i_19_n_0 ,\vAddr_cnt[7]_i_20_n_0 ,\vAddr_cnt[7]_i_21_n_0 }));
  CARRY4 \vAddr_cnt_reg[7]_i_55 
       (.CI(\vAddr_cnt_reg[7]_i_56_n_0 ),
        .CO({\vAddr_cnt_reg[7]_i_55_n_0 ,\vAddr_cnt_reg[7]_i_55_n_1 ,\vAddr_cnt_reg[7]_i_55_n_2 ,\vAddr_cnt_reg[7]_i_55_n_3 }),
        .CYINIT(1'b0),
        .DI(N_IBUF[12:9]),
        .O(currentState3[12:9]),
        .S({\vAddr_cnt[7]_i_66_n_0 ,\vAddr_cnt[7]_i_67_n_0 ,\vAddr_cnt[7]_i_68_n_0 ,\vAddr_cnt[7]_i_69_n_0 }));
  CARRY4 \vAddr_cnt_reg[7]_i_56 
       (.CI(\vAddr_cnt_reg[7]_i_65_n_0 ),
        .CO({\vAddr_cnt_reg[7]_i_56_n_0 ,\vAddr_cnt_reg[7]_i_56_n_1 ,\vAddr_cnt_reg[7]_i_56_n_2 ,\vAddr_cnt_reg[7]_i_56_n_3 }),
        .CYINIT(1'b0),
        .DI(N_IBUF[8:5]),
        .O(currentState3[8:5]),
        .S({\vAddr_cnt[7]_i_70_n_0 ,\vAddr_cnt[7]_i_71_n_0 ,\vAddr_cnt[7]_i_72_n_0 ,\vAddr_cnt[7]_i_73_n_0 }));
  CARRY4 \vAddr_cnt_reg[7]_i_65 
       (.CI(1'b0),
        .CO({\vAddr_cnt_reg[7]_i_65_n_0 ,\vAddr_cnt_reg[7]_i_65_n_1 ,\vAddr_cnt_reg[7]_i_65_n_2 ,\vAddr_cnt_reg[7]_i_65_n_3 }),
        .CYINIT(N_IBUF[0]),
        .DI(N_IBUF[4:1]),
        .O(currentState3[4:1]),
        .S({\vAddr_cnt[7]_i_74_n_0 ,\vAddr_cnt[7]_i_75_n_0 ,\vAddr_cnt[7]_i_76_n_0 ,\vAddr_cnt[7]_i_77_n_0 }));
  IBUF \weightAddr_IBUF[0]_inst 
       (.I(weightAddr[0]),
        .O(weightAddr_IBUF[0]));
  IBUF \weightAddr_IBUF[1]_inst 
       (.I(weightAddr[1]),
        .O(weightAddr_IBUF[1]));
  IBUF \weightAddr_IBUF[2]_inst 
       (.I(weightAddr[2]),
        .O(weightAddr_IBUF[2]));
  IBUF \weightAddr_IBUF[3]_inst 
       (.I(weightAddr[3]),
        .O(weightAddr_IBUF[3]));
  IBUF \weightAddr_IBUF[4]_inst 
       (.I(weightAddr[4]),
        .O(weightAddr_IBUF[4]));
  LUT2 #(
    .INIT(4'h2)) 
    weightAddr_internal_inferred_i_1
       (.I0(weightAddr_internal0[7]),
        .I1(reset_IBUF),
        .O(weightAddr_internal[7]));
  CARRY4 weightAddr_internal_inferred_i_10
       (.CI(1'b0),
        .CO({weightAddr_internal_inferred_i_10_n_0,weightAddr_internal_inferred_i_10_n_1,weightAddr_internal_inferred_i_10_n_2,weightAddr_internal_inferred_i_10_n_3}),
        .CYINIT(1'b0),
        .DI(hAddr_cnt[3:0]),
        .O(weightAddr_internal0[3:0]),
        .S({weightAddr_internal_inferred_i_15_n_0,weightAddr_internal_inferred_i_16_n_0,weightAddr_internal_inferred_i_17_n_0,weightAddr_internal_inferred_i_18_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    weightAddr_internal_inferred_i_11
       (.I0(hAddr_cnt[7]),
        .I1(weightAddr_internal1[7]),
        .O(weightAddr_internal_inferred_i_11_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    weightAddr_internal_inferred_i_12
       (.I0(hAddr_cnt[6]),
        .I1(weightAddr_internal1[6]),
        .O(weightAddr_internal_inferred_i_12_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    weightAddr_internal_inferred_i_13
       (.I0(hAddr_cnt[5]),
        .I1(weightAddr_internal1[5]),
        .O(weightAddr_internal_inferred_i_13_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    weightAddr_internal_inferred_i_14
       (.I0(hAddr_cnt[4]),
        .I1(weightAddr_internal1[4]),
        .O(weightAddr_internal_inferred_i_14_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    weightAddr_internal_inferred_i_15
       (.I0(hAddr_cnt[3]),
        .I1(weightAddr_internal_inferred_i_21_n_7),
        .I2(weightAddr_internal_inferred_i_22_n_4),
        .O(weightAddr_internal_inferred_i_15_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    weightAddr_internal_inferred_i_16
       (.I0(hAddr_cnt[2]),
        .I1(weightAddr_internal1[2]),
        .O(weightAddr_internal_inferred_i_16_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    weightAddr_internal_inferred_i_17
       (.I0(hAddr_cnt[1]),
        .I1(weightAddr_internal1[1]),
        .O(weightAddr_internal_inferred_i_17_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    weightAddr_internal_inferred_i_18
       (.I0(hAddr_cnt[0]),
        .I1(weightAddr_internal1[0]),
        .O(weightAddr_internal_inferred_i_18_n_0));
  CARRY4 weightAddr_internal_inferred_i_19
       (.CI(weightAddr_internal_inferred_i_20_n_0),
        .CO(NLW_weightAddr_internal_inferred_i_19_CO_UNCONNECTED[3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({NLW_weightAddr_internal_inferred_i_19_O_UNCONNECTED[3:1],weightAddr_internal1[7]}),
        .S({1'b0,1'b0,1'b0,weightAddr_internal_inferred_i_23_n_0}));
  LUT2 #(
    .INIT(4'h2)) 
    weightAddr_internal_inferred_i_2
       (.I0(weightAddr_internal0[6]),
        .I1(reset_IBUF),
        .O(weightAddr_internal[6]));
  CARRY4 weightAddr_internal_inferred_i_20
       (.CI(1'b0),
        .CO({weightAddr_internal_inferred_i_20_n_0,weightAddr_internal_inferred_i_20_n_1,weightAddr_internal_inferred_i_20_n_2,weightAddr_internal_inferred_i_20_n_3}),
        .CYINIT(1'b0),
        .DI({weightAddr_internal_inferred_i_24_n_0,weightAddr_internal_inferred_i_25_n_6,weightAddr_internal_inferred_i_25_n_7,weightAddr_internal_inferred_i_22_n_4}),
        .O({weightAddr_internal1[6:4],NLW_weightAddr_internal_inferred_i_20_O_UNCONNECTED[0]}),
        .S({weightAddr_internal_inferred_i_26_n_0,weightAddr_internal_inferred_i_27_n_0,weightAddr_internal_inferred_i_28_n_0,weightAddr_internal1[3]}));
  CARRY4 weightAddr_internal_inferred_i_21
       (.CI(1'b0),
        .CO({weightAddr_internal_inferred_i_21_n_0,weightAddr_internal_inferred_i_21_n_1,weightAddr_internal_inferred_i_21_n_2,weightAddr_internal_inferred_i_21_n_3}),
        .CYINIT(1'b0),
        .DI({weightAddr_internal_inferred_i_30_n_0,weightAddr_internal_inferred_i_31_n_0,weightAddr_internal_inferred_i_32_n_0,1'b0}),
        .O({weightAddr_internal_inferred_i_21_n_4,weightAddr_internal_inferred_i_21_n_5,weightAddr_internal_inferred_i_21_n_6,weightAddr_internal_inferred_i_21_n_7}),
        .S({weightAddr_internal_inferred_i_33_n_0,weightAddr_internal_inferred_i_34_n_0,weightAddr_internal_inferred_i_35_n_0,weightAddr_internal_inferred_i_36_n_0}));
  CARRY4 weightAddr_internal_inferred_i_22
       (.CI(1'b0),
        .CO({weightAddr_internal_inferred_i_22_n_0,weightAddr_internal_inferred_i_22_n_1,weightAddr_internal_inferred_i_22_n_2,weightAddr_internal_inferred_i_22_n_3}),
        .CYINIT(1'b0),
        .DI({weightAddr_internal_inferred_i_37_n_0,weightAddr_internal_inferred_i_38_n_0,weightAddr_internal_inferred_i_39_n_0,1'b0}),
        .O({weightAddr_internal_inferred_i_22_n_4,weightAddr_internal1[2:0]}),
        .S({weightAddr_internal_inferred_i_40_n_0,weightAddr_internal_inferred_i_41_n_0,weightAddr_internal_inferred_i_42_n_0,weightAddr_internal_inferred_i_43_n_0}));
  LUT5 #(
    .INIT(32'h69999666)) 
    weightAddr_internal_inferred_i_23
       (.I0(weightAddr_internal_inferred_i_44_n_7),
        .I1(weightAddr_internal_inferred_i_25_n_4),
        .I2(f_IBUF[7]),
        .I3(vAddr_cnt[0]),
        .I4(weightAddr_internal_inferred_i_45_n_0),
        .O(weightAddr_internal_inferred_i_23_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    weightAddr_internal_inferred_i_24
       (.I0(weightAddr_internal_inferred_i_25_n_5),
        .I1(weightAddr_internal_inferred_i_21_n_4),
        .O(weightAddr_internal_inferred_i_24_n_0));
  CARRY4 weightAddr_internal_inferred_i_25
       (.CI(weightAddr_internal_inferred_i_22_n_0),
        .CO({NLW_weightAddr_internal_inferred_i_25_CO_UNCONNECTED[3],weightAddr_internal_inferred_i_25_n_1,weightAddr_internal_inferred_i_25_n_2,weightAddr_internal_inferred_i_25_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,weightAddr_internal_inferred_i_46_n_0,weightAddr_internal_inferred_i_47_n_0,weightAddr_internal_inferred_i_48_n_0}),
        .O({weightAddr_internal_inferred_i_25_n_4,weightAddr_internal_inferred_i_25_n_5,weightAddr_internal_inferred_i_25_n_6,weightAddr_internal_inferred_i_25_n_7}),
        .S({weightAddr_internal_inferred_i_49_n_0,weightAddr_internal_inferred_i_50_n_0,weightAddr_internal_inferred_i_51_n_0,weightAddr_internal_inferred_i_52_n_0}));
  LUT4 #(
    .INIT(16'h9666)) 
    weightAddr_internal_inferred_i_26
       (.I0(weightAddr_internal_inferred_i_21_n_4),
        .I1(weightAddr_internal_inferred_i_25_n_5),
        .I2(vAddr_cnt[0]),
        .I3(f_IBUF[6]),
        .O(weightAddr_internal_inferred_i_26_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    weightAddr_internal_inferred_i_27
       (.I0(weightAddr_internal_inferred_i_25_n_6),
        .I1(weightAddr_internal_inferred_i_21_n_5),
        .O(weightAddr_internal_inferred_i_27_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    weightAddr_internal_inferred_i_28
       (.I0(weightAddr_internal_inferred_i_25_n_7),
        .I1(weightAddr_internal_inferred_i_21_n_6),
        .O(weightAddr_internal_inferred_i_28_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    weightAddr_internal_inferred_i_29
       (.I0(weightAddr_internal_inferred_i_22_n_4),
        .I1(weightAddr_internal_inferred_i_21_n_7),
        .O(weightAddr_internal1[3]));
  LUT2 #(
    .INIT(4'h2)) 
    weightAddr_internal_inferred_i_3
       (.I0(weightAddr_internal0[5]),
        .I1(reset_IBUF),
        .O(weightAddr_internal[5]));
  LUT6 #(
    .INIT(64'h8777788878887888)) 
    weightAddr_internal_inferred_i_30
       (.I0(vAddr_cnt[3]),
        .I1(f_IBUF[3]),
        .I2(f_IBUF[4]),
        .I3(vAddr_cnt[2]),
        .I4(f_IBUF[5]),
        .I5(vAddr_cnt[1]),
        .O(weightAddr_internal_inferred_i_30_n_0));
  LUT4 #(
    .INIT(16'h7888)) 
    weightAddr_internal_inferred_i_31
       (.I0(vAddr_cnt[1]),
        .I1(f_IBUF[4]),
        .I2(vAddr_cnt[0]),
        .I3(f_IBUF[5]),
        .O(weightAddr_internal_inferred_i_31_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    weightAddr_internal_inferred_i_32
       (.I0(f_IBUF[4]),
        .I1(vAddr_cnt[0]),
        .O(weightAddr_internal_inferred_i_32_n_0));
  LUT6 #(
    .INIT(64'h6A3F953F6AC06AC0)) 
    weightAddr_internal_inferred_i_33
       (.I0(vAddr_cnt[2]),
        .I1(f_IBUF[3]),
        .I2(vAddr_cnt[3]),
        .I3(f_IBUF[4]),
        .I4(vAddr_cnt[0]),
        .I5(weightAddr_internal_inferred_i_53_n_0),
        .O(weightAddr_internal_inferred_i_33_n_0));
  LUT6 #(
    .INIT(64'h8777788878887888)) 
    weightAddr_internal_inferred_i_34
       (.I0(f_IBUF[5]),
        .I1(vAddr_cnt[0]),
        .I2(f_IBUF[4]),
        .I3(vAddr_cnt[1]),
        .I4(vAddr_cnt[2]),
        .I5(f_IBUF[3]),
        .O(weightAddr_internal_inferred_i_34_n_0));
  LUT4 #(
    .INIT(16'h7888)) 
    weightAddr_internal_inferred_i_35
       (.I0(vAddr_cnt[1]),
        .I1(f_IBUF[3]),
        .I2(vAddr_cnt[0]),
        .I3(f_IBUF[4]),
        .O(weightAddr_internal_inferred_i_35_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    weightAddr_internal_inferred_i_36
       (.I0(f_IBUF[3]),
        .I1(vAddr_cnt[0]),
        .O(weightAddr_internal_inferred_i_36_n_0));
  LUT6 #(
    .INIT(64'h8777788878887888)) 
    weightAddr_internal_inferred_i_37
       (.I0(vAddr_cnt[3]),
        .I1(f_IBUF[0]),
        .I2(f_IBUF[1]),
        .I3(vAddr_cnt[2]),
        .I4(f_IBUF[2]),
        .I5(vAddr_cnt[1]),
        .O(weightAddr_internal_inferred_i_37_n_0));
  LUT4 #(
    .INIT(16'h7888)) 
    weightAddr_internal_inferred_i_38
       (.I0(vAddr_cnt[1]),
        .I1(f_IBUF[1]),
        .I2(vAddr_cnt[0]),
        .I3(f_IBUF[2]),
        .O(weightAddr_internal_inferred_i_38_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    weightAddr_internal_inferred_i_39
       (.I0(f_IBUF[1]),
        .I1(vAddr_cnt[0]),
        .O(weightAddr_internal_inferred_i_39_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    weightAddr_internal_inferred_i_4
       (.I0(weightAddr_internal0[4]),
        .I1(reset_IBUF),
        .O(weightAddr_internal[4]));
  LUT6 #(
    .INIT(64'h6A3F953F6AC06AC0)) 
    weightAddr_internal_inferred_i_40
       (.I0(vAddr_cnt[2]),
        .I1(f_IBUF[0]),
        .I2(vAddr_cnt[3]),
        .I3(f_IBUF[1]),
        .I4(vAddr_cnt[0]),
        .I5(weightAddr_internal_inferred_i_54_n_0),
        .O(weightAddr_internal_inferred_i_40_n_0));
  LUT6 #(
    .INIT(64'h8777788878887888)) 
    weightAddr_internal_inferred_i_41
       (.I0(f_IBUF[2]),
        .I1(vAddr_cnt[0]),
        .I2(f_IBUF[1]),
        .I3(vAddr_cnt[1]),
        .I4(vAddr_cnt[2]),
        .I5(f_IBUF[0]),
        .O(weightAddr_internal_inferred_i_41_n_0));
  LUT4 #(
    .INIT(16'h7888)) 
    weightAddr_internal_inferred_i_42
       (.I0(vAddr_cnt[1]),
        .I1(f_IBUF[0]),
        .I2(vAddr_cnt[0]),
        .I3(f_IBUF[1]),
        .O(weightAddr_internal_inferred_i_42_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    weightAddr_internal_inferred_i_43
       (.I0(f_IBUF[0]),
        .I1(vAddr_cnt[0]),
        .O(weightAddr_internal_inferred_i_43_n_0));
  CARRY4 weightAddr_internal_inferred_i_44
       (.CI(weightAddr_internal_inferred_i_21_n_0),
        .CO(NLW_weightAddr_internal_inferred_i_44_CO_UNCONNECTED[3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({NLW_weightAddr_internal_inferred_i_44_O_UNCONNECTED[3:1],weightAddr_internal_inferred_i_44_n_7}),
        .S({1'b0,1'b0,1'b0,weightAddr_internal_inferred_i_55_n_0}));
  LUT4 #(
    .INIT(16'h7888)) 
    weightAddr_internal_inferred_i_45
       (.I0(vAddr_cnt[1]),
        .I1(f_IBUF[6]),
        .I2(weightAddr_internal_inferred_i_21_n_4),
        .I3(weightAddr_internal_inferred_i_25_n_5),
        .O(weightAddr_internal_inferred_i_45_n_0));
  LUT6 #(
    .INIT(64'hF888800080008000)) 
    weightAddr_internal_inferred_i_46
       (.I0(vAddr_cnt[3]),
        .I1(f_IBUF[2]),
        .I2(vAddr_cnt[4]),
        .I3(f_IBUF[1]),
        .I4(f_IBUF[0]),
        .I5(vAddr_cnt[5]),
        .O(weightAddr_internal_inferred_i_46_n_0));
  LUT6 #(
    .INIT(64'hF888800080008000)) 
    weightAddr_internal_inferred_i_47
       (.I0(vAddr_cnt[2]),
        .I1(f_IBUF[2]),
        .I2(vAddr_cnt[3]),
        .I3(f_IBUF[1]),
        .I4(f_IBUF[0]),
        .I5(vAddr_cnt[4]),
        .O(weightAddr_internal_inferred_i_47_n_0));
  LUT6 #(
    .INIT(64'hF888800080008000)) 
    weightAddr_internal_inferred_i_48
       (.I0(vAddr_cnt[1]),
        .I1(f_IBUF[2]),
        .I2(vAddr_cnt[2]),
        .I3(f_IBUF[1]),
        .I4(f_IBUF[0]),
        .I5(vAddr_cnt[3]),
        .O(weightAddr_internal_inferred_i_48_n_0));
  LUT6 #(
    .INIT(64'hEA808080157F7F7F)) 
    weightAddr_internal_inferred_i_49
       (.I0(weightAddr_internal_inferred_i_56_n_0),
        .I1(f_IBUF[1]),
        .I2(vAddr_cnt[5]),
        .I3(f_IBUF[2]),
        .I4(vAddr_cnt[4]),
        .I5(weightAddr_internal_inferred_i_57_n_0),
        .O(weightAddr_internal_inferred_i_49_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    weightAddr_internal_inferred_i_5
       (.I0(weightAddr_internal0[3]),
        .I1(reset_IBUF),
        .O(weightAddr_internal[3]));
  LUT6 #(
    .INIT(64'h6999966696669666)) 
    weightAddr_internal_inferred_i_50
       (.I0(weightAddr_internal_inferred_i_46_n_0),
        .I1(weightAddr_internal_inferred_i_58_n_0),
        .I2(vAddr_cnt[5]),
        .I3(f_IBUF[1]),
        .I4(f_IBUF[0]),
        .I5(vAddr_cnt[6]),
        .O(weightAddr_internal_inferred_i_50_n_0));
  LUT6 #(
    .INIT(64'h6A95956A956A956A)) 
    weightAddr_internal_inferred_i_51
       (.I0(weightAddr_internal_inferred_i_47_n_0),
        .I1(vAddr_cnt[3]),
        .I2(f_IBUF[2]),
        .I3(weightAddr_internal_inferred_i_59_n_0),
        .I4(f_IBUF[0]),
        .I5(vAddr_cnt[5]),
        .O(weightAddr_internal_inferred_i_51_n_0));
  LUT6 #(
    .INIT(64'h6A95956A956A956A)) 
    weightAddr_internal_inferred_i_52
       (.I0(weightAddr_internal_inferred_i_48_n_0),
        .I1(vAddr_cnt[2]),
        .I2(f_IBUF[2]),
        .I3(weightAddr_internal_inferred_i_60_n_0),
        .I4(f_IBUF[0]),
        .I5(vAddr_cnt[4]),
        .O(weightAddr_internal_inferred_i_52_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    weightAddr_internal_inferred_i_53
       (.I0(f_IBUF[5]),
        .I1(vAddr_cnt[1]),
        .O(weightAddr_internal_inferred_i_53_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    weightAddr_internal_inferred_i_54
       (.I0(f_IBUF[2]),
        .I1(vAddr_cnt[1]),
        .O(weightAddr_internal_inferred_i_54_n_0));
  LUT6 #(
    .INIT(64'hEA808080157F7F7F)) 
    weightAddr_internal_inferred_i_55
       (.I0(weightAddr_internal_inferred_i_61_n_0),
        .I1(f_IBUF[4]),
        .I2(vAddr_cnt[2]),
        .I3(f_IBUF[5]),
        .I4(vAddr_cnt[1]),
        .I5(weightAddr_internal_inferred_i_62_n_0),
        .O(weightAddr_internal_inferred_i_55_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    weightAddr_internal_inferred_i_56
       (.I0(f_IBUF[0]),
        .I1(vAddr_cnt[6]),
        .O(weightAddr_internal_inferred_i_56_n_0));
  LUT6 #(
    .INIT(64'h7888877787778777)) 
    weightAddr_internal_inferred_i_57
       (.I0(vAddr_cnt[7]),
        .I1(f_IBUF[0]),
        .I2(f_IBUF[1]),
        .I3(vAddr_cnt[6]),
        .I4(f_IBUF[2]),
        .I5(vAddr_cnt[5]),
        .O(weightAddr_internal_inferred_i_57_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    weightAddr_internal_inferred_i_58
       (.I0(f_IBUF[2]),
        .I1(vAddr_cnt[4]),
        .O(weightAddr_internal_inferred_i_58_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    weightAddr_internal_inferred_i_59
       (.I0(f_IBUF[1]),
        .I1(vAddr_cnt[4]),
        .O(weightAddr_internal_inferred_i_59_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    weightAddr_internal_inferred_i_6
       (.I0(weightAddr_internal0[2]),
        .I1(reset_IBUF),
        .O(weightAddr_internal[2]));
  LUT2 #(
    .INIT(4'h8)) 
    weightAddr_internal_inferred_i_60
       (.I0(f_IBUF[1]),
        .I1(vAddr_cnt[3]),
        .O(weightAddr_internal_inferred_i_60_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    weightAddr_internal_inferred_i_61
       (.I0(f_IBUF[3]),
        .I1(vAddr_cnt[3]),
        .O(weightAddr_internal_inferred_i_61_n_0));
  LUT6 #(
    .INIT(64'h7888877787778777)) 
    weightAddr_internal_inferred_i_62
       (.I0(vAddr_cnt[4]),
        .I1(f_IBUF[3]),
        .I2(f_IBUF[4]),
        .I3(vAddr_cnt[3]),
        .I4(f_IBUF[5]),
        .I5(vAddr_cnt[2]),
        .O(weightAddr_internal_inferred_i_62_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    weightAddr_internal_inferred_i_7
       (.I0(weightAddr_internal0[1]),
        .I1(reset_IBUF),
        .O(weightAddr_internal[1]));
  LUT2 #(
    .INIT(4'h2)) 
    weightAddr_internal_inferred_i_8
       (.I0(weightAddr_internal0[0]),
        .I1(reset_IBUF),
        .O(weightAddr_internal[0]));
  CARRY4 weightAddr_internal_inferred_i_9
       (.CI(weightAddr_internal_inferred_i_10_n_0),
        .CO({NLW_weightAddr_internal_inferred_i_9_CO_UNCONNECTED[3],weightAddr_internal_inferred_i_9_n_1,weightAddr_internal_inferred_i_9_n_2,weightAddr_internal_inferred_i_9_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,hAddr_cnt[6:4]}),
        .O(weightAddr_internal0[7:4]),
        .S({weightAddr_internal_inferred_i_11_n_0,weightAddr_internal_inferred_i_12_n_0,weightAddr_internal_inferred_i_13_n_0,weightAddr_internal_inferred_i_14_n_0}));
  IBUF \weightMem_in_IBUF[0]_inst 
       (.I(weightMem_in[0]),
        .O(weightMem_in_IBUF[0]));
  IBUF \weightMem_in_IBUF[10]_inst 
       (.I(weightMem_in[10]),
        .O(weightMem_in_IBUF[10]));
  IBUF \weightMem_in_IBUF[11]_inst 
       (.I(weightMem_in[11]),
        .O(weightMem_in_IBUF[11]));
  IBUF \weightMem_in_IBUF[12]_inst 
       (.I(weightMem_in[12]),
        .O(weightMem_in_IBUF[12]));
  IBUF \weightMem_in_IBUF[13]_inst 
       (.I(weightMem_in[13]),
        .O(weightMem_in_IBUF[13]));
  IBUF \weightMem_in_IBUF[14]_inst 
       (.I(weightMem_in[14]),
        .O(weightMem_in_IBUF[14]));
  IBUF \weightMem_in_IBUF[15]_inst 
       (.I(weightMem_in[15]),
        .O(weightMem_in_IBUF[15]));
  IBUF \weightMem_in_IBUF[16]_inst 
       (.I(weightMem_in[16]),
        .O(weightMem_in_IBUF[16]));
  IBUF \weightMem_in_IBUF[17]_inst 
       (.I(weightMem_in[17]),
        .O(weightMem_in_IBUF[17]));
  IBUF \weightMem_in_IBUF[18]_inst 
       (.I(weightMem_in[18]),
        .O(weightMem_in_IBUF[18]));
  IBUF \weightMem_in_IBUF[19]_inst 
       (.I(weightMem_in[19]),
        .O(weightMem_in_IBUF[19]));
  IBUF \weightMem_in_IBUF[1]_inst 
       (.I(weightMem_in[1]),
        .O(weightMem_in_IBUF[1]));
  IBUF \weightMem_in_IBUF[20]_inst 
       (.I(weightMem_in[20]),
        .O(weightMem_in_IBUF[20]));
  IBUF \weightMem_in_IBUF[21]_inst 
       (.I(weightMem_in[21]),
        .O(weightMem_in_IBUF[21]));
  IBUF \weightMem_in_IBUF[22]_inst 
       (.I(weightMem_in[22]),
        .O(weightMem_in_IBUF[22]));
  IBUF \weightMem_in_IBUF[23]_inst 
       (.I(weightMem_in[23]),
        .O(weightMem_in_IBUF[23]));
  IBUF \weightMem_in_IBUF[24]_inst 
       (.I(weightMem_in[24]),
        .O(weightMem_in_IBUF[24]));
  IBUF \weightMem_in_IBUF[25]_inst 
       (.I(weightMem_in[25]),
        .O(weightMem_in_IBUF[25]));
  IBUF \weightMem_in_IBUF[26]_inst 
       (.I(weightMem_in[26]),
        .O(weightMem_in_IBUF[26]));
  IBUF \weightMem_in_IBUF[27]_inst 
       (.I(weightMem_in[27]),
        .O(weightMem_in_IBUF[27]));
  IBUF \weightMem_in_IBUF[28]_inst 
       (.I(weightMem_in[28]),
        .O(weightMem_in_IBUF[28]));
  IBUF \weightMem_in_IBUF[29]_inst 
       (.I(weightMem_in[29]),
        .O(weightMem_in_IBUF[29]));
  IBUF \weightMem_in_IBUF[2]_inst 
       (.I(weightMem_in[2]),
        .O(weightMem_in_IBUF[2]));
  IBUF \weightMem_in_IBUF[30]_inst 
       (.I(weightMem_in[30]),
        .O(weightMem_in_IBUF[30]));
  IBUF \weightMem_in_IBUF[31]_inst 
       (.I(weightMem_in[31]),
        .O(weightMem_in_IBUF[31]));
  IBUF \weightMem_in_IBUF[3]_inst 
       (.I(weightMem_in[3]),
        .O(weightMem_in_IBUF[3]));
  IBUF \weightMem_in_IBUF[4]_inst 
       (.I(weightMem_in[4]),
        .O(weightMem_in_IBUF[4]));
  IBUF \weightMem_in_IBUF[5]_inst 
       (.I(weightMem_in[5]),
        .O(weightMem_in_IBUF[5]));
  IBUF \weightMem_in_IBUF[6]_inst 
       (.I(weightMem_in[6]),
        .O(weightMem_in_IBUF[6]));
  IBUF \weightMem_in_IBUF[7]_inst 
       (.I(weightMem_in[7]),
        .O(weightMem_in_IBUF[7]));
  IBUF \weightMem_in_IBUF[8]_inst 
       (.I(weightMem_in[8]),
        .O(weightMem_in_IBUF[8]));
  IBUF \weightMem_in_IBUF[9]_inst 
       (.I(weightMem_in[9]),
        .O(weightMem_in_IBUF[9]));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \weightMem_out_internal_reg[0] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(weightMem_out[0]),
        .Q(weightMem_out_internal[0]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \weightMem_out_internal_reg[10] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(weightMem_out[10]),
        .Q(weightMem_out_internal[10]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \weightMem_out_internal_reg[11] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(weightMem_out[11]),
        .Q(weightMem_out_internal[11]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \weightMem_out_internal_reg[12] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(weightMem_out[12]),
        .Q(weightMem_out_internal[12]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \weightMem_out_internal_reg[13] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(weightMem_out[13]),
        .Q(weightMem_out_internal[13]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \weightMem_out_internal_reg[14] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(weightMem_out[14]),
        .Q(weightMem_out_internal[14]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \weightMem_out_internal_reg[15] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(weightMem_out[15]),
        .Q(weightMem_out_internal[15]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \weightMem_out_internal_reg[16] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(weightMem_out[16]),
        .Q(weightMem_out_internal[16]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \weightMem_out_internal_reg[17] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(weightMem_out[17]),
        .Q(weightMem_out_internal[17]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \weightMem_out_internal_reg[18] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(weightMem_out[18]),
        .Q(weightMem_out_internal[18]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \weightMem_out_internal_reg[19] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(weightMem_out[19]),
        .Q(weightMem_out_internal[19]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \weightMem_out_internal_reg[1] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(weightMem_out[1]),
        .Q(weightMem_out_internal[1]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \weightMem_out_internal_reg[20] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(weightMem_out[20]),
        .Q(weightMem_out_internal[20]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \weightMem_out_internal_reg[21] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(weightMem_out[21]),
        .Q(weightMem_out_internal[21]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \weightMem_out_internal_reg[22] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(weightMem_out[22]),
        .Q(weightMem_out_internal[22]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \weightMem_out_internal_reg[23] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(weightMem_out[23]),
        .Q(weightMem_out_internal[23]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \weightMem_out_internal_reg[24] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(weightMem_out[24]),
        .Q(weightMem_out_internal[24]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \weightMem_out_internal_reg[25] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(weightMem_out[25]),
        .Q(weightMem_out_internal[25]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \weightMem_out_internal_reg[26] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(weightMem_out[26]),
        .Q(weightMem_out_internal[26]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \weightMem_out_internal_reg[27] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(weightMem_out[27]),
        .Q(weightMem_out_internal[27]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \weightMem_out_internal_reg[28] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(weightMem_out[28]),
        .Q(weightMem_out_internal[28]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \weightMem_out_internal_reg[29] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(weightMem_out[29]),
        .Q(weightMem_out_internal[29]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \weightMem_out_internal_reg[2] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(weightMem_out[2]),
        .Q(weightMem_out_internal[2]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \weightMem_out_internal_reg[30] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(weightMem_out[30]),
        .Q(weightMem_out_internal[30]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \weightMem_out_internal_reg[31] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(weightMem_out[31]),
        .Q(weightMem_out_internal[31]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \weightMem_out_internal_reg[3] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(weightMem_out[3]),
        .Q(weightMem_out_internal[3]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \weightMem_out_internal_reg[4] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(weightMem_out[4]),
        .Q(weightMem_out_internal[4]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \weightMem_out_internal_reg[5] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(weightMem_out[5]),
        .Q(weightMem_out_internal[5]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \weightMem_out_internal_reg[6] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(weightMem_out[6]),
        .Q(weightMem_out_internal[6]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \weightMem_out_internal_reg[7] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(weightMem_out[7]),
        .Q(weightMem_out_internal[7]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \weightMem_out_internal_reg[8] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(weightMem_out[8]),
        .Q(weightMem_out_internal[8]),
        .R(reset_IBUF));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \weightMem_out_internal_reg[9] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(weightMem_out[9]),
        .Q(weightMem_out_internal[9]),
        .R(reset_IBUF));
endmodule

module rdp_256X32
   (D,
    clk_IBUF_BUFG,
    out,
    \vAddr_cnt_reg[6] ,
    ADDRBWRADDR,
    dataMem_in_IBUF,
    cen_dataMemWR_reg);
  output [31:0]D;
  input clk_IBUF_BUFG;
  input out;
  input [4:0]\vAddr_cnt_reg[6] ;
  input [4:0]ADDRBWRADDR;
  input [31:0]dataMem_in_IBUF;
  input cen_dataMemWR_reg;

  wire [4:0]ADDRBWRADDR;
  wire [31:0]D;
  wire cen_dataMemWR_reg;
  wire clk_IBUF_BUFG;
  wire [31:0]dataMem_in_IBUF;
  wire out;
  wire [4:0]\vAddr_cnt_reg[6] ;
  wire [1:0]NLW_r_2p_reg_DOPADOP_UNCONNECTED;
  wire [1:0]NLW_r_2p_reg_DOPBDOP_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d32" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d32" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "1024" *) 
  (* RTL_RAM_NAME = "r_2p" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "511" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "31" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_10(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_11(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_12(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_13(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_14(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_15(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_16(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_17(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_18(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_19(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_20(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_21(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_22(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_23(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_24(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_25(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_26(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_27(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_28(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_29(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_30(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_31(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_32(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_33(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_34(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_35(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_36(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_37(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_38(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_39(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .INIT_FILE("NONE"),
    .RAM_MODE("SDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(36),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(0),
    .WRITE_WIDTH_B(36)) 
    r_2p_reg
       (.ADDRARDADDR({1'b1,1'b1,1'b1,1'b1,\vAddr_cnt_reg[6] ,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,ADDRBWRADDR,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CLKARDCLK(clk_IBUF_BUFG),
        .CLKBWRCLK(clk_IBUF_BUFG),
        .DIADI(dataMem_in_IBUF[15:0]),
        .DIBDI(dataMem_in_IBUF[31:16]),
        .DIPADIP({1'b1,1'b1}),
        .DIPBDIP({1'b1,1'b1}),
        .DOADO(D[15:0]),
        .DOBDO(D[31:16]),
        .DOPADOP(NLW_r_2p_reg_DOPADOP_UNCONNECTED[1:0]),
        .DOPBDOP(NLW_r_2p_reg_DOPBDOP_UNCONNECTED[1:0]),
        .ENARDEN(out),
        .ENBWREN(1'b1),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({1'b0,1'b0}),
        .WEBWE({cen_dataMemWR_reg,cen_dataMemWR_reg,cen_dataMemWR_reg,cen_dataMemWR_reg}));
endmodule

(* ORIG_REF_NAME = "rdp_256X32" *) 
module rdp_256X32_0
   (outMem_data_OBUF,
    clk_IBUF_BUFG,
    cen_outMem_IBUF,
    ADDRARDADDR,
    out,
    \dataOut_reg[31] ,
    end_ops_internal4_reg);
  output [31:0]outMem_data_OBUF;
  input clk_IBUF_BUFG;
  input cen_outMem_IBUF;
  input [4:0]ADDRARDADDR;
  input [4:0]out;
  input [31:0]\dataOut_reg[31] ;
  input end_ops_internal4_reg;

  wire [4:0]ADDRARDADDR;
  wire cen_outMem_IBUF;
  wire clk_IBUF_BUFG;
  wire [31:0]\dataOut_reg[31] ;
  wire end_ops_internal4_reg;
  wire [4:0]out;
  wire [31:0]outMem_data_OBUF;
  wire [1:0]NLW_r_2p_reg_DOPADOP_UNCONNECTED;
  wire [1:0]NLW_r_2p_reg_DOPBDOP_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d32" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d32" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "1024" *) 
  (* RTL_RAM_NAME = "r_2p" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "511" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "31" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_10(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_11(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_12(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_13(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_14(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_15(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_16(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_17(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_18(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_19(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_20(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_21(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_22(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_23(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_24(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_25(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_26(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_27(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_28(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_29(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_30(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_31(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_32(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_33(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_34(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_35(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_36(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_37(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_38(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_39(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .INIT_FILE("NONE"),
    .RAM_MODE("SDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(36),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(0),
    .WRITE_WIDTH_B(36)) 
    r_2p_reg
       (.ADDRARDADDR({1'b1,1'b1,1'b1,1'b1,ADDRARDADDR,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,out,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CLKARDCLK(clk_IBUF_BUFG),
        .CLKBWRCLK(clk_IBUF_BUFG),
        .DIADI(\dataOut_reg[31] [15:0]),
        .DIBDI(\dataOut_reg[31] [31:16]),
        .DIPADIP({1'b1,1'b1}),
        .DIPBDIP({1'b1,1'b1}),
        .DOADO(outMem_data_OBUF[15:0]),
        .DOBDO(outMem_data_OBUF[31:16]),
        .DOPADOP(NLW_r_2p_reg_DOPADOP_UNCONNECTED[1:0]),
        .DOPBDOP(NLW_r_2p_reg_DOPBDOP_UNCONNECTED[1:0]),
        .ENARDEN(cen_outMem_IBUF),
        .ENBWREN(1'b1),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({1'b0,1'b0}),
        .WEBWE({end_ops_internal4_reg,end_ops_internal4_reg,end_ops_internal4_reg,end_ops_internal4_reg}));
endmodule

(* ORIG_REF_NAME = "rdp_256X32" *) 
module rdp_256X32_1
   (D,
    clk_IBUF_BUFG,
    out,
    \hAddr_cnt_reg[6] ,
    ADDRBWRADDR,
    weightMem_in_IBUF,
    cen_weightMemWR_reg);
  output [31:0]D;
  input clk_IBUF_BUFG;
  input out;
  input [4:0]\hAddr_cnt_reg[6] ;
  input [4:0]ADDRBWRADDR;
  input [31:0]weightMem_in_IBUF;
  input cen_weightMemWR_reg;

  wire [4:0]ADDRBWRADDR;
  wire [31:0]D;
  wire cen_weightMemWR_reg;
  wire clk_IBUF_BUFG;
  wire [4:0]\hAddr_cnt_reg[6] ;
  wire out;
  wire [31:0]weightMem_in_IBUF;
  wire [1:0]NLW_r_2p_reg_DOPADOP_UNCONNECTED;
  wire [1:0]NLW_r_2p_reg_DOPBDOP_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d32" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d32" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "1024" *) 
  (* RTL_RAM_NAME = "r_2p" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "511" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "31" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_10(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_11(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_12(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_13(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_14(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_15(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_16(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_17(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_18(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_19(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_20(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_21(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_22(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_23(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_24(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_25(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_26(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_27(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_28(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_29(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_30(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_31(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_32(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_33(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_34(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_35(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_36(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_37(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_38(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_39(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .INIT_FILE("NONE"),
    .RAM_MODE("SDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(36),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(0),
    .WRITE_WIDTH_B(36)) 
    r_2p_reg
       (.ADDRARDADDR({1'b1,1'b1,1'b1,1'b1,\hAddr_cnt_reg[6] ,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,ADDRBWRADDR,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CLKARDCLK(clk_IBUF_BUFG),
        .CLKBWRCLK(clk_IBUF_BUFG),
        .DIADI(weightMem_in_IBUF[15:0]),
        .DIBDI(weightMem_in_IBUF[31:16]),
        .DIPADIP({1'b1,1'b1}),
        .DIPBDIP({1'b1,1'b1}),
        .DOADO(D[15:0]),
        .DOBDO(D[31:16]),
        .DOPADOP(NLW_r_2p_reg_DOPADOP_UNCONNECTED[1:0]),
        .DOPBDOP(NLW_r_2p_reg_DOPBDOP_UNCONNECTED[1:0]),
        .ENARDEN(out),
        .ENBWREN(1'b1),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({1'b0,1'b0}),
        .WEBWE({cen_weightMemWR_reg,cen_weightMemWR_reg,cen_weightMemWR_reg,cen_weightMemWR_reg}));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif

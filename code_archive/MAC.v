`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06/01/2019 11:51:15 PM
// Design Name: 
// Module Name: MAC
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module MAC  #(
                parameter dataWidth = 32
    )(
                input wire clk,
                input wire reset,
                input wire signed [dataWidth-1:0] dataA,
                input wire signed [dataWidth-1:0] dataB,
                input wire begin_ops,
                input wire end_ops,
                (*keep = "true"*) output wire [dataWidth-1:0] dataOut
    );
    
    (*keep = "true"*) reg signed [(2*dataWidth)-1:0] prodVal;
    (*keep = "true"*) reg signed [(2*dataWidth):0] sumVal;
    
    (*keep = "true"*) reg signed [(2*dataWidth):0] dataOut_internal;
//    reg begin_ops_internal, end_ops_internal;
    
    (*keep = "true"*) reg [1:0] currentState;
    localparam idle = 2'b00, s1 = 2'b01, s2 = 2'b10;
    
//    always @(posedge clk) begin
//        if(reset) begin
//            begin_ops_internal <= 1'b0;
//            end_ops_internal <= 1'b0;
//        end
//        else begin
//            begin_ops_internal <= begin_ops;
//            end_ops_internal <= end_ops;
//        end
//    end
    
    always @(posedge clk) begin
        if(reset) begin
            currentState <= idle;
            prodVal <= {(2*dataWidth){1'b0}};
            sumVal <= {(2*dataWidth+1){1'b0}};
            dataOut_internal <= {dataWidth{1'b0}};
        end
        else begin
            case(currentState)
                idle: begin
                    if(begin_ops) begin
                        if(end_ops) begin
                            dataOut_internal <= dataA * dataB;
                            currentState <= idle;
                        end
                        else begin
                            currentState <= s1;
                            prodVal <= dataA * dataB;
                            sumVal <= sumVal + {prodVal[2*dataWidth-1],prodVal};
                        end
                    end
                    else begin
                        currentState <= idle;
                        sumVal <= {(2*dataWidth+1){1'b0}};
                        prodVal <= {(2*dataWidth){1'b0}};
                    end
                end
                
                s1: begin
                    if(!end_ops)begin
                        prodVal <= dataA * dataB;
                        sumVal <= sumVal + {prodVal[2*dataWidth-1],prodVal};
                        currentState <= s1;
                    end
                    else begin
                        prodVal <= dataA * dataB;
                        sumVal <= sumVal + {prodVal[2*dataWidth-1],prodVal};
                        currentState <= s2;
                    end
                end
                
                s2: begin
                    if(begin_ops) begin
                        prodVal <= dataA * dataB;
                        sumVal <= {(2*dataWidth+1){1'b0}};
                        dataOut_internal <= sumVal + {prodVal[2*dataWidth-1],prodVal};
                        currentState <= s1;
                    end
                    else begin
                        dataOut_internal <= sumVal + {prodVal[2*dataWidth-1],prodVal};
                        currentState <= idle;
                    end
                end
            endcase
        end
    end

assign dataOut = {dataOut_internal[2*dataWidth-1],dataOut_internal[dataWidth-2:0]};

endmodule
